#include <SPI.h>
#include <WiFi101.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include "DHT.h"

#define DHTPIN 8
#define DHTTYPE DHT22

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels



// Generate SAS key using https://github.com/Azure/azure-iot-sdks/releases
const int buttonPin = 7;

char hostname[] = "applicationhub.azure-devices.net";
char authSAS[] = "SharedAccessSignature sr=applicationhub.azure-devices.net%2Fdevices%2Farduino&sig=hrMrnnoMhFuyoHvfp5QPoB25KB59pACKIuLXuvhlaGs%3D&se=1611997129";




String deviceName = "arduino";
char ssid[] = "PDM Technology";      // your network SSID (name)
char pass[] = "istogrrtistbfmay";   // your network password
String posturi = "/devices/arduino/messages/events?api-version=2016-02-03";
String geturi = "/devices/arduino/messages/devicebound?api-version=2016-02-03";


float h;
float t;
int i = 1;
String msgType = "regular";
bool setLight = false;
bool wantNotification = false;
int status = WL_IDLE_STATUS;
WiFiClient client;
DHT dht(DHTPIN, DHTTYPE);




void httpRequest() {

  Serial.println("Making request...");
 
  // close any connection before send a new request.
  // This will free the socket on the WiFi shield
  client.stop();
 
  // if there's a successful connection:
  if (client.connectSSL(hostname, 443)) {
    //make the GET request to the Azure IOT device feed uri
    client.print("GET ");  //Do a GET
    client.print(geturi);  // On the feedURI
    client.println(" HTTP/1.1"); 
    client.print("Host: "); 
    client.println(hostname);  //with hostname header
    client.print("Authorization: ");
    client.println(authSAS);  //Authorization SAS token obtained from Azure IoT device explorer
    client.println("Connection: close");
    client.println();
  }
  else {
    // if you couldn't make a connection:
    Serial.println("Request connection failed");
  }
}

void httpPost(float hum, float temp){
    // close any connection before send a new request.
    client.stop();

    if(wantNotification){
      msgType = "notification";
      wantNotification = false;
    }else{
      msgType = "regular";
    }

    String json = "{\"message\":{\"systemProperties\":{\"contentType\":\"application/json\",\"contentEncoding\":\"utf-8\"}},type:\""+msgType+"\", body:{\"boardid\":\"MKR1000\",\"sensors\":[{\"sensorid\":\"0\",\"value\":"+hum+"},{\"sensorid\":\"1\",\"value\":"+temp+"}]}}";
    Serial.println("Sending: "+json);
  
    if (client.connectSSL(hostname, 443)) {
      client.print("POST ");
      client.print(posturi);
      client.println(" HTTP/1.1"); 
      client.print("Host: "); 
      client.println(hostname);
      client.print("Authorization: ");
      client.println(authSAS); 
      client.println("Connection: close");

      client.print("Content-Type: ");
      client.println("text/plain");
      client.print("Content-Length: ");
      client.println(json.length());
      client.println();
      client.println(json);
      
      delay(200);
      
    } else {
      Serial.println("HTTP POST connection failed");
      ledBlink(7);
    }
}

void httpGet(){
    // close any connection before send a new request.
    client.stop();

    Serial.println("Asking for a message...");
  
    if (client.connectSSL(hostname, 443)) {
      client.print("GET ");
      client.print(geturi);
      client.println(" HTTP/1.1"); 
      client.print("Host: "); 
      client.println(hostname);
      client.print("Authorization: ");
      client.println(authSAS); 
      client.println("Connection: close");
      
      delay(200);
      
    } else {
      Serial.println("HTTP POST connection failed");
      ledBlink(7);
    }
}

void readResponse(){
  
  String response = "";
  char c;
  while (client.connected()) {
    if (client.available()){
      c = client.read();
      response.concat(c);
    }
    
  }
    
  if (!response.equals("")) 
  {
    if (response.startsWith("HTTP/1.1 204")) {
      Serial.println();
      Serial.println("Data was sent to Azure");
      Serial.println();

       ledBlink(6);
       
    } else {
      Serial.println("Responce: ");
      ledBlink(6);
      ledBlink(6);
    }
    Serial.println(response);
  }
}

void readCommand(){
  
  String response = "";
  String responseOld = "";
  do{
  char c;
  while (client.connected()) {
    Serial.println("Client is connected");
    if (client.available()){
      Serial.println("Client is available");
      c = client.read();
      if (String(c)=="\n"){
        while (client.connected()) {
          if (client.available()){
            c = client.read();
            response.concat(c);
            }
          } 
        }
      }
    }
    Serial.println(response);
    
  if (!response.equals("")) 
  {
    Serial.println(response);
    responseOld = response;
    if (response.endsWith("\"on\"}"))
    {
      setLight = true;
      Serial.println("Set on");
    }
    if (response.endsWith("\"off\"}")){
      setLight = false;
      Serial.println("Set off");
    }
    }
  }while(responseOld!=response);        
}

void ledBlink(int pin){
  pinMode(pin,OUTPUT);
  digitalWrite(pin, HIGH);
  delay(100);
  digitalWrite(pin, LOW);
  delay(100);
  digitalWrite(pin, HIGH);
  delay(100);
  digitalWrite(pin, LOW);
}

void giveMeNotification(){
  wantNotification =true;
}

void(* resetFunc) (void) = 0;


void setup() {
  Serial.begin(9600);
  //dht.begin();
  Serial.println("Setup begin");
  pinMode(A1,OUTPUT);
  pinMode(buttonPin, INPUT);
  attachInterrupt(7, giveMeNotification, RISING);

   // check for the presence of the shield:
   if (WiFi.status() == WL_NO_SHIELD) {
     Serial.println("WiFi shield not present");
     // don't continue:
     while (true);
   }

   // attempt to connect to Wifi network:
   while ( status != WL_CONNECTED) {
     Serial.print("Attempting to connect to SSID: ");
     Serial.println(ssid);
     status = WiFi.begin(ssid, pass);

     // wait 10 seconds for connection:
     delay(10000);
   }
  
  Serial.println("Connected to Wi-Fi");
}

void loop() {

httpRequest();

  String response = "";
  char c;
  ///read response if WiFi Client is available
  while(!client.available()){}
  while (client.available()) {
    c = client.read();
    response.concat(c);
  }
 
  if (!response.equals(""))
  {
      Serial.println(response);
  }

  
    

  
  /*
  h = dht.readHumidity();
  t = dht.readTemperature();
  //httpPost(h,t); // this string will be send to Azure
  readResponse();
  readCommand();
  //Serial.println("Light on = "+String(setLight));
  Serial.println("================================================");
  
  if (setLight){
    digitalWrite(A1,HIGH);
  }
  if (!setLight){
    digitalWrite(A1,LOW);
  }
  */
  delay(3000);
}



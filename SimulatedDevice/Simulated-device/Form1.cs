﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Azure.Devices.Client;
using Message = Microsoft.Azure.Devices.Client.Message;

namespace Simulated_device
{
    public partial class Form1 : Form
    {
        string path = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + @"/config.json";
        BindingList<Device> deviceList = new BindingList<Device>();
        List<Sensor> sensorlist = new List<Sensor>();
        List<GroupBox> groupBoxes = new List<GroupBox>();
        Device selectedDevice;
        BindingSource bindingSourse = new BindingSource();
        bool sending = false;

        private static DeviceClient _deviceClient;

        public Form1()
        {
            InitializeComponent();
            label_InfoHeader.Visible = false;
            label_Info.Visible = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            checkBox_deviceNames.Checked = true;
            LoadJson();
            button_add.Enabled = false;

            //ListGroupBoxes();
            //numberDeviceNames();
        }

        public void LoadJson()
        {
            try
            {
                // Read devices from config.json
                string inputJson = File.ReadAllText(path);
                deviceList = JsonConvert.DeserializeObject<BindingList<Device>>(inputJson);

                // Link datasourse with devices
                bindingSourse.DataSource = deviceList;

                // Put devices to combobox
                comboBox1.DisplayMember = "name";
                comboBox1.ValueMember = "deviceParameters";
                comboBox1.DataSource = bindingSourse.DataSource;

            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR during deserialization 'Config.json'.");
                label_InfoHeader.Visible = true;
                label_Info.Visible = true;
                label_Info.Text = "Error while deserialization 'Config.json'.";
            }
        }

        // Add all groupboxes to list
        public void ListGroupBoxes()
        {
            groupBoxes = new List<GroupBox>();
            for (int i = 0; i < this.splitContainer1.Panel2.Controls.Count; i++)
            {
                if (this.splitContainer1.Panel2.Controls[i] is GroupBox)
                {
                    groupBoxes.Add(this.splitContainer1.Panel2.Controls[i] as GroupBox);
                }
            }
        }

        // Stop adding sensors
        public void BlockAddButton()
        {
            if(groupBoxes.Count >= 10)
            {
                button_add.Enabled = false;
            }
            else
            {
                button_add.Enabled = true;
            }
            this.Refresh();
        }

        // For adding a new groupbox
        public Point GetLastGroupBoxLocation()
        {
            Point h = new Point(18, 40);
            
            foreach(var c in this.splitContainer1.Panel2.Controls.OfType<GroupBox>())
            {
                Point pos = c.FindForm().PointToClient(c.Parent.PointToScreen(c.Location));
                if (pos.Y>h.Y)
                {
                    h.Y = pos.Y;
                }
            }
            h.Y = h.Y - 32;

            return h;
        }

        public void saveDeviceState()
        {
            Device newDevice = new Device();

            string iotHubName = textBox_iotHubName.Text;
            string deviceName = textBox_deviceName.Text;
            string deviceKey = textBox_deviceKey.Text;

            List<KeyValuePair<string, string>> newParameters = new List<KeyValuePair<string, string>>();
            newParameters.Add(new KeyValuePair<string, string>("iotHubName", iotHubName));
            newParameters.Add(new KeyValuePair<string, string>("deviceName", deviceName));
            newParameters.Add(new KeyValuePair<string, string>("deviceKey", deviceKey));

            List<KeyValuePair<string, Sensor>> newSensors = new List<KeyValuePair<string, Sensor>>();

            foreach (GroupBox gb in groupBoxes)
            {
                Sensor s = new Sensor(gb.Controls.Find("sensorIdTextbox", false)[0].Text, Int32.Parse(gb.Controls.Find("minValueTextBox", false)[0].Text), Int32.Parse(gb.Controls.Find("maxValueTextBox", false)[0].Text));
                newSensors.Add(new KeyValuePair<string, Sensor>(gb.Name, s));
            }

            DeviceParameters newDeviceParaneters = new DeviceParameters();
            newDeviceParaneters.connection = newParameters;
            newDeviceParaneters.sensors = newSensors;

            newDevice.name = deviceName;
            newDevice.deviceParameters = newDeviceParaneters;

            int i = 0;
            bool replaced = false;
            foreach (Device device in deviceList)
            {
                if (device.name.Equals(newDevice.name))
                {
                    deviceList[i] = newDevice;
                    replaced = true;
                    break;
                }
                i++;
            }
            if (!replaced)
            {
                deviceList.Add(newDevice);
            }

            comboBox1.SelectedItem = newDevice;

            numberDeviceNames();

            string json = JsonConvert.SerializeObject(deviceList);
            System.IO.File.WriteAllText(path, json);

            label_InfoHeader.Visible = true;
            label_Info.Visible = true;
            label_Info.Text = "Device state saved.";
        }

        // Redraw group boxes
        public void RedrawGrpopBoxes()
        {
            foreach (GroupBox gb in groupBoxes)
            {
                if (this.splitContainer1.Panel2.Contains(gb))
                {
                    this.splitContainer1.Panel2.Controls.Remove(gb);
                }
            }

            foreach (KeyValuePair<string, Sensor> s in selectedDevice.deviceParameters.sensors)
            {
                DrawGroupBox(s.Key, s.Value.Name, s.Value.MinValue.ToString(), s.Value.MaxValue.ToString());
            }
            this.Refresh();
        }

        // Draw group box
        private void DrawGroupBox(string gbName)
        {
            Point pos = GetLastGroupBoxLocation();
            string gbNumber = gbName.Substring(8);

            GroupBox newGroupBox = new GroupBox();
            newGroupBox.Size = new Size(1000, 48);
            newGroupBox.Location = pos;
            newGroupBox.Name = gbName;
            newGroupBox.Text = "Sensor" + gbNumber;

            Label newSensorIdLabel = new Label();
            newSensorIdLabel.Location = new Point(76, 23);
            newSensorIdLabel.Size = new Size(60, 13);
            newSensorIdLabel.Name = "sensorIdLabel";// + gbNumber;
            newSensorIdLabel.Text = "Sensor ID:";

            TextBox newSensorIdTextBox = new TextBox();
            newSensorIdTextBox.Location = new Point(145, 19);
            newSensorIdTextBox.Size = new Size(180, 20);
            newSensorIdTextBox.Name = "sensorIdTextbox";// + gbNumber;


            Label newMinValueLabel = new Label();
            newMinValueLabel.Location = new Point(390, 23);
            newMinValueLabel.Size = new Size(56, 13);
            newMinValueLabel.Name = "minValueLabel";// + gbNumber;
            newMinValueLabel.Text = "Min value:";

            TextBox newMinValueTextbox = new TextBox();
            newMinValueTextbox.Location = new Point(455, 19);
            newMinValueTextbox.Size = new Size(100, 20);
            newMinValueTextbox.Name = "minValueTextBox";// + gbNumber;

            Label newMaxValueLabel = new Label();
            newMaxValueLabel.Location = new Point(595, 23);
            newMaxValueLabel.Size = new Size(59, 13);
            newMaxValueLabel.Name = "maxValueLabel";// + gbNumber;
            newMaxValueLabel.Text = "Max value:";

            TextBox newMaxValueTextbox = new TextBox();
            newMaxValueTextbox.Location = new Point(660, 19);
            newMaxValueTextbox.Size = new Size(100, 20);
            newMaxValueTextbox.Name = "maxValueTextBox";// + gbNumber;


            Button newDeleteButton = new Button();
            newDeleteButton.Location = new Point(880, 18);
            newDeleteButton.Size = new Size(110, 20);
            newDeleteButton.Name = "deleteButton";// + gbNumber;
            newDeleteButton.Text = "Delete sensor";
            newDeleteButton.Click += new EventHandler(this.button_delete_Click);

            newGroupBox.Controls.Add(newSensorIdLabel);
            newGroupBox.Controls.Add(newSensorIdTextBox);
            newGroupBox.Controls.Add(newMinValueLabel);
            newGroupBox.Controls.Add(newMinValueTextbox);
            newGroupBox.Controls.Add(newMaxValueLabel);
            newGroupBox.Controls.Add(newMaxValueTextbox);
            newGroupBox.Controls.Add(newDeleteButton);

            this.splitContainer1.Panel2.Controls.Add(newGroupBox);
            ListGroupBoxes();
            this.Refresh();
        }

        // Draw group box RELOADED
        private void DrawGroupBox(string gbName, string sensorId, string minValue, string maxValue)
        {
            Point pos = GetLastGroupBoxLocation();
            string gbNumber = gbName.Substring(8);

            GroupBox newGroupBox = new GroupBox();
            newGroupBox.Size = new Size(1000, 48);
            newGroupBox.Location = pos;
            newGroupBox.Name = gbName;
            newGroupBox.Text = "Sensor" + gbNumber;

            Label newSensorIdLabel = new Label();
            newSensorIdLabel.Location = new Point(76, 23);
            newSensorIdLabel.Size = new Size(60, 13);
            newSensorIdLabel.Name = "sensorIdLabel";// + gbNumber;
            newSensorIdLabel.Text = "Sensor ID:";

            TextBox newSensorIdTextBox = new TextBox();
            newSensorIdTextBox.Location = new Point(145, 19);
            newSensorIdTextBox.Size = new Size(180, 20);
            newSensorIdTextBox.Name = "sensorIdTextbox";// + gbNumber;
            newSensorIdTextBox.Text = sensorId;


            Label newMinValueLabel = new Label();
            newMinValueLabel.Location = new Point(390, 23);
            newMinValueLabel.Size = new Size(56, 13);
            newMinValueLabel.Name = "minValueLabel";// + gbNumber;
            newMinValueLabel.Text = "Min value:";

            TextBox newMinValueTextbox = new TextBox();
            newMinValueTextbox.Location = new Point(455, 19);
            newMinValueTextbox.Size = new Size(100, 20);
            newMinValueTextbox.Name = "minValueTextBox";// + gbNumber;
            newMinValueTextbox.Text = minValue;

            Label newMaxValueLabel = new Label();
            newMaxValueLabel.Location = new Point(595, 23);
            newMaxValueLabel.Size = new Size(59, 13);
            newMaxValueLabel.Name = "maxValueLabel";// + gbNumber;
            newMaxValueLabel.Text = "Max value:";

            TextBox newMaxValueTextbox = new TextBox();
            newMaxValueTextbox.Location = new Point(660, 19);
            newMaxValueTextbox.Size = new Size(100, 20);
            newMaxValueTextbox.Name = "maxValueTextBox";// + gbNumber;
            newMaxValueTextbox.Text = maxValue;


            Button newDeleteButton = new Button();
            newDeleteButton.Location = new Point(880, 18);
            newDeleteButton.Size = new Size(110, 20);
            newDeleteButton.Name = "deleteButton";// + gbNumber;
            newDeleteButton.Text = "Delete sensor";
            newDeleteButton.Click += new EventHandler(this.button_delete_Click);

            newGroupBox.Controls.Add(newSensorIdLabel);
            newGroupBox.Controls.Add(newSensorIdTextBox);
            newGroupBox.Controls.Add(newMinValueLabel);
            newGroupBox.Controls.Add(newMinValueTextbox);
            newGroupBox.Controls.Add(newMaxValueLabel);
            newGroupBox.Controls.Add(newMaxValueTextbox);
            newGroupBox.Controls.Add(newDeleteButton);

            this.splitContainer1.Panel2.Controls.Add(newGroupBox);
            ListGroupBoxes();
            this.Refresh();
        }

        // Draw group box for each sensor of device
        private void DrawGroupBox(Device device)
        {
            foreach(KeyValuePair<string, Sensor> s in device.deviceParameters.sensors)
            {
                Point pos = GetLastGroupBoxLocation();
                string gbNumber = s.Key.Substring(8);

                GroupBox newGroupBox = new GroupBox();
                newGroupBox.Size = new Size(1000, 48);
                newGroupBox.Location = pos;
                newGroupBox.Name = s.Key;
                newGroupBox.Text = "Sensor" + gbNumber;

                Label newSensorIdLabel = new Label();
                newSensorIdLabel.Location = new Point(76, 23);
                newSensorIdLabel.Size = new Size(60, 13);
                newSensorIdLabel.Name = "sensorIdLabel";// + gbNumber;
                newSensorIdLabel.Text = "Sensor ID:";

                TextBox newSensorIdTextBox = new TextBox();
                newSensorIdTextBox.Location = new Point(145, 19);
                newSensorIdTextBox.Size = new Size(180, 20);
                newSensorIdTextBox.Name = "sensorIdTextbox";// + gbNumber;
                newSensorIdTextBox.Text = s.Value.Name;

                Label newMinValueLabel = new Label();
                newMinValueLabel.Location = new Point(390, 23);
                newMinValueLabel.Size = new Size(56, 13);
                newMinValueLabel.Name = "minValueLabel";// + gbNumber;
                newMinValueLabel.Text = "Min value:";

                TextBox newMinValueTextbox = new TextBox();
                newMinValueTextbox.Location = new Point(455, 19);
                newMinValueTextbox.Size = new Size(100, 20);
                newMinValueTextbox.Name = "minValueTextBox";// + gbNumber;
                newMinValueTextbox.Text = s.Value.MinValue.ToString();

                Label newMaxValueLabel = new Label();
                newMaxValueLabel.Location = new Point(595, 23);
                newMaxValueLabel.Size = new Size(59, 13);
                newMaxValueLabel.Name = "maxValueLabel";// + gbNumber;
                newMaxValueLabel.Text = "Max value:";

                TextBox newMaxValueTextbox = new TextBox();
                newMaxValueTextbox.Location = new Point(660, 19);
                newMaxValueTextbox.Size = new Size(100, 20);
                newMaxValueTextbox.Name = "maxValueTextBox";// + gbNumber;
                newMaxValueTextbox.Text = s.Value.MaxValue.ToString();


                Button newDeleteButton = new Button();
                newDeleteButton.Location = new Point(880, 18);
                newDeleteButton.Size = new Size(110, 20);
                newDeleteButton.Name = "deleteButton";// + gbNumber;
                newDeleteButton.Text = "Delete sensor";
                newDeleteButton.Click += new EventHandler(this.button_delete_Click);

                newGroupBox.Controls.Add(newSensorIdLabel);
                newGroupBox.Controls.Add(newSensorIdTextBox);
                newGroupBox.Controls.Add(newMinValueLabel);
                newGroupBox.Controls.Add(newMinValueTextbox);
                newGroupBox.Controls.Add(newMaxValueLabel);
                newGroupBox.Controls.Add(newMaxValueTextbox);
                newGroupBox.Controls.Add(newDeleteButton);

                this.splitContainer1.Panel2.Controls.Add(newGroupBox);
            }

            ListGroupBoxes();
            this.Refresh();
        }

        void updateGroupBoxesAfterItemChange()
        {
            //foreach (GroupBox gb in groupBoxes)
            //{
            //    if (this.splitContainer1.Panel2.Contains(gb))
            //    {
            //        this.splitContainer1.Panel2.Controls.Remove(gb);
            //    }
            //}

            this.splitContainer1.Panel2.Controls.Clear();

            selectedDevice = (Device)comboBox1.SelectedItem;

            if(selectedDevice != null)
            {
                textBox_iotHubName.Text = selectedDevice.deviceParameters.connection[0].Value;
                textBox_deviceName.Text = selectedDevice.deviceParameters.connection[1].Value;
                textBox_deviceKey.Text = selectedDevice.deviceParameters.connection[2].Value;

                DrawGroupBox(selectedDevice);

            }else
            {
                textBox_iotHubName.Text = "";
                textBox_deviceName.Text = "";
                textBox_deviceKey.Text = "";
            }

            this.Refresh();
        }

        async void SendDeviceToCloudMessagesAsync()
        {
            Random rand = new Random();
            string BoardSerial = selectedDevice.deviceParameters.connection[1].Value;

            MessageToSend message = new MessageToSend();
            message.systemProperties = new SystemProperties();
            message.systemProperties.Time = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss,sss");

            Board board = new Board();
            board.BoardSerial = BoardSerial;
            board.sensors = new List<SensorToSend>();
            foreach(KeyValuePair<string, Sensor> s in selectedDevice.deviceParameters.sensors)
            {
                SensorToSend newSensorTosend = new SensorToSend();
                newSensorTosend.SensorId = s.Value.Name;
                newSensorTosend.Value = rand.Next(s.Value.MinValue, s.Value.MaxValue).ToString();
                board.sensors.Add(newSensorTosend);
            }

            TelemetryDataPoint telemetryDataPoint = new TelemetryDataPoint();
            telemetryDataPoint.message = message;
            telemetryDataPoint.message.body = board;

            string msgStr = telemetryDataPoint.serialize();
            var msg = new Message(Encoding.ASCII.GetBytes(msgStr));

            await _deviceClient.SendEventAsync(msg);

            Console.WriteLine("Message sent:");
            Console.WriteLine(msgStr);
        }

        async Task Wait()
        {
            await Task.Delay(1000);
        }

        async void StartSending()
        {
            while (true)
            {
                if (sending)
                {
                    SendDeviceToCloudMessagesAsync();
                    await Wait();
                }
                else
                {
                    break;
                }
            }
        }

        private void button_add_Click(object sender, EventArgs e)
        {
            List<int> gbNumbers = new List<int>();
            int nextGbIndex = -1;

            if(selectedDevice != null)
            {
                selectedDevice.deviceParameters.sensors.Add(new KeyValuePair<string, Sensor>("groupBox" + groupBoxes.Count(), new Sensor()));

                foreach (GroupBox gb in groupBoxes)
                {
                    gbNumbers.Add(Convert.ToInt32(gb.Name.Substring(8)));
                }

                gbNumbers.Sort();
                for (int i = 0; i < gbNumbers.Count - 1; i++)
                {
                    if ((gbNumbers[0] != 0) || (gbNumbers.Count == 0))
                    {
                        nextGbIndex = 0;
                        break;
                    }
                    else
                    {
                        if ((gbNumbers[i] + 1) != (gbNumbers[i + 1]))
                        {
                            nextGbIndex = gbNumbers[i] + 1;
                            break;
                        }
                    }
                }

                Point h = GetLastGroupBoxLocation();

                GroupBox newGroupBox = new GroupBox();
                newGroupBox.Size = new Size(1000, 48);
                newGroupBox.Location = h;
                if (nextGbIndex != -1)
                {
                    newGroupBox.Name = "groupBox" + nextGbIndex;
                    newGroupBox.Text = "Sensor" + nextGbIndex;
                }
                else
                {
                    newGroupBox.Name = "groupBox" + groupBoxes.Count();
                    newGroupBox.Text = "Sensor" + groupBoxes.Count();
                }

                Label newSensorIdLabel = new Label();
                newSensorIdLabel.Location = new Point(76, 23);
                newSensorIdLabel.Size = new Size(60, 13);
                newSensorIdLabel.Name = "sensorIdLabel";// + groupBoxes.Count();
                newSensorIdLabel.Text = "Sensor ID:";

                TextBox newSensorIdTextBox = new TextBox();
                newSensorIdTextBox.Location = new Point(145, 19);
                newSensorIdTextBox.Size = new Size(180, 20);
                newSensorIdTextBox.Name = "sensorIdTextbox";// + groupBoxes.Count();


                Label newMinValueLabel = new Label();
                newMinValueLabel.Location = new Point(390, 23);
                newMinValueLabel.Size = new Size(56, 13);
                newMinValueLabel.Name = "minValueLabel";// + groupBoxes.Count();
                newMinValueLabel.Text = "Min value:";

                TextBox newMinValueTextbox = new TextBox();
                newMinValueTextbox.Location = new Point(455, 19);
                newMinValueTextbox.Size = new Size(100, 20);
                newMinValueTextbox.Name = "minValueTextBox";// + groupBoxes.Count();
                newMinValueTextbox.Text = "0";

                Label newMaxValueLabel = new Label();
                newMaxValueLabel.Location = new Point(595, 23);
                newMaxValueLabel.Size = new Size(59, 13);
                newMaxValueLabel.Name = "maxValueLabel";// + groupBoxes.Count();
                newMaxValueLabel.Text = "Max value:";

                TextBox newMaxValueTextbox = new TextBox();
                newMaxValueTextbox.Location = new Point(660, 19);
                newMaxValueTextbox.Size = new Size(100, 20);
                newMaxValueTextbox.Name = "maxValueTextBox";// + groupBoxes.Count();
                newMaxValueTextbox.Text = "100";


                Button newDeleteButton = new Button();
                newDeleteButton.Location = new Point(880, 18);
                newDeleteButton.Size = new Size(110, 20);
                newDeleteButton.Name = "deleteButton";// + groupBoxes.Count();
                newDeleteButton.Text = "Delete sensor";
                newDeleteButton.Click += new EventHandler(this.button_delete_Click);

                newGroupBox.Controls.Add(newSensorIdLabel);
                newGroupBox.Controls.Add(newSensorIdTextBox);
                newGroupBox.Controls.Add(newMinValueLabel);
                newGroupBox.Controls.Add(newMinValueTextbox);
                newGroupBox.Controls.Add(newMaxValueLabel);
                newGroupBox.Controls.Add(newMaxValueTextbox);
                newGroupBox.Controls.Add(newDeleteButton);

                this.splitContainer1.Panel2.Controls.Add(newGroupBox);
                this.Refresh();

                ListGroupBoxes();
                numberDeviceNames();

                BlockAddButton();

                Console.WriteLine("Groupbox added: " + newGroupBox.Name);
            }else
            {

            }
        }

        private void button_delete_Click(object sender, EventArgs e)
        {
            //this.splitContainer1.Panel2.Controls.Remove((sender as Button).Parent);
            
            foreach(KeyValuePair<string, Sensor> s in selectedDevice.deviceParameters.sensors)
            {
                if (s.Key.Equals((sender as Button).Parent.Name))
                {
                    selectedDevice.deviceParameters.sensors.Remove(s);
                    break;
                }
            }

            ListGroupBoxes();

            RedrawGrpopBoxes();

            numberDeviceNames();

            this.Refresh();
            BlockAddButton();

            label_InfoHeader.Visible = true;
            label_Info.Visible = true;
            label_Info.Text = "Sensor was deleted.";
        }

        private void button_addDevice_Click(object sender, EventArgs e)
        {
            saveDeviceState();
            LoadJson();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedDevice = (Device)comboBox1.SelectedItem;
            if (selectedDevice != null)
            {
                button_add.Enabled = true;
            }
            else
            {
                button_add.Enabled = false;
            }
            updateGroupBoxesAfterItemChange();
            numberDeviceNames();
        }

        private void button_deleteDevice_Click(object sender, EventArgs e)
        {
            String deletedDeviceName = selectedDevice.name;
            deviceList.Remove(selectedDevice);
            comboBox1.SelectedIndex = comboBox1.Items.Count-1;

            groupBoxes.Clear();

            updateGroupBoxesAfterItemChange();

            string json = JsonConvert.SerializeObject(deviceList);
            System.IO.File.WriteAllText(path, json);
            LoadJson();

            label_InfoHeader.Visible = true;
            label_Info.Visible = true;
            label_Info.Text = "Device " + deletedDeviceName + " was deleted.";
        }

        private void button_sendOnce_Click(object sender, EventArgs e)
        {
            //new
            saveDeviceState();

            string iotHubName = selectedDevice.deviceParameters.connection[0].Value;
            string deviceName = selectedDevice.deviceParameters.connection[1].Value;
            string deviceKey = selectedDevice.deviceParameters.connection[2].Value;

            _deviceClient = DeviceClient.Create(iotHubName+ ".azure-devices.net", new DeviceAuthenticationWithRegistrySymmetricKey(deviceName, deviceKey), TransportType.Mqtt);
            SendDeviceToCloudMessagesAsync();

            label_InfoHeader.Visible = true;
            label_Info.Visible = true;
            label_Info.Text = "Message was sent.";
        }

        private void button_startSending_Click(object sender, EventArgs e)
        {
            //new
            saveDeviceState();

            string iotHubName = selectedDevice.deviceParameters.connection[0].Value;
            string deviceName = selectedDevice.deviceParameters.connection[1].Value;
            string deviceKey = selectedDevice.deviceParameters.connection[2].Value;

            _deviceClient = DeviceClient.Create(iotHubName + ".azure-devices.net", new DeviceAuthenticationWithRegistrySymmetricKey(deviceName, deviceKey), TransportType.Mqtt);
           

            if (!sending)
            {
                foreach (var c in this.splitContainer1.Panel2.Controls.OfType<GroupBox>())
                {
                    foreach (Control cnt in c.Controls)
                    {
                        if(cnt is TextBox || cnt is Button)
                        {
                            cnt.Enabled = false;
                        }
                    }
                }
                //new
                blockUI();

                sending = true;
                button_startSending.Text = "Stop sending";
                StartSending();
            }
            else
            {
                foreach (var c in this.splitContainer1.Panel2.Controls.OfType<GroupBox>())
                {
                    foreach (Control cnt in c.Controls)
                    {
                        if (cnt is TextBox || cnt is Button)
                        {
                            cnt.Enabled = true;
                        }
                    }
                }
                //new
                unblockUI();
                numberDeviceNames();

                sending = false;
                button_startSending.Text = "Start sending";
            }

            label_InfoHeader.Visible = true;
            label_Info.Visible = true;
            label_Info.Text = "Sending...";
        }

        private void numberDeviceNames()
        {
            if (checkBox_deviceNames.Checked)
            {
                foreach (var c in this.splitContainer1.Panel2.Controls.OfType<GroupBox>())
                {
                    foreach (Control cnt in c.Controls)
                    {
                        if (cnt is TextBox && cnt.Name == "sensorIdTextbox")
                        {
                            cnt.Enabled = false;
                            cnt.Text = c.Name.Substring(8);
                        }
                    }
                }
            }
            else
            {
                foreach (var c in this.splitContainer1.Panel2.Controls.OfType<GroupBox>())
                {
                    foreach (Control cnt in c.Controls)
                    {
                        if (cnt is TextBox && cnt.Name == "sensorIdTextbox")
                        {
                            cnt.Enabled = true;
                        }
                    }
                }
                updateGroupBoxesAfterItemChange();
            }
        }

        private void checkBox_deviceNames_CheckedChanged(object sender, EventArgs e)
        {
            numberDeviceNames();
        }

        public void blockUI()
        {
            comboBox1.Enabled = false;
            button_addDevice.Enabled = false;
            button_deleteDevice.Enabled = false;
            button_sendOnce.Enabled = false;
            textBox_iotHubName.Enabled = false;
            textBox_deviceName.Enabled = false;
            textBox_deviceKey.Enabled = false;
            checkBox_deviceNames.Enabled = false;
        }

        public void unblockUI()
        {
            comboBox1.Enabled = true;
            button_addDevice.Enabled = true;
            button_deleteDevice.Enabled = true;
            button_sendOnce.Enabled = true;
            textBox_iotHubName.Enabled = true;
            textBox_deviceName.Enabled = true;
            textBox_deviceKey.Enabled = true;
            checkBox_deviceNames.Enabled = true;
        }
    }

    public class Device
    {
        public string name { get; set; }
        public DeviceParameters deviceParameters;
    }

    public class DeviceParameters
    {
        public List<KeyValuePair<string, string>> connection;
        public List<KeyValuePair<string, Sensor>> sensors;
    }

    public class Sensor
    {
        public string Name;
        public int MinValue;
        public int MaxValue;

        public Sensor(string newName, int newMinValue, int newMAxValue)
        {
            Name = newName;
            MinValue = newMinValue;
            MaxValue = newMAxValue;
        }

        public Sensor()
        {

        }
    }

    public class MessageToSend
    {
        public SystemProperties systemProperties;
        public Board body;
    }

    public class SystemProperties
    {
        public String contentType = "application/json";
        public String contentEncoding = "utf-8";
        public String Time;
    }

    public class Board
    {
        public String BoardSerial;
        public List<SensorToSend> sensors;
    }

    public class SensorToSend
    {
        public String SensorId;
        public String Value;
    }

    public class TelemetryDataPoint
    {
        public MessageToSend message;

        public String serialize()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}

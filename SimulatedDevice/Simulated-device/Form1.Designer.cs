﻿namespace Simulated_device
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.button_startSending = new System.Windows.Forms.Button();
            this.button_sendOnce = new System.Windows.Forms.Button();
            this.textBox_deviceKey = new System.Windows.Forms.TextBox();
            this.label_deviceKey = new System.Windows.Forms.Label();
            this.textBox_deviceName = new System.Windows.Forms.TextBox();
            this.label_deviceName = new System.Windows.Forms.Label();
            this.textBox_iotHubName = new System.Windows.Forms.TextBox();
            this.label_iotHubName = new System.Windows.Forms.Label();
            this.button_deleteDevice = new System.Windows.Forms.Button();
            this.button_addDevice = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox_deviceNames = new System.Windows.Forms.CheckBox();
            this.button_add = new System.Windows.Forms.Button();
            this.label_InfoHeader = new System.Windows.Forms.Label();
            this.label_Info = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.button_startSending);
            this.splitContainer1.Panel1.Controls.Add(this.button_sendOnce);
            this.splitContainer1.Panel1.Controls.Add(this.textBox_deviceKey);
            this.splitContainer1.Panel1.Controls.Add(this.label_deviceKey);
            this.splitContainer1.Panel1.Controls.Add(this.textBox_deviceName);
            this.splitContainer1.Panel1.Controls.Add(this.label_deviceName);
            this.splitContainer1.Panel1.Controls.Add(this.textBox_iotHubName);
            this.splitContainer1.Panel1.Controls.Add(this.label_iotHubName);
            this.splitContainer1.Panel1.Controls.Add(this.button_deleteDevice);
            this.splitContainer1.Panel1.Controls.Add(this.button_addDevice);
            this.splitContainer1.Panel1.Controls.Add(this.comboBox1);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Size = new System.Drawing.Size(1045, 579);
            this.splitContainer1.SplitterDistance = 74;
            this.splitContainer1.TabIndex = 0;
            // 
            // button_startSending
            // 
            this.button_startSending.Location = new System.Drawing.Point(926, 11);
            this.button_startSending.Name = "button_startSending";
            this.button_startSending.Size = new System.Drawing.Size(107, 21);
            this.button_startSending.TabIndex = 23;
            this.button_startSending.Text = "Start sending";
            this.button_startSending.UseVisualStyleBackColor = true;
            this.button_startSending.Click += new System.EventHandler(this.button_startSending_Click);
            // 
            // button_sendOnce
            // 
            this.button_sendOnce.Location = new System.Drawing.Point(799, 11);
            this.button_sendOnce.Name = "button_sendOnce";
            this.button_sendOnce.Size = new System.Drawing.Size(107, 21);
            this.button_sendOnce.TabIndex = 22;
            this.button_sendOnce.Text = "Send once";
            this.button_sendOnce.UseVisualStyleBackColor = true;
            this.button_sendOnce.Click += new System.EventHandler(this.button_sendOnce_Click);
            // 
            // textBox_deviceKey
            // 
            this.textBox_deviceKey.Location = new System.Drawing.Point(590, 52);
            this.textBox_deviceKey.Name = "textBox_deviceKey";
            this.textBox_deviceKey.Size = new System.Drawing.Size(443, 20);
            this.textBox_deviceKey.TabIndex = 21;
            // 
            // label_deviceKey
            // 
            this.label_deviceKey.AutoSize = true;
            this.label_deviceKey.Location = new System.Drawing.Point(520, 55);
            this.label_deviceKey.Name = "label_deviceKey";
            this.label_deviceKey.Size = new System.Drawing.Size(64, 13);
            this.label_deviceKey.TabIndex = 20;
            this.label_deviceKey.Text = "Device key:";
            // 
            // textBox_deviceName
            // 
            this.textBox_deviceName.Location = new System.Drawing.Point(326, 52);
            this.textBox_deviceName.Name = "textBox_deviceName";
            this.textBox_deviceName.Size = new System.Drawing.Size(188, 20);
            this.textBox_deviceName.TabIndex = 19;
            // 
            // label_deviceName
            // 
            this.label_deviceName.AutoSize = true;
            this.label_deviceName.Location = new System.Drawing.Point(247, 55);
            this.label_deviceName.Name = "label_deviceName";
            this.label_deviceName.Size = new System.Drawing.Size(73, 13);
            this.label_deviceName.TabIndex = 18;
            this.label_deviceName.Text = "Device name:";
            // 
            // textBox_iotHubName
            // 
            this.textBox_iotHubName.Location = new System.Drawing.Point(95, 52);
            this.textBox_iotHubName.Name = "textBox_iotHubName";
            this.textBox_iotHubName.Size = new System.Drawing.Size(149, 20);
            this.textBox_iotHubName.TabIndex = 17;
            // 
            // label_iotHubName
            // 
            this.label_iotHubName.AutoSize = true;
            this.label_iotHubName.Location = new System.Drawing.Point(16, 55);
            this.label_iotHubName.Name = "label_iotHubName";
            this.label_iotHubName.Size = new System.Drawing.Size(73, 13);
            this.label_iotHubName.TabIndex = 16;
            this.label_iotHubName.Text = "IoT hub name";
            // 
            // button_deleteDevice
            // 
            this.button_deleteDevice.Location = new System.Drawing.Point(520, 12);
            this.button_deleteDevice.Name = "button_deleteDevice";
            this.button_deleteDevice.Size = new System.Drawing.Size(114, 21);
            this.button_deleteDevice.TabIndex = 15;
            this.button_deleteDevice.Text = "Delete selected";
            this.button_deleteDevice.UseVisualStyleBackColor = true;
            this.button_deleteDevice.Click += new System.EventHandler(this.button_deleteDevice_Click);
            // 
            // button_addDevice
            // 
            this.button_addDevice.Location = new System.Drawing.Point(421, 12);
            this.button_addDevice.Name = "button_addDevice";
            this.button_addDevice.Size = new System.Drawing.Size(93, 21);
            this.button_addDevice.TabIndex = 14;
            this.button_addDevice.Text = "Save";
            this.button_addDevice.UseVisualStyleBackColor = true;
            this.button_addDevice.Click += new System.EventHandler(this.button_addDevice_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(81, 12);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(319, 21);
            this.comboBox1.TabIndex = 13;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Device:";
            // 
            // checkBox_deviceNames
            // 
            this.checkBox_deviceNames.AutoSize = true;
            this.checkBox_deviceNames.Location = new System.Drawing.Point(134, 588);
            this.checkBox_deviceNames.Name = "checkBox_deviceNames";
            this.checkBox_deviceNames.Size = new System.Drawing.Size(134, 17);
            this.checkBox_deviceNames.TabIndex = 15;
            this.checkBox_deviceNames.Text = "Numeric device names";
            this.checkBox_deviceNames.UseVisualStyleBackColor = true;
            this.checkBox_deviceNames.CheckedChanged += new System.EventHandler(this.checkBox_deviceNames_CheckedChanged);
            // 
            // button_add
            // 
            this.button_add.Location = new System.Drawing.Point(12, 585);
            this.button_add.Name = "button_add";
            this.button_add.Size = new System.Drawing.Size(107, 20);
            this.button_add.TabIndex = 14;
            this.button_add.Text = "Add sensor";
            this.button_add.UseVisualStyleBackColor = true;
            this.button_add.Click += new System.EventHandler(this.button_add_Click);
            // 
            // label_InfoHeader
            // 
            this.label_InfoHeader.AutoSize = true;
            this.label_InfoHeader.Location = new System.Drawing.Point(308, 589);
            this.label_InfoHeader.Name = "label_InfoHeader";
            this.label_InfoHeader.Size = new System.Drawing.Size(28, 13);
            this.label_InfoHeader.TabIndex = 16;
            this.label_InfoHeader.Text = "Info:";
            // 
            // label_Info
            // 
            this.label_Info.AutoSize = true;
            this.label_Info.Location = new System.Drawing.Point(343, 589);
            this.label_Info.Name = "label_Info";
            this.label_Info.Size = new System.Drawing.Size(35, 13);
            this.label_Info.TabIndex = 17;
            this.label_Info.Text = "label2";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1045, 613);
            this.Controls.Add(this.label_Info);
            this.Controls.Add(this.label_InfoHeader);
            this.Controls.Add(this.checkBox_deviceNames);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.button_add);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button button_deleteDevice;
        private System.Windows.Forms.Button button_addDevice;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_add;
        private System.Windows.Forms.Button button_startSending;
        private System.Windows.Forms.Button button_sendOnce;
        private System.Windows.Forms.TextBox textBox_deviceKey;
        private System.Windows.Forms.Label label_deviceKey;
        private System.Windows.Forms.TextBox textBox_deviceName;
        private System.Windows.Forms.Label label_deviceName;
        private System.Windows.Forms.TextBox textBox_iotHubName;
        private System.Windows.Forms.Label label_iotHubName;
        private System.Windows.Forms.CheckBox checkBox_deviceNames;
        private System.Windows.Forms.Label label_InfoHeader;
        private System.Windows.Forms.Label label_Info;
    }
}


﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureConfig6
{
    /// <summary>
    /// Class provides static methos for work with Azure tables. Firebase Realtime database is used to all neccessary data to mobile application.
    /// </summary>
    class TableHelper
    {
        /// <summary>
        /// Retreives all rows from Specification table as a list of Specification Table Row objects. Uses Microsoft.WindowsAzure.Storage.Table SDK.
        /// </summary>
        /// <param name="storageAccountName"> <see cref="System.String"/> representing table's parent Azure Storage account name.</param>
        /// <param name="storageAccountKey"> <see cref="System.String"/> representing table's parent Azure Storage account primary key.</param>
        /// <returns> Returns <see cref="List{T}"/> of <see cref="AzureConfig6.SpecificationTableRow"/> objects.</returns>
        public static List<SpecificationTableRow> fetchSpecificationTableWithSdk(string storageAccountName, string storageAccountKey)
        {
            CloudStorageAccount storageAccount = new CloudStorageAccount(new Microsoft.WindowsAzure.Storage.Auth.StorageCredentials(storageAccountName, storageAccountKey), true);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable tableReference = tableClient.GetTableReference("specifications");
            TableQuery<DynamicTableEntity> serialQuery = new TableQuery<DynamicTableEntity>();
            IEnumerable<DynamicTableEntity> retrievedResult = tableReference.ExecuteQuery(serialQuery);
            ICollection<DynamicTableEntity> retrievedResultCollection = retrievedResult.ToList<DynamicTableEntity>();
            if (retrievedResultCollection.Count > 0)
            {
                List<SpecificationTableRow> spec = new List<SpecificationTableRow>();
                foreach (DynamicTableEntity el in retrievedResult)
                {
                    SpecificationTableRow tableRow;
                    tableRow = ObjectFlattenerRecomposer.EntityPropertyConverter.ConvertBack<SpecificationTableRow>(el.Properties);
                    tableRow.PartitionKey = el.PartitionKey;
                    tableRow.RowKey = el.RowKey;
                    tableRow.Timestamp = el.Timestamp.ToString();
                    spec.Add(tableRow);
                }
                return spec;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Retreives specified row from Specification table as a Specification Table Row object. Uses Microsoft.WindowsAzure.Storage.Table SDK.
        /// </summary>
        /// <param name="storageAccountName"> <see cref="System.String"/> representing table's parent Azure Storage account name.</param>
        /// <param name="storageAccountKey"> <see cref="System.String"/> representing table's parent Azure Storage account primary key.</param>
        /// /// <param name="serial"> <see cref="System.String"/> representing devices Serial number (Row Key in Specification table).</param>
        /// <returns> Returns <see cref="AzureConfig6.SpecificationTableRow"/> objects.</returns>
        public static SpecificationTableRow fetchSpecificationRowWithSdk(string storageAccountName, string storageAccountKey, string serial)
        {
            CloudStorageAccount storageAccount = new CloudStorageAccount(new Microsoft.WindowsAzure.Storage.Auth.StorageCredentials(storageAccountName, storageAccountKey), true);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable tableReference = tableClient.GetTableReference("specifications");
            TableQuery<DynamicTableEntity> serialQuery = new TableQuery<DynamicTableEntity>().Where(TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, serial));
            IEnumerable<DynamicTableEntity> retrievedResult = tableReference.ExecuteQuery(serialQuery);
            ICollection<DynamicTableEntity> retrievedResultCollection = retrievedResult.ToList<DynamicTableEntity>();
            if (retrievedResultCollection.Count > 0)
            {
                List<SpecificationTableRow> spec = new List<SpecificationTableRow>();
                foreach (DynamicTableEntity el in retrievedResult)
                {
                    SpecificationTableRow tableRow = new SpecificationTableRow();
                    tableRow = ObjectFlattenerRecomposer.EntityPropertyConverter.ConvertBack<SpecificationTableRow>(el.Properties);
                    tableRow.PartitionKey = el.PartitionKey;
                    tableRow.RowKey = el.RowKey;
                    tableRow.Timestamp = el.Timestamp.ToString();
                    spec.Add(tableRow);
                }
                return spec[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Retreives all rows from Configuration table as a list of Configuration Table Row objects. Uses Microsoft.WindowsAzure.Storage.Table SDK.
        /// </summary>
        /// <param name="storageAccountName"> <see cref="System.String"/> representing table's parent Azure Storage account name.</param>
        /// <param name="storageAccountKey"> <see cref="System.String"/> representing table's parent Azure Storage account primary key.</param>
        /// <returns> Returns <see cref="List{T}"/> of <see cref="AzureConfig6.ConfigurationTableRow"/> objects.</returns>
        public static List<ConfigurationTableRow> fetchConfigurationTableWithSdk(string storageAccountName, string storageAccountKey)
        {
            CloudStorageAccount storageAccount = new CloudStorageAccount(new Microsoft.WindowsAzure.Storage.Auth.StorageCredentials(storageAccountName, storageAccountKey), true);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable tableReference = tableClient.GetTableReference("configurations");
            TableQuery<DynamicTableEntity> serialQuery = new TableQuery<DynamicTableEntity>();
            IEnumerable<DynamicTableEntity> retrievedResult = tableReference.ExecuteQuery(serialQuery);
            ICollection<DynamicTableEntity> retrievedResultCollection = retrievedResult.ToList<DynamicTableEntity>();
            if (retrievedResultCollection.Count > 0)
            {
                List<ConfigurationTableRow> config = new List<ConfigurationTableRow>();
                foreach (DynamicTableEntity el in retrievedResult)
                {
                    ConfigurationTableRow tableRow;
                    tableRow = ObjectFlattenerRecomposer.EntityPropertyConverter.ConvertBack<ConfigurationTableRow>(el.Properties);
                    tableRow.PartitionKey = el.PartitionKey;
                    tableRow.RowKey = el.RowKey;
                    tableRow.Timestamp = el.Timestamp.ToString();
                    config.Add(tableRow);
                }
                return config;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Retreives specified row from Specification table as a Specification Table Row object. Uses Microsoft.WindowsAzure.Storage.Table SDK.
        /// </summary>
        /// <param name="storageAccountName"> <see cref="System.String"/> representing table's parent Azure Storage account name.</param>
        /// <param name="storageAccountKey"> <see cref="System.String"/> representing table's parent Azure Storage account primary key.</param>
        /// <param name="model"> <see cref="System.String"/> representing devices Model (Partition Key in Configuration table).</param>
        /// <param name="version"> <see cref="System.String"/> representing devices Version (Row Key in Configuration table).</param>
        /// <returns> Returns <see cref="AzureConfig6.ConfigurationTableRow"/> object.</returns>
        public static ConfigurationTableRow fetchConfigurationRowWithSdk(string storageAccountName, string storageAccountKey, string model, string version)
        {
            CloudStorageAccount storageAccount = new CloudStorageAccount(new Microsoft.WindowsAzure.Storage.Auth.StorageCredentials(storageAccountName, storageAccountKey), true);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable tableReference = tableClient.GetTableReference("configurations");
            String modelFilter = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, model);
            String versionFilter = TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, version);
            String finalFilter = TableQuery.CombineFilters(modelFilter, TableOperators.And, versionFilter);
            TableQuery<DynamicTableEntity> serialQuery = new TableQuery<DynamicTableEntity>().Where(finalFilter);
            IEnumerable<DynamicTableEntity> retrievedResult = tableReference.ExecuteQuery(serialQuery);
            ICollection<DynamicTableEntity> retrievedResultCollection = retrievedResult.ToList<DynamicTableEntity>();
            if (retrievedResultCollection.Count > 0)
            {
                List<ConfigurationTableRow> config = new List<ConfigurationTableRow>();
                foreach (DynamicTableEntity el in retrievedResult)
                {
                    ConfigurationTableRow tableRow;
                    tableRow = ObjectFlattenerRecomposer.EntityPropertyConverter.ConvertBack<ConfigurationTableRow>(el.Properties);
                    tableRow.PartitionKey = el.PartitionKey;
                    tableRow.RowKey = el.RowKey;
                    tableRow.Timestamp = el.Timestamp.ToString();
                    config.Add(tableRow);
                }
                return config[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Puts Specification Table Row to Specifications table. Uses Microsoft.WindowsAzure.Storage.Table SDK.
        /// </summary>
        /// <param name="storageAccountName"> <see cref="System.String"/> representing table's parent Azure Storage account name.</param>
        /// <param name="storageAccountKey"> <see cref="System.String"/> representing table's parent Azure Storage account primary key.</param>
        /// <param name="specificationTableRow"> <see cref="AzureConfig6.SpecificationTableRow"/> representing device's specification.</param>
        public static void putSpecification(string storageAccountName, string storageAccountKey, SpecificationTableRow specificationTableRow)
        {
            CloudStorageAccount storageAccount = new CloudStorageAccount(new Microsoft.WindowsAzure.Storage.Auth.StorageCredentials(storageAccountName, storageAccountKey), true);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable tableReference = tableClient.GetTableReference("specifications");
            Dictionary<string, EntityProperty> deviceDictionary = ObjectFlattenerRecomposer.EntityPropertyConverter.Flatten(
                new DeviceSpecificationToFlatten(specificationTableRow.PartitionKey, specificationTableRow.RowKey, specificationTableRow.Model, specificationTableRow.Version)); 
            DynamicTableEntity dynamicTableEntity = new DynamicTableEntity(specificationTableRow.PartitionKey, specificationTableRow.RowKey);
            dynamicTableEntity.Properties = deviceDictionary;
            TableOperation insertOperation = TableOperation.InsertOrReplace(dynamicTableEntity);
            tableReference.Execute(insertOperation);
            Console.WriteLine("Ineserted...");
        }

        /// <summary>
        /// Updates Specification Table Row in Specifications table. Uses Microsoft.WindowsAzure.Storage.Table SDK.
        /// </summary>
        /// <param name="storageAccountName"> <see cref="System.String"/> representing table's parent Azure Storage account name.</param>
        /// <param name="storageAccountKey"> <see cref="System.String"/> representing table's parent Azure Storage account primary key.</param>
        /// <param name="oldSpecificationTableRow"> <see cref="AzureConfig6.SpecificationTableRow"/> representing device's specification which will be overwrited.</param>
        /// <param name="newSpecificationTableRow"> <see cref="AzureConfig6.SpecificationTableRow"/> representing device's specification which will be saved to table.</param>
        public static void updateSpecification(string storageAccountName, string storageAccountKey, SpecificationTableRow oldSpecificationTableRow, SpecificationTableRow newSpecificationTableRow)
        {
            CloudStorageAccount storageAccount = new CloudStorageAccount(new Microsoft.WindowsAzure.Storage.Auth.StorageCredentials(storageAccountName, storageAccountKey), true);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable tableReference = tableClient.GetTableReference("specifications");
            DynamicTableEntity dynamicTableEntity;
            try
            {
                dynamicTableEntity = new DynamicTableEntity(oldSpecificationTableRow.PartitionKey, oldSpecificationTableRow.RowKey) { ETag = "*" };
                TableOperation deleteOpertaion = TableOperation.Delete(dynamicTableEntity);
                tableReference.Execute(deleteOpertaion);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            Dictionary<string, EntityProperty> deviceDictionary = ObjectFlattenerRecomposer.EntityPropertyConverter.Flatten(
                new DeviceSpecificationToFlatten(newSpecificationTableRow.PartitionKey, newSpecificationTableRow.RowKey, newSpecificationTableRow.Model, newSpecificationTableRow.Version));
            dynamicTableEntity = new DynamicTableEntity(newSpecificationTableRow.PartitionKey, newSpecificationTableRow.RowKey);
            dynamicTableEntity.Properties = deviceDictionary;
            TableOperation insertOperation = TableOperation.InsertOrReplace(dynamicTableEntity);
            tableReference.Execute(insertOperation);
            Console.WriteLine("Updated...");
        }

        /// <summary>
        /// Puts Configuration Table Row to Configurations table. Uses Microsoft.WindowsAzure.Storage.Table SDK.
        /// </summary>
        /// <param name="storageAccountName"> <see cref="System.String"/> representing table's parent Azure Storage account name.</param>
        /// <param name="storageAccountKey"> <see cref="System.String"/> representing table's parent Azure Storage account primary key.</param>
        /// <param name="configurationTableRow"> <see cref="AzureConfig6.ConfigurationTableRow"/> representing device's configuration.</param>
        public static void putConfiguration(string storageAccountName, string storageAccountKey, ConfigurationTableRow configurationTableRow)
        {
            CloudStorageAccount storageAccount = new CloudStorageAccount(new Microsoft.WindowsAzure.Storage.Auth.StorageCredentials(storageAccountName, storageAccountKey), true);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable tableReference = tableClient.GetTableReference("configurations");
            Dictionary<string, EntityProperty> deviceDictionary = ObjectFlattenerRecomposer.EntityPropertyConverter.Flatten(new DeviceConfigurationToFlatten(configurationTableRow.PartitionKey, configurationTableRow.RowKey, configurationTableRow.Sensors));
            DynamicTableEntity dynamicTableEntity = new DynamicTableEntity(configurationTableRow.PartitionKey, configurationTableRow.RowKey);
            dynamicTableEntity.Properties = deviceDictionary;
            TableOperation insertOperation = TableOperation.InsertOrReplace(dynamicTableEntity);
            tableReference.Execute(insertOperation);
            Console.WriteLine("Ineserted...");
        }
    }
}

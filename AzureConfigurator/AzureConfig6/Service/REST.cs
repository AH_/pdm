﻿
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AzureConfig6
{
    /// <summary>
    /// Class provides static methos for configuring REST requests to Azure.
    /// </summary>
    public static class REST
    {
        /// <summary>
        /// Retreives Azures Access Token to sign REST requests.
        /// </summary>
        /// <param name="tenantId">The <see cref="System.String"/> instance that represents Tenant ID.</param>
        /// <param name="applicationId">The <see cref="System.String"/> instance that represents Application ID.</param>
        /// <param name="clientSecret">The <see cref="System.String"/> instance that represents Application (Client) Secret.</param>
        /// <returns>A <see cref="String"/> that represents Azures Access Token.</returns>
        public static string GetAccessToken(String tenantId, String applicationId, String clientSecret)
        {
            string authContextURL = "https://login.windows.net/" + tenantId;
            AuthenticationContext authenticationContext = new AuthenticationContext(authContextURL);
            ClientCredential credential = new ClientCredential(applicationId, clientSecret);
            Task<AuthenticationResult> result = authenticationContext.AcquireTokenAsync("https://management.azure.com/", credential);

            if (result == null)
            {
                throw new InvalidOperationException("Failed to obtain the JWT token");
            }

            //await result;
            string token = null;
            try
            {
                token = result.Result.AccessToken.ToString();
            }catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
            return token;
        }

        /// <summary>
        /// Makes GET request.
        /// </summary>
        /// <param name="URI">The <see cref="System.String"/> instance that represents URI for GET request.</param>
        /// <param name="token">The <see cref="System.String"/> instance that represents Azure Access Token to sign the request.</param>
        /// /// <returns>A <see cref="String"/> that represents responce to GET request.</returns>
        public static string doGET(String URI, String token)
        {
            Uri uri = new Uri(String.Format(URI));

            // Create the request
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + token);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";

            // Get the response
            HttpWebResponse httpResponse = null;
            try
            {
                httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error from : " + uri + ": " + ex.Message, "HttpWebResponse exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("Error from : " + uri + ": " + ex.Message, "HttpWebResponse exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }

            string result = null;
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }

            return result;
        }

        /// <summary>
        /// Makes GET request signed with Shared Access Signature.
        /// </summary>
        /// <param name="URI">The <see cref="System.String"/> instance that represents URI for GET request.</param>
        /// <param name="sasToken">The <see cref="System.String"/> instance that represents Azure Shared Access Token to sign the request.</param>
        /// /// <returns>A <see cref="String"/> that represents responce to GET request.</returns>
        public static string doGETwithSas(String URI, String sasToken)
        {
            Uri uri = new Uri(String.Format(URI));

            // Create the request
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, sasToken);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";

            // Get the response
            HttpWebResponse httpResponse = null;
            try
            {
                httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error from : " + uri + ": " + ex.Message, "HttpWebResponse exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("Error from : " + uri + ": " + ex.Message, "HttpWebResponse exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }

            string result = null;
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }

            return result;
        }

        /// <summary>
        /// Makes PUT request.
        /// </summary>
        /// <param name="URI">The <see cref="System.String"/> instance that represents URI for PUT request.</param>
        /// <param name="body">The <see cref="System.String"/> instance that represents body for PUT request.</param>
        /// <param name="token">The <see cref="System.String"/> instance that represents Azure Access Token to sign the request.</param>
        /// <returns>A <see cref="String"/> that represents responce to PUT request.</returns>
        public static string doPUT(string URI, string body, String token)
        {
            Uri uri = new Uri(String.Format(URI));

            // Create the request
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + token);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "PUT";

            try
            {
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(body);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error setting up stream writer: " + ex.Message,
                //    "GetRequestStream exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("Error setting up stream writer: " + ex.Message,
                    "GetRequestStream exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            // Get the response
            HttpWebResponse httpResponse = null;
            try
            {
                httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error from : " + uri + ": " + ex.Message,
                //                "HttpWebResponse exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("Error from : " + uri + ": " + ex.Message,
                                "HttpWebResponse exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }

            string result = null;
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }
            return result;
        }

        /// <summary>
        /// Makes PUT request signed with Shared Access Signature.
        /// </summary>
        /// <param name="URI">The <see cref="System.String"/> instance that represents URI for PUT request.</param>
        /// <param name="body">The <see cref="System.String"/> instance that represents body for PUT request.</param>
        /// <param name="token">The <see cref="System.String"/> instance that represents Azure Shared Access Token to sign the request.</param>
        /// <returns>A <see cref="String"/> that represents responce to PUT request.</returns>
        public static string doPUTwithSas(string URI, string body, String token)
        {
            Uri uri = new Uri(String.Format(URI));

            // Create the request
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, token);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "PUT";

            try
            {
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(body);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error setting up stream writer: " + ex.Message,
                //    "GetRequestStream exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("Error setting up stream writer: " + ex.Message,
                    "GetRequestStream exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            // Get the response
            HttpWebResponse httpResponse = null;
            try
            {
                httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error from : " + uri + ": " + ex.Message,
                //                "HttpWebResponse exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("Error from : " + uri + ": " + ex.Message,
                                "HttpWebResponse exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }

            string result = null;
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }
            return result;
        }

        /// <summary>
        /// Makes POST request.
        /// </summary>
        /// <param name="URI">The <see cref="System.String"/> instance that represents URI for POST request.</param>
        /// <param name="body">The <see cref="System.String"/> instance that represents body for POST request.</param>
        /// <param name="token">The <see cref="System.String"/> instance that represents Azure Access Token to sign the request.</param>
        /// <returns>A <see cref="String"/> that represents responce to POST request.</returns>
        public static string doPOST(string URI, string body, String token)
        {
            Uri uri = new Uri(String.Format(URI));

            // Create the request
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + token);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            try
            {
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(body);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error setting up stream writer: " + ex.Message,
                //    "GetRequestStream exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("Error setting up stream writer: " + ex.Message,
                    "GetRequestStream exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            // Get the response
            HttpWebResponse httpResponse = null;
            try
            {
                httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error from : " + uri + ": " + ex.Message,
                //                "HttpWebResponse exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("Error from : " + uri + ": " + ex.Message,
                                "HttpWebResponse exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }

            string result = null;
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }
            return result;
        }

        /// <summary>
        /// Makes POST request signed with Shared Access Signature.
        /// </summary>
        /// <param name="URI">The <see cref="System.String"/> instance that represents URI for POST request.</param>
        /// <param name="body">The <see cref="System.String"/> instance that represents body for POST request.</param>
        /// <param name="token">The <see cref="System.String"/> instance that represents Azure Shared Access Token to sign the request.</param>
        /// <returns>A <see cref="String"/> that represents responce to POST request.</returns>
        public static string doPOSTwithSas(string URI, string body, String token)
        {
            Uri uri = new Uri(String.Format(URI));

            // Create the request
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, token);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            try
            {
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(body);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error setting up stream writer: " + ex.Message,
                //    "GetRequestStream exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("Error setting up stream writer: " + ex.Message,
                    "GetRequestStream exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            // Get the response
            HttpWebResponse httpResponse = null;
            try
            {
                httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error from : " + uri + ": " + ex.Message,
                //                "HttpWebResponse exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("Error from : " + uri + ": " + ex.Message,
                                "HttpWebResponse exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }

            string result = null;
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }
            return result;
        }

        /// <summary>
        /// Makes DELETE request.
        /// </summary>
        /// <param name="URI">The <see cref="System.String"/> instance that represents URI for DELETE request.</param>
        /// <param name="token">The <see cref="System.String"/> instance that represents Azure Access Token to sign the request.</param>
        /// <returns>A <see cref="String"/> that represents responce to DELETE request.</returns>
        public static string doDELETE(string URI, String token)
        {
            Uri uri = new Uri(String.Format(URI));

            // Create the request
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + token);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "DELETE";

            try
            {
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Flush();
                    streamWriter.Close();
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error setting up stream writer: " + ex.Message,
                //    "GetRequestStream exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("Error setting up stream writer: " + ex.Message,
                    "GetRequestStream exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            // Get the response
            HttpWebResponse httpResponse = null;
            try
            {
                httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error from : " + uri + ": " + ex.Message,
                //                "HttpWebResponse exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("Error from : " + uri + ": " + ex.Message,
                                "HttpWebResponse exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }

            string result = null;
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }
            return result;
        }

        /// <summary>
        /// Makes DELETE request signed with Shared Access Signature.
        /// </summary>
        /// <param name="URI">The <see cref="System.String"/> instance that represents URI for DELETE request.</param>
        /// <param name="token">The <see cref="System.String"/> instance that represents Azure Shared Access Token to sign the request.</param>
        /// <returns>A <see cref="String"/> that represents responce to POST request.</returns>
        public static string doDELETEwithSas(string URI, String token)
        {
            Uri uri = new Uri(String.Format(URI));

            // Create the request
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, token);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Headers.Add(HttpRequestHeader.IfMatch, "*");
            httpWebRequest.Method = "DELETE";

            try
            {
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Flush();
                    streamWriter.Close();
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error setting up stream writer: " + ex.Message,
                //    "GetRequestStream exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("Error setting up stream writer: " + ex.Message,
                    "GetRequestStream exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            // Get the response
            HttpWebResponse httpResponse = null;
            try
            {
                httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error from : " + uri + ": " + ex.Message,
                //                "HttpWebResponse exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("Error from : " + uri + ": " + ex.Message,
                                "HttpWebResponse exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }

            string result = null;
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }
            return result;
        }

        /// <summary>
        /// Makes HEAD request.
        /// </summary>
        /// <param name="URI">The <see cref="System.String"/> instance that represents URI for HEAD request.</param>
        /// <param name="token">The <see cref="System.String"/> instance that represents Azure Access Token to sign the request.</param>
        /// <returns>A <see cref="String"/> that represents responce to HEAD request.</returns>
        public static Boolean doHEAD(String URI, String token)
        {
            Uri uri = new Uri(String.Format(URI));

            // Create the request
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + token);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "HEAD";

            // Get the response
            HttpWebResponse httpResponse = null;
            try
            {
                httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                //MessageBox.Show("Error from : " + uri + ": " + ex.Message, "HttpWebResponse exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //return null;
            }

            Boolean result;
            if(httpResponse == null)
            {
                result = false;
            }
            else
            {
                result = true;
            }

            return result;
        }
    }
}
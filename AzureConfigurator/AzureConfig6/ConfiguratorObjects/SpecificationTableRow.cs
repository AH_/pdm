﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureConfig6
{
    public class SpecificationTableRow
    {
        public String PartitionKey { get; set; }        //Owner
        public String RowKey { get; set; }      //Serial
        public String Timestamp { get; set; }
        public String Model { get; set; }
        public String Version { get; set; }

        public SpecificationTableRow()
        {
            this.PartitionKey = "";
            this.RowKey = "";
            this.Model = "";
            this.Version = "";
        }

        public SpecificationTableRow(String owner, String serial, String model, String version)
        {
            this.PartitionKey = owner;
            this.RowKey = serial;
            this.Model = model;
            this.Version = version;
        }

        public SpecificationTableRow(String serial)
        {
            this.PartitionKey = "";
            this.RowKey = serial;
            this.Model = "";
            this.Version = "";
        }

        public SpecificationTableRow(SpecificationTableRow row)
        {
            this.PartitionKey = row.PartitionKey;
            this.RowKey = row.RowKey;
            this.Model = row.Model;
            this.Version = row.Version;
        }
    }
}
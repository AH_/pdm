﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureConfig6
{
    public class ConfiguratorStreamAnalyticsTransformation
    {
        public String name { get; set; }
        public String inputsString { get; set; }
        public String outputsString { get; set; }
        public List<AzureStreamAnalyticsInput> inputs { get; set; }
        public List<AzureStreamAnalyticsOutput> outputs { get; set; }
    }
}
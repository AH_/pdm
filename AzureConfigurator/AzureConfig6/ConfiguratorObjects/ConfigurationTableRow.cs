﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureConfig6
{
    public class ConfigurationTableRow
    {
        public String PartitionKey { get; set; }        //Model
        public String RowKey { get; set; }      //Version
        public String Timestamp { get; set; }
        public List<Sensor> Sensors { get; set; }

        public ConfigurationTableRow()
        {
            this.PartitionKey = null;
            this.RowKey = null;
            this.Timestamp = null;
            this.Sensors = new List<Sensor>();
        }

        public ConfigurationTableRow(String model, String version, List<Sensor> sensors)
        {
            this.PartitionKey = model;
            this.RowKey = version;
            this.Timestamp = "";
            this.Sensors = sensors;
        }
    }
}
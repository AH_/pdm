﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureConfig6
{
    public class AzureTransformationPair
    {
        public String inputName { get; set; }
        public String outputName { get; set; }
        public AzureStreamAnalyticsInput input { get; set; }
        public AzureStreamAnalyticsOutput output { get; set; }
        //AzureIoThub input;
        //AzureTable output;

        //public AzureTransformationPair(AzureIoThub input, AzureTable output)
        //{
        //    this.inputName = input.name;
        //    this.outputName = output.name;
        //    this.input = input;
        //    this.output = output;
        //}

        public AzureTransformationPair(AzureStreamAnalyticsInput input, AzureStreamAnalyticsOutput output)
        {
            this.inputName = input.name;
            this.outputName = output.name;
            this.input = input;
            this.output = output;
        }

        public override bool Equals(object obj)
        {
            AzureTransformationPair transformationPair = obj as AzureTransformationPair;
            if(this.inputName.Equals(transformationPair.inputName) &&
                this.outputName.Equals(transformationPair.outputName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
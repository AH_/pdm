﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureConfig6
{
    public class IoThubKey
    {
        public String keyName { get; set; }
        public String primaryKey { get; set; }
        public String secondaryKey { get; set; }
        public String rights { get; set; }
    }
}

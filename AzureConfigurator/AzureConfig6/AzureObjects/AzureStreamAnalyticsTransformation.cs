﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureConfig6
{
    public class AzureStreamAnalyticsTransformation
    {
        public String name { get; set; }
        public String id { get; set; }
        public String type { get; set; }
        public Properties properties { get; set; }

        public class Properties
        {
            public int streamingUnits { get; set; }
            public String query { get; set; }
        }
    }
}

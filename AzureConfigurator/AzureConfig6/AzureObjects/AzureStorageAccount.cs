﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure.Storage;
using System.Threading;

namespace AzureConfig6
{
    public class AzureStorageAccount
    {
        public Sku sku { get; set; }
        public String kind { get; set; }
        public String id { get; set; }
        public String name { get; set; }
        public String type { get; set; }
        public String location { get; set; }
        public List<Key> keys { get; set; }

        public class Sku
        {
            public String name { get; set; }
            public String tier { get; set; }
        }

        public class Key
        {
            public String keyName { get; set; }
            public String value { get; set; }
            public String permissions { get; set; }
        }

        /// <summary>
        /// Retreives specified Azure Storage Account as an Azure Storage Account object.
        /// </summary>
        /// <param name="storageName">The <see cref="System.String"/> instance that represents Azure Storage account name.</param>
        /// <returns>A <see cref="AzureConfig6.AzureStorageAccount"/> that represents Azure Storage Account instanse.</returns>
        public static async Task<AzureStorageAccount> getStorageAsync(String storageName)
        {
            String uri = string.Format("https://management.azure.com/subscriptions/{0}/resourceGroups/{1}/providers/Microsoft.Storage/storageAccounts/{2}?api-version=2019-06-01",
                Globals.subscription.subscriptionId, Globals.resourceGroup.name, storageName);

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Globals.token);
            HttpResponseMessage response = await client.GetAsync(uri);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                Console.WriteLine(response);
                return null;
            }
            else
            {
                String responceContent = await response.Content.ReadAsStringAsync();
                AzureStorageAccount storageAccount = JObject.Parse(responceContent).ToObject<AzureStorageAccount>();
                storageAccount.keys = await storageAccount.getStorageKeysAsync();
                return storageAccount;
            }
        }

        /// <summary>
        /// Retreives Azure Storage Account keys as a list async.
        /// </summary>
        /// <returns>A <see cref="List{T}"/>  of <see cref="AzureConfig6.AzureStorageAccount.Key"/> that contains all Storage Account's keys.</returns>
        public async Task<List<AzureStorageAccount.Key>> getStorageKeysAsync()
        {
            String uri = string.Format("https://management.azure.com/subscriptions/{0}/resourceGroups/{1}/providers/Microsoft.Storage/storageAccounts/{2}/listKeys?api-version=2019-04-01",
                Globals.subscription.subscriptionId, Globals.resourceGroup.name, this.name);

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Globals.token);
            HttpResponseMessage response = await client.PostAsync(uri, new StringContent("", Encoding.UTF8, "application/json"));
            if(!(response.StatusCode == System.Net.HttpStatusCode.OK))
            {
                Console.WriteLine(response);
                return null;
            }else
            {
                String responseString = await response.Content.ReadAsStringAsync();
                List<AzureStorageAccount.Key> keys = JObject.Parse(responseString)["keys"].ToObject<List<AzureStorageAccount.Key>>();
                return keys;
            }
        }

        /// <summary>
        /// Creates Azure Storage Account async.
        /// </summary>
        /// <param name="stoarageName">The <see cref="System.String"/> instance that represents Azure Storage Account name.</param>
        public static async Task<Boolean> createStorageAsync(String storageName)
        {
            String uri = string.Format("https://management.azure.com/subscriptions/{0}/resourceGroups/{1}/providers/Microsoft.Storage/storageAccounts/{2}?api-version=2019-04-01",
                Globals.subscription.subscriptionId, Globals.resourceGroup.name, storageName);
            String body = "{\"sku\": {\"name\": \"Standard_GRS\"}, \"kind\": \"Storage\",\"location\":\"eastus\"}";
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Globals.token);
            HttpResponseMessage response = await client.PutAsync(uri, new StringContent(body, Encoding.UTF8, "application/json"));

            if (response.StatusCode != System.Net.HttpStatusCode.Accepted)
            {
                Console.WriteLine(response);
                return false;
            }
            else
            {
                uri = string.Format("https://management.azure.com/subscriptions/{0}/resourceGroups/{1}/providers/Microsoft.Storage/storageAccounts/{2}/listKeys?api-version=2019-04-01",
                Globals.subscription.subscriptionId, Globals.resourceGroup.name, storageName);
                int safetyCounter = 0;
                do
                {
                    Thread.Sleep(1000);
                    response = await client.PostAsync(uri, new StringContent("", Encoding.UTF8, "application/json"));
                    safetyCounter++;
                } while (response.StatusCode != System.Net.HttpStatusCode.OK && safetyCounter < 100);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return true;
                }
                else
                {
                    Console.WriteLine(response);
                    return false;
                }
            }
        }

        /// <summary>
        /// Removes Azure Storage Account.
        /// </summary>
        public async Task<Boolean> deleteStorageAsync()
        {
            String storageName = this.name;
            String uri = string.Format("https://management.azure.com/subscriptions/{0}/resourceGroups/{1}/providers/Microsoft.Storage/storageAccounts/{2}?api-version=2019-04-01",
                Globals.subscription.subscriptionId, Globals.resourceGroup.name, storageName);

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Globals.token);
            HttpResponseMessage response = await client.DeleteAsync(uri);

            if (response.StatusCode != System.Net.HttpStatusCode.Accepted)
            {
                Console.WriteLine(response);
                return false;
            }
            else
            {
                uri = string.Format("https://management.azure.com/subscriptions/{0}/resourceGroups/{1}/providers/Microsoft.Storage/storageAccounts/{2}?api-version=2019-06-01",
                Globals.subscription.subscriptionId, Globals.resourceGroup.name, storageName);
                int safetyCounter = 0;
                do
                {
                    Thread.Sleep(1000);
                    response = await client.GetAsync(uri);
                    safetyCounter++;
                } while (response.StatusCode != System.Net.HttpStatusCode.NotFound && safetyCounter < 100);

                if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    return true;
                }
                else
                {
                    Console.WriteLine(response);
                    return false;
                }
            }
        }


        /// <summary>
        /// Retreives all Storage Account's child tables as a list of Azure Table objects async.
        /// </summary>
        /// <returns>A <see cref="List{T}"/> of <see cref="AzureConfig6.AzureTable"/> that contains all Storage Account's child tables.</returns>
        public async Task<List<AzureTable>> listTablesAsync()
        {
            HttpClient Client = new HttpClient();
            var requestUri = new Uri($"https://{this.name}.table.core.windows.net/Tables");

            //Date header start
            var RequestDateString = DateTime.UtcNow.ToString("R", CultureInfo.InvariantCulture);

            if (Client.DefaultRequestHeaders.Contains("x-ms-date"))
                Client.DefaultRequestHeaders.Remove("x-ms-date");
            Client.DefaultRequestHeaders.Add("x-ms-date", RequestDateString);

            if (Client.DefaultRequestHeaders.Contains("Authorization"))
                Client.DefaultRequestHeaders.Remove("Authorization");

            if (Client.DefaultRequestHeaders.Contains("x-ms-version"))
                Client.DefaultRequestHeaders.Remove("x-ms-version");

            Client.DefaultRequestHeaders.Add("x-ms-version", "2015-12-11");

            Client.DefaultRequestHeaders.Accept.Clear();
            Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var canonicalizedStringToBuild = string.Format("{0}\n{1}", RequestDateString, $"/{this.name}/{requestUri.AbsolutePath.TrimStart('/')}");
            string signature;
            using (var hmac = new HMACSHA256(Convert.FromBase64String(this.keys[0].value)))
            {
                byte[] dataToHmac = Encoding.UTF8.GetBytes(canonicalizedStringToBuild);
                signature = Convert.ToBase64String(hmac.ComputeHash(dataToHmac));
            }

            string authorizationHeader = string.Format($"{this.name}:" + signature);
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("SharedKeyLite", authorizationHeader);

            HttpResponseMessage responseMessage = await Client.GetAsync(requestUri);
            String responseString = await responseMessage.Content.ReadAsStringAsync();

            JArray ta = JArray.Parse(JObject.Parse(responseString)["value"].ToString());

            List<AzureTable> tables = new List<AzureTable>();

            foreach (JObject el in ta)
            {
                tables.Add(new AzureTable(JObject.Parse(el.ToString())["TableName"].ToString()));
            }
            return tables;
        }
        public List<AzureTable> listTablesBySdk()
        {
            String storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=" + this.name + ";AccountKey=" + this.keys[0].value + ";EndpointSuffix=core.windows.net";

            List<AzureTable> azureTables = new List<AzureTable>();
            CloudStorageAccount cloudStorageAccount = CloudStorageAccount.Parse(storageConnectionString);
            CloudTableClient tableClient = cloudStorageAccount.CreateCloudTableClient();
            List<CloudTable> cloudTables = tableClient.ListTables().ToList();
            foreach(CloudTable ctable in cloudTables)
            {
                azureTables.Add(new AzureTable(ctable.Name));
            }
            return azureTables;
        }




        /// <summary>
        /// Creates Azure Table in the Storage Account.
        /// </summary>
        /// <param name="tablename">The <see cref="System.String"/> instance that represents Azure Table's name.</param>
        public void createTableRest(String tablename)
        {
            String body = "{\"TableName\":\"" + tablename + "\"}";
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] bodyBytes = encoding.GetBytes(body);

            var requestUri = new Uri($"https://{this.name}.table.core.windows.net/Tables");
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestUri);
            request.Method = "POST";

            //Authorization START
            var RequestDateString = DateTime.UtcNow.ToString("R", CultureInfo.InvariantCulture);

            if (request.Headers.Get("Authorization") != null)
                request.Headers.Remove("Authorization");

            var canonicalizedStringToBuild = string.Format("{0}\n{1}", RequestDateString, $"/{this.name}/{requestUri.AbsolutePath.TrimStart('/')}");
            string signature;
            using (var hmac = new HMACSHA256(Convert.FromBase64String(this.keys[0].value)))
            {
                byte[] dataToHmac = Encoding.UTF8.GetBytes(canonicalizedStringToBuild);
                signature = Convert.ToBase64String(hmac.ComputeHash(dataToHmac));
            }

            string authorizationHeader = string.Format($"{this.name}:" + signature);
            request.Headers.Add("Authorization", new AuthenticationHeaderValue("SharedKeyLite", authorizationHeader).ToString());
            //Authorization END

            //x-ms-date START
            if (request.Headers.Get("x-ms-date") != null)
                request.Headers.Remove("x-ms-date");
            request.Headers.Add("x-ms-date", RequestDateString);
            //x-ms-date END

            //x-ms-version START
            if (request.Headers.Get("x-ms-version") != null)
                request.Headers.Remove("x-ms-version");
            request.Headers.Add("x-ms-version", "2015-12-11");
            //x-ms-version END

            //Accept START
            if (request.Accept != null)
                request.Accept = null;
            request.Accept = "application/json";
            //Accept END

            //Content-Type START
            if (request.ContentType != null)
                request.ContentType = null;
            request.ContentType = "application/json";
            //Content-Type END

            //Content-Length START
            request.ContentLength = bodyBytes.Length;
            //Content-Length END

            Stream stream = request.GetRequestStream();
            stream.Write(bodyBytes, 0, bodyBytes.Length);

            String s = request.GetResponse().ToString();
            return;
        }
        public async Task<Boolean> createTableRestAsync(String tablename)
        {
            String body = "{\"TableName\":\"" + tablename + "\"}";
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] bodyBytes = encoding.GetBytes(body);

            var requestUri = new Uri($"https://{this.name}.table.core.windows.net/Tables");
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestUri);
            request.Method = "POST";

            //Authorization START
            var RequestDateString = DateTime.UtcNow.ToString("R", CultureInfo.InvariantCulture);

            if (request.Headers.Get("Authorization") != null)
                request.Headers.Remove("Authorization");

            var canonicalizedStringToBuild = string.Format("{0}\n{1}", RequestDateString, $"/{this.name}/{requestUri.AbsolutePath.TrimStart('/')}");
            string signature;
            using (var hmac = new HMACSHA256(Convert.FromBase64String(this.keys[0].value)))
            {
                byte[] dataToHmac = Encoding.UTF8.GetBytes(canonicalizedStringToBuild);
                signature = Convert.ToBase64String(hmac.ComputeHash(dataToHmac));
            }

            string authorizationHeader = string.Format($"{this.name}:" + signature);
            request.Headers.Add("Authorization", new AuthenticationHeaderValue("SharedKeyLite", authorizationHeader).ToString());
            //Authorization END

            //x-ms-date START
            if (request.Headers.Get("x-ms-date") != null)
                request.Headers.Remove("x-ms-date");
            request.Headers.Add("x-ms-date", RequestDateString);
            //x-ms-date END

            //x-ms-version START
            if (request.Headers.Get("x-ms-version") != null)
                request.Headers.Remove("x-ms-version");
            request.Headers.Add("x-ms-version", "2015-12-11");
            //x-ms-version END

            //Accept START
            if (request.Accept != null)
                request.Accept = null;
            request.Accept = "application/json";
            //Accept END

            //Content-Type START
            if (request.ContentType != null)
                request.ContentType = null;
            request.ContentType = "application/json";
            //Content-Type END

            //Content-Length START
            request.ContentLength = bodyBytes.Length;
            //Content-Length END

            Stream stream = request.GetRequestStream();
            stream.Write(bodyBytes, 0, bodyBytes.Length);

            try
            {
                String s = request.GetResponse().ToString();
                return true;
            }catch(Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("AG: AzureStorageAccount.CreateTableAsync(358)");
                Console.WriteLine(e);
                Console.ResetColor();
                return false;
            }

            
        }
        public void CreateTableBySdkAsync(String tablename)
        {
            // xAPIZKctRErqHWD3sYNpZkI2sIbmyFh2Y0NVqzyeOWF6Dk9j48Iv5K/4yEziBNrQbtkArXvAQUTkehTqm+SxCw==
            // DefaultEndpointsProtocol=https;AccountName=storage946;AccountKey=xAPIZKctRErqHWD3sYNpZkI2sIbmyFh2Y0NVqzyeOWF6Dk9j48Iv5K/4yEziBNrQbtkArXvAQUTkehTqm+SxCw==;EndpointSuffix=core.windows.net

            String storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=" + this.name + ";AccountKey=" + this.keys[0].value + ";EndpointSuffix=core.windows.net";

            CloudStorageAccount cloudStorageAccount = CloudStorageAccount.Parse(storageConnectionString);
            CloudTableClient tableClient = cloudStorageAccount.CreateCloudTableClient();
            CloudTable cloudTable = tableClient.GetTableReference(tablename);
            cloudTable.CreateIfNotExistsAsync();
        }



        /// <summary>
        /// Removes Azure Table from the Storage Account.
        /// </summary>
        /// <param name="tablename">The <see cref="System.String"/> instance that represents Azure Table's name.</param>
        public void deleteTableRest(String tablename)
        {
            var requestUri = new Uri($"https://{this.name}.table.core.windows.net/Tables('{tablename}')");
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestUri);
            request.Method = "DELETE";

            //Authorization START
            var RequestDateString = DateTime.UtcNow.ToString("R", CultureInfo.InvariantCulture);

            if (request.Headers.Get("Authorization") != null)
                request.Headers.Remove("Authorization");

            var canonicalizedStringToBuild = string.Format("{0}\n{1}", RequestDateString, $"/{this.name}/{requestUri.AbsolutePath.TrimStart('/')}");
            string signature;
            using (var hmac = new HMACSHA256(Convert.FromBase64String(this.keys[0].value)))
            {
                byte[] dataToHmac = Encoding.UTF8.GetBytes(canonicalizedStringToBuild);
                signature = Convert.ToBase64String(hmac.ComputeHash(dataToHmac));
            }

            string authorizationHeader = string.Format($"{this.name}:" + signature);
            request.Headers.Add("Authorization", new AuthenticationHeaderValue("SharedKeyLite", authorizationHeader).ToString());
            //Authorization END

            //x-ms-date START
            if (request.Headers.Get("x-ms-date") != null)
                request.Headers.Remove("x-ms-date");
            request.Headers.Add("x-ms-date", RequestDateString);
            //x-ms-date END

            //x-ms-version START
            if (request.Headers.Get("x-ms-version") != null)
                request.Headers.Remove("x-ms-version");
            request.Headers.Add("x-ms-version", "2015-12-11");
            //x-ms-version END

            //Accept START
            if (request.Accept != null)
                request.Accept = null;
            request.Accept = "application/json";
            //Accept END

            //Content-Type START
            if (request.ContentType != null)
                request.ContentType = null;
            request.ContentType = "application/json";
            //Content-Type END

            String response = request.GetResponse().ToString();
        }
        public void DeleteTableBySdkAsync(String tablename)
        {
            String storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=" + this.name + ";AccountKey=" + this.keys[0].value + ";EndpointSuffix=core.windows.net";

            CloudStorageAccount cloudStorageAccount = CloudStorageAccount.Parse(storageConnectionString);
            CloudTableClient tableClient = cloudStorageAccount.CreateCloudTableClient();
            
            CloudTable cloudTable = tableClient.GetTableReference(tablename);
            cloudTable.DeleteIfExistsAsync();
        }


        public void RemoveAssociatedOutputs()
        {
            foreach (AzureStreamAnalyticsJob saJob in Globals.streamAnalyticsList)
            {
                List<AzureStreamAnalyticsOutput> outputs = saJob.listOutputs();
                List<AzureStreamAnalyticsOutput> outputsToDelete = new List<AzureStreamAnalyticsOutput>();
                foreach (AzureStreamAnalyticsOutput output in outputs)
                {
                    if (output.properties.datasource.properties.accountName.Equals(this.name))
                    {
                        outputsToDelete.Add(output);
                    }
                }
                foreach (AzureStreamAnalyticsOutput outputToDelete in outputsToDelete)
                {
                    saJob.removeOutput(outputToDelete.name);
                }
            }
        }
    }
}

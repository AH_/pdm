﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureConfig6
{
    public class AzureTable
    {
        public String name { get; set; }

        public AzureTable(String name)
        {
            this.name = name;
        }
    }
}

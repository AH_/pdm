﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AzureConfig6
{
    /// <summary>
    /// Class represents Azure Resource Group and provides methods for work with Azure Resource Groups.
    /// </summary>
    public class AzureResourceGroup
    {
        public String id { get; set; }
        public String name { get; set; }
        public String type { get; set; }
        public String location { get; set; }

        public ResourceGroupProperties properties { get; set; }

        public class ResourceGroupProperties
        {
            public String provisioningState { get; set; }
        }

        /// <summary>
        /// Retreives all Resource Group's child IoT hubs as a list of Azure IoT Hub objects.
        /// </summary>
        /// <returns>A <see cref="List{T}"/> of <see cref="AzureConfig6.AzureIoThub"/> that contains all Resource Group's child IoT hubs.</returns>
        public List<AzureIoThub> listHubs()
        {
            List<AzureIoThub> hubsList = new List<AzureIoThub>();
            String uri = string.Format("https://management.azure.com/subscriptions/{0}/resourceGroups/{1}/providers/Microsoft.Devices/IotHubs?api-version=2018-04-01",
                Globals.subscription.subscriptionId, this.name);
            String hubsResponseString = REST.doGET(uri, Globals.token);
            List<AzureIoThub> hubList = JObject.Parse(hubsResponseString)["value"].ToObject<List<AzureIoThub>>();

            return hubList;
        }

        /// <summary>
        /// Retreives all Resource Group's child Azure Storage Accounts as a list of Azure Storage Account objects.
        /// </summary>
        /// <returns>A <see cref="List{T}"/> of <see cref="AzureConfig6.AzureStorageAccount"/> that contains all Resource Group's child Storage Accounts.</returns>
        public async Task<List<AzureStorageAccount>> listStorageAccounts()
        {
            String uri = "https://management.azure.com/subscriptions/" + Globals.subscription.subscriptionId + "/providers/Microsoft.Storage/storageAccounts?api-version=2019-04-01";
            String result = REST.doGET(uri, Globals.token);

            List<AzureStorageAccount> storageList = JObject.Parse(result)["value"].ToObject<List<AzureStorageAccount>>();
            return storageList;
        }

        /// <summary>
        /// Retreives all Resource Group's child Stream Analytics Jobs as a list of Azure Stream Analytics objects.
        /// </summary>
        /// <returns>A <see cref="List{T}"/> of <see cref="AzureConfig6.AzureStreamAnalyticsJob"/> that contains all Resource Group's child Stream Analytics Jobs.</returns>
        public List<AzureStreamAnalyticsJob> listStreamAnalyticsJobs()
        {
            List<AzureStreamAnalyticsJob> streamJobList = new List<AzureStreamAnalyticsJob>();

            String uri = string.Format("https://management.azure.com/subscriptions/{0}/resourceGroups/{1}/providers/Microsoft.StreamAnalytics/streamingjobs?$expand=Inputs,Transformation,Outputs,Functions&api-version=2015-10-01",
                Globals.subscription.subscriptionId, this.name);
            String jobsResponseString = REST.doGET(uri, Globals.token);

            streamJobList = JObject.Parse(jobsResponseString)["value"].ToObject<List<AzureStreamAnalyticsJob>>();

            return streamJobList;
        }
    }
}
﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace AzureConfig6
{
    /// <summary>
    /// Class represents Azure IoT hub and provides methods for work with Azure IoT hubs.
    /// </summary>
    public class AzureIoThub
    {
        public String id { get; set; }
        public String name { get; set; }
        public String type { get; set; }
        public String location { get; set; }
        public String subscriptionId { get; set; }
        public String resourceGroup { get; set; }
        public String eTag { get; set; }
        public Sku sku { get; set; }
        public Properties properties { get; set; }
        public Dictionary<String, IoThubKey> keys { get; set; }

        public class Sku
        {
            public String name { get; set; }
            public String tier { get; set; }
            public String capacity { get; set; }
        }

        public class Properties
        {
            public String state { get; set; }
            public String provisioningState { get; set; }
            public String hostName { get; set; }
        }

        //  Create IoT Hub

        /// <summary>
        /// Creates Azure IoT hub async.
        /// </summary>
        /// <param name="hubName">The <see cref="System.String"/> instance that represents Azure IoT hub name.</param>
        public static async Task<Boolean> createIotHubAsync(String hubName)
        {
            var description = new
            {
                name = hubName,
                location = "North Europe",  //TODO: Add ability to chose region and sku
                sku = new
                {
                    name = "S1",
                    tier = "Standard",
                    capacity = 1
                }
            };
            String bodyJson = JsonConvert.SerializeObject(description, Formatting.Indented);

            String uri = string.Format("https://management.azure.com/subscriptions/{0}/resourceGroups/{1}/providers/Microsoft.Devices/IotHubs/{2}?api-version=2018-04-01",
                Globals.subscription.subscriptionId, Globals.resourceGroup.name, hubName);

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Globals.token);
            HttpResponseMessage response = await client.PutAsync(uri, new StringContent(bodyJson, Encoding.UTF8, "application/json"));

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                Console.WriteLine(response);
                return false;
            }else
            {
                uri = string.Format("https://management.azure.com/subscriptions/{0}/resourceGroups/{1}/providers/Microsoft.Devices/IotHubs/{2}/listkeys?api-version=2018-04-01",
                Globals.subscription.subscriptionId, Globals.resourceGroup.name, hubName);
                int safetyCounter = 0;
                do
                {
                    Thread.Sleep(1000);
                    response = await client.GetAsync(uri);
                    safetyCounter++;
                } while (response.StatusCode != System.Net.HttpStatusCode.OK && safetyCounter < 100);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return true;
                }
                else
                {
                    Console.WriteLine(response);
                    return false;
                }
            }
        }


        //  Keys

        /// <summary>
        /// Retreives IoT hub's keys as a dictionary async.
        /// </summary>
        /// <returns>A <see cref="Dictionary{TKey, TValue}"/>  with <see cref="String"/> keys and <see cref="AzureConfig6.IoThubKey"/> values that contains all IoT hub's keys.</returns>
        public async Task<Dictionary<String, IoThubKey>> getHubKeysAsync()
        {
            String uri = string.Format("https://management.azure.com/subscriptions/{0}/resourceGroups/{1}/providers/Microsoft.Devices/IotHubs/{2}/listkeys?api-version=2018-04-01",
                Globals.subscription.subscriptionId, Globals.resourceGroup.name, this.name);

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Globals.token);
            HttpResponseMessage response;

            int safetyCounter = 0;
            do
            {
                await Task.Delay(1000);
                response = await client.PostAsync(uri, new StringContent("", Encoding.UTF8, "application/json"));
                ++safetyCounter;
                Console.WriteLine(response);
            } while (response.StatusCode != System.Net.HttpStatusCode.OK && safetyCounter < 100);

            if(response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                String responseString = await response.Content.ReadAsStringAsync();
                List<IoThubKey> hubKeyList = JObject.Parse(responseString)["value"].ToObject<List<IoThubKey>>();

                Dictionary<String, IoThubKey> hubKeyDict = new Dictionary<string, IoThubKey>();
                foreach (IoThubKey key in hubKeyList)
                {
                    hubKeyDict.Add(key.keyName, key);
                }
                Console.WriteLine("Got keys from " + this.name);
                return hubKeyDict;
            }else
            {
                Console.WriteLine(response);
                return null;
            }

        }


        //  Remove IoT hub
        /// <summary>
        /// Removes Azure IoT hub async.
        /// </summary>
        public async Task<Boolean> removeIoThubAsync()
        {
            String hubName = this.name;
            String uri = string.Format("https://management.azure.com/subscriptions/{0}/resourceGroups/{1}/providers/Microsoft.Devices/IotHubs/{2}?api-version=2018-04-01",
                Globals.subscription.subscriptionId, Globals.resourceGroup.name, hubName);

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Globals.token);
            HttpResponseMessage response = await client.DeleteAsync(uri);

            if(response.StatusCode != System.Net.HttpStatusCode.Accepted)
            {
                Console.WriteLine(response);
                return false;
            }else
            {
                uri = string.Format("https://management.azure.com/subscriptions/{0}/resourceGroups/{1}/providers/Microsoft.Devices/IotHubs/{2}?api-version=2018-04-01",
                Globals.subscription.subscriptionId, Globals.resourceGroup.name, hubName);
                int safetyCounter = 0;
                do
                {
                    Thread.Sleep(1000);
                    response = await client.GetAsync(uri);
                    safetyCounter++;
                } while (response.StatusCode!= System.Net.HttpStatusCode.NotFound && safetyCounter<100);

                if(response.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    return true;
                }else
                {
                    Console.WriteLine(response);
                    return false;
                }
            }
        }


        //  Token

        /// <summary>
        /// Retreives Azure IoT hub SaS token which can be used to sign http requests to the IoT hub.
        /// </summary>
        /// <returns>A <see cref="String"/> that represents IoT hub's SaS token.</returns>
        private String createToken()
        {
            String resourceUri = this.name + ".azure-devices.net";
            String keyName = "iothubowner";
            String key = this.keys["iothubowner"].primaryKey;
            //For example we decided to create a token expiring in a week
            TimeSpan sinceEpoch = DateTime.UtcNow - new DateTime(1970, 1, 1);
            var week = 60 * 60 * 24 * 7;
            var expiry = Convert.ToString((int)sinceEpoch.TotalSeconds + week);
            //IoT Hub Security docs here: https://docs.microsoft.com/en-us/azure/iot-hub/iot-hub-devguide-security
            string stringToSign = HttpUtility.UrlEncode(resourceUri) + "\n" + expiry;
            HMACSHA256 hmac = new HMACSHA256(Convert.FromBase64String(key));
            var signature = Convert.ToBase64String(hmac.ComputeHash(Encoding.UTF8.GetBytes(stringToSign)));
            var sasToken = String.Format(CultureInfo.InvariantCulture,
                                        "SharedAccessSignature sr={0}&sig={1}&se={2}&skn={3}",
                                        HttpUtility.UrlEncode(resourceUri),
                                        HttpUtility.UrlEncode(signature),
                                        expiry,
                                        keyName);
            return sasToken;
        }


        //  Devices

        /// <summary>
        /// Creates Azure IoT Device in the IoT hub.
        /// </summary>
        /// <param name="deviceName">The <see cref="System.String"/> instance that represents Azure IoT Device's name.</param>
        public void createDevice(String deviceName)
        {
            String uri = string.Format("https://{0}.azure-devices.net/devices/{1}?api-version=2019-07-01-preview",
                this.name, deviceName);
            String sasToken = createToken();
            String body = "{\"deviceId\": \"" + deviceName + "\"}";
            String createResponseString = REST.doPUTwithSas(uri, body, sasToken);
        }
        /// <summary>
        /// Creates Azure IoT Device in the IoT hub async.
        /// </summary>
        /// <param name="deviceName">The <see cref="System.String"/> instance that represents Azure IoT Device's name.</param>
        public async Task createDeviceAsync(String deviceName)
        {
            String uri = string.Format("https://{0}.azure-devices.net/devices/{1}?api-version=2019-07-01-preview",
                this.name, deviceName);
            String sasToken = createToken();
            String body = "{\"deviceId\": \"" + deviceName + "\"}";

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", sasToken);

            HttpResponseMessage response = await client.PutAsync(uri, new StringContent(body, Encoding.UTF8, "application/json"));
        }
        /// <summary>
        /// Creates Azure IoT Device in the IoT hub async.
        /// </summary>
        /// <param name="deviceName">The <see cref="System.String"/> instance that represents Azure IoT Device's name.</param>
        /// <param name="primaryKey">The <see cref="System.String"/> instance that represents Azure IoT Device's primary key (when the key is not generated automatically).</param>
        /// <param name="secondaryKey">The <see cref="System.String"/> instance that represents Azure IoT Device's secondary key (when the key is not generated automatically).</param>
        public async Task createDeviceAsync(String deviceName, String primaryKey, String secondaryKey)
        {
            String uri = string.Format("https://{0}.azure-devices.net/devices/{1}?api-version=2019-07-01-preview",
                this.name, deviceName);
            String sasToken = createToken();
            String body = "{\"deviceId\": \"" + deviceName + "\", \"authentication\":{\"symmetricKey\":{\"primaryKey\":\"" + primaryKey + "\", \"secondaryKey\":\"" + secondaryKey + "\"}}}";

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", sasToken);

            HttpResponseMessage response = await client.PutAsync(uri, new StringContent(body, Encoding.UTF8, "application/json"));
        }


        /// <summary>
        /// Retreives specified IoT hub's child device as an Azure IoT Device object.
        /// </summary>
        /// <param name="deviceName">The <see cref="System.String"/> instance that represents Azure IoT Device's name.</param>
        /// <returns>A <see cref="AzureConfig6.AzureIotDevice"/> that represents specified IoT hub's child device.</returns>
        public AzureIotDevice getDevice(String deviceName)
        {
            String uri = string.Format("https://{0}.azure-devices.net/devices/{1}?api-version=2019-07-01-preview",
                this.name, deviceName);
            String sasToken = createToken();
            String devicesResponseString = REST.doGETwithSas(uri, sasToken);

            AzureIotDevice fetchedDevice = JObject.Parse(devicesResponseString).ToObject<AzureIotDevice>();
            return fetchedDevice;
        }
        /// <summary>
        /// Retreives all IoT hub's child devices as a list of Azure IoT Device objects.
        /// </summary>
        /// <returns>A <see cref="List{T}"/> of <see cref="AzureConfig6.AzureIotDevice"/> that contains all IoT hub's child devices.</returns>
        public List<AzureIotDevice> listDevices()
        {
            List<AzureIotDevice> deviceList = new List<AzureIotDevice>();
            String uri = string.Format("https://{0}.azure-devices.net/devices/query?api-version=2019-07-01-preview",
                this.name);
            String body = "{\"query\": \"SELECT deviceId FROM devices\"}";
            String sasToken = createToken();
            String devicesResponseString = REST.doPOSTwithSas(uri, body, sasToken);

            JArray tempJArray = JArray.Parse(devicesResponseString);
            foreach (JObject el in tempJArray)
            {
                deviceList.Add(JObject.Parse(el.ToString()).ToObject<AzureIotDevice>());
            }
            return deviceList;
        }


        /// <summary>
        /// Removes Azure IoT Device from the IoT hub.
        /// </summary>
        /// <param name="deviceName">The <see cref="System.String"/> instance that represents Azure IoT Device's name.</param>
        public void removeDevice(String deviceName)
        {
            String uri = string.Format("https://{0}.azure-devices.net/devices/{1}?api-version=2019-07-01-preview",
                this.name, deviceName);
            String sasToken = createToken();
            REST.doDELETEwithSas(uri, sasToken);
            Firebase.RemoveDeviceFromFirebase(deviceName);
        }



        //   Other
        public void RemoveAssociatedInputs()
        {
            foreach(AzureStreamAnalyticsJob saJob in Globals.streamAnalyticsList)
            {
                List<AzureStreamAnalyticsInput> inputs = saJob.listInputs();
                List<AzureStreamAnalyticsInput> inputsToDelete = new List<AzureStreamAnalyticsInput>();
                foreach(AzureStreamAnalyticsInput input in inputs)
                {
                    if(input.properties.datasource.properties.iotHubNamespace.Equals(this.name))
                    {
                        inputsToDelete.Add(input);
                    }
                }
                foreach(AzureStreamAnalyticsInput inputToDelete in inputsToDelete)
                {
                    saJob.removeInput(inputToDelete.name);
                }
            }
        }


        /// <summary>
        /// Creates Azure IoT hub.
        /// </summary>
        /// <param name="hubName">The <see cref="System.String"/> instance that represents Azure IoT hub name.</param>
        public AzureStorageAccount getHubsStorageFromStreamAnalytics()
        {
            AzureStorageAccount storage;
                //Here I get Storage Account to which Device is sending messages (Device -> IoThub -> Stream Analytics -> Storage Account)
                foreach (AzureStreamAnalyticsJob el in Globals.streamAnalyticsList) //Go through all SA jobs in account
                {
                    //For each SA job:
                    List<AzureStreamAnalyticsInput> elInputs = el.properties.inputs; //Get list of inputs
                    foreach (AzureStreamAnalyticsInput input in elInputs)  //Go through all inputs in SA job
                    {
                        if (input.properties.datasource.properties.iotHubNamespace.Equals(this.name)) //if input Datasourse equals chosen IoT hub:
                        {
                            List<AzureTransformationPair> transformationPairs = el.ParseQueryToTransformPairs(); //get all transformation pairs of this input's parent SA job
                            foreach (AzureTransformationPair pair in transformationPairs)  //Go through all transformation pairs
                            {
                                if (pair.input.properties.datasource.properties.iotHubNamespace.Equals(input.properties.datasource.properties.iotHubNamespace))  //Find Transformation Pair with requied input
                                {
                                    String storageAccountName = pair.output.properties.datasource.properties.accountName;  //Get Storage Account name from this TransformationPair
                                    storage = Globals.storageAccountList.Find(x => x.name == storageAccountName); //Using this Storage Account name get Storage Account entity from storage accounts list
                                    return storage;
                                }
                            }
                        }
                    }
                }
            return null;
        }
    }
}
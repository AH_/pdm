﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureConfig6
{
    public class AzureStreamAnalyticsOutput
    {
        public String id { get; set; }
        public String name { get; set; }
        public String type { get; set; }
        public String datasource { get; set; }
        public Properties properties { get; set; }


        public class Properties
        {
            public String etag { get; set; }
            public Datasource datasource { get; set; }

            public class Datasource
            {
                public String type { get; set; }
                public Properties properties { get; set; }
                public class Properties
                {
                    public String accountName { get; set; }
                    public String table { get; set; }
                    public String partitionKey { get; set; }
                    public String rowKey { get; set; }
                    public List<String> columnsToRemove { get; set; }
                    public String batchSize { get; set; }
                }
            }

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureConfig6
{
    public class AzureStreamAnalyticsInput
    {
        public String id { get; set; }
        public String name { get; set; }
        public String type { get; set; }
        public String datasource { get; set; }

        public Properties properties { get; set; }

        public class Properties
        {
            public String type { get; set; }
            public String partitionKey { get; set; }
            public String etag { get; set; }
            public Datasource datasource { get; set; }
            public Compression compression { get; set; }
            public Serialization serialization { get; set; }

            public class Datasource
            {
                public String type { get; set; }
                public Properties properties { get; set; }

                public class Properties
                {
                    public String iotHubNamespace { get; set; }
                    public String sharedAccessPolicyName { get; set; }
                    public String endpoint { get; set; }
                    public String consumerGroupName { get; set; }
                }
            }
            public class Compression
            {
                public String type { get; set; }
            }
            public class Serialization
            {
                public String type { get; set; }
                public Properties properties { get; set; }

                public class Properties
                {
                    public String encoding { get; set; }
                }
            }
        }
    }
}

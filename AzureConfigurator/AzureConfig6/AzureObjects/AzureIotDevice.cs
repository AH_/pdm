﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureConfig6
{
    /// <summary>
    /// Class represents Azure IoT device and provides methods for work with AzureIoT devices.
    /// </summary>
    public class AzureIotDevice
    {
        public String deviceId { get; set; }
        public String generationId { get; set; }
        public String etag { get; set; }
        public String connectionState { get; set; }
        public String status { get; set; }
        public String statusReason { get; set; }
        public String connectionStateUpdatedTime { get; set; }
        public String statusUpdatedTime { get; set; }
        public String lastActivityTime { get; set; }
        public String cloudToDeviceMessageCount { get; set; }

        public AzureIotDevice(String deviceId)
        {
            this.deviceId = deviceId;
        }

        /// <summary>
        /// Retreives Azure Storage Account to which device's IoT hub is connected by Stream Analytics.
        /// </summary>
        /// <returns>A <see cref="AzureConfig6.AzureStorageAccount"/> to which device's parent IoT hub is connected; if parent IoT hub has no associated Azure Storage Account, returns <c>null</c>.</returns>
        public AzureStorageAccount getStorageFromStreamAnalytics()
        {
            AzureStorageAccount storage;
            AzureIoThub hub = this.getParentHub();

            if(hub != null)
            {
                //Here I get Storage Account to which Device is sending messages (Device -> IoThub -> Stream Analytics -> Storage Account)
                foreach (AzureStreamAnalyticsJob el in Globals.streamAnalyticsList) //Go through all SA jobs in account
                {
                    //For each SA job:
                    List<AzureStreamAnalyticsInput> elInputs = el.properties.inputs; //Get list of inputs
                    foreach (AzureStreamAnalyticsInput input in elInputs)  //Go through all inputs in SA job
                    {
                        if (input.properties.datasource.properties.iotHubNamespace.Equals(hub.name)) //if input Datasourse equals chosen IoT hub:
                        {
                            List<AzureTransformationPair> transformationPairs = el.ParseQueryToTransformPairs(); //get all transformation pairs of this input's parent SA job
                            foreach (AzureTransformationPair pair in transformationPairs)  //Go through all transformation pairs
                            {
                                if (pair.input.properties.datasource.properties.iotHubNamespace.Equals(input.properties.datasource.properties.iotHubNamespace))  //Find Transformation Pair with requied input
                                {
                                    String storageAccountName = pair.output.properties.datasource.properties.accountName;  //Get Storage Account name from this TransformationPair
                                    storage = Globals.storageAccountList.Find(x => x.name == storageAccountName); //Using this Storage Account name get Storage Account entity from storage accounts list
                                    return storage;
                                }
                            }
                        }
                    }
                }
            }
            return null;
        }
        /// <summary>
        /// Retreives device's parent IoT hub.
        /// </summary>
        /// <returns>A <see cref="AzureConfig6.AzureIoThub"/> that represents device's parent IoT hub.</returns>
        public AzureIoThub getParentHub()
        {
            AzureIoThub hub = null;
            foreach (AzureIoThub h in Globals.hubList)
            {
                List<AzureIotDevice> hubDeviceList = h.listDevices();
                foreach (AzureIotDevice d in hubDeviceList)
                {
                    if (d.deviceId.Equals(this.deviceId))
                    {
                        hub = h;
                        return hub;
                    }
                }
            }
            return hub;
        }

    }
}

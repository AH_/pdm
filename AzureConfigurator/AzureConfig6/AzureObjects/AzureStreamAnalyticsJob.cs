﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AzureConfig6
{
    public class AzureStreamAnalyticsJob
    {
        public String name { get; set; }
        public Properties properties { get; set; }

        public class Properties
        {
            public Sku sku { get; set; }
            public List<AzureStreamAnalyticsInput> inputs { get; set; }
            public List<AzureStreamAnalyticsOutput> outputs { get; set; }
            public AzureStreamAnalyticsTransformation transformation { get; set; }
            public String jobId { get; set; }
            public String provisioningState { get; set; }
            public String jobState { get; set; }
            public String eventsOutOfOrderPolicy { get; set; }
            public String outputErrorPolicy { get; set; }
            public String eventsOutOfOrderMaxDelayInSeconds { get; set; }
            public String eventsLateArrivalMaxDelayInSeconds { get; set; }
            public String dataLocale { get; set; }
            public String createdDate { get; set; }
            public String compatibilityLevel { get; set; }
            public String package { get; set; }
            public String jobStorageAccount { get; set; }
        }

        public class Sku
        {
            public String name { get; set; }
        }

        /// <summary>
        /// Retreives specified Azure Stream Analytics Job.
        /// </summary>
        /// <param name="jobName">The <see cref="System.String"/> instance that represents Azure Stream Analytics Job name.</param>
        /// <returns>A <see cref="AzureConfig6.AzureStreamAnalyticsJob"/> that represents Azure Stream Analytics Job instanse.</returns>
        public static async Task<AzureStreamAnalyticsJob> getStreamAnalyticsJobAsync(String jobName)
        {
            String uri = string.Format("https://management.azure.com/subscriptions/{0}/resourcegroups/{1}/providers/Microsoft.StreamAnalytics/streamingjobs/{2}?$expand=Inputs,Transformation,Outputs,Functions&api-version=2015-10-01",
                Globals.subscription.subscriptionId, Globals.resourceGroup.name, jobName);

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Globals.token);
            HttpResponseMessage response = await client.GetAsync(uri);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                Console.WriteLine(response);
                return null;
            }
            else
            {
                String responceContent = await response.Content.ReadAsStringAsync();
                AzureStreamAnalyticsJob streamJob = JObject.Parse(responceContent).ToObject<AzureStreamAnalyticsJob>();
                return streamJob;
            } 
        }

        /// <summary>
        /// Creates Azure Stream Analytics Job.
        /// </summary>
        /// <param name="jobName">The <see cref="System.String"/> instance that represents Azure Stream Analytics Job name.</param>
        public static async Task<Boolean> createStreamAnalyticsJobAsync(String jobName)
        {
            String uri = string.Format("https://management.azure.com/subscriptions/{0}/resourcegroups/{1}/providers/Microsoft.StreamAnalytics/streamingjobs/{2}?api-version=2015-10-01",
                Globals.subscription.subscriptionId, Globals.resourceGroup.name, jobName);
            String body = "{\"location\":\"Central US\", \"properties\":{\"sku\":{\"name\":\"standard\"}}}";

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Globals.token);
            HttpResponseMessage response = await client.PutAsync(uri, new StringContent(body, Encoding.UTF8, "application/json"));

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                Console.WriteLine(response);
                return false;
            }
            else
            {
                uri = string.Format("https://management.azure.com/subscriptions/{0}/resourcegroups/{1}/providers/Microsoft.StreamAnalytics/streamingjobs/{2}?$expand=Inputs,Transformation,Outputs,Functions&api-version=2015-10-01",
                    Globals.subscription.subscriptionId, Globals.resourceGroup.name, jobName);
                int safetyCounter = 0;
                do
                {
                    Thread.Sleep(1000);
                    response = await client.GetAsync(uri);
                    safetyCounter++;
                } while (response.StatusCode != System.Net.HttpStatusCode.OK && safetyCounter < 100);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return true;
                }
                else
                {
                    Console.WriteLine(response);
                    return false;
                }
            }
        }

        /// <summary>
        /// Removes Stream Analytics Job.
        /// </summary>
        public async Task<Boolean> removeStreamAnalyticsJobAsync()
        {
            String jobName = this.name;
            String uri = string.Format("https://management.azure.com/subscriptions/{0}/resourcegroups/{1}/providers/Microsoft.StreamAnalytics/streamingjobs/{2}?api-version=2015-10-01",
                Globals.subscription.subscriptionId, Globals.resourceGroup.name, jobName);

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Globals.token);
            HttpResponseMessage response = await client.DeleteAsync(uri);

            if (response.StatusCode != System.Net.HttpStatusCode.Accepted)
            {
                Console.WriteLine(response);
                return false;
            }
            else
            {
                uri = string.Format("https://management.azure.com/subscriptions/{0}/resourcegroups/{1}/providers/Microsoft.StreamAnalytics/streamingjobs/{2}?$expand=Inputs,Transformation,Outputs,Functions&api-version=2015-10-01",
                    Globals.subscription.subscriptionId, Globals.resourceGroup.name, jobName);
                int safetyCounter = 0;
                do
                {
                    Thread.Sleep(1000);
                    response = await client.GetAsync(uri);
                    safetyCounter++;
                } while (response.StatusCode != System.Net.HttpStatusCode.NotFound && safetyCounter < 100);

                if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    return true;
                }
                else
                {
                    Console.WriteLine(response);
                    return false;
                }
            }
        }

        /// <summary>
        /// Retreives all Stream Analytics Job's inputs as a list of Azure Stream Analytics Input objects.
        /// </summary>
        /// <returns>A <see cref="List{T}"/> of <see cref="AzureConfig6.AzureStreamAnalyticsInput"/> that contains all Stream Analytics Job's inputs.</returns>
        public List<AzureStreamAnalyticsInput> listInputs()
        {
            List<AzureStreamAnalyticsInput> inputList = new List<AzureStreamAnalyticsInput>();

            String uri = string.Format("https://management.azure.com/subscriptions/{0}/resourceGroups/{1}/providers/Microsoft.StreamAnalytics/streamingjobs/{2}/inputs?api-version=2019-06-01",
                Globals.subscription.subscriptionId, Globals.resourceGroup.name, this.name);

            String responseString = null;

            try
            {
                responseString = REST.doGET(uri, Globals.token);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            if (!String.IsNullOrEmpty(responseString))
            {
                inputList = JObject.Parse(responseString)["value"].ToObject<List<AzureStreamAnalyticsInput>>();
                foreach(AzureStreamAnalyticsInput input in inputList)
                {
                    input.datasource = input.properties.datasource.properties.iotHubNamespace;
                }
            }

            return inputList;
        }

        /// <summary>
        /// Retreives all Stream Analytics Job's outputs as a list of Azure Stream Analytics Input objects.
        /// </summary>
        /// <returns>A <see cref="List{T}"/> of <see cref="AzureConfig6.AzureStreamAnalyticsOutput"/> that contains all Stream Analytics Job's outputs.</returns>
        public List<AzureStreamAnalyticsOutput> listOutputs()
        {
            List<AzureStreamAnalyticsOutput> outputList = new List<AzureStreamAnalyticsOutput>();

            String uri = string.Format("https://management.azure.com/subscriptions/{0}/resourceGroups/{1}/providers/Microsoft.StreamAnalytics/streamingjobs/{2}/outputs?api-version=2019-06-01",
                Globals.subscription.subscriptionId, Globals.resourceGroup.name, this.name);

            String responseString = null;

            try
            {
                responseString = REST.doGET(uri, Globals.token);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            if (!String.IsNullOrEmpty(responseString))
            {
                outputList = JObject.Parse(responseString)["value"].ToObject<List<AzureStreamAnalyticsOutput>>();
                foreach (AzureStreamAnalyticsOutput output in outputList)
                {
                    output.datasource = output.properties.datasource.properties.accountName;
                }
            }
            return outputList;
        }

        /// <summary>
        /// Creates an Input in the Stream Analytics Job.
        /// </summary>
        /// <param name="hub">The <see cref="AzureConfig6.AzureIoThub"/> instance that represents IoT hub whith which Input will be associated.</param>
        /// <param name="inputName">The <see cref="System.String"/> instance that represents Input's name.</param>
        public void addInput(AzureIoThub hub, String inputName)
        {
            String uri = string.Format("https://management.azure.com/subscriptions/{0}/resourceGroups/{1}/providers/Microsoft.StreamAnalytics/streamingjobs/{2}/inputs/{3}?api-version=2019-06-01",
                Globals.subscription.subscriptionId, Globals.resourceGroup.name, this.name, inputName);

            String body = string.Format("{{\"properties\":{{\"type\":\"stream\",\"serialization\":{{\"type\":\"json\",\"properties\":{{\"encoding\":\"UTF8\"}}}},\"datasource\":{{\"type\":\"Microsoft.Devices/IotHubs\",\"properties\":{{\"iotHubNamespace\":\"{0}\",\"sharedAccessPolicyName\":\"iothubowner\",\"sharedAccessPolicyKey\":\"{1}\",\"eventHubName\":\"{0}eventhub\",\"endpoint\":\"messages/events\"}}}},\"compression\":{{\"type\":\"None\"}}}}}}",
                hub.name, hub.keys["iothubowner"].primaryKey);

            String responseString = REST.doPUT(uri, body, Globals.token);
        }

        /// <summary>
        /// Removes Stream Analytics Job's Input.
        /// </summary>
        /// <param name="inputName">The <see cref="System.String"/> instance that represents Input's name.</param>
        public void removeInput(String inputName)
        {
            String uri = string.Format("https://management.azure.com/subscriptions/{0}/resourceGroups/{1}/providers/Microsoft.StreamAnalytics/streamingjobs/{2}/inputs/{3}?api-version=2019-06-01",
                Globals.subscription.subscriptionId, Globals.resourceGroup.name, this.name, inputName);

            REST.doDELETE(uri, Globals.token);
        }

        /// <summary>
        /// Creates an Output in the Stream Analytics Job.
        /// </summary>
        /// <param name="storage">The <see cref="AzureConfig6.AzureStorageAccount"/> instance that represents Storage Account whith which Output will be associated.</param>
        /// <param name="tableName">The <see cref="AzureConfig6.AzureTable"/> instance that represents Azure Table whith which Output will be associated.</param>
        /// <param name="outputName">The <see cref="System.String"/> instance that represents Outout's name.</param>
        public void addOutput(AzureStorageAccount storage, String tableName, String outputName)
        {
            String uri = string.Format("https://management.azure.com/subscriptions/{0}/resourceGroups/{1}/providers/Microsoft.StreamAnalytics/streamingjobs/{2}/outputs/{3}?api-version=2019-06-01",
                Globals.subscription.subscriptionId, Globals.resourceGroup.name, this.name, outputName);

            String body = string.Format("{{\"properties\":{{\"datasource\":{{\"type\":\"Microsoft.Storage/Table\",\"properties\":{{\"accountName\":\"{0}\",\"accountKey\":\"{1}\",\"table\":\"{2}\",\"partitionKey\":\"sensorid\",\"rowKey\":\"time\",\"columnsToRemove\":[\"*\"],\"batchSize\":100}}}}}}}}",
                storage.name, storage.keys[0].value, tableName);

            String responseString = REST.doPUT(uri, body, Globals.token);
        }

        /// <summary>
        /// Removes Stream Analytics Job's Output.
        /// </summary>
        /// <param name="outputName">The <see cref="System.String"/> instance that represents Output's name.</param>
        public void removeOutput(String outputName)
        {
            String uri = string.Format("https://management.azure.com/subscriptions/{0}/resourceGroups/{1}/providers/Microsoft.StreamAnalytics/streamingjobs/{2}/outputs/{3}?api-version=2019-06-01",
                Globals.subscription.subscriptionId, Globals.resourceGroup.name, this.name, outputName);

            REST.doDELETE(uri, Globals.token);
        }

        /// <summary>
        /// Retreives Azure Stream Analytics Job's transformation.
        /// </summary>
        /// <returns>A <see cref="AzureConfig6.AzureStreamAnalyticsTransformation"/> that represents Azure Stream Analytics Job's transformation.</returns>
        public AzureStreamAnalyticsTransformation getTransform()
        {
            AzureStreamAnalyticsTransformation tranform = null;

            String uri = string.Format("https://management.azure.com/subscriptions/{0}/resourceGroups/{1}/providers/Microsoft.StreamAnalytics/streamingjobs/{2}/transformations/{3}?api-version=2019-06-01",
                Globals.subscription.subscriptionId, Globals.resourceGroup.name, this.name, "transformation");

            String responseString = null;
                
            try
            {
               responseString = REST.doGET(uri, Globals.token);
            }catch(Exception e)
            {
                Console.WriteLine(e);
            }
                
            if(!String.IsNullOrEmpty(responseString))
            {
                tranform = JObject.Parse(responseString).ToObject<AzureStreamAnalyticsTransformation>();
            }
            return tranform;
        }

        /// <summary>
        /// Creates a Transformation for the Stream Analytics Job.
        /// </summary>
        /// <param name="query">The <see cref="System.String"/> instance that Transformation's query.</param>
        public void addTransform(String query)
        {
            String uri = string.Format("https://management.azure.com/subscriptions/{0}/resourceGroups/{1}/providers/Microsoft.StreamAnalytics/streamingjobs/{2}/transformations/{3}?api-version=2019-06-01",
                Globals.subscription.subscriptionId, Globals.resourceGroup.name, this.name, "transformation");

            if (String.IsNullOrEmpty(query))
            {
                query = " ";
            }

            String body = string.Format("{{\"properties\":{{\"streamingUnits\":1,\"query\":\"{0}\"}}}}",
                query);

            String responseString = REST.doPUT(uri, body, Globals.token);            
        }

        /// <summary>
        /// Removes Stream Analytics Job's Transformation.
        /// </summary>
        /// <param name="transformName">The <see cref="System.String"/> instance that represents Transformation's name.</param>
        public void removeTransform(String transformName)
        {
            String uri = string.Format("https://management.azure.com/subscriptions/{0}/resourceGroups/{1}/providers/Microsoft.StreamAnalytics/streamingjobs/{2}/outputs/{3}?api-version=2019-06-01",
                Globals.subscription.subscriptionId, Globals.resourceGroup.name, this.name, transformName);
            REST.doDELETE(uri, Globals.token);
        }

        /// <summary>
        /// Retreives Azure Stream Analytics Job's transformation's query as a set of Transformation Pair objects.
        /// </summary>
        /// <returns>A <see cref="List{T}"/> of <see cref="AzureConfig6.AzureTransformationPair"/> that represents Azure Stream Analytics Job's transformation's query.</returns>
        public List<AzureTransformationPair> ParseQueryToTransformPairs()
        {
            List<AzureTransformationPair> transformationPairs = new List<AzureTransformationPair>();
            Dictionary<String, String> toFromPairs = new Dictionary<string, string>();
            Dictionary<String, List<String>> inputgroups = new Dictionary<string, List<string>>();
            AzureStreamAnalyticsTransformation transformation = this.getTransform();
            List<AzureStreamAnalyticsInput> inputs = this.listInputs();
            List<AzureStreamAnalyticsOutput> outputs = this.listOutputs();

            if (transformation == null)
            {
                return transformationPairs;
            }
            String query = transformation.properties.query;
            query = query.Replace("\r\n", string.Empty);

            if (!String.IsNullOrWhiteSpace(query))
            {
                //  Outputs
                String outputsQuery = query.Substring(query.IndexOf("INTO"));
                List<String> outputSubQuerys = outputsQuery.Split(new string[] { "SELECT" }, StringSplitOptions.None).ToList<String>();
                foreach(String sub in outputSubQuerys)
                {
                    String from = sub.Substring(sub.IndexOf("FROM") + 4);
                    from = from.Replace(" ", "");
                    String into = sub.Substring(sub.IndexOf("[") + 1);
                    into = into.Substring(0, into.IndexOf("]"));
                    into = into.Replace(" ", "");
                    toFromPairs.Add(into, from);
                }

                //  Inputs
                String inputsQuery = query.Substring(query.IndexOf("WITH") + 5);
                inputsQuery = inputsQuery.Substring(0, inputsQuery.LastIndexOf(")") + 1);
                List<String> inputsSubQuerys = inputsQuery.Split(new string[] { ")," }, StringSplitOptions.None).ToList<String>();
                foreach (String sub in inputsSubQuerys)
                {
                    String groupName = sub.Substring(0, sub.IndexOf("AS") - 1);
                    groupName = groupName.Replace(" ", "");
                    List<String> inputsRaw = sub.Split(new string[] { "UNION" }, StringSplitOptions.None).ToList<String>();
                    List<String> inputsClean = new List<string>();
                    foreach (String inpt in inputsRaw)
                    {
                        String input = inpt.Substring(inpt.IndexOf("[")+1);
                        input = input.Substring(0, input.IndexOf("]"));
                        input = input.Replace(" ", "");
                        inputsClean.Add(input);
                    }
                    inputgroups.Add(groupName, inputsClean);
                }

                //  Pairs
                foreach(KeyValuePair<String, String> pair in toFromPairs)
                {
                    AzureStreamAnalyticsOutput output = null;
                    foreach(AzureStreamAnalyticsOutput outp in outputs)
                    {
                        if(outp.name.Equals(pair.Key))
                        {
                            output = outp;
                            break;
                        }
                    }

                    List<String> inputNames = inputgroups[pair.Value];
                    foreach(String inputName in inputNames)
                    {
                        foreach(AzureStreamAnalyticsInput input in inputs)
                        {
                            if(inputName.Equals(input.name))
                            {
                                transformationPairs.Add(new AzureTransformationPair(input, output));
                                break;
                            }
                        }
                    }
                }
            }
            return transformationPairs;
        }

        /// <summary>
        /// Parses a set of Trandformation Pairs to a String query.
        /// </summary>
        /// <param name="transformations">The <see cref="List{T}"/> of <see cref="AzureConfig6.AzureTransformationPair"/> that represents Azure Stream Analytics Job's transformation's query.</param>
        /// <returns>A <see cref="String"/> that represents Azure Stream Analytics Job's transformation's query.</returns>
        public static String ParseTransformPairsToQuery(List<AzureTransformationPair> transformations)
        {
            if(transformations != null && transformations.Count>0)
            {
                Dictionary<String, List<String>> outputsWithInputs = new Dictionary<string, List<string>>();

                foreach (AzureTransformationPair pair in transformations)
                {
                    if (!outputsWithInputs.ContainsKey(pair.output.name))
                    {
                        List<String> inputNames = new List<string>();
                        foreach (AzureTransformationPair p in transformations)
                        {
                            if (p.output.name.Equals(pair.output.name))
                            {
                                inputNames.Add(p.input.name);
                            }
                        }
                        outputsWithInputs.Add(pair.output.name, inputNames);
                    }
                }

                String query = "WITH ";

                foreach (KeyValuePair<String, List<String>> outputWithInputs in outputsWithInputs)
                {
                    String outputName = outputWithInputs.Key;
                    List<String> inputs = outputWithInputs.Value;

                    query += "TO" + outputName + " AS (";

                    foreach(String inputName in inputs)
                    {
                        query += "SELECT " +
                        "event.message.body.BoardSerial, " +
                        "arrayElement.ArrayValue.SensorId, " +
                        "arrayElement.ArrayValue.Value, " +
                        "event.message.systemproperties.Time " +
                        "FROM [" + inputName + "] " +
                        "AS EVENT CROSS APPLY GetArrayElements(event.message.body.sensors) AS arrayElement UNION ";
                    }

                    query = query.Substring(0, query.Length - 7);
                    query += "), ";
                }
                query = query.Substring(0, query.Length - 2);
                query += " ";

                foreach (KeyValuePair<String, List<String>> outputWithInputs in outputsWithInputs)
                {
                    query += "SELECT * INTO [" + outputWithInputs.Key + "] FROM TO"+ outputWithInputs.Key + " ";
                }
                    
                return query;
            }else
            {
                return " ";
            }
        }

        /// <summary>
        /// Stops Azure Stream Analytics Job async.
        /// </summary>
        public async Task WaitForJobStopAsync()
        {
            AzureStreamAnalyticsJob streamJob;
            int safetyCounter = 0;
            do
            {
                await Task.Delay(3000);
                streamJob = await getStreamAnalyticsJobAsync(this.name);
                Console.WriteLine("Job state: " + streamJob.properties.jobState);
                ++safetyCounter;
            } while (!(streamJob.properties.jobState.Equals("Stopped") || streamJob.properties.jobState.Equals("Failed")) && safetyCounter < 100);
        }

        /// <summary>
        /// Starts Azure Stream Analytics Job async.
        /// </summary>
        public async Task WaitForJobStartAsync()
        {
            AzureStreamAnalyticsJob streamJob;
            int safetyCounter = 0;
            do
            {
                await Task.Delay(3000);
                streamJob = await getStreamAnalyticsJobAsync(this.name);
                Console.WriteLine("Job state: " + streamJob.properties.jobState);
                ++safetyCounter;
            } while (!(streamJob.properties.jobState.Equals("Running") || streamJob.properties.jobState.Equals("Failed")) && safetyCounter < 100);

            if (streamJob.properties.jobState.Equals("Failed"))
            {
                MessageBox.Show("Stream Analytics Job start failed.");
            }
            return;
        }

        /// <summary>
        /// Checks if the Azure Stream Analytics Job's is ready for start.
        /// </summary>
        /// <returns>A <see cref="bool"/> that that indicates if the Azure Stream Analytics Job's is ready for start.</returns>
        public bool IsReadyToStart()
        {
            List<AzureStreamAnalyticsInput> inputs = new List<AzureStreamAnalyticsInput>();
            List<AzureStreamAnalyticsOutput> outputs = new List<AzureStreamAnalyticsOutput>();
            String query = "";
            try
            {
                inputs = this.properties.inputs;
            }catch(Exception e)
            {
                Console.WriteLine(e);
            }

            try
            {
                outputs = this.properties.outputs;
            }catch(Exception e)
            {
                Console.WriteLine(e);
            }

            try
            {
                query = this.properties.transformation.properties.query;
            }catch(Exception e)
            {
                Console.WriteLine(e);
            }

            if(inputs.Count > 0 &&
                outputs.Count > 0 &&
                !String.IsNullOrWhiteSpace(query))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

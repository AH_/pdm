﻿namespace AzureConfig6
{
    partial class Form_NewStreamAnalyticsTransform
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.button_Cancel = new System.Windows.Forms.Button();
            this.button_ok = new System.Windows.Forms.Button();
            this.dataGridView_inputs = new System.Windows.Forms.DataGridView();
            this.label_input = new System.Windows.Forms.Label();
            this.dataGridView_outputs = new System.Windows.Forms.DataGridView();
            this.label_output = new System.Windows.Forms.Label();
            this.label_transformation_info = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_inputs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_outputs)).BeginInit();
            this.SuspendLayout();
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(190, 376);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.button_Cancel.TabIndex = 7;
            this.button_Cancel.Text = "Cancel";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // button_ok
            // 
            this.button_ok.Location = new System.Drawing.Point(271, 376);
            this.button_ok.Name = "button_ok";
            this.button_ok.Size = new System.Drawing.Size(75, 23);
            this.button_ok.TabIndex = 6;
            this.button_ok.Text = "OK";
            this.button_ok.UseVisualStyleBackColor = true;
            this.button_ok.Click += new System.EventHandler(this.button_ok_Click);
            // 
            // dataGridView_inputs
            // 
            this.dataGridView_inputs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_inputs.Location = new System.Drawing.Point(12, 25);
            this.dataGridView_inputs.Name = "dataGridView_inputs";
            this.dataGridView_inputs.RowHeadersWidth = 51;
            this.dataGridView_inputs.Size = new System.Drawing.Size(334, 137);
            this.dataGridView_inputs.TabIndex = 11;
            this.dataGridView_inputs.SelectionChanged += new System.EventHandler(this.dataGridView_inputs_SelectionChanged);
            // 
            // label_input
            // 
            this.label_input.AutoSize = true;
            this.label_input.Location = new System.Drawing.Point(12, 9);
            this.label_input.Name = "label_input";
            this.label_input.Size = new System.Drawing.Size(181, 13);
            this.label_input.TabIndex = 10;
            this.label_input.Text = "Stream analytics job input (IoT hubs):";
            // 
            // dataGridView_outputs
            // 
            this.dataGridView_outputs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_outputs.Location = new System.Drawing.Point(12, 193);
            this.dataGridView_outputs.Name = "dataGridView_outputs";
            this.dataGridView_outputs.RowHeadersWidth = 51;
            this.dataGridView_outputs.Size = new System.Drawing.Size(334, 144);
            this.dataGridView_outputs.TabIndex = 13;
            this.dataGridView_outputs.SelectionChanged += new System.EventHandler(this.dataGridView_outputs_SelectionChanged);
            // 
            // label_output
            // 
            this.label_output.AutoSize = true;
            this.label_output.Location = new System.Drawing.Point(12, 177);
            this.label_output.Name = "label_output";
            this.label_output.Size = new System.Drawing.Size(214, 13);
            this.label_output.TabIndex = 12;
            this.label_output.Text = "Stream analytics job output (Storage tables):";
            // 
            // label_transformation_info
            // 
            this.label_transformation_info.AutoSize = true;
            this.label_transformation_info.Location = new System.Drawing.Point(12, 344);
            this.label_transformation_info.Name = "label_transformation_info";
            this.label_transformation_info.Size = new System.Drawing.Size(35, 13);
            this.label_transformation_info.TabIndex = 14;
            this.label_transformation_info.Text = "label1";
            // 
            // Form_NewStreamAnalyticsTransform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 411);
            this.Controls.Add(this.label_transformation_info);
            this.Controls.Add(this.dataGridView_outputs);
            this.Controls.Add(this.label_output);
            this.Controls.Add(this.dataGridView_inputs);
            this.Controls.Add(this.label_input);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.button_ok);
            this.Name = "Form_NewStreamAnalyticsTransform";
            this.Text = "New Stream Analytics Job Transformation";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_inputs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_outputs)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.Button button_ok;
        private System.Windows.Forms.DataGridView dataGridView_inputs;
        private System.Windows.Forms.Label label_input;
        private System.Windows.Forms.DataGridView dataGridView_outputs;
        private System.Windows.Forms.Label label_output;
        private System.Windows.Forms.Label label_transformation_info;
    }
}
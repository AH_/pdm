﻿namespace AzureConfig6
{
    partial class Form_NewStreamAnalyticsJob
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.textBox_newStreamAnalyticsJobName = new System.Windows.Forms.TextBox();
            this.label_newJob = new System.Windows.Forms.Label();
            this.button_ok = new System.Windows.Forms.Button();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.label_nameCheck = new System.Windows.Forms.Label();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox_newStreamAnalyticsJobName
            // 
            this.textBox_newStreamAnalyticsJobName.Location = new System.Drawing.Point(12, 25);
            this.textBox_newStreamAnalyticsJobName.Name = "textBox_newStreamAnalyticsJobName";
            this.textBox_newStreamAnalyticsJobName.Size = new System.Drawing.Size(195, 20);
            this.textBox_newStreamAnalyticsJobName.TabIndex = 0;
            this.textBox_newStreamAnalyticsJobName.TextChanged += new System.EventHandler(this.textBox_newStorageAccountName_TextChanged);
            // 
            // label_newJob
            // 
            this.label_newJob.AutoSize = true;
            this.label_newJob.Location = new System.Drawing.Point(12, 9);
            this.label_newJob.Name = "label_newJob";
            this.label_newJob.Size = new System.Drawing.Size(78, 13);
            this.label_newJob.TabIndex = 1;
            this.label_newJob.Text = "New job name:";
            // 
            // button_ok
            // 
            this.button_ok.Location = new System.Drawing.Point(162, 54);
            this.button_ok.Name = "button_ok";
            this.button_ok.Size = new System.Drawing.Size(75, 23);
            this.button_ok.TabIndex = 2;
            this.button_ok.Text = "OK";
            this.button_ok.UseVisualStyleBackColor = true;
            this.button_ok.Click += new System.EventHandler(this.button_ok_Click);
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(81, 54);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.button_Cancel.TabIndex = 3;
            this.button_Cancel.Text = "Cancel";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // label_nameCheck
            // 
            this.label_nameCheck.AutoSize = true;
            this.label_nameCheck.Location = new System.Drawing.Point(218, 70);
            this.label_nameCheck.Name = "label_nameCheck";
            this.label_nameCheck.Size = new System.Drawing.Size(0, 13);
            this.label_nameCheck.TabIndex = 4;
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(212, 23);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(25, 25);
            this.pictureBox.TabIndex = 6;
            this.pictureBox.TabStop = false;
            // 
            // Form_NewStreamAnalyticsJob
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(244, 81);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.label_nameCheck);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.button_ok);
            this.Controls.Add(this.label_newJob);
            this.Controls.Add(this.textBox_newStreamAnalyticsJobName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form_NewStreamAnalyticsJob";
            this.Text = "New Stream Analytisc Job";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_newStreamAnalyticsJobName;
        private System.Windows.Forms.Label label_newJob;
        private System.Windows.Forms.Button button_ok;
        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.Label label_nameCheck;
        private System.Windows.Forms.PictureBox pictureBox;
    }
}
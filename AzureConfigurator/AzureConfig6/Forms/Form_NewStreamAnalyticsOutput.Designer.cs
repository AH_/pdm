﻿namespace AzureConfig6
{
    partial class Form_NewStreamAnalyticsOutput
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.button_Cancel = new System.Windows.Forms.Button();
            this.button_ok = new System.Windows.Forms.Button();
            this.textBox_new_output_name = new System.Windows.Forms.TextBox();
            this.label_name = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView_storageAccount = new System.Windows.Forms.DataGridView();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_storageAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(190, 205);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.button_Cancel.TabIndex = 7;
            this.button_Cancel.Text = "Cancel";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // button_ok
            // 
            this.button_ok.Location = new System.Drawing.Point(271, 205);
            this.button_ok.Name = "button_ok";
            this.button_ok.Size = new System.Drawing.Size(75, 23);
            this.button_ok.TabIndex = 6;
            this.button_ok.Text = "OK";
            this.button_ok.UseVisualStyleBackColor = true;
            this.button_ok.Click += new System.EventHandler(this.button_ok_Click);
            // 
            // textBox_new_output_name
            // 
            this.textBox_new_output_name.Location = new System.Drawing.Point(13, 34);
            this.textBox_new_output_name.Name = "textBox_new_output_name";
            this.textBox_new_output_name.Size = new System.Drawing.Size(290, 20);
            this.textBox_new_output_name.TabIndex = 14;
            this.textBox_new_output_name.TextChanged += new System.EventHandler(this.textBox_new_output_name_TextChanged);
            // 
            // label_name
            // 
            this.label_name.AutoSize = true;
            this.label_name.Location = new System.Drawing.Point(12, 18);
            this.label_name.Name = "label_name";
            this.label_name.Size = new System.Drawing.Size(94, 13);
            this.label_name.TabIndex = 13;
            this.label_name.Text = "New output name:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Output storage account:";
            // 
            // dataGridView_storageAccount
            // 
            this.dataGridView_storageAccount.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_storageAccount.Location = new System.Drawing.Point(13, 96);
            this.dataGridView_storageAccount.Name = "dataGridView_storageAccount";
            this.dataGridView_storageAccount.Size = new System.Drawing.Size(333, 103);
            this.dataGridView_storageAccount.TabIndex = 18;
            this.dataGridView_storageAccount.SelectionChanged += new System.EventHandler(this.dataGridView_sAccs_SelectionChanged);
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(310, 32);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(25, 25);
            this.pictureBox.TabIndex = 19;
            this.pictureBox.TabStop = false;
            // 
            // Form_NewStreamAnalyticsOutput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 235);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.dataGridView_storageAccount);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox_new_output_name);
            this.Controls.Add(this.label_name);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.button_ok);
            this.Name = "Form_NewStreamAnalyticsOutput";
            this.Text = "New Stream Analytics Job Output";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_storageAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.Button button_ok;
        private System.Windows.Forms.TextBox textBox_new_output_name;
        private System.Windows.Forms.Label label_name;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView_storageAccount;
        private System.Windows.Forms.PictureBox pictureBox;
    }
}
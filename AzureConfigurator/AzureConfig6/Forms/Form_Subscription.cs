﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AzureConfig6
{
    public partial class Form_Subscription : Form
    {
        AzureSubscription choosenAzureSubscription = new AzureSubscription();

        List<AzureSubscription> subscriptionList = new List<AzureSubscription>();
        Dictionary<String, AzureSubscription> subscriptionDict = new Dictionary<string, AzureSubscription>();

        public Form_Subscription()
        {
            InitializeComponent();
        }

        private void Form_Subscription_Load(object sender, EventArgs e)
        {
            String subscriptons = REST.doGET("https://management.azure.com/subscriptions?api-version=2016-06-01", Globals.token);

            var subs = JObject.Parse(subscriptons);
            
            foreach(var el in subs)
            {
                AzureSubscription tempSubscription = JsonConvert.DeserializeObject<List<AzureSubscription>>(el.Value.ToString())[0];
                subscriptionDict.Add(tempSubscription.displayName, tempSubscription);
            }

            comboBox_subscriptions.DataSource = new BindingSource(subscriptionDict, null);
            comboBox_subscriptions.DisplayMember = "Key";
            comboBox_subscriptions.ValueMember = "Value";
        }

        private void button_next_Click(object sender, EventArgs e)
        {
            Form_ResourceGroup fResourceGroup = new Form_ResourceGroup();
            fResourceGroup.MdiParent = this.MdiParent;
            fResourceGroup.FormBorderStyle = FormBorderStyle.None;
            fResourceGroup.Dock = DockStyle.Fill;
            this.Close();
            fResourceGroup.Show();
        }

        private void button_logout_Click(object sender, EventArgs e)
        {
            Form_Login_With_AD form_Login_With_AD = new Form_Login_With_AD();
            form_Login_With_AD.MdiParent = this.MdiParent;
            form_Login_With_AD.FormBorderStyle = FormBorderStyle.None;
            form_Login_With_AD.Dock = DockStyle.Fill;
            this.Close();
            form_Login_With_AD.Show();
        }

        private void comboBox_subscriptions_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            KeyValuePair<String, AzureSubscription> selectedEntry = (KeyValuePair<String, AzureSubscription>)comboBox.SelectedItem;
            Globals.subscription = selectedEntry.Value;
        }
    }
}

﻿namespace AzureConfig6
{
    partial class Form_NewResourceGroup
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.label_newResourceGroupLabel = new System.Windows.Forms.Label();
            this.label_nameCheck = new System.Windows.Forms.Label();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.button_ok = new System.Windows.Forms.Button();
            this.textBox_newResourceGroupName = new System.Windows.Forms.TextBox();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // label_newResourceGroupLabel
            // 
            this.label_newResourceGroupLabel.AutoSize = true;
            this.label_newResourceGroupLabel.Location = new System.Drawing.Point(12, 9);
            this.label_newResourceGroupLabel.Name = "label_newResourceGroupLabel";
            this.label_newResourceGroupLabel.Size = new System.Drawing.Size(142, 13);
            this.label_newResourceGroupLabel.TabIndex = 1;
            this.label_newResourceGroupLabel.Text = "New Resource Group name:";
            // 
            // label_nameCheck
            // 
            this.label_nameCheck.AutoSize = true;
            this.label_nameCheck.Location = new System.Drawing.Point(221, 67);
            this.label_nameCheck.Name = "label_nameCheck";
            this.label_nameCheck.Size = new System.Drawing.Size(0, 13);
            this.label_nameCheck.TabIndex = 8;
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(81, 54);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.button_Cancel.TabIndex = 7;
            this.button_Cancel.Text = "Cancel";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // button_ok
            // 
            this.button_ok.Location = new System.Drawing.Point(162, 54);
            this.button_ok.Name = "button_ok";
            this.button_ok.Size = new System.Drawing.Size(75, 23);
            this.button_ok.TabIndex = 6;
            this.button_ok.Text = "OK";
            this.button_ok.UseVisualStyleBackColor = true;
            this.button_ok.Click += new System.EventHandler(this.button_ok_Click);
            // 
            // textBox_newResourceGroupName
            // 
            this.textBox_newResourceGroupName.Location = new System.Drawing.Point(12, 25);
            this.textBox_newResourceGroupName.Name = "textBox_newResourceGroupName";
            this.textBox_newResourceGroupName.Size = new System.Drawing.Size(195, 20);
            this.textBox_newResourceGroupName.TabIndex = 5;
            this.textBox_newResourceGroupName.TextChanged += new System.EventHandler(this.textBox_newResourceGroupName_TextChanged);
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(212, 23);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(25, 25);
            this.pictureBox.TabIndex = 9;
            this.pictureBox.TabStop = false;
            // 
            // Form_NewResourceGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(244, 81);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.label_nameCheck);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.button_ok);
            this.Controls.Add(this.textBox_newResourceGroupName);
            this.Controls.Add(this.label_newResourceGroupLabel);
            this.Name = "Form_NewResourceGroup";
            this.Text = "New Resource Group";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label_newResourceGroupLabel;
        private System.Windows.Forms.Label label_nameCheck;
        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.Button button_ok;
        private System.Windows.Forms.TextBox textBox_newResourceGroupName;
        private System.Windows.Forms.PictureBox pictureBox;
    }
}
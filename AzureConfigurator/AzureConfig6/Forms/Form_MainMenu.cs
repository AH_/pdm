﻿using Microsoft.Azure.Management.ResourceManager;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Net;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace AzureConfig6
{
    public partial class Form_MainMenu : Form
    {
        public Form_MainMenu()
        {
            InitializeComponent();

            tabControl_mainMenu.Selecting += new TabControlCancelEventHandler(tabControlSelecting);

            initStorageGridView();
            initTableGridView();
            initHubsGridView();
            initDevicesGridView();
            initStreamJobsGridView();

            LoadStorageTab();
            LoadStreamAnalyticsTab();
            LoadIoThubTab();

            Firebase.LoadConnectionData();
        }

        private async void LoadStorageTab()
        {
            label_storage_info.Text = "";
            Globals.storageAccountList = await Globals.resourceGroup.listStorageAccounts();
            Globals.tableList = new List<AzureTable>();
            foreach(AzureStorageAccount storage in Globals.storageAccountList)
            {
                storage.keys = await storage.getStorageKeysAsync();
            }
            refreshStorageGridView(Globals.storageAccountList);
        }
        private async void LoadIoThubTab()
        {
            label_hubs_info.Text = "";
            Globals.hubList = Globals.resourceGroup.listHubs();
            foreach (AzureIoThub hub in Globals.hubList)
            {
                hub.keys = await hub.getHubKeysAsync();
            }
            refreshHubsGridView(Globals.hubList);
        }
        private void LoadStreamAnalyticsTab()
        {
            label_stream_info.Text = "";
            Globals.streamAnalyticsList = Globals.resourceGroup.listStreamAnalyticsJobs();
            refreshStreamJobsGridView(Globals.streamAnalyticsList);
            try
            {
                Globals.streamAnalyticsJob = Globals.streamAnalyticsList[0];
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }


        private void tabControlSelecting(object sender, TabControlCancelEventArgs e)
        {
            TabPage currentTabPage = (sender as TabControl).SelectedTab;
            if(currentTabPage.Name.Equals("tabPage_storageAccounts"))
            {
                LoadStorageTab();
            }
            if (currentTabPage.Name.Equals("tabPage_iotHubs"))
            {
                LoadIoThubTab();
            }
            if (currentTabPage.Name.Equals("tabPage_streamAnalytics"))
            {
                LoadStreamAnalyticsTab();
            }
        }
        private void button_back_Click(object sender, EventArgs e)
        {
            Form_ResourceGroup fResourceGroup = new Form_ResourceGroup();
            fResourceGroup.MdiParent = this.MdiParent;
            fResourceGroup.FormBorderStyle = FormBorderStyle.None;
            fResourceGroup.Dock = DockStyle.Fill;
            this.Close();
            fResourceGroup.Show();
        }

        private void CheckFirebaseConnection()
        {
            int firebaseResponseCode = Firebase.TryDatabaseConnection();
            switch (firebaseResponseCode)
            {
                case -1:
                    button_deviceAdd.Enabled = false;
                    label_hubs_info.Text = "ERROR: Firebase client = null.";
                    break;
                case 0:
                    button_deviceAdd.Enabled = true;
                    label_hubs_info.Text = "";
                    break;
                case 1:
                    button_deviceAdd.Enabled = false;
                    label_hubs_info.Text = "Can't connect to Firebase. Check URL.";
                    break;
                case 2:
                    button_deviceAdd.Enabled = false;
                    label_hubs_info.Text = "Can't connect to Firebase. Check secret.";
                    break;
                default:
                    button_deviceAdd.Enabled = false;
                    label_hubs_info.Text = "ERROR: switch -> default.";
                    break;
            }
        }

        //Storage START
        private void initStorageGridView()
        {
            dataGridView_storages.Columns.Clear();

            dataGridView_storages.AutoGenerateColumns = false;
            dataGridView_storages.AllowUserToAddRows = false;

            DataGridViewTextBoxColumn colName = new DataGridViewTextBoxColumn();
            colName.HeaderText = "Storage account name";
            colName.DataPropertyName = "name";

            DataGridViewTextBoxColumn colLocation = new DataGridViewTextBoxColumn();
            colLocation.HeaderText = "Storage account location";
            colLocation.DataPropertyName = "location";

            DataGridViewTextBoxColumn colId = new DataGridViewTextBoxColumn();
            colId.HeaderText = "Storage account id";
            colId.DataPropertyName = "id";

            DataGridViewTextBoxColumn colKind = new DataGridViewTextBoxColumn();
            colId.HeaderText = "Storage account kind";
            colId.DataPropertyName = "kind";

            dataGridView_storages.Columns.Add(colName);
            dataGridView_storages.Columns.Add(colLocation);
            dataGridView_storages.Columns.Add(colId);

            dataGridView_storages.ReadOnly = true;
            dataGridView_storages.AllowUserToAddRows = false;
            dataGridView_storages.RowHeadersVisible = false;
            dataGridView_storages.AllowUserToResizeRows = false;
            dataGridView_storages.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }
        private void refreshStorageGridView(List<AzureStorageAccount> accounts)
        {
            if (accounts != null && accounts.Count > 0)
            {
                BindingList<AzureStorageAccount> bindingList = new BindingList<AzureStorageAccount>(accounts);
                dataGridView_storages.DataSource = bindingList;
                button_removeStorage.Enabled = true;
            }
            else
            {
                dataGridView_storages.Rows.Clear();
                button_removeStorage.Enabled = false;
                button_addTable.Enabled = false;
                button_removeTable.Enabled = false;
            }
        }
        private async void dataGridView_sAccs_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                Globals.storageAccount = (AzureStorageAccount)dataGridView_storages.CurrentRow.DataBoundItem;
                Globals.tableList = null;

                if (Globals.storageAccount.keys != null)
                {
                    Globals.tableList = Globals.storageAccount.listTablesBySdk();
                    button_addTable.Enabled = true;
                }else
                {
                    label_storageInfo.Text = "Waiting for storage keys.";
                    Globals.storageAccount.keys = await Globals.storageAccount.getStorageKeysAsync();
                    Globals.tableList = Globals.storageAccount.listTablesBySdk();
                    label_storageInfo.Text = "";
                }
                refreshTableGridView(Globals.tableList);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
        private async void button_addStorage_Click(object sender, EventArgs e)
        {
            using (var newSAForm = new Form_NewStorageAccount())
            {
                DialogResult result = newSAForm.ShowDialog();
                if (result == DialogResult.OK)
                {
                    Cursor = Cursors.WaitCursor;
                    label_storage_info.Text = "Creating Storage account...";
                    String newSaName = newSAForm.newSaName;
                    await AzureStorageAccount.createStorageAsync(newSaName);
                    AzureStorageAccount createdStorage = await AzureStorageAccount.getStorageAsync(newSaName);
                    createdStorage.keys = await createdStorage.getStorageKeysAsync();

                    if (createdStorage != null)
                    {
                        createdStorage.CreateTableBySdkAsync("values");
                        createdStorage.CreateTableBySdkAsync("specifications");
                        createdStorage.CreateTableBySdkAsync("configurations");
                    }

                    Globals.storageAccountList = await Globals.resourceGroup.listStorageAccounts();
                    foreach(AzureStorageAccount storage in Globals.storageAccountList)
                    {
                        storage.keys = await storage.getStorageKeysAsync();
                    }
                    refreshStorageGridView(Globals.storageAccountList);
                    Cursor = Cursors.Default;
                    label_storage_info.Text = "";
                }
            }
        }
        private async void button_removeStorage_Click(object sender, EventArgs e)
        {
            label_storage_info.Text = "Deleting Storage account...";
            if (Globals.storageAccount != null)
            {
                await Globals.storageAccount.deleteStorageAsync();
                Globals.storageAccount.RemoveAssociatedOutputs();
                Globals.storageAccount = null;
                Globals.tableList = null;
                Globals.storageAccountList = await Globals.resourceGroup.listStorageAccounts();
                refreshStorageGridView(Globals.storageAccountList);
                refreshTableGridView(Globals.tableList);
            }
            label_storage_info.Text = "";
        }

        //Storage END

        //Table START
        private void initTableGridView()
        {
            dataGridView_tables.Columns.Clear();
            dataGridView_tables.AutoGenerateColumns = false;
            dataGridView_tables.AllowUserToAddRows = false;

            DataGridViewTextBoxColumn colName = new DataGridViewTextBoxColumn();
            colName.HeaderText = "Table name";
            colName.DataPropertyName = "name";

            dataGridView_tables.Columns.Add(colName);

            dataGridView_tables.ReadOnly = true;
            dataGridView_tables.AllowUserToAddRows = false;
            dataGridView_tables.RowHeadersVisible = false;
            dataGridView_tables.AllowUserToResizeRows = false;
            dataGridView_tables.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }
        private void refreshTableGridView(List<AzureTable> tables)
        {
            if (tables != null && tables.Count > 0)
            {
                BindingList<AzureTable> bindingList = new BindingList<AzureTable>(tables);
                dataGridView_tables.DataSource = bindingList;
            }
            else
            {
                dataGridView_tables.Rows.Clear();
                button_removeTable.Enabled = false;
            }
        }
        private void dataGridView_tables_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                AzureTable selectedTable = (AzureTable)dataGridView_tables.CurrentRow.DataBoundItem;
                checkPermissionToDeleteTable(selectedTable);
                Globals.table = selectedTable;
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
            }
        }
        private void button_addTable_Click(object sender, EventArgs e)
        {
            using (var newTableForm = new Form_NewTable())
            {
                DialogResult result = newTableForm.ShowDialog();
                if (result == DialogResult.OK)
                {
                    String newTableName = newTableForm.newTableName;
                    Globals.storageAccount.CreateTableBySdkAsync(newTableName);
                    Thread.Sleep(1500);
                    Globals.tableList = Globals.storageAccount.listTablesBySdk();
                    refreshTableGridView(Globals.tableList);
                }
            }
        }
        private void button_removeTable_Click(object sender, EventArgs e)
        {
            if (Globals.table != null)
            {
                Globals.storageAccount.DeleteTableBySdkAsync(Globals.table.name);
                Globals.tableList.Remove(Globals.table);
                refreshTableGridView(Globals.tableList);
            }
        }
        private void checkPermissionToDeleteTable(AzureTable table)
        {
            if (table.name.Equals("values") ||
                table.name.Equals("specifications") ||
                table.name.Equals("configurations"))
            {
                button_removeTable.Enabled = false;
            }
            else
            {
                button_removeTable.Enabled = true;
            }
        }
        //Table END

        //IoT Hubs START
        private void initHubsGridView()
        {
            dataGridView_hubs.Columns.Clear();

            dataGridView_hubs.AutoGenerateColumns = false;
            dataGridView_hubs.AllowUserToAddRows = false;

            DataGridViewTextBoxColumn colName = new DataGridViewTextBoxColumn();
            colName.HeaderText = "IoT hub name";
            colName.DataPropertyName = "name";

            DataGridViewTextBoxColumn colLocation = new DataGridViewTextBoxColumn();
            colLocation.HeaderText = "IoT hub location";
            colLocation.DataPropertyName = "location";

            DataGridViewTextBoxColumn colType = new DataGridViewTextBoxColumn();
            colType.HeaderText = "IoT hub type";
            colType.DataPropertyName = "type";

            dataGridView_hubs.Columns.Add(colName);
            dataGridView_hubs.Columns.Add(colLocation);
            dataGridView_hubs.Columns.Add(colType);

            dataGridView_hubs.ReadOnly = true;
            dataGridView_hubs.AllowUserToAddRows = false;
            dataGridView_hubs.RowHeadersVisible = false;
            dataGridView_hubs.AllowUserToResizeRows = false;
            dataGridView_hubs.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView_hubs.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }
        private void refreshHubsGridView(List<AzureIoThub> hubs)
        {
            if(hubs!=null && hubs.Count>0)
            {
                BindingList<AzureIoThub> bindingList = new BindingList<AzureIoThub>(hubs);
                dataGridView_hubs.DataSource = bindingList;
                button_hubRemove.Enabled = true;
            }
            else
            {
                dataGridView_hubs.Rows.Clear();
                button_hubRemove.Enabled = false;
                button_deviceAdd.Enabled = false;
                button_deviceRemove.Enabled = false;
                buttonDeviceSettings.Enabled = false;
            }
        }
        private async void dataGridView_hubs_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                Globals.iotHub = (AzureIoThub)dataGridView_hubs.CurrentRow.DataBoundItem;
                Globals.deviceList = null;

                if (Globals.iotHub.keys != null)
                {
                    Globals.deviceList = Globals.iotHub.listDevices();
                    refreshDevicesGridView(Globals.deviceList);

                    CheckHubConnectionWithStorage(Globals.iotHub);
                }
                else
                {
                    label_hubs_info.Text = "Waiting for IoT hub keys...";
                    Globals.iotHub.keys = await Globals.iotHub.getHubKeysAsync();
                    Globals.deviceList = Globals.iotHub.listDevices();
                    refreshDevicesGridView(Globals.deviceList);
                    CheckHubConnectionWithStorage(Globals.iotHub);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }

        }
        private async void button_addHub_Click(object sender, EventArgs e)
        {
            using (var newHubForm = new Form_NewIoThub())
            {
                DialogResult result = newHubForm.ShowDialog();
                if (result == DialogResult.OK)
                {
                    Cursor = Cursors.WaitCursor;
                    label_hubs_info.Text = "Creating IoT hub...";
                    String newHubName = newHubForm.newHubName;
                    await AzureIoThub.createIotHubAsync(newHubName);
                    Globals.hubList = Globals.resourceGroup.listHubs();
                    refreshHubsGridView(Globals.hubList);
                    foreach(AzureIoThub hub in Globals.hubList)
                    {
                        hub.keys = await hub.getHubKeysAsync();
                    }
                    Cursor = Cursors.Default;
                    //label_hubs_info.Text = "";
                }
            }
        }
        private async void button_removeHub_Click(object sender, EventArgs e)
        {
            if(Globals.iotHub != null)
            {
                label_hubs_info.Text = "Deleting IoT hub...";
                await Globals.iotHub.removeIoThubAsync();
                Globals.iotHub.RemoveAssociatedInputs();
                Globals.iotHub = null;
                Globals.deviceList = null;
                refreshDevicesGridView(Globals.deviceList);
                Globals.hubList = Globals.resourceGroup.listHubs();
                refreshHubsGridView(Globals.hubList);
                label_hubs_info.Text = "";
            }
        }
        //IoT Hubs END

        //Devices START
        private void initDevicesGridView()
        {
            dataGridView_device.Columns.Clear();

            dataGridView_device.AutoGenerateColumns = false;
            dataGridView_device.AllowUserToAddRows = false;

            DataGridViewTextBoxColumn colName = new DataGridViewTextBoxColumn();
            colName.HeaderText = "Device name";
            colName.DataPropertyName = "deviceId";

            dataGridView_device.Columns.Add(colName);

            dataGridView_device.ReadOnly = true;
            dataGridView_device.AllowUserToAddRows = false;
            dataGridView_device.RowHeadersVisible = false;
            dataGridView_device.AllowUserToResizeRows = false;
            dataGridView_device.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }
        private void refreshDevicesGridView(List<AzureIotDevice> devices)
        {
            if(devices!=null && devices.Count>0)
            {
                BindingList<AzureIotDevice> bindingList = new BindingList<AzureIotDevice>(devices);
                dataGridView_device.DataSource = bindingList;
            }
            else
            {
                dataGridView_device.Rows.Clear();
                buttonDeviceSettings.Enabled = false;
                button_deviceRemove.Enabled = false;
            }
        }
        private async void button_addDevice_Click(object sender, EventArgs e)
        {
            using (var newDeviceForm = new Form_NewDevice())
            {
                DialogResult result = newDeviceForm.ShowDialog();
                if (result == DialogResult.OK)
                {
                    String newDeviceName = newDeviceForm.name;
                    Boolean autoKeys = newDeviceForm.autoKeys;
                    String primaryKey = "";
                    String secondaryKey = "";
                    ConfigurationTableRow configuration = newDeviceForm.configurationTableRow;
                    SpecificationTableRow specification = newDeviceForm.specificationTableRow;

                    if (autoKeys)
                    {
                        await Globals.iotHub.createDeviceAsync(newDeviceName);
                    }
                    else
                    {
                        primaryKey = newDeviceForm.primaryKey;
                        secondaryKey = newDeviceForm.secondaryKey;
                        await Globals.iotHub.createDeviceAsync(newDeviceName, primaryKey, secondaryKey);
                    }
                    if(configuration != null)
                    {
                        AzureStorageAccount storage = Globals.iotHub.getHubsStorageFromStreamAnalytics();
                        TableHelper.putConfiguration(storage.name, storage.keys[0].value, configuration);
                        TableHelper.putSpecification(storage.name, storage.keys[0].value, specification);
                    }

                    System.Threading.Thread.Sleep(2000);    //  TODO - async update instead of sleep

                    Globals.deviceList = Globals.iotHub.listDevices();
                    refreshDevicesGridView(Globals.deviceList);

                    //KOSTYL
                    foreach(AzureIotDevice dev in Globals.deviceList)
                    {
                        if(dev.deviceId.Equals(newDeviceName))
                        {
                            Firebase.AddDeviceToFirebase(new AzureIotDevice(newDeviceName), primaryKey);
                            break;
                        }
                    }
                    
                }
            }
        }
        private void button_removeDevice_Click(object sender, EventArgs e)
        {
            if(Globals.device != null)
            {
                Globals.iotHub.removeDevice(Globals.device.deviceId);
                Firebase.RemoveDeviceFromFirebase(Globals.device.deviceId);
                Globals.deviceList.Remove(Globals.device);
                Globals.device = null;
                refreshDevicesGridView(Globals.deviceList);
            }
        }
        private void dataGridView_device_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                Globals.device = (AzureIotDevice)dataGridView_device.CurrentRow.DataBoundItem;
                buttonDeviceSettings.Enabled = true;
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
            }

        }
        private void dataGridView_device_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            using (var devicePropsForm = new Form_DeviceProperties())
            {
                DialogResult result = devicePropsForm.ShowDialog();
                if (result == DialogResult.OK)
                {

                }
            }
        }
        private void buttonDeviceSettings_Click(object sender, EventArgs e)
        {
            using (var devicePropsForm = new Form_DeviceProperties())
            {
                DialogResult result = devicePropsForm.ShowDialog();
                if (result == DialogResult.OK)
                {

                }
            }
        }
        //Devices END

        //Stream START
        private void initStreamJobsGridView()
        {
            dataGridView_StreamAnalytics.Columns.Clear();

            dataGridView_StreamAnalytics.AutoGenerateColumns = false;
            dataGridView_StreamAnalytics.AllowUserToAddRows = false;

            DataGridViewTextBoxColumn colName = new DataGridViewTextBoxColumn();
            colName.HeaderText = "Strean Analytics Job name";
            colName.DataPropertyName = "name";

            DataGridViewTextBoxColumn colLocation = new DataGridViewTextBoxColumn();
            colLocation.HeaderText = "Strean Analytics Job location";
            colLocation.DataPropertyName = "location";

            dataGridView_StreamAnalytics.Columns.Add(colName);
            dataGridView_StreamAnalytics.Columns.Add(colLocation);

            dataGridView_StreamAnalytics.ReadOnly = true;
            dataGridView_StreamAnalytics.AllowUserToAddRows = false;
            dataGridView_StreamAnalytics.RowHeadersVisible = false;
            dataGridView_StreamAnalytics.AllowUserToResizeRows = false;
            dataGridView_StreamAnalytics.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }
        private void refreshStreamJobsGridView(List<AzureStreamAnalyticsJob> jobs)
        {
            dataGridView_StreamAnalytics.Rows.Clear();
            if (jobs.Count > 0 && jobs != null)
            {
                BindingList<AzureStreamAnalyticsJob> bindingList = new BindingList<AzureStreamAnalyticsJob>(jobs);
                dataGridView_StreamAnalytics.DataSource = bindingList;
                //button_settingsStreamAnalytics.Enabled = true;
                //button_removeStreamAnalytics.Enabled = true;
            }
            else
            {
                button_settingsStreamAnalytics.Enabled = false;
                button_removeStreamAnalytics.Enabled = false;
            }
        }
        private void dataGridView_StreamAnalytics_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                Globals.streamAnalyticsJob = (AzureStreamAnalyticsJob)dataGridView_StreamAnalytics.CurrentRow.DataBoundItem;
                button_settingsStreamAnalytics.Enabled = true;
                button_removeStreamAnalytics.Enabled = true;
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
                button_settingsStreamAnalytics.Enabled = false;
                button_removeStreamAnalytics.Enabled = false;
            }
        }
        private async void button_addStreamJob_Click(object sender, EventArgs e)
        {
            using (var newSAForm = new Form_NewStreamAnalyticsJob())
            {
                DialogResult result = newSAForm.ShowDialog();
                if (result == DialogResult.OK)
                {
                    Cursor = Cursors.WaitCursor;
                    label_stream_info.Text = "Creating Stream analytics job...";
                    String newJobName = newSAForm.newSaName;
                    await AzureStreamAnalyticsJob.createStreamAnalyticsJobAsync(newJobName);
                    //await AzureStreamAnalyticsJob.waitForStreamJob(newJobName);
                    Globals.streamAnalyticsList = Globals.resourceGroup.listStreamAnalyticsJobs();
                    refreshStreamJobsGridView(Globals.streamAnalyticsList);
                    Cursor = Cursors.Default;
                    label_stream_info.Text = "";
                }
            }
        }
        private async void button_removeStreamJob_Click(object sender, EventArgs e)
        {
            if(Globals.streamAnalyticsJob != null)
            {
                label_stream_info.Text = "Deleting Stream analytics job...";
                await Globals.streamAnalyticsJob.removeStreamAnalyticsJobAsync();
                Globals.streamAnalyticsJob = null;
                Globals.streamAnalyticsList = Globals.resourceGroup.listStreamAnalyticsJobs();
                refreshStreamJobsGridView(Globals.streamAnalyticsList);
                label_stream_info.Text = "";
            }
        }
        private void button_settingsStreamJob_Click(object sender, EventArgs e)
        {
            using (var newSettingsForm = new Form_StreanAnalyticsJob_Settings())
            {
                DialogResult result = newSettingsForm.ShowDialog();
                if (result == DialogResult.OK)
                {
                        String inputHubName = newSettingsForm.newSaName;
                        String queryName = newSettingsForm.newSaName;
                        String outputTableName = newSettingsForm.newSaName;
                }
            }
        }
        private void dataGridView_StreamAnalytics_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            using (var newSettingsForm = new Form_StreanAnalyticsJob_Settings())
            {
                DialogResult result = newSettingsForm.ShowDialog();
                if (result == DialogResult.OK)
                {
                    String inputHubName = newSettingsForm.newSaName;
                    String queryName = newSettingsForm.newSaName;
                    String outputTableName = newSettingsForm.newSaName;
                }
            }
        }
        //Stream END


        //Fetch with SDK
        private void Form_MainMenu_Load(object sender, EventArgs e)
        {
            Form1 f1 = this.MdiParent as Form1;
            f1.FormBorderStyle = FormBorderStyle.Sizable;
        }
        private void CheckHubConnectionWithStorage(AzureIoThub hub)
        {
            if (Globals.iotHub.getHubsStorageFromStreamAnalytics() == null)
            {
                button_deviceAdd.Enabled = false;
                label_hubs_info.Text = "IoT hub in not conected to any storage account.";
            }
            else
            {
                button_deviceAdd.Enabled = true;
                label_hubs_info.Text = "";
            }
        }
        private void SetUpFirebaseConnectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var setupFirebaseFormConnection = new Form_FirebaseConnection())
            {
                DialogResult result = setupFirebaseFormConnection.ShowDialog();
                if (result == DialogResult.OK)
                {
                    Firebase.SaveConnectionData(setupFirebaseFormConnection.connectionData);

                    Firebase.LoadConnectionData();
                    CheckFirebaseConnection();
                }
            }
        }
        private void SeuUpFirebaseUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var setupFirebaseFormUser = new Form_FirebaseUser())
            {
                DialogResult result = setupFirebaseFormUser.ShowDialog();
                if (result == DialogResult.OK)
                {
                    Firebase.SaveConnectionData(Globals.firebaseConnectionData);

                    Firebase.LoadConnectionData();
                }
            }
        }
        //Specifications END
    }
}
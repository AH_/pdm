﻿namespace AzureConfig6
{
    partial class Form_DeviceProperties
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.label_deviceName_title = new System.Windows.Forms.Label();
            this.label_devicename = new System.Windows.Forms.Label();
            this.label_iotHubName = new System.Windows.Forms.Label();
            this.label_iotHubName_title = new System.Windows.Forms.Label();
            this.label_owner = new System.Windows.Forms.Label();
            this.label_owner_title = new System.Windows.Forms.Label();
            this.pictureBox_qr = new System.Windows.Forms.PictureBox();
            this.button_switchOn = new System.Windows.Forms.Button();
            this.button_switchOff = new System.Windows.Forms.Button();
            this.button_close = new System.Windows.Forms.Button();
            this.dataGridView_sensors = new System.Windows.Forms.DataGridView();
            this.label_sensorList = new System.Windows.Forms.Label();
            this.button_settings = new System.Windows.Forms.Button();
            this.button_editSpecification = new System.Windows.Forms.Button();
            this.label_version_value = new System.Windows.Forms.Label();
            this.label_version_title = new System.Windows.Forms.Label();
            this.label_model_value = new System.Windows.Forms.Label();
            this.label_model_title = new System.Windows.Forms.Label();
            this.linkLabel_firebaseSync = new System.Windows.Forms.LinkLabel();
            this.button_save_qr = new System.Windows.Forms.Button();
            this.label_qrInfo = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_qr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_sensors)).BeginInit();
            this.SuspendLayout();
            // 
            // label_deviceName_title
            // 
            this.label_deviceName_title.AutoSize = true;
            this.label_deviceName_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_deviceName_title.Location = new System.Drawing.Point(12, 19);
            this.label_deviceName_title.Name = "label_deviceName_title";
            this.label_deviceName_title.Size = new System.Drawing.Size(48, 17);
            this.label_deviceName_title.TabIndex = 0;
            this.label_deviceName_title.Text = "Serial:";
            // 
            // label_devicename
            // 
            this.label_devicename.AutoSize = true;
            this.label_devicename.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_devicename.Location = new System.Drawing.Point(66, 17);
            this.label_devicename.Name = "label_devicename";
            this.label_devicename.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_devicename.Size = new System.Drawing.Size(49, 18);
            this.label_devicename.TabIndex = 1;
            this.label_devicename.Text = "name";
            this.label_devicename.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label_iotHubName
            // 
            this.label_iotHubName.AutoSize = true;
            this.label_iotHubName.Location = new System.Drawing.Point(66, 35);
            this.label_iotHubName.Name = "label_iotHubName";
            this.label_iotHubName.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_iotHubName.Size = new System.Drawing.Size(33, 13);
            this.label_iotHubName.TabIndex = 3;
            this.label_iotHubName.Text = "name";
            this.label_iotHubName.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label_iotHubName_title
            // 
            this.label_iotHubName_title.AutoSize = true;
            this.label_iotHubName_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_iotHubName_title.Location = new System.Drawing.Point(12, 36);
            this.label_iotHubName_title.Name = "label_iotHubName_title";
            this.label_iotHubName_title.Size = new System.Drawing.Size(46, 13);
            this.label_iotHubName_title.TabIndex = 2;
            this.label_iotHubName_title.Text = "IoT Hub:";
            // 
            // label_owner
            // 
            this.label_owner.AutoSize = true;
            this.label_owner.Location = new System.Drawing.Point(59, 58);
            this.label_owner.Name = "label_owner";
            this.label_owner.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_owner.Size = new System.Drawing.Size(33, 13);
            this.label_owner.TabIndex = 7;
            this.label_owner.Text = "name";
            this.label_owner.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label_owner_title
            // 
            this.label_owner_title.AutoSize = true;
            this.label_owner_title.Location = new System.Drawing.Point(12, 58);
            this.label_owner_title.Name = "label_owner_title";
            this.label_owner_title.Size = new System.Drawing.Size(41, 13);
            this.label_owner_title.TabIndex = 6;
            this.label_owner_title.Text = "Owner:";
            // 
            // pictureBox_qr
            // 
            this.pictureBox_qr.Location = new System.Drawing.Point(12, 136);
            this.pictureBox_qr.Name = "pictureBox_qr";
            this.pictureBox_qr.Size = new System.Drawing.Size(310, 310);
            this.pictureBox_qr.TabIndex = 8;
            this.pictureBox_qr.TabStop = false;
            // 
            // button_switchOn
            // 
            this.button_switchOn.Location = new System.Drawing.Point(144, 452);
            this.button_switchOn.Name = "button_switchOn";
            this.button_switchOn.Size = new System.Drawing.Size(86, 24);
            this.button_switchOn.TabIndex = 9;
            this.button_switchOn.Text = "Switch On";
            this.button_switchOn.UseVisualStyleBackColor = true;
            this.button_switchOn.Visible = false;
            this.button_switchOn.Click += new System.EventHandler(this.button_switchOn_Click);
            // 
            // button_switchOff
            // 
            this.button_switchOff.Location = new System.Drawing.Point(236, 452);
            this.button_switchOff.Name = "button_switchOff";
            this.button_switchOff.Size = new System.Drawing.Size(86, 24);
            this.button_switchOff.TabIndex = 10;
            this.button_switchOff.Text = "Switch Off";
            this.button_switchOff.UseVisualStyleBackColor = true;
            this.button_switchOff.Visible = false;
            this.button_switchOff.Click += new System.EventHandler(this.button_switchOff_Click);
            // 
            // button_close
            // 
            this.button_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_close.Location = new System.Drawing.Point(751, 519);
            this.button_close.Name = "button_close";
            this.button_close.Size = new System.Drawing.Size(86, 24);
            this.button_close.TabIndex = 11;
            this.button_close.Text = "Close";
            this.button_close.UseVisualStyleBackColor = true;
            this.button_close.Click += new System.EventHandler(this.button_close_Click);
            // 
            // dataGridView_sensors
            // 
            this.dataGridView_sensors.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_sensors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_sensors.Location = new System.Drawing.Point(337, 39);
            this.dataGridView_sensors.Name = "dataGridView_sensors";
            this.dataGridView_sensors.Size = new System.Drawing.Size(500, 407);
            this.dataGridView_sensors.TabIndex = 12;
            // 
            // label_sensorList
            // 
            this.label_sensorList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_sensorList.AutoSize = true;
            this.label_sensorList.Location = new System.Drawing.Point(334, 23);
            this.label_sensorList.Name = "label_sensorList";
            this.label_sensorList.Size = new System.Drawing.Size(90, 13);
            this.label_sensorList.TabIndex = 13;
            this.label_sensorList.Text = "Device\'s sensors:";
            // 
            // button_settings
            // 
            this.button_settings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_settings.Location = new System.Drawing.Point(751, 461);
            this.button_settings.Name = "button_settings";
            this.button_settings.Size = new System.Drawing.Size(86, 24);
            this.button_settings.TabIndex = 14;
            this.button_settings.Text = "Edit sensors";
            this.button_settings.UseVisualStyleBackColor = true;
            this.button_settings.Click += new System.EventHandler(this.button_settings_Click);
            // 
            // button_editSpecification
            // 
            this.button_editSpecification.Location = new System.Drawing.Point(12, 106);
            this.button_editSpecification.Name = "button_editSpecification";
            this.button_editSpecification.Size = new System.Drawing.Size(86, 24);
            this.button_editSpecification.TabIndex = 15;
            this.button_editSpecification.Text = "Edit";
            this.button_editSpecification.UseVisualStyleBackColor = true;
            this.button_editSpecification.Click += new System.EventHandler(this.button_editSpecification_Click);
            // 
            // label_version_value
            // 
            this.label_version_value.AutoSize = true;
            this.label_version_value.Location = new System.Drawing.Point(59, 83);
            this.label_version_value.Name = "label_version_value";
            this.label_version_value.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_version_value.Size = new System.Drawing.Size(33, 13);
            this.label_version_value.TabIndex = 19;
            this.label_version_value.Text = "name";
            this.label_version_value.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label_version_title
            // 
            this.label_version_title.AutoSize = true;
            this.label_version_title.Location = new System.Drawing.Point(12, 83);
            this.label_version_title.Name = "label_version_title";
            this.label_version_title.Size = new System.Drawing.Size(45, 13);
            this.label_version_title.TabIndex = 18;
            this.label_version_title.Text = "Version:";
            // 
            // label_model_value
            // 
            this.label_model_value.AutoSize = true;
            this.label_model_value.Location = new System.Drawing.Point(59, 70);
            this.label_model_value.Name = "label_model_value";
            this.label_model_value.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label_model_value.Size = new System.Drawing.Size(33, 13);
            this.label_model_value.TabIndex = 17;
            this.label_model_value.Text = "name";
            this.label_model_value.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label_model_title
            // 
            this.label_model_title.AutoSize = true;
            this.label_model_title.Location = new System.Drawing.Point(12, 70);
            this.label_model_title.Name = "label_model_title";
            this.label_model_title.Size = new System.Drawing.Size(39, 13);
            this.label_model_title.TabIndex = 16;
            this.label_model_title.Text = "Model:";
            // 
            // linkLabel_firebaseSync
            // 
            this.linkLabel_firebaseSync.Location = new System.Drawing.Point(0, 0);
            this.linkLabel_firebaseSync.Name = "linkLabel_firebaseSync";
            this.linkLabel_firebaseSync.Size = new System.Drawing.Size(100, 23);
            this.linkLabel_firebaseSync.TabIndex = 33;
            // 
            // button_save_qr
            // 
            this.button_save_qr.Location = new System.Drawing.Point(12, 452);
            this.button_save_qr.Name = "button_save_qr";
            this.button_save_qr.Size = new System.Drawing.Size(86, 24);
            this.button_save_qr.TabIndex = 29;
            this.button_save_qr.Text = "Save QR";
            this.button_save_qr.UseVisualStyleBackColor = true;
            this.button_save_qr.Click += new System.EventHandler(this.button_save_qr_Click);
            // 
            // label_qrInfo
            // 
            this.label_qrInfo.AutoSize = true;
            this.label_qrInfo.Location = new System.Drawing.Point(54, 243);
            this.label_qrInfo.Name = "label_qrInfo";
            this.label_qrInfo.Size = new System.Drawing.Size(35, 13);
            this.label_qrInfo.TabIndex = 30;
            this.label_qrInfo.Text = "label1";
            // 
            // Form_DeviceProperties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 551);
            this.Controls.Add(this.label_qrInfo);
            this.Controls.Add(this.button_save_qr);
            this.Controls.Add(this.linkLabel_firebaseSync);
            this.Controls.Add(this.label_version_value);
            this.Controls.Add(this.label_version_title);
            this.Controls.Add(this.label_model_value);
            this.Controls.Add(this.label_model_title);
            this.Controls.Add(this.button_editSpecification);
            this.Controls.Add(this.button_settings);
            this.Controls.Add(this.label_sensorList);
            this.Controls.Add(this.dataGridView_sensors);
            this.Controls.Add(this.button_close);
            this.Controls.Add(this.button_switchOff);
            this.Controls.Add(this.button_switchOn);
            this.Controls.Add(this.pictureBox_qr);
            this.Controls.Add(this.label_owner);
            this.Controls.Add(this.label_owner_title);
            this.Controls.Add(this.label_iotHubName);
            this.Controls.Add(this.label_iotHubName_title);
            this.Controls.Add(this.label_devicename);
            this.Controls.Add(this.label_deviceName_title);
            this.MinimumSize = new System.Drawing.Size(865, 590);
            this.Name = "Form_DeviceProperties";
            this.Text = "Device Properties";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_qr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_sensors)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_deviceName_title;
        private System.Windows.Forms.Label label_devicename;
        private System.Windows.Forms.Label label_iotHubName;
        private System.Windows.Forms.Label label_iotHubName_title;
        private System.Windows.Forms.Label label_owner;
        private System.Windows.Forms.Label label_owner_title;
        private System.Windows.Forms.PictureBox pictureBox_qr;
        private System.Windows.Forms.Button button_switchOn;
        private System.Windows.Forms.Button button_switchOff;
        private System.Windows.Forms.Button button_close;
        private System.Windows.Forms.DataGridView dataGridView_sensors;
        private System.Windows.Forms.Label label_sensorList;
        private System.Windows.Forms.Button button_settings;
        private System.Windows.Forms.Button button_editSpecification;
        private System.Windows.Forms.Label label_version_value;
        private System.Windows.Forms.Label label_version_title;
        private System.Windows.Forms.Label label_model_value;
        private System.Windows.Forms.Label label_model_title;
        private System.Windows.Forms.LinkLabel linkLabel_firebaseSync;
        private System.Windows.Forms.Button button_save_qr;
        private System.Windows.Forms.Label label_qrInfo;
    }
}
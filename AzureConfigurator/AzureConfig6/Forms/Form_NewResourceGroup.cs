﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AzureConfig6
{
    public partial class Form_NewResourceGroup : Form
    {
        Timer typingTimer = new Timer();
        public String newRgName;
        String textBoxString;
        Boolean isChecked  = false;

        public Form_NewResourceGroup()
        {
            InitializeComponent();
            button_ok.Enabled = false;
            typingTimer.Interval = 300;
            typingTimer.Tick += new EventHandler(this.typingTimerHandler);
        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            this.newRgName = textBox_newResourceGroupName.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void textBox_newResourceGroupName_TextChanged(object sender, EventArgs e)
        {
            isChecked = false;
            TextBox textBox = sender as TextBox;
            textBoxString = textBox.Text;
            typingTimer.Stop();
            typingTimer.Start();
        }

        private class Response
        {
            public bool nameAvailable { get; set; }
            public String reason { get; set; }
            public String message { get; set; }
        }

        private void typingTimerHandler(object sender, EventArgs e)
        {
            if (!isChecked)
            {
                if (!String.IsNullOrEmpty(textBoxString) && Regex.IsMatch(textBoxString, @"^[a-zA-Z0-9_-]+$"))
                {
                    Boolean rgExists = REST.doHEAD("https://management.azure.com/subscriptions/" + Globals.subscription.subscriptionId + "/resourcegroups/" + textBoxString + "?api-version=2019-05-10",
                    Globals.token);

                    if (!rgExists)
                    {
                        pictureBox.Image = Globals.iconSuccess;
                        button_ok.Enabled = true;
                    }
                    else
                    {
                        pictureBox.Image = Globals.iconError;
                        button_ok.Enabled = false;
                    }
                }
                else
                {
                    pictureBox.Image = Globals.iconError;
                    button_ok.Enabled = false;
                }
            }
            isChecked = true;
        }
    }
}

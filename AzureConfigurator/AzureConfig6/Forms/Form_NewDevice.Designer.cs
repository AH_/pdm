﻿namespace AzureConfig6
{
    partial class Form_NewDevice
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.label_newDeviceLabel = new System.Windows.Forms.Label();
            this.label_nameCheck = new System.Windows.Forms.Label();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.button_ok = new System.Windows.Forms.Button();
            this.textBox_newDeviceName = new System.Windows.Forms.TextBox();
            this.textBox_primaryKey = new System.Windows.Forms.TextBox();
            this.label_primaryKey = new System.Windows.Forms.Label();
            this.textBox_secondaryKey = new System.Windows.Forms.TextBox();
            this.label_secondaryKey = new System.Windows.Forms.Label();
            this.checkBox_generateKeys = new System.Windows.Forms.CheckBox();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.comboBox_model = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox_version = new System.Windows.Forms.ComboBox();
            this.button_browse = new System.Windows.Forms.Button();
            this.label_configPath = new System.Windows.Forms.Label();
            this.button_removeJson = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox_owner = new System.Windows.Forms.ComboBox();
            this.label_newDevice_info = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // label_newDeviceLabel
            // 
            this.label_newDeviceLabel.AutoSize = true;
            this.label_newDeviceLabel.Location = new System.Drawing.Point(8, 10);
            this.label_newDeviceLabel.Name = "label_newDeviceLabel";
            this.label_newDeviceLabel.Size = new System.Drawing.Size(96, 13);
            this.label_newDeviceLabel.TabIndex = 1;
            this.label_newDeviceLabel.Text = "New device name:";
            // 
            // label_nameCheck
            // 
            this.label_nameCheck.AutoSize = true;
            this.label_nameCheck.Location = new System.Drawing.Point(221, 67);
            this.label_nameCheck.Name = "label_nameCheck";
            this.label_nameCheck.Size = new System.Drawing.Size(0, 13);
            this.label_nameCheck.TabIndex = 8;
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(210, 385);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.button_Cancel.TabIndex = 7;
            this.button_Cancel.Text = "Cancel";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // button_ok
            // 
            this.button_ok.Location = new System.Drawing.Point(291, 385);
            this.button_ok.Name = "button_ok";
            this.button_ok.Size = new System.Drawing.Size(75, 23);
            this.button_ok.TabIndex = 6;
            this.button_ok.Text = "OK";
            this.button_ok.UseVisualStyleBackColor = true;
            this.button_ok.Click += new System.EventHandler(this.button_ok_Click);
            // 
            // textBox_newDeviceName
            // 
            this.textBox_newDeviceName.Location = new System.Drawing.Point(10, 26);
            this.textBox_newDeviceName.Name = "textBox_newDeviceName";
            this.textBox_newDeviceName.Size = new System.Drawing.Size(325, 20);
            this.textBox_newDeviceName.TabIndex = 5;
            this.textBox_newDeviceName.TextChanged += new System.EventHandler(this.textBox_newResourceGroupName_TextChanged);
            // 
            // textBox_primaryKey
            // 
            this.textBox_primaryKey.Location = new System.Drawing.Point(10, 66);
            this.textBox_primaryKey.Name = "textBox_primaryKey";
            this.textBox_primaryKey.Size = new System.Drawing.Size(325, 20);
            this.textBox_primaryKey.TabIndex = 10;
            this.textBox_primaryKey.TextChanged += new System.EventHandler(this.TextBox_primaryKey_TextChanged);
            // 
            // label_primaryKey
            // 
            this.label_primaryKey.AutoSize = true;
            this.label_primaryKey.Location = new System.Drawing.Point(8, 50);
            this.label_primaryKey.Name = "label_primaryKey";
            this.label_primaryKey.Size = new System.Drawing.Size(64, 13);
            this.label_primaryKey.TabIndex = 9;
            this.label_primaryKey.Text = "Primary key:";
            // 
            // textBox_secondaryKey
            // 
            this.textBox_secondaryKey.Location = new System.Drawing.Point(10, 106);
            this.textBox_secondaryKey.Name = "textBox_secondaryKey";
            this.textBox_secondaryKey.Size = new System.Drawing.Size(325, 20);
            this.textBox_secondaryKey.TabIndex = 12;
            this.textBox_secondaryKey.TextChanged += new System.EventHandler(this.TextBox_secondaryKey_TextChanged);
            // 
            // label_secondaryKey
            // 
            this.label_secondaryKey.AutoSize = true;
            this.label_secondaryKey.Location = new System.Drawing.Point(8, 90);
            this.label_secondaryKey.Name = "label_secondaryKey";
            this.label_secondaryKey.Size = new System.Drawing.Size(81, 13);
            this.label_secondaryKey.TabIndex = 11;
            this.label_secondaryKey.Text = "Secondary key:";
            // 
            // checkBox_generateKeys
            // 
            this.checkBox_generateKeys.AutoSize = true;
            this.checkBox_generateKeys.Location = new System.Drawing.Point(10, 135);
            this.checkBox_generateKeys.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox_generateKeys.Name = "checkBox_generateKeys";
            this.checkBox_generateKeys.Size = new System.Drawing.Size(159, 17);
            this.checkBox_generateKeys.TabIndex = 13;
            this.checkBox_generateKeys.Text = "Generate keys automatically";
            this.checkBox_generateKeys.UseVisualStyleBackColor = true;
            this.checkBox_generateKeys.CheckedChanged += new System.EventHandler(this.CheckBox_generateKeys_CheckedChanged);
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(341, 23);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(25, 25);
            this.pictureBox.TabIndex = 14;
            this.pictureBox.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(341, 63);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(25, 25);
            this.pictureBox1.TabIndex = 15;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(341, 103);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(25, 25);
            this.pictureBox2.TabIndex = 16;
            this.pictureBox2.TabStop = false;
            // 
            // comboBox_model
            // 
            this.comboBox_model.FormattingEnabled = true;
            this.comboBox_model.Location = new System.Drawing.Point(56, 254);
            this.comboBox_model.Name = "comboBox_model";
            this.comboBox_model.Size = new System.Drawing.Size(279, 21);
            this.comboBox_model.TabIndex = 17;
            this.comboBox_model.SelectedValueChanged += new System.EventHandler(this.comboBox_model_SelectedValueChanged);
            this.comboBox_model.TextChanged += new System.EventHandler(this.ComboBox_model_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 257);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Model";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 284);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Version";
            // 
            // comboBox_version
            // 
            this.comboBox_version.FormattingEnabled = true;
            this.comboBox_version.Location = new System.Drawing.Point(56, 281);
            this.comboBox_version.Name = "comboBox_version";
            this.comboBox_version.Size = new System.Drawing.Size(279, 21);
            this.comboBox_version.TabIndex = 19;
            this.comboBox_version.SelectedValueChanged += new System.EventHandler(this.comboBox_version_SelectedValueChanged);
            this.comboBox_version.TextChanged += new System.EventHandler(this.ComboBox_version_TextChanged);
            // 
            // button_browse
            // 
            this.button_browse.Location = new System.Drawing.Point(11, 308);
            this.button_browse.Name = "button_browse";
            this.button_browse.Size = new System.Drawing.Size(75, 23);
            this.button_browse.TabIndex = 21;
            this.button_browse.Text = "Browse";
            this.button_browse.UseVisualStyleBackColor = true;
            this.button_browse.Click += new System.EventHandler(this.button_browse_Click);
            // 
            // label_configPath
            // 
            this.label_configPath.AutoSize = true;
            this.label_configPath.Location = new System.Drawing.Point(90, 315);
            this.label_configPath.Name = "label_configPath";
            this.label_configPath.Size = new System.Drawing.Size(35, 13);
            this.label_configPath.TabIndex = 22;
            this.label_configPath.Text = "label3";
            // 
            // button_removeJson
            // 
            this.button_removeJson.Location = new System.Drawing.Point(341, 311);
            this.button_removeJson.Name = "button_removeJson";
            this.button_removeJson.Size = new System.Drawing.Size(20, 20);
            this.button_removeJson.TabIndex = 23;
            this.button_removeJson.Text = "X";
            this.button_removeJson.UseVisualStyleBackColor = true;
            this.button_removeJson.Click += new System.EventHandler(this.button_removeJson_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 230);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Owner";
            // 
            // comboBox_owner
            // 
            this.comboBox_owner.FormattingEnabled = true;
            this.comboBox_owner.Location = new System.Drawing.Point(56, 227);
            this.comboBox_owner.Name = "comboBox_owner";
            this.comboBox_owner.Size = new System.Drawing.Size(279, 21);
            this.comboBox_owner.TabIndex = 25;
            this.comboBox_owner.SelectedValueChanged += new System.EventHandler(this.comboBox_owner_SelectedValueChanged);
            this.comboBox_owner.TextChanged += new System.EventHandler(this.ComboBox_owner_TextChanged);
            // 
            // label_newDevice_info
            // 
            this.label_newDevice_info.AutoSize = true;
            this.label_newDevice_info.Location = new System.Drawing.Point(13, 158);
            this.label_newDevice_info.Name = "label_newDevice_info";
            this.label_newDevice_info.Size = new System.Drawing.Size(35, 13);
            this.label_newDevice_info.TabIndex = 26;
            this.label_newDevice_info.Text = "label4";
            // 
            // Form_NewDevice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(370, 411);
            this.Controls.Add(this.label_newDevice_info);
            this.Controls.Add(this.comboBox_owner);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button_removeJson);
            this.Controls.Add(this.label_configPath);
            this.Controls.Add(this.button_browse);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBox_version);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox_model);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.checkBox_generateKeys);
            this.Controls.Add(this.textBox_secondaryKey);
            this.Controls.Add(this.label_secondaryKey);
            this.Controls.Add(this.textBox_primaryKey);
            this.Controls.Add(this.label_primaryKey);
            this.Controls.Add(this.label_nameCheck);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.button_ok);
            this.Controls.Add(this.textBox_newDeviceName);
            this.Controls.Add(this.label_newDeviceLabel);
            this.Name = "Form_NewDevice";
            this.Text = "New Device";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label_newDeviceLabel;
        private System.Windows.Forms.Label label_nameCheck;
        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.Button button_ok;
        private System.Windows.Forms.TextBox textBox_newDeviceName;
        private System.Windows.Forms.TextBox textBox_primaryKey;
        private System.Windows.Forms.Label label_primaryKey;
        private System.Windows.Forms.TextBox textBox_secondaryKey;
        private System.Windows.Forms.Label label_secondaryKey;
        private System.Windows.Forms.CheckBox checkBox_generateKeys;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ComboBox comboBox_model;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox_version;
        private System.Windows.Forms.Button button_browse;
        private System.Windows.Forms.Label label_configPath;
        private System.Windows.Forms.Button button_removeJson;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox_owner;
        private System.Windows.Forms.Label label_newDevice_info;
    }
}
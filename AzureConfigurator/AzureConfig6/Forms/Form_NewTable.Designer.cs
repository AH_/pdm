﻿namespace AzureConfig6
{
    partial class Form_NewTable
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.textBox_newTableName = new System.Windows.Forms.TextBox();
            this.label_newResourceGroupLabel = new System.Windows.Forms.Label();
            this.button_ok = new System.Windows.Forms.Button();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox_newTableName
            // 
            this.textBox_newTableName.Location = new System.Drawing.Point(12, 25);
            this.textBox_newTableName.Name = "textBox_newTableName";
            this.textBox_newTableName.Size = new System.Drawing.Size(195, 20);
            this.textBox_newTableName.TabIndex = 0;
            this.textBox_newTableName.TextChanged += new System.EventHandler(this.textBox_newTableName_TextChanged);
            // 
            // label_newResourceGroupLabel
            // 
            this.label_newResourceGroupLabel.AutoSize = true;
            this.label_newResourceGroupLabel.Location = new System.Drawing.Point(12, 9);
            this.label_newResourceGroupLabel.Name = "label_newResourceGroupLabel";
            this.label_newResourceGroupLabel.Size = new System.Drawing.Size(87, 13);
            this.label_newResourceGroupLabel.TabIndex = 1;
            this.label_newResourceGroupLabel.Text = "New table name:";
            // 
            // button_ok
            // 
            this.button_ok.Location = new System.Drawing.Point(162, 54);
            this.button_ok.Name = "button_ok";
            this.button_ok.Size = new System.Drawing.Size(75, 23);
            this.button_ok.TabIndex = 2;
            this.button_ok.Text = "OK";
            this.button_ok.UseVisualStyleBackColor = true;
            this.button_ok.Click += new System.EventHandler(this.button_ok_Click);
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(81, 54);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.button_Cancel.TabIndex = 3;
            this.button_Cancel.Text = "Cancel";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(212, 23);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(25, 25);
            this.pictureBox.TabIndex = 4;
            this.pictureBox.TabStop = false;
            // 
            // Form_NewTable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(244, 81);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.button_ok);
            this.Controls.Add(this.label_newResourceGroupLabel);
            this.Controls.Add(this.textBox_newTableName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form_NewTable";
            this.Text = "New Table";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_newTableName;
        private System.Windows.Forms.Label label_newResourceGroupLabel;
        private System.Windows.Forms.Button button_ok;
        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.PictureBox pictureBox;
    }
}
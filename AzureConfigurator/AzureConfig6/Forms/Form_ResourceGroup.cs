﻿using Microsoft.Azure.Management.ResourceManager;
using Microsoft.Azure.Management.ResourceManager.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AzureConfig6
{
    public partial class Form_ResourceGroup : Form
    {
        Dictionary<String, AzureResourceGroup> resourceGroupDict = new Dictionary<string, AzureResourceGroup>();

        Timer timer = new Timer();
        int plus = 5;

        public Form_ResourceGroup()
        {
            InitializeComponent();
        }

        private void Form_ResourceGroup_Load(object sender, EventArgs e)
        {
            Size s = this.Size;
            Form1 f1 = this.MdiParent as Form1;
            s = f1.Size;
            f1.Size = new Size(750, 500);
            f1.FormBorderStyle = FormBorderStyle.FixedSingle;

            listResourceGroups();
            refreshGridView(resourceGroupDict);
        }

        //Control elements methods
        private void button_next_Click(object sender, EventArgs e)
        {

            Form_MainMenu fMainMenu = new Form_MainMenu();
            fMainMenu.MdiParent = this.MdiParent;
            fMainMenu.FormBorderStyle = FormBorderStyle.None;
            fMainMenu.Dock = DockStyle.Fill;
            this.Close();
            fMainMenu.Show();
        }

        private void button_back_Click(object sender, EventArgs e)
        {
            Form_Subscription fSubscription = new Form_Subscription();
            fSubscription.MdiParent = this.MdiParent;
            fSubscription.FormBorderStyle = FormBorderStyle.None;
            fSubscription.Dock = DockStyle.Fill;
            this.Close();
            fSubscription.Show();
        }

        //My methods
        private void listResourceGroups()
        {
            String result = REST.doGET("https://management.azure.com/subscriptions/" + Globals.subscription.subscriptionId + "/resourcegroups?api-version=2019-05-10", Globals.token);

            resourceGroupDict.Clear();

            var rGroups = JObject.Parse(result);
            var first = JObject.Parse(rGroups.ToString())["value"];
            List<AzureResourceGroup> rGroupsList = first.ToObject<List<AzureResourceGroup>>();

            if (rGroupsList.Count > 0)
            {
                foreach (var el in rGroupsList)
                {
                    resourceGroupDict.Add(el.name, el);
                }
            }

        }

        private void createAzureResourceGroup(string name)
        {
            String uri = "https://management.azure.com/subscriptions/" + Globals.subscription.subscriptionId + "/resourcegroups/" + name + "?api-version=2019-05-10";
            String body = "{\"location\":\"eastus\"}";
            REST.doPUT(uri, body, Globals.token);
        }

        private void removeAzureResourceGroup(string name)
        {
            String uri = "https://management.azure.com/subscriptions/" + Globals.subscription.subscriptionId + "/resourcegroups/" + name + "?api-version=2019-05-10";
            REST.doDELETE(uri, Globals.token);
        }

        private void refreshGridView(Dictionary<string, AzureResourceGroup> dict)
        {
            dataGridView_rGroups.Columns.Clear();

            List<AzureResourceGroup> rgList = new List<AzureResourceGroup>();
            foreach(KeyValuePair<string, AzureResourceGroup> rg in dict)
            {
                rgList.Add(rg.Value);
            }

            dataGridView_rGroups.AutoGenerateColumns = false;
            dataGridView_rGroups.AllowUserToAddRows = false;

            DataGridViewTextBoxColumn colName = new DataGridViewTextBoxColumn();
            colName.HeaderText = "Resource Group name";
            colName.DataPropertyName = "name";

            DataGridViewTextBoxColumn colLocation = new DataGridViewTextBoxColumn();
            colLocation.HeaderText = "Resource Group location";
            colLocation.DataPropertyName = "location";

            DataGridViewTextBoxColumn colId = new DataGridViewTextBoxColumn();
            colId.HeaderText = "Resource Group id";
            colId.DataPropertyName = "id";

            dataGridView_rGroups.Columns.Add(colName);
            dataGridView_rGroups.Columns.Add(colLocation);
            dataGridView_rGroups.Columns.Add(colId);

            dataGridView_rGroups.ReadOnly = true;
            dataGridView_rGroups.AllowUserToAddRows = false;
            dataGridView_rGroups.RowHeadersVisible = false;
            dataGridView_rGroups.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            BindingList<AzureResourceGroup> bindingList = new BindingList<AzureResourceGroup>(rgList);
            dataGridView_rGroups.DataSource = bindingList;
        }

        private void dataGridView_rGroups_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                Globals.resourceGroup = (AzureResourceGroup)dataGridView_rGroups.CurrentRow.DataBoundItem;
            }
            catch(Exception exc)
            {
                Console.WriteLine(exc);
            }
            
        }

        private void button_add_Click(object sender, EventArgs e)
        {
            using (var newRGForm = new Form_NewResourceGroup())
            {
                DialogResult result = newRGForm.ShowDialog();
                if(result == DialogResult.OK)
                {
                    String newRgName = newRGForm.newRgName;
                    createAzureResourceGroup(newRgName);
                    listResourceGroups();
                    refreshGridView(resourceGroupDict);
                }
            }
        }

        private void button_remove_Click(object sender, EventArgs e)
        {
            removeAzureResourceGroup(Globals.resourceGroup.name);
            resourceGroupDict.Remove(Globals.resourceGroup.name);
            refreshGridView(resourceGroupDict);
        }
    }
}
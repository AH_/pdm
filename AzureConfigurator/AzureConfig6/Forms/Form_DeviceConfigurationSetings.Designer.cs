﻿namespace AzureConfig6
{
    partial class Form_DeviceConfigurationSetings
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.label_newSpecificationSettingsLabel = new System.Windows.Forms.Label();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.button_ok = new System.Windows.Forms.Button();
            this.dataGridView_sensors = new System.Windows.Forms.DataGridView();
            this.button_addRow = new System.Windows.Forms.Button();
            this.button_removeRow = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_sensors)).BeginInit();
            this.SuspendLayout();
            // 
            // label_newSpecificationSettingsLabel
            // 
            this.label_newSpecificationSettingsLabel.AutoSize = true;
            this.label_newSpecificationSettingsLabel.Location = new System.Drawing.Point(8, 11);
            this.label_newSpecificationSettingsLabel.Name = "label_newSpecificationSettingsLabel";
            this.label_newSpecificationSettingsLabel.Size = new System.Drawing.Size(83, 13);
            this.label_newSpecificationSettingsLabel.TabIndex = 1;
            this.label_newSpecificationSettingsLabel.Text = "Device sensors:";
            // 
            // button_Cancel
            // 
            this.button_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Cancel.Location = new System.Drawing.Point(426, 380);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.button_Cancel.TabIndex = 7;
            this.button_Cancel.Text = "Cancel";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // button_ok
            // 
            this.button_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_ok.Location = new System.Drawing.Point(507, 380);
            this.button_ok.Name = "button_ok";
            this.button_ok.Size = new System.Drawing.Size(75, 23);
            this.button_ok.TabIndex = 6;
            this.button_ok.Text = "OK";
            this.button_ok.UseVisualStyleBackColor = true;
            this.button_ok.Click += new System.EventHandler(this.button_ok_Click);
            // 
            // dataGridView_sensors
            // 
            this.dataGridView_sensors.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_sensors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_sensors.Location = new System.Drawing.Point(12, 27);
            this.dataGridView_sensors.Name = "dataGridView_sensors";
            this.dataGridView_sensors.Size = new System.Drawing.Size(570, 347);
            this.dataGridView_sensors.TabIndex = 8;
            this.dataGridView_sensors.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_sensors_CellValueChanged);
            // 
            // button_addRow
            // 
            this.button_addRow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_addRow.Location = new System.Drawing.Point(11, 380);
            this.button_addRow.Name = "button_addRow";
            this.button_addRow.Size = new System.Drawing.Size(75, 23);
            this.button_addRow.TabIndex = 9;
            this.button_addRow.Text = "Add";
            this.button_addRow.UseVisualStyleBackColor = true;
            this.button_addRow.Click += new System.EventHandler(this.button_addRow_Click);
            // 
            // button_removeRow
            // 
            this.button_removeRow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_removeRow.Location = new System.Drawing.Point(92, 380);
            this.button_removeRow.Name = "button_removeRow";
            this.button_removeRow.Size = new System.Drawing.Size(75, 23);
            this.button_removeRow.TabIndex = 10;
            this.button_removeRow.Text = "Remove";
            this.button_removeRow.UseVisualStyleBackColor = true;
            this.button_removeRow.Click += new System.EventHandler(this.button_removeRow_Click);
            // 
            // Form_DeviceConfigurationSetings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(588, 406);
            this.Controls.Add(this.button_removeRow);
            this.Controls.Add(this.button_addRow);
            this.Controls.Add(this.dataGridView_sensors);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.button_ok);
            this.Controls.Add(this.label_newSpecificationSettingsLabel);
            this.MinimumSize = new System.Drawing.Size(604, 445);
            this.Name = "Form_DeviceConfigurationSetings";
            this.Text = "Device specification settings";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_sensors)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label_newSpecificationSettingsLabel;
        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.Button button_ok;
        private System.Windows.Forms.DataGridView dataGridView_sensors;
        private System.Windows.Forms.Button button_addRow;
        private System.Windows.Forms.Button button_removeRow;
    }
}
﻿using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AzureConfig6
{
    public partial class Form_FirebaseUser : Form
    {
        public String firebaseUserMail;
        public List<String> firebaseUsers = new List<String>();
        public List<String> firebaseUsersAfterCharReplace = new List<String>();
        String comboPlaceholder = "-- Enter value or choose it from list. --";

        public Form_FirebaseUser()
        {
            InitializeComponent();

            if(Firebase.TryDatabaseConnection() != 0)
            {
                label_info.Text = "Firebase database is not connected.";
            }else
            {
                label_info.Text = "";
                Dictionary<String, FirebaseUser> firebaseUserDict = Firebase.ListUsers();
                if(firebaseUserDict.Count != 0 && firebaseUserDict != null)
                {
                    firebaseUsers = firebaseUserDict.Keys.ToList<String>();
                    foreach (String mailString in firebaseUsers)
                    {
                        firebaseUsersAfterCharReplace.Add(mailString.Replace("-", "."));
                    }
                    comboBox_firebaseUser.Items.AddRange(firebaseUsersAfterCharReplace.ToArray());
                    if (Globals.firebaseConnectionData.email != null)
                    {
                        comboBox_firebaseUser.Text = Globals.firebaseConnectionData.email.Replace("-", ".");
                    }
                    else
                    {
                        comboBox_firebaseUser.Text = comboPlaceholder;
                    }
                }
                checkComboBox();
            }
        }

        private void checkComboBox()
        {
            if (comboBox_firebaseUser.Text.Equals(comboPlaceholder) || !comboBox_firebaseUser.Text.Contains("@"))
            {
                pictureBox_mail.Image = Globals.iconError;
                button_saveFirebase.Enabled = false;
            }
            else
            {
                pictureBox_mail.Image = Globals.iconSuccess;
                button_saveFirebase.Enabled = true;
            }
        }

        private async void button_login_Click(object sender, EventArgs e)
        {
            firebaseUserMail = comboBox_firebaseUser.Text;
            Globals.firebaseConnectionData.email = firebaseUserMail.Replace(".", "-");

            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void Button_cancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void ComboBox_firebaseUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkComboBox();
        }

        private void ComboBox_firebaseUser_TextChanged(object sender, EventArgs e)
        {
            checkComboBox();
        }
    }
}
﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AzureConfig6
{
    public partial class Form_StreanAnalyticsJob_Settings : Form
    {
        AzureStreamAnalyticsJob streamAnalyticsJob;
        List<AzureStreamAnalyticsInput> inputList;
        List<AzureStreamAnalyticsOutput> outputList;
        List<AzureStreamAnalyticsTransformation> tranformList;

        public String newSaName;

        private class Response
        {
            public bool nameAvailable { get; set; }
            public String reason { get; set; }
            public String message { get; set; }
        }

        public Form_StreanAnalyticsJob_Settings()
        {
            InitializeComponent();
        }

        private async void Form_StreanAnalyticsJob_Settings_Load(object sender, EventArgs e)
        {
            streamAnalyticsJob = await AzureStreamAnalyticsJob.getStreamAnalyticsJobAsync(Globals.streamAnalyticsJob.name);

            Globals.inputList = Globals.streamAnalyticsJob.listInputs();
            refreshInputsGridView(Globals.inputList);

            Globals.outputList = Globals.streamAnalyticsJob.listOutputs();
            refreshOutputsGridView(Globals.outputList);

            Globals.transformation = Globals.streamAnalyticsJob.getTransform();

            Globals.transformationPairs = Globals.streamAnalyticsJob.ParseQueryToTransformPairs();
            refreshTransformsGridView(Globals.transformationPairs);

            await CheckJobStateAsync();
        }

        public void blockUI()
        {
            button_inputAdd.Enabled = false;
            button_inputRemove.Enabled = false;

            button_outputAdd.Enabled = false;
            button_outputRemove.Enabled = false;

            button_transformAdd.Enabled = false;
            button_transformRemove.Enabled = false;

            button_start.Enabled = false;
            button_stop.Enabled = true;
        }
        public void blockUINotReady()
        {
            //button_inputAdd.Enabled = true;
            //button_inputRemove.Enabled = true;

            //button_outputAdd.Enabled = true;
            //button_outputRemove.Enabled = true;

            //button_transformAdd.Enabled = true;
            //button_transformRemove.Enabled = true;

            button_start.Enabled = false;
            button_stop.Enabled = false;
        }

        public void unblockUI()
        {
            if (Globals.inputList != null && Globals.inputList.Count > 0)
            {
                button_inputRemove.Enabled = true;
            }
            else
            {
                button_inputRemove.Enabled = false;
            }
            button_inputAdd.Enabled = true;

            if (Globals.outputList != null && Globals.outputList.Count > 0)
            {
                button_outputRemove.Enabled = true;
            }
            else
            {
                button_outputRemove.Enabled = false;
            }
            button_outputAdd.Enabled = true;

            if (Globals.transformationPairs != null && Globals.transformationPairs.Count > 0)
            {
                button_transformRemove.Enabled = true;
            }
            else
            {
                button_transformRemove.Enabled = false;
            }
            button_transformAdd.Enabled = true;


            button_start.Enabled = true;
            button_stop.Enabled = false;
        }

        //Common controls START
        private void button_ok_Click(object sender, EventArgs e)
        {
            //this.newSaName = textBox_newStreamAnalyticsJobName.Text;
            Console.WriteLine("Settings OK.");
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        private void button_Cancel_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Settings Cancelled.");
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
        //Common controls END

        //Inputs START
        private async void button_AddInput_Click(object sender, EventArgs e)
        {
            using (var newInputForm = new Form_NewStreamAnalyticsInput())
            {
                DialogResult result = newInputForm.ShowDialog();
                if (result == DialogResult.OK)
                {
                    AzureIoThub chosenIotHub = newInputForm.iotHub;
                    String newInputName = newInputForm.newInputName;
                    Globals.streamAnalyticsJob.addInput(chosenIotHub, newInputName);
                    Globals.inputList = Globals.streamAnalyticsJob.listInputs();
                    Globals.streamAnalyticsJob = await AzureStreamAnalyticsJob.getStreamAnalyticsJobAsync(Globals.streamAnalyticsJob.name);
                    refreshInputsGridView(Globals.inputList);
                }
            }
            await CheckJobStateAsync();
        }
        private async void button_RemoveInput_Click(object sender, EventArgs e)
        {
            removeStreamInput(Globals.input);
            await CheckJobStateAsync();
        }
        private async void removeStreamInput(AzureStreamAnalyticsInput input)
        {
            Globals.streamAnalyticsJob.removeInput(input.name);
            Globals.inputList = Globals.streamAnalyticsJob.listInputs();
            refreshInputsGridView(Globals.inputList);

            foreach (AzureTransformationPair pair in Globals.transformationPairs)
            {
                if (pair.inputName.Equals(input.name))
                {
                    Globals.transformationPairs.Remove(pair);
                    break;
                }
            }

            String query = AzureStreamAnalyticsJob.ParseTransformPairsToQuery(Globals.transformationPairs);
            Globals.streamAnalyticsJob.addTransform(query);
            refreshTransformsGridView(Globals.transformationPairs);

            Globals.streamAnalyticsJob = await AzureStreamAnalyticsJob.getStreamAnalyticsJobAsync(Globals.streamAnalyticsJob.name);
        }
        private void refreshInputsGridView(List<AzureStreamAnalyticsInput> inputs)
        {
            dataGridView_inputs.Columns.Clear();

            dataGridView_inputs.AutoGenerateColumns = false;
            dataGridView_inputs.AllowUserToAddRows = false;

            DataGridViewTextBoxColumn colName = new DataGridViewTextBoxColumn();
            colName.HeaderText = "Input name";
            colName.DataPropertyName = "name";
            dataGridView_inputs.Columns.Add(colName);

            DataGridViewTextBoxColumn colIotHub = new DataGridViewTextBoxColumn();
            colIotHub.HeaderText = "IoT hub";
            colIotHub.DataPropertyName = "datasource";
            dataGridView_inputs.Columns.Add(colIotHub);

            dataGridView_inputs.ReadOnly = true;
            dataGridView_inputs.AllowUserToAddRows = false;
            dataGridView_inputs.RowHeadersVisible = false;
            dataGridView_inputs.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            if(inputs != null && inputs.Count>0)
            {
                BindingList<AzureStreamAnalyticsInput> bindingList = new BindingList<AzureStreamAnalyticsInput>(inputs);
                dataGridView_inputs.DataSource = bindingList;
            }else
            {
                dataGridView_inputs.Rows.Clear();
                button_inputRemove.Enabled = false;
            }
        }
        private async void dataGridView_inputs_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                Globals.input = (AzureStreamAnalyticsInput)dataGridView_inputs.CurrentRow.DataBoundItem;
                button_inputRemove.Enabled = true;
                await CheckJobStateAsync();
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
            }
        }
        //Inputs END


        //Outputs START
        private async void button_AddOutput_Click(object sender, EventArgs e)
        {
            using (var newOutputForm = new Form_NewStreamAnalyticsOutput())
            {
                DialogResult result = newOutputForm.ShowDialog();
                if (result == DialogResult.OK)
                {
                    String newOutputName = newOutputForm.newOutputName;
                    AzureStorageAccount storage = newOutputForm.storageAccount;
                    
                    Globals.streamAnalyticsJob.addOutput(storage, "values", newOutputName);
                    Globals.outputList = Globals.streamAnalyticsJob.listOutputs();
                    Globals.streamAnalyticsJob = await AzureStreamAnalyticsJob.getStreamAnalyticsJobAsync(Globals.streamAnalyticsJob.name);
                    refreshOutputsGridView(Globals.outputList);
                }
            }
            await CheckJobStateAsync();
        }
        private async void button_RemoveOutput_Click(object sender, EventArgs e)
        {
            removeStreamOutput(Globals.output);
            await CheckJobStateAsync();
        }
        private async void removeStreamOutput(AzureStreamAnalyticsOutput output)
        {
            Globals.streamAnalyticsJob.removeOutput(output.name);
            Globals.outputList = Globals.streamAnalyticsJob.listOutputs();
            refreshOutputsGridView(Globals.outputList);

            foreach (AzureTransformationPair pair in Globals.transformationPairs)
            {
                if (pair.outputName.Equals(output.name))
                {
                    Globals.transformationPairs.Remove(pair);
                    break;
                }
            }

            String query = AzureStreamAnalyticsJob.ParseTransformPairsToQuery(Globals.transformationPairs);
            if(String.IsNullOrEmpty(query))
            {
                Globals.streamAnalyticsJob.addTransform(" ");
            }
            else
            {
                Globals.streamAnalyticsJob.addTransform(query);
            }

            refreshTransformsGridView(Globals.transformationPairs);

            Globals.streamAnalyticsJob = await AzureStreamAnalyticsJob.getStreamAnalyticsJobAsync(Globals.streamAnalyticsJob.name);
        }
        private void refreshOutputsGridView(List<AzureStreamAnalyticsOutput> outputs)
        {
            dataGridView_outputs.Columns.Clear();

            dataGridView_outputs.AutoGenerateColumns = false;
            dataGridView_outputs.AllowUserToAddRows = false;

            DataGridViewTextBoxColumn colName = new DataGridViewTextBoxColumn();
            colName.HeaderText = "Output name";
            colName.DataPropertyName = "name";
            dataGridView_outputs.Columns.Add(colName);

            DataGridViewTextBoxColumn colStorage = new DataGridViewTextBoxColumn();
            colStorage.HeaderText = "Storage account";
            colStorage.DataPropertyName = "datasource";
            dataGridView_outputs.Columns.Add(colStorage);

            dataGridView_outputs.ReadOnly = true;
            dataGridView_outputs.AllowUserToAddRows = false;
            dataGridView_outputs.RowHeadersVisible = false;
            dataGridView_outputs.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            if (outputs != null && outputs.Count > 0)
            {
                BindingList<AzureStreamAnalyticsOutput> bindingList = new BindingList<AzureStreamAnalyticsOutput>(outputs);
                dataGridView_outputs.DataSource = bindingList;
            }
            else
            {
                dataGridView_outputs.Rows.Clear();
                button_outputRemove.Enabled = false;
            }
        }
        private async void dataGridView_outputs_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                Globals.output = (AzureStreamAnalyticsOutput)dataGridView_outputs.CurrentRow.DataBoundItem;
                button_outputRemove.Enabled = true;
                await CheckJobStateAsync();
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
            }
        }
        //Outputs END


        //Transformation START
        private async void button_addTransformationPair_Click(object sender, EventArgs e)
        {
            using (var newTransfomationForm = new Form_NewStreamAnalyticsTransform())
            {
                DialogResult result = newTransfomationForm.ShowDialog();
                if (result == DialogResult.OK)
                {
                    if(!Globals.transformationPairs.Contains(newTransfomationForm.transformationPair))
                    {
                        Globals.transformationPairs.Add(newTransfomationForm.transformationPair);
                    }

                    //label_transformInfo.Font = new Font("Arial", 8, FontStyle.Regular);
                    String query = AzureStreamAnalyticsJob.ParseTransformPairsToQuery(Globals.transformationPairs);
                    Globals.streamAnalyticsJob.addTransform(query);
                    Thread.Sleep(1000);
                    Globals.streamAnalyticsJob = await AzureStreamAnalyticsJob.getStreamAnalyticsJobAsync(Globals.streamAnalyticsJob.name);
                    Globals.transformationPairs = Globals.streamAnalyticsJob.ParseQueryToTransformPairs();
                    refreshTransformsGridView(Globals.transformationPairs);
                }
            }
            await CheckJobStateAsync();
        }
        private async void button_RemoveTransformationPair_Click(object sender, EventArgs e)
        {
            Globals.transformationPairs.Remove(Globals.transformationPair);

            String query = AzureStreamAnalyticsJob.ParseTransformPairsToQuery(Globals.transformationPairs);
            Globals.streamAnalyticsJob.addTransform(query);
            Thread.Sleep(1000);
            Globals.streamAnalyticsJob = await AzureStreamAnalyticsJob.getStreamAnalyticsJobAsync(Globals.streamAnalyticsJob.name);
            Globals.transformationPairs = Globals.streamAnalyticsJob.ParseQueryToTransformPairs();
            refreshTransformsGridView(Globals.transformationPairs);
            await CheckJobStateAsync();
        }
        private void refreshTransformsGridView(List<AzureTransformationPair> transgormations)
        {
            dataGridView_transforms.Columns.Clear();

            dataGridView_transforms.AutoGenerateColumns = false;
            dataGridView_transforms.AllowUserToAddRows = false;
            dataGridView_transforms.ReadOnly = true;
            dataGridView_transforms.AllowUserToAddRows = false;
            dataGridView_transforms.RowHeadersVisible = false;
            dataGridView_transforms.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            DataGridViewTextBoxColumn input = new DataGridViewTextBoxColumn();
            input.HeaderText = "Input name";
            input.DataPropertyName = "inputName";

            DataGridViewTextBoxColumn output = new DataGridViewTextBoxColumn();
            output.HeaderText = "Output name";
            output.DataPropertyName = "outputName";

            dataGridView_transforms.Columns.Add(input);
            dataGridView_transforms.Columns.Add(output);

            if (transgormations != null && transgormations.Count > 0)
            {
                BindingList<AzureTransformationPair> bindingList = new BindingList<AzureTransformationPair>(transgormations);
                dataGridView_transforms.DataSource = bindingList;
            }
            else
            {
                dataGridView_transforms.Rows.Clear();
                button_transformRemove.Enabled = false;
            }
        }
        private async void Button_start_Click(object sender, EventArgs e)
        {
            blockUI();
            Cursor = Cursors.WaitCursor;
            String uri = "https://management.azure.com/subscriptions/"+Globals.subscription.subscriptionId+
                "/resourceGroups/"+Globals.resourceGroup.name+
                "/providers/Microsoft.StreamAnalytics/streamingjobs/"+Globals.streamAnalyticsJob.name+
                "/start?api-version=2019-06-01";
            String body = "";
            REST.doPOST(uri, body,  Globals.token);

            label_jobState.Text = "Job state: Starting.";

            await Task.Run(() => Globals.streamAnalyticsJob.WaitForJobStartAsync());
            Cursor = Cursors.Default;
            label_jobState.Text = "Job state: Running.";
        }
        private async void Stop_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            String uri = "https://management.azure.com/subscriptions/" + Globals.subscription.subscriptionId + "" +
                "/resourceGroups/" + Globals.resourceGroup.name + "" +
                "/providers/Microsoft.StreamAnalytics/streamingjobs/" + Globals.streamAnalyticsJob.name + "" +
                "/stop?api-version=2019-06-01";
            String body = "";
            REST.doPOST(uri, body, Globals.token);

            label_jobState.Text = "Job state: Stopping.";

            await Globals.streamAnalyticsJob.WaitForJobStopAsync();
            unblockUI();
            Cursor = Cursors.Default;
        }
        private async void DataGridView_transforms_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                Globals.transformationPair = (AzureTransformationPair)dataGridView_transforms.CurrentRow.DataBoundItem;
                button_transformRemove.Enabled = true;
                await CheckJobStateAsync();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
            
        }
        //Transformation END


        private async Task<int> CheckJobStateAsync()
        {
            AzureStreamAnalyticsJob job =await  AzureStreamAnalyticsJob.getStreamAnalyticsJobAsync(Globals.streamAnalyticsJob.name);
            if(job.properties.jobState.Equals("Running"))
            {
                blockUI();
                label_jobState.Text = "Job state: Running";
                return 2;
            }
            if (job.properties.jobState.Equals("Starting"))
            {
                blockUI();
                label_jobState.Text = "Job state: Starting";
                return 1;
            }
            if (job.properties.jobState.Equals("Created"))
            {
                unblockUI();
                label_jobState.Text = "Job state: Created";
                if (!Globals.streamAnalyticsJob.IsReadyToStart())
                {
                    blockUINotReady();
                    label_jobState.Text = "Job is not configured for start";
                }
                return 0;
            }
            if (job.properties.jobState.Equals("Stopped"))
            {
                unblockUI();
                label_jobState.Text = "Job state: Stopped";
                if (!Globals.streamAnalyticsJob.IsReadyToStart())
                {
                    blockUINotReady();
                    label_jobState.Text = "Job is not configured for start";
                }
                return 3;
            }
            if (job.properties.jobState.Equals("Failed"))
            {
                unblockUI();
                if (!Globals.streamAnalyticsJob.IsReadyToStart())
                {
                    blockUINotReady();
                    label_jobState.Text = "Job is not configured for start";
                }
                label_jobState.Text = "Job state: Failed";
                return -1;
            }
            return -2;
        }
    }
}
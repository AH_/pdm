﻿using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AzureConfig6
{
    public partial class Form_FirebaseConnection : Form
    {
        public String firebaseUrl;
        public String firebaseSecret;
        public FirebaseConnectionData connectionData;

        public Form_FirebaseConnection()
        {
            InitializeComponent();

            label_info.Text = "";
            if (Globals.firebaseConnectionData != null)
            {
                if (!String.IsNullOrEmpty(Globals.firebaseConnectionData.firebaseUrl))
                {
                    textBox_firebaseUrl.Text = Globals.firebaseConnectionData.firebaseUrl;
                }
                if (!String.IsNullOrEmpty(Globals.firebaseConnectionData.firebaseSecret))
                {
                    textBox_firebaseSecret.Text = Globals.firebaseConnectionData.firebaseSecret;
                }
            }
            checkTextBoxes();
        }

        private async void button_login_Click(object sender, EventArgs e)
        {
            firebaseUrl = textBox_firebaseUrl.Text;
            firebaseSecret = textBox_firebaseSecret.Text;
            connectionData = new FirebaseConnectionData(firebaseUrl, firebaseSecret, null);

            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void Button_cancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void checkTextBoxes()
        {
            if (!String.IsNullOrEmpty(textBox_firebaseUrl.Text) && !String.IsNullOrWhiteSpace(textBox_firebaseUrl.Text))
            {
                pictureBox_url.Image = Globals.iconSuccess;
                button_saveFirebase.Enabled = true;
            }
            else
            {
                pictureBox_url.Image = Globals.iconError;
                button_saveFirebase.Enabled = false;
            }

            if (!String.IsNullOrEmpty(textBox_firebaseSecret.Text) && !String.IsNullOrWhiteSpace(textBox_firebaseSecret.Text))
            {
                pictureBox1_secret.Image = Globals.iconSuccess;
                button_saveFirebase.Enabled = true;
            }
            else
            {
                pictureBox1_secret.Image = Globals.iconError;
                button_saveFirebase.Enabled = false;
            }
        }

        private void TextBox_firebaseUrl_TextChanged(object sender, EventArgs e)
        {
            checkTextBoxes();
        }

        private void TextBox_firebaseSecret_TextChanged(object sender, EventArgs e)
        {
            checkTextBoxes();
        }
    }
}
﻿namespace AzureConfig6
{
    partial class Form_FirebaseUser
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.label_email = new System.Windows.Forms.Label();
            this.button_saveFirebase = new System.Windows.Forms.Button();
            this.label_info = new System.Windows.Forms.Label();
            this.button_cancel = new System.Windows.Forms.Button();
            this.pictureBox_mail = new System.Windows.Forms.PictureBox();
            this.comboBox_firebaseUser = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_mail)).BeginInit();
            this.SuspendLayout();
            // 
            // label_email
            // 
            this.label_email.AutoSize = true;
            this.label_email.Location = new System.Drawing.Point(16, 30);
            this.label_email.Name = "label_email";
            this.label_email.Size = new System.Drawing.Size(35, 13);
            this.label_email.TabIndex = 0;
            this.label_email.Text = "Email:";
            // 
            // button_saveFirebase
            // 
            this.button_saveFirebase.Location = new System.Drawing.Point(462, 55);
            this.button_saveFirebase.Name = "button_saveFirebase";
            this.button_saveFirebase.Size = new System.Drawing.Size(107, 30);
            this.button_saveFirebase.TabIndex = 6;
            this.button_saveFirebase.Text = "Save";
            this.button_saveFirebase.UseVisualStyleBackColor = true;
            this.button_saveFirebase.Click += new System.EventHandler(this.button_login_Click);
            // 
            // label_info
            // 
            this.label_info.AutoSize = true;
            this.label_info.Location = new System.Drawing.Point(16, 64);
            this.label_info.Name = "label_info";
            this.label_info.Size = new System.Drawing.Size(35, 13);
            this.label_info.TabIndex = 8;
            this.label_info.Text = "label1";
            // 
            // button_cancel
            // 
            this.button_cancel.Location = new System.Drawing.Point(349, 55);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new System.Drawing.Size(107, 30);
            this.button_cancel.TabIndex = 9;
            this.button_cancel.Text = "Cancel";
            this.button_cancel.UseVisualStyleBackColor = true;
            this.button_cancel.Click += new System.EventHandler(this.Button_cancel_Click);
            // 
            // pictureBox_mail
            // 
            this.pictureBox_mail.Location = new System.Drawing.Point(544, 24);
            this.pictureBox_mail.Name = "pictureBox_mail";
            this.pictureBox_mail.Size = new System.Drawing.Size(25, 25);
            this.pictureBox_mail.TabIndex = 15;
            this.pictureBox_mail.TabStop = false;
            // 
            // comboBox_firebaseUser
            // 
            this.comboBox_firebaseUser.FormattingEnabled = true;
            this.comboBox_firebaseUser.Location = new System.Drawing.Point(57, 26);
            this.comboBox_firebaseUser.Name = "comboBox_firebaseUser";
            this.comboBox_firebaseUser.Size = new System.Drawing.Size(480, 21);
            this.comboBox_firebaseUser.TabIndex = 16;
            this.comboBox_firebaseUser.SelectedIndexChanged += new System.EventHandler(this.ComboBox_firebaseUser_SelectedIndexChanged);
            this.comboBox_firebaseUser.TextChanged += new System.EventHandler(this.ComboBox_firebaseUser_TextChanged);
            // 
            // Form_FirebaseUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(577, 95);
            this.Controls.Add(this.comboBox_firebaseUser);
            this.Controls.Add(this.pictureBox_mail);
            this.Controls.Add(this.button_cancel);
            this.Controls.Add(this.label_info);
            this.Controls.Add(this.button_saveFirebase);
            this.Controls.Add(this.label_email);
            this.Name = "Form_FirebaseUser";
            this.Text = "Set up Firebase user";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_mail)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_email;
        private System.Windows.Forms.Button button_saveFirebase;
        private System.Windows.Forms.Label label_info;
        private System.Windows.Forms.Button button_cancel;
        private System.Windows.Forms.PictureBox pictureBox_mail;
        private System.Windows.Forms.ComboBox comboBox_firebaseUser;
    }
}
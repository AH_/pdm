﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using ObjectFlattenerRecomposer;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AzureConfig6
{
    public partial class Form_DeviceSpecificationSetings : Form
    {
        String accountName = Globals.storageAccount.name;
        String accountKey = Globals.storageAccount.keys[0].value;

        SpecificationTableRow refDeviceSpec;
        SpecificationTableRow deviceSpec;
        List<Sensor> sensors;
        List<Sensor> sensorsInTable;
        Boolean specificationChanged = false;

        public Form_DeviceSpecificationSetings(SpecificationTableRow deviceSpecification)
        {
            InitializeComponent();
            sensors = new List<Sensor>();
            sensorsInTable = new List<Sensor>();

            if(deviceSpecification != null)
            {
                this.deviceSpec = deviceSpecification;
            }
            else
            {
                this.deviceSpec = new SpecificationTableRow();
            }

            refDeviceSpec = new SpecificationTableRow(deviceSpecification);
            deviceSpec = new SpecificationTableRow(deviceSpecification);

            label_owner_value.Text = deviceSpec.PartitionKey;
            label_model_value.Text = deviceSpec.Model;
            label_version_value.Text = deviceSpec.Version;
        }

        public Form_DeviceSpecificationSetings(String owner, String serial)
        {
            InitializeComponent();
            sensors = new List<Sensor>();
            sensorsInTable = new List<Sensor>();

            this.deviceSpec = new SpecificationTableRow();

            this.deviceSpec.PartitionKey = owner;
            this.deviceSpec.RowKey = serial;

            label_owner_value.Text = owner;
            label_model_value.Text = "";
            label_version_value.Text = "";
        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            if(SpecificationChanged())
            {
                TableHelper.updateSpecification(accountName, accountKey, refDeviceSpec, deviceSpec);
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private List<Sensor> copyList(List<Sensor> list)
        {
            List<Sensor> resultList = new List<Sensor>();
            foreach(Sensor el in list)
            {
                Sensor s = new Sensor();
                s.id = el.id;
                s.name = el.name;
                s.units = el.units;
                resultList.Add(s);
            }
            return resultList;
        }

        private bool listsEquals(List<Sensor> list1, List<Sensor> list2)
        {
            if(list1.Count == list2.Count)
            {
                for(int i=0; i<list1.Count; i++)
                {
                    if(!list1[i].Equals(list2[i]))
                    {
                        return false;
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool SpecificationChanged()
        {
            if(deviceSpec != null && refDeviceSpec != null)
            {
                if (!deviceSpec.PartitionKey.Equals(refDeviceSpec.PartitionKey) ||
                       !deviceSpec.RowKey.Equals(refDeviceSpec.PartitionKey) ||
                       !deviceSpec.Model.Equals(refDeviceSpec.Model) ||
                       !deviceSpec.Version.Equals(refDeviceSpec.Version))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }

        }

        // Owner START
        private void button_edit_owner_Click(object sender, EventArgs e)
        {
            button_ok.Enabled = false;

            button_edit_owner.Visible = false;
            button_owner_ok.Visible = true;
            button_owner_cancel.Visible = true;

            label_owner_value.Visible = false;
            textBox_owner.Text = deviceSpec.PartitionKey;
            textBox_owner.Visible = true;
        }

        private void button_owner_ok_Click(object sender, EventArgs e)
        {
            button_ok.Enabled = true;

            button_edit_owner.Visible = true;
            button_owner_ok.Visible = false;
            button_owner_cancel.Visible = false;

            if(!String.IsNullOrEmpty(textBox_owner.Text) && !String.IsNullOrWhiteSpace(textBox_owner.Text))
            {
                deviceSpec.PartitionKey = textBox_owner.Text;
                label_owner_value.Text = textBox_owner.Text;
            }else
            {
                label_owner_value.Text = deviceSpec.PartitionKey;
            }
            
            label_owner_value.Visible = true;
            textBox_owner.Visible = false;
        }

        private void button_owner_cancel_Click(object sender, EventArgs e)
        {
            button_ok.Enabled = true;

            button_edit_owner.Visible = true;
            button_owner_ok.Visible = false;
            button_owner_cancel.Visible = false;

            label_owner_value.Text = deviceSpec.PartitionKey;

            label_owner_value.Visible = true;
            textBox_owner.Visible = false;
        }
        // Owner END

        // Model START
        private void button_edit_model_Click(object sender, EventArgs e)
        {
            button_ok.Enabled = false;

            button_edit_model.Visible = false;
            button_model_ok.Visible = true;
            button_model_cancel.Visible = true;

            label_model_value.Visible = false;
            textBox_model.Text = deviceSpec.Model;
            textBox_model.Visible = true;
        }

        private void button_model_ok_Click(object sender, EventArgs e)
        {
            button_ok.Enabled = true;

            button_edit_model.Visible = true;
            button_model_ok.Visible = false;
            button_model_cancel.Visible = false;

            if (!String.IsNullOrEmpty(textBox_model.Text) && !String.IsNullOrWhiteSpace(textBox_model.Text))
            {
                deviceSpec.Model = textBox_model.Text;
                label_model_value.Text = textBox_model.Text;
            }
            else
            {
                label_model_value.Text = deviceSpec.Model;
            }

            label_model_value.Visible = true;
            textBox_model.Visible = false;
        }

        private void button_model_cancel_Click(object sender, EventArgs e)
        {
            button_ok.Enabled = true;

            button_edit_model.Visible = true;
            button_model_ok.Visible = false;
            button_model_cancel.Visible = false;

            label_model_value.Text = deviceSpec.Model;

            label_model_value.Visible = true;
            textBox_model.Visible = false;
        }
        // Model END

        // Version START
        private void button_edit_version_Click(object sender, EventArgs e)
        {
            button_ok.Enabled = false;

            button_edit_version.Visible = false;
            button_version_ok.Visible = true;
            button_version_cancel.Visible = true;

            label_version_value.Visible = false;
            textBox_version.Text = deviceSpec.Version;
            textBox_version.Visible = true;
        }

        private void button_version_ok_Click(object sender, EventArgs e)
        {
            button_ok.Enabled = true;

            button_edit_version.Visible = true;
            button_version_ok.Visible = false;
            button_version_cancel.Visible = false;

            if (!String.IsNullOrEmpty(textBox_version.Text) && !String.IsNullOrWhiteSpace(textBox_version.Text))
            {
                deviceSpec.Version = textBox_version.Text;
                label_version_value.Text = textBox_version.Text;
            }
            else
            {
                label_version_value.Text = deviceSpec.Version;
            }

            label_version_value.Visible = true;
            textBox_version.Visible = false;
        }

        private void button_version_cancel_Click(object sender, EventArgs e)
        {
            button_ok.Enabled = true;

            button_edit_version.Visible = true;
            button_version_ok.Visible = false;
            button_version_cancel.Visible = false;

            label_version_value.Text = deviceSpec.Version;

            label_version_value.Visible = true;
            textBox_version.Visible = false;
        }
        // Version END
    }
}

﻿namespace AzureConfig6
{
    partial class Form_MainMenu
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabControl_mainMenu = new System.Windows.Forms.TabControl();
            this.tabPage_storageAccounts = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label_storages = new System.Windows.Forms.Label();
            this.label_storageInfo = new System.Windows.Forms.Label();
            this.dataGridView_storages = new System.Windows.Forms.DataGridView();
            this.button_removeStorage = new System.Windows.Forms.Button();
            this.button_addStorage = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label_storage_info = new System.Windows.Forms.Label();
            this.label_tables = new System.Windows.Forms.Label();
            this.button_removeTable = new System.Windows.Forms.Button();
            this.dataGridView_tables = new System.Windows.Forms.DataGridView();
            this.button_addTable = new System.Windows.Forms.Button();
            this.tabPage_iotHubs = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label_hubs = new System.Windows.Forms.Label();
            this.dataGridView_hubs = new System.Windows.Forms.DataGridView();
            this.button_hubRemove = new System.Windows.Forms.Button();
            this.button_hubAdd = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label_hubs_info = new System.Windows.Forms.Label();
            this.buttonDeviceSettings = new System.Windows.Forms.Button();
            this.label_iotDevice = new System.Windows.Forms.Label();
            this.button_deviceRemove = new System.Windows.Forms.Button();
            this.button_deviceAdd = new System.Windows.Forms.Button();
            this.dataGridView_device = new System.Windows.Forms.DataGridView();
            this.tabPage_streamAnalytics = new System.Windows.Forms.TabPage();
            this.label_stream_info = new System.Windows.Forms.Label();
            this.button_settingsStreamAnalytics = new System.Windows.Forms.Button();
            this.button_removeStreamAnalytics = new System.Windows.Forms.Button();
            this.button_addStreamAnalytics = new System.Windows.Forms.Button();
            this.dataGridView_StreamAnalytics = new System.Windows.Forms.DataGridView();
            this.label_StreamAnalytics = new System.Windows.Forms.Label();
            this.button_back = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.firebaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serUpFirebaseConnectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seuUpFirebaseUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControl_mainMenu.SuspendLayout();
            this.tabPage_storageAccounts.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_storages)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_tables)).BeginInit();
            this.tabPage_iotHubs.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_hubs)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_device)).BeginInit();
            this.tabPage_streamAnalytics.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_StreamAnalytics)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tabControl_mainMenu);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.button_back);
            this.splitContainer1.Size = new System.Drawing.Size(734, 437);
            this.splitContainer1.SplitterDistance = 395;
            this.splitContainer1.TabIndex = 1;
            // 
            // tabControl_mainMenu
            // 
            this.tabControl_mainMenu.Controls.Add(this.tabPage_storageAccounts);
            this.tabControl_mainMenu.Controls.Add(this.tabPage_iotHubs);
            this.tabControl_mainMenu.Controls.Add(this.tabPage_streamAnalytics);
            this.tabControl_mainMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl_mainMenu.Location = new System.Drawing.Point(0, 0);
            this.tabControl_mainMenu.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl_mainMenu.Name = "tabControl_mainMenu";
            this.tabControl_mainMenu.SelectedIndex = 0;
            this.tabControl_mainMenu.Size = new System.Drawing.Size(734, 395);
            this.tabControl_mainMenu.TabIndex = 0;
            // 
            // tabPage_storageAccounts
            // 
            this.tabPage_storageAccounts.Controls.Add(this.tableLayoutPanel1);
            this.tabPage_storageAccounts.Location = new System.Drawing.Point(4, 22);
            this.tabPage_storageAccounts.Margin = new System.Windows.Forms.Padding(0);
            this.tabPage_storageAccounts.Name = "tabPage_storageAccounts";
            this.tabPage_storageAccounts.Size = new System.Drawing.Size(726, 369);
            this.tabPage_storageAccounts.TabIndex = 0;
            this.tabPage_storageAccounts.Text = "Storage Accounts";
            this.tabPage_storageAccounts.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(726, 369);
            this.tableLayoutPanel1.TabIndex = 27;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label_storages);
            this.panel1.Controls.Add(this.label_storageInfo);
            this.panel1.Controls.Add(this.dataGridView_storages);
            this.panel1.Controls.Add(this.button_removeStorage);
            this.panel1.Controls.Add(this.button_addStorage);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(726, 184);
            this.panel1.TabIndex = 0;
            // 
            // label_storages
            // 
            this.label_storages.AllowDrop = true;
            this.label_storages.AutoSize = true;
            this.label_storages.Location = new System.Drawing.Point(10, 10);
            this.label_storages.Name = "label_storages";
            this.label_storages.Size = new System.Drawing.Size(90, 13);
            this.label_storages.TabIndex = 18;
            this.label_storages.Text = "Storage Account:";
            // 
            // label_storageInfo
            // 
            this.label_storageInfo.AutoSize = true;
            this.label_storageInfo.Location = new System.Drawing.Point(11, 165);
            this.label_storageInfo.Name = "label_storageInfo";
            this.label_storageInfo.Size = new System.Drawing.Size(64, 13);
            this.label_storageInfo.TabIndex = 26;
            this.label_storageInfo.Text = "Storage info";
            // 
            // dataGridView_storages
            // 
            this.dataGridView_storages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_storages.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_storages.Location = new System.Drawing.Point(13, 25);
            this.dataGridView_storages.Name = "dataGridView_storages";
            this.dataGridView_storages.RowHeadersWidth = 51;
            this.dataGridView_storages.Size = new System.Drawing.Size(710, 127);
            this.dataGridView_storages.TabIndex = 19;
            this.dataGridView_storages.SelectionChanged += new System.EventHandler(this.dataGridView_sAccs_SelectionChanged);
            // 
            // button_removeStorage
            // 
            this.button_removeStorage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_removeStorage.Location = new System.Drawing.Point(648, 159);
            this.button_removeStorage.Name = "button_removeStorage";
            this.button_removeStorage.Size = new System.Drawing.Size(75, 23);
            this.button_removeStorage.TabIndex = 21;
            this.button_removeStorage.Text = "Remove";
            this.button_removeStorage.UseVisualStyleBackColor = true;
            this.button_removeStorage.Click += new System.EventHandler(this.button_removeStorage_Click);
            // 
            // button_addStorage
            // 
            this.button_addStorage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_addStorage.Location = new System.Drawing.Point(567, 159);
            this.button_addStorage.Name = "button_addStorage";
            this.button_addStorage.Size = new System.Drawing.Size(75, 23);
            this.button_addStorage.TabIndex = 20;
            this.button_addStorage.Text = "Add";
            this.button_addStorage.UseVisualStyleBackColor = true;
            this.button_addStorage.Click += new System.EventHandler(this.button_addStorage_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label_storage_info);
            this.panel2.Controls.Add(this.label_tables);
            this.panel2.Controls.Add(this.button_removeTable);
            this.panel2.Controls.Add(this.dataGridView_tables);
            this.panel2.Controls.Add(this.button_addTable);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 184);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(726, 185);
            this.panel2.TabIndex = 1;
            // 
            // label_storage_info
            // 
            this.label_storage_info.AutoSize = true;
            this.label_storage_info.Location = new System.Drawing.Point(243, 164);
            this.label_storage_info.Name = "label_storage_info";
            this.label_storage_info.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label_storage_info.Size = new System.Drawing.Size(35, 13);
            this.label_storage_info.TabIndex = 47;
            this.label_storage_info.Text = "label1";
            this.label_storage_info.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label_tables
            // 
            this.label_tables.AutoSize = true;
            this.label_tables.Location = new System.Drawing.Point(10, 10);
            this.label_tables.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_tables.Name = "label_tables";
            this.label_tables.Size = new System.Drawing.Size(113, 13);
            this.label_tables.TabIndex = 22;
            this.label_tables.Text = "Chose a storage table:";
            // 
            // button_removeTable
            // 
            this.button_removeTable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_removeTable.Location = new System.Drawing.Point(648, 159);
            this.button_removeTable.Name = "button_removeTable";
            this.button_removeTable.Size = new System.Drawing.Size(75, 23);
            this.button_removeTable.TabIndex = 25;
            this.button_removeTable.Text = "Remove";
            this.button_removeTable.UseVisualStyleBackColor = true;
            this.button_removeTable.Click += new System.EventHandler(this.button_removeTable_Click);
            // 
            // dataGridView_tables
            // 
            this.dataGridView_tables.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_tables.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_tables.Location = new System.Drawing.Point(13, 28);
            this.dataGridView_tables.Name = "dataGridView_tables";
            this.dataGridView_tables.RowHeadersWidth = 51;
            this.dataGridView_tables.Size = new System.Drawing.Size(710, 127);
            this.dataGridView_tables.TabIndex = 23;
            this.dataGridView_tables.SelectionChanged += new System.EventHandler(this.dataGridView_tables_SelectionChanged);
            // 
            // button_addTable
            // 
            this.button_addTable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_addTable.Location = new System.Drawing.Point(567, 159);
            this.button_addTable.Name = "button_addTable";
            this.button_addTable.Size = new System.Drawing.Size(75, 23);
            this.button_addTable.TabIndex = 24;
            this.button_addTable.Text = "Add";
            this.button_addTable.UseVisualStyleBackColor = true;
            this.button_addTable.Click += new System.EventHandler(this.button_addTable_Click);
            // 
            // tabPage_iotHubs
            // 
            this.tabPage_iotHubs.Controls.Add(this.tableLayoutPanel2);
            this.tabPage_iotHubs.Location = new System.Drawing.Point(4, 22);
            this.tabPage_iotHubs.Margin = new System.Windows.Forms.Padding(0);
            this.tabPage_iotHubs.Name = "tabPage_iotHubs";
            this.tabPage_iotHubs.Size = new System.Drawing.Size(726, 369);
            this.tabPage_iotHubs.TabIndex = 1;
            this.tabPage_iotHubs.Text = "IoT Hubs";
            this.tabPage_iotHubs.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.panel3, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel4, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(726, 369);
            this.tableLayoutPanel2.TabIndex = 45;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label_hubs);
            this.panel3.Controls.Add(this.dataGridView_hubs);
            this.panel3.Controls.Add(this.button_hubRemove);
            this.panel3.Controls.Add(this.button_hubAdd);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(726, 184);
            this.panel3.TabIndex = 0;
            // 
            // label_hubs
            // 
            this.label_hubs.AutoSize = true;
            this.label_hubs.Location = new System.Drawing.Point(10, 10);
            this.label_hubs.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_hubs.Name = "label_hubs";
            this.label_hubs.Size = new System.Drawing.Size(54, 13);
            this.label_hubs.TabIndex = 37;
            this.label_hubs.Text = "IoT Hubs:";
            // 
            // dataGridView_hubs
            // 
            this.dataGridView_hubs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_hubs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_hubs.Location = new System.Drawing.Point(13, 25);
            this.dataGridView_hubs.Name = "dataGridView_hubs";
            this.dataGridView_hubs.RowHeadersWidth = 51;
            this.dataGridView_hubs.Size = new System.Drawing.Size(710, 127);
            this.dataGridView_hubs.TabIndex = 38;
            this.dataGridView_hubs.SelectionChanged += new System.EventHandler(this.dataGridView_hubs_SelectionChanged);
            // 
            // button_hubRemove
            // 
            this.button_hubRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_hubRemove.Location = new System.Drawing.Point(648, 159);
            this.button_hubRemove.Name = "button_hubRemove";
            this.button_hubRemove.Size = new System.Drawing.Size(75, 23);
            this.button_hubRemove.TabIndex = 40;
            this.button_hubRemove.Text = "Remove";
            this.button_hubRemove.UseVisualStyleBackColor = true;
            this.button_hubRemove.Click += new System.EventHandler(this.button_removeHub_Click);
            // 
            // button_hubAdd
            // 
            this.button_hubAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_hubAdd.Location = new System.Drawing.Point(567, 159);
            this.button_hubAdd.Name = "button_hubAdd";
            this.button_hubAdd.Size = new System.Drawing.Size(75, 23);
            this.button_hubAdd.TabIndex = 39;
            this.button_hubAdd.Text = "Add";
            this.button_hubAdd.UseVisualStyleBackColor = true;
            this.button_hubAdd.Click += new System.EventHandler(this.button_addHub_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label_hubs_info);
            this.panel4.Controls.Add(this.buttonDeviceSettings);
            this.panel4.Controls.Add(this.label_iotDevice);
            this.panel4.Controls.Add(this.button_deviceRemove);
            this.panel4.Controls.Add(this.button_deviceAdd);
            this.panel4.Controls.Add(this.dataGridView_device);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 184);
            this.panel4.Margin = new System.Windows.Forms.Padding(0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(726, 185);
            this.panel4.TabIndex = 1;
            // 
            // label_hubs_info
            // 
            this.label_hubs_info.AutoSize = true;
            this.label_hubs_info.Location = new System.Drawing.Point(216, 164);
            this.label_hubs_info.Name = "label_hubs_info";
            this.label_hubs_info.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label_hubs_info.Size = new System.Drawing.Size(35, 13);
            this.label_hubs_info.TabIndex = 46;
            this.label_hubs_info.Text = "label1";
            this.label_hubs_info.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // buttonDeviceSettings
            // 
            this.buttonDeviceSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonDeviceSettings.Location = new System.Drawing.Point(13, 159);
            this.buttonDeviceSettings.Name = "buttonDeviceSettings";
            this.buttonDeviceSettings.Size = new System.Drawing.Size(75, 23);
            this.buttonDeviceSettings.TabIndex = 45;
            this.buttonDeviceSettings.Text = "Settings";
            this.buttonDeviceSettings.UseVisualStyleBackColor = true;
            this.buttonDeviceSettings.Click += new System.EventHandler(this.buttonDeviceSettings_Click);
            // 
            // label_iotDevice
            // 
            this.label_iotDevice.AutoSize = true;
            this.label_iotDevice.Location = new System.Drawing.Point(10, 10);
            this.label_iotDevice.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_iotDevice.Name = "label_iotDevice";
            this.label_iotDevice.Size = new System.Drawing.Size(68, 13);
            this.label_iotDevice.TabIndex = 41;
            this.label_iotDevice.Text = "IoT Devices:";
            // 
            // button_deviceRemove
            // 
            this.button_deviceRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_deviceRemove.Location = new System.Drawing.Point(648, 159);
            this.button_deviceRemove.Name = "button_deviceRemove";
            this.button_deviceRemove.Size = new System.Drawing.Size(75, 23);
            this.button_deviceRemove.TabIndex = 44;
            this.button_deviceRemove.Text = "Remove";
            this.button_deviceRemove.UseVisualStyleBackColor = true;
            this.button_deviceRemove.Click += new System.EventHandler(this.button_removeDevice_Click);
            // 
            // button_deviceAdd
            // 
            this.button_deviceAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_deviceAdd.Location = new System.Drawing.Point(567, 159);
            this.button_deviceAdd.Name = "button_deviceAdd";
            this.button_deviceAdd.Size = new System.Drawing.Size(75, 23);
            this.button_deviceAdd.TabIndex = 43;
            this.button_deviceAdd.Text = "Add";
            this.button_deviceAdd.UseVisualStyleBackColor = true;
            this.button_deviceAdd.Click += new System.EventHandler(this.button_addDevice_Click);
            // 
            // dataGridView_device
            // 
            this.dataGridView_device.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_device.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_device.Location = new System.Drawing.Point(13, 28);
            this.dataGridView_device.Name = "dataGridView_device";
            this.dataGridView_device.RowHeadersWidth = 51;
            this.dataGridView_device.Size = new System.Drawing.Size(710, 127);
            this.dataGridView_device.TabIndex = 42;
            this.dataGridView_device.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_device_CellContentDoubleClick);
            this.dataGridView_device.SelectionChanged += new System.EventHandler(this.dataGridView_device_SelectionChanged);
            // 
            // tabPage_streamAnalytics
            // 
            this.tabPage_streamAnalytics.Controls.Add(this.label_stream_info);
            this.tabPage_streamAnalytics.Controls.Add(this.button_settingsStreamAnalytics);
            this.tabPage_streamAnalytics.Controls.Add(this.button_removeStreamAnalytics);
            this.tabPage_streamAnalytics.Controls.Add(this.button_addStreamAnalytics);
            this.tabPage_streamAnalytics.Controls.Add(this.dataGridView_StreamAnalytics);
            this.tabPage_streamAnalytics.Controls.Add(this.label_StreamAnalytics);
            this.tabPage_streamAnalytics.Location = new System.Drawing.Point(4, 22);
            this.tabPage_streamAnalytics.Name = "tabPage_streamAnalytics";
            this.tabPage_streamAnalytics.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_streamAnalytics.Size = new System.Drawing.Size(726, 369);
            this.tabPage_streamAnalytics.TabIndex = 2;
            this.tabPage_streamAnalytics.Text = "Stream Analytics Jobs";
            this.tabPage_streamAnalytics.UseVisualStyleBackColor = true;
            // 
            // label_stream_info
            // 
            this.label_stream_info.AutoSize = true;
            this.label_stream_info.Location = new System.Drawing.Point(239, 340);
            this.label_stream_info.Name = "label_stream_info";
            this.label_stream_info.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label_stream_info.Size = new System.Drawing.Size(35, 13);
            this.label_stream_info.TabIndex = 47;
            this.label_stream_info.Text = "label1";
            this.label_stream_info.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // button_settingsStreamAnalytics
            // 
            this.button_settingsStreamAnalytics.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_settingsStreamAnalytics.Location = new System.Drawing.Point(13, 335);
            this.button_settingsStreamAnalytics.Name = "button_settingsStreamAnalytics";
            this.button_settingsStreamAnalytics.Size = new System.Drawing.Size(75, 23);
            this.button_settingsStreamAnalytics.TabIndex = 32;
            this.button_settingsStreamAnalytics.Text = "Settings";
            this.button_settingsStreamAnalytics.UseVisualStyleBackColor = true;
            this.button_settingsStreamAnalytics.Click += new System.EventHandler(this.button_settingsStreamJob_Click);
            // 
            // button_removeStreamAnalytics
            // 
            this.button_removeStreamAnalytics.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_removeStreamAnalytics.Location = new System.Drawing.Point(648, 335);
            this.button_removeStreamAnalytics.Name = "button_removeStreamAnalytics";
            this.button_removeStreamAnalytics.Size = new System.Drawing.Size(75, 23);
            this.button_removeStreamAnalytics.TabIndex = 31;
            this.button_removeStreamAnalytics.Text = "Remove";
            this.button_removeStreamAnalytics.UseVisualStyleBackColor = true;
            this.button_removeStreamAnalytics.Click += new System.EventHandler(this.button_removeStreamJob_Click);
            // 
            // button_addStreamAnalytics
            // 
            this.button_addStreamAnalytics.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_addStreamAnalytics.Location = new System.Drawing.Point(567, 335);
            this.button_addStreamAnalytics.Name = "button_addStreamAnalytics";
            this.button_addStreamAnalytics.Size = new System.Drawing.Size(75, 23);
            this.button_addStreamAnalytics.TabIndex = 30;
            this.button_addStreamAnalytics.Text = "Add";
            this.button_addStreamAnalytics.UseVisualStyleBackColor = true;
            this.button_addStreamAnalytics.Click += new System.EventHandler(this.button_addStreamJob_Click);
            // 
            // dataGridView_StreamAnalytics
            // 
            this.dataGridView_StreamAnalytics.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_StreamAnalytics.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_StreamAnalytics.Location = new System.Drawing.Point(13, 25);
            this.dataGridView_StreamAnalytics.Name = "dataGridView_StreamAnalytics";
            this.dataGridView_StreamAnalytics.RowHeadersWidth = 51;
            this.dataGridView_StreamAnalytics.Size = new System.Drawing.Size(710, 304);
            this.dataGridView_StreamAnalytics.TabIndex = 29;
            this.dataGridView_StreamAnalytics.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_StreamAnalytics_CellDoubleClick);
            this.dataGridView_StreamAnalytics.SelectionChanged += new System.EventHandler(this.dataGridView_StreamAnalytics_SelectionChanged);
            // 
            // label_StreamAnalytics
            // 
            this.label_StreamAnalytics.AutoSize = true;
            this.label_StreamAnalytics.Location = new System.Drawing.Point(10, 10);
            this.label_StreamAnalytics.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_StreamAnalytics.Name = "label_StreamAnalytics";
            this.label_StreamAnalytics.Size = new System.Drawing.Size(110, 13);
            this.label_StreamAnalytics.TabIndex = 28;
            this.label_StreamAnalytics.Text = "Stream Analytics jobs:";
            // 
            // button_back
            // 
            this.button_back.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_back.Location = new System.Drawing.Point(12, 5);
            this.button_back.Name = "button_back";
            this.button_back.Size = new System.Drawing.Size(80, 23);
            this.button_back.TabIndex = 0;
            this.button_back.Text = "Back";
            this.button_back.UseVisualStyleBackColor = true;
            this.button_back.Click += new System.EventHandler(this.button_back_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.firebaseToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(734, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // firebaseToolStripMenuItem
            // 
            this.firebaseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.serUpFirebaseConnectionToolStripMenuItem,
            this.seuUpFirebaseUserToolStripMenuItem});
            this.firebaseToolStripMenuItem.Name = "firebaseToolStripMenuItem";
            this.firebaseToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.firebaseToolStripMenuItem.Text = "Firebase";
            // 
            // serUpFirebaseConnectionToolStripMenuItem
            // 
            this.serUpFirebaseConnectionToolStripMenuItem.Name = "serUpFirebaseConnectionToolStripMenuItem";
            this.serUpFirebaseConnectionToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.serUpFirebaseConnectionToolStripMenuItem.Text = "Firebase connection";
            this.serUpFirebaseConnectionToolStripMenuItem.Click += new System.EventHandler(this.SetUpFirebaseConnectionToolStripMenuItem_Click);
            // 
            // seuUpFirebaseUserToolStripMenuItem
            // 
            this.seuUpFirebaseUserToolStripMenuItem.Name = "seuUpFirebaseUserToolStripMenuItem";
            this.seuUpFirebaseUserToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.seuUpFirebaseUserToolStripMenuItem.Text = "Firebase user";
            this.seuUpFirebaseUserToolStripMenuItem.Click += new System.EventHandler(this.SeuUpFirebaseUserToolStripMenuItem_Click);
            // 
            // Form_MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 461);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form_MainMenu";
            this.Load += new System.EventHandler(this.Form_MainMenu_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tabControl_mainMenu.ResumeLayout(false);
            this.tabPage_storageAccounts.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_storages)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_tables)).EndInit();
            this.tabPage_iotHubs.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_hubs)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_device)).EndInit();
            this.tabPage_streamAnalytics.ResumeLayout(false);
            this.tabPage_streamAnalytics.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_StreamAnalytics)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button button_back;
        private System.Windows.Forms.TabControl tabControl_mainMenu;
        private System.Windows.Forms.TabPage tabPage_storageAccounts;
        private System.Windows.Forms.Button button_removeTable;
        private System.Windows.Forms.Button button_addTable;
        private System.Windows.Forms.DataGridView dataGridView_tables;
        private System.Windows.Forms.Label label_tables;
        private System.Windows.Forms.Button button_removeStorage;
        private System.Windows.Forms.Button button_addStorage;
        private System.Windows.Forms.DataGridView dataGridView_storages;
        private System.Windows.Forms.Label label_storages;
        private System.Windows.Forms.TabPage tabPage_iotHubs;
        private System.Windows.Forms.Button button_deviceRemove;
        private System.Windows.Forms.Button button_deviceAdd;
        private System.Windows.Forms.DataGridView dataGridView_device;
        private System.Windows.Forms.Label label_iotDevice;
        private System.Windows.Forms.Button button_hubRemove;
        private System.Windows.Forms.Button button_hubAdd;
        private System.Windows.Forms.DataGridView dataGridView_hubs;
        private System.Windows.Forms.Label label_hubs;
        private System.Windows.Forms.TabPage tabPage_streamAnalytics;
        private System.Windows.Forms.Button button_settingsStreamAnalytics;
        private System.Windows.Forms.Button button_removeStreamAnalytics;
        private System.Windows.Forms.Button button_addStreamAnalytics;
        private System.Windows.Forms.DataGridView dataGridView_StreamAnalytics;
        private System.Windows.Forms.Label label_StreamAnalytics;
        private System.Windows.Forms.Label label_storageInfo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button buttonDeviceSettings;
        private System.Windows.Forms.Label label_hubs_info;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem firebaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem serUpFirebaseConnectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem seuUpFirebaseUserToolStripMenuItem;
        private System.Windows.Forms.Label label_storage_info;
        private System.Windows.Forms.Label label_stream_info;
    }
}
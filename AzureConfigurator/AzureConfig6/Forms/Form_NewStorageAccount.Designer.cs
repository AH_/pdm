﻿namespace AzureConfig6
{
    partial class Form_NewStorageAccount
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.textBox_newStorageAccountName = new System.Windows.Forms.TextBox();
            this.label_newStorageAccountLabel = new System.Windows.Forms.Label();
            this.button_ok = new System.Windows.Forms.Button();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.label_nameCheck = new System.Windows.Forms.Label();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox_newStorageAccountName
            // 
            this.textBox_newStorageAccountName.Location = new System.Drawing.Point(12, 25);
            this.textBox_newStorageAccountName.Name = "textBox_newStorageAccountName";
            this.textBox_newStorageAccountName.Size = new System.Drawing.Size(195, 20);
            this.textBox_newStorageAccountName.TabIndex = 0;
            this.textBox_newStorageAccountName.TextChanged += new System.EventHandler(this.textBox_newStorageAccountName_TextChanged);
            // 
            // label_newStorageAccountLabel
            // 
            this.label_newStorageAccountLabel.AutoSize = true;
            this.label_newStorageAccountLabel.Location = new System.Drawing.Point(12, 9);
            this.label_newStorageAccountLabel.Name = "label_newStorageAccountLabel";
            this.label_newStorageAccountLabel.Size = new System.Drawing.Size(144, 13);
            this.label_newStorageAccountLabel.TabIndex = 1;
            this.label_newStorageAccountLabel.Text = "New Storage Account name:";
            // 
            // button_ok
            // 
            this.button_ok.Location = new System.Drawing.Point(162, 54);
            this.button_ok.Name = "button_ok";
            this.button_ok.Size = new System.Drawing.Size(75, 23);
            this.button_ok.TabIndex = 2;
            this.button_ok.Text = "OK";
            this.button_ok.UseVisualStyleBackColor = true;
            this.button_ok.Click += new System.EventHandler(this.button_ok_Click);
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(81, 54);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.button_Cancel.TabIndex = 3;
            this.button_Cancel.Text = "Cancel";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // label_nameCheck
            // 
            this.label_nameCheck.AutoSize = true;
            this.label_nameCheck.Location = new System.Drawing.Point(218, 70);
            this.label_nameCheck.Name = "label_nameCheck";
            this.label_nameCheck.Size = new System.Drawing.Size(0, 13);
            this.label_nameCheck.TabIndex = 4;
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(212, 23);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(25, 25);
            this.pictureBox.TabIndex = 5;
            this.pictureBox.TabStop = false;
            // 
            // Form_NewStorageAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(244, 81);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.label_nameCheck);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.button_ok);
            this.Controls.Add(this.label_newStorageAccountLabel);
            this.Controls.Add(this.textBox_newStorageAccountName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form_NewStorageAccount";
            this.Text = "New Storage Account";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_newStorageAccountName;
        private System.Windows.Forms.Label label_newStorageAccountLabel;
        private System.Windows.Forms.Button button_ok;
        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.Label label_nameCheck;
        private System.Windows.Forms.PictureBox pictureBox;
    }
}
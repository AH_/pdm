﻿namespace AzureConfig6
{
    partial class Form_StreanAnalyticsJob_Settings
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.label_input = new System.Windows.Forms.Label();
            this.button_ok = new System.Windows.Forms.Button();
            this.dataGridView_inputs = new System.Windows.Forms.DataGridView();
            this.dataGridView_outputs = new System.Windows.Forms.DataGridView();
            this.label_output = new System.Windows.Forms.Label();
            this.button_inputAdd = new System.Windows.Forms.Button();
            this.button_inputRemove = new System.Windows.Forms.Button();
            this.button_outputRemove = new System.Windows.Forms.Button();
            this.button_outputAdd = new System.Windows.Forms.Button();
            this.button_start = new System.Windows.Forms.Button();
            this.button_stop = new System.Windows.Forms.Button();
            this.button_transformRemove = new System.Windows.Forms.Button();
            this.button_transformAdd = new System.Windows.Forms.Button();
            this.dataGridView_transforms = new System.Windows.Forms.DataGridView();
            this.label_transformation = new System.Windows.Forms.Label();
            this.label_jobState = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_inputs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_outputs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_transforms)).BeginInit();
            this.SuspendLayout();
            // 
            // label_input
            // 
            this.label_input.AutoSize = true;
            this.label_input.Location = new System.Drawing.Point(12, 19);
            this.label_input.Name = "label_input";
            this.label_input.Size = new System.Drawing.Size(181, 13);
            this.label_input.TabIndex = 1;
            this.label_input.Text = "Stream analytics job input (IoT hubs):";
            // 
            // button_ok
            // 
            this.button_ok.Location = new System.Drawing.Point(510, 454);
            this.button_ok.Name = "button_ok";
            this.button_ok.Size = new System.Drawing.Size(75, 23);
            this.button_ok.TabIndex = 2;
            this.button_ok.Text = "OK";
            this.button_ok.UseVisualStyleBackColor = true;
            this.button_ok.Click += new System.EventHandler(this.button_ok_Click);
            // 
            // dataGridView_inputs
            // 
            this.dataGridView_inputs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_inputs.Location = new System.Drawing.Point(12, 35);
            this.dataGridView_inputs.Name = "dataGridView_inputs";
            this.dataGridView_inputs.RowHeadersWidth = 51;
            this.dataGridView_inputs.Size = new System.Drawing.Size(574, 84);
            this.dataGridView_inputs.TabIndex = 4;
            this.dataGridView_inputs.SelectionChanged += new System.EventHandler(this.dataGridView_inputs_SelectionChanged);
            // 
            // dataGridView_outputs
            // 
            this.dataGridView_outputs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_outputs.Location = new System.Drawing.Point(11, 170);
            this.dataGridView_outputs.Name = "dataGridView_outputs";
            this.dataGridView_outputs.RowHeadersWidth = 51;
            this.dataGridView_outputs.Size = new System.Drawing.Size(574, 88);
            this.dataGridView_outputs.TabIndex = 8;
            this.dataGridView_outputs.SelectionChanged += new System.EventHandler(this.dataGridView_outputs_SelectionChanged);
            // 
            // label_output
            // 
            this.label_output.AutoSize = true;
            this.label_output.Location = new System.Drawing.Point(11, 154);
            this.label_output.Name = "label_output";
            this.label_output.Size = new System.Drawing.Size(214, 13);
            this.label_output.TabIndex = 7;
            this.label_output.Text = "Stream analytics job output (Storage tables):";
            // 
            // button_inputAdd
            // 
            this.button_inputAdd.Location = new System.Drawing.Point(510, 122);
            this.button_inputAdd.Name = "button_inputAdd";
            this.button_inputAdd.Size = new System.Drawing.Size(75, 23);
            this.button_inputAdd.TabIndex = 9;
            this.button_inputAdd.Text = "Add";
            this.button_inputAdd.UseVisualStyleBackColor = true;
            this.button_inputAdd.Click += new System.EventHandler(this.button_AddInput_Click);
            // 
            // button_inputRemove
            // 
            this.button_inputRemove.Location = new System.Drawing.Point(430, 122);
            this.button_inputRemove.Name = "button_inputRemove";
            this.button_inputRemove.Size = new System.Drawing.Size(75, 23);
            this.button_inputRemove.TabIndex = 10;
            this.button_inputRemove.Text = "Remove";
            this.button_inputRemove.UseVisualStyleBackColor = true;
            this.button_inputRemove.Click += new System.EventHandler(this.button_RemoveInput_Click);
            // 
            // button_outputRemove
            // 
            this.button_outputRemove.Location = new System.Drawing.Point(430, 264);
            this.button_outputRemove.Name = "button_outputRemove";
            this.button_outputRemove.Size = new System.Drawing.Size(75, 23);
            this.button_outputRemove.TabIndex = 14;
            this.button_outputRemove.Text = "Remove";
            this.button_outputRemove.UseVisualStyleBackColor = true;
            this.button_outputRemove.Click += new System.EventHandler(this.button_RemoveOutput_Click);
            // 
            // button_outputAdd
            // 
            this.button_outputAdd.Location = new System.Drawing.Point(511, 264);
            this.button_outputAdd.Name = "button_outputAdd";
            this.button_outputAdd.Size = new System.Drawing.Size(75, 23);
            this.button_outputAdd.TabIndex = 13;
            this.button_outputAdd.Text = "Add";
            this.button_outputAdd.UseVisualStyleBackColor = true;
            this.button_outputAdd.Click += new System.EventHandler(this.button_AddOutput_Click);
            // 
            // button_start
            // 
            this.button_start.Location = new System.Drawing.Point(12, 454);
            this.button_start.Name = "button_start";
            this.button_start.Size = new System.Drawing.Size(75, 23);
            this.button_start.TabIndex = 16;
            this.button_start.Text = "Start Job";
            this.button_start.UseVisualStyleBackColor = true;
            this.button_start.Click += new System.EventHandler(this.Button_start_Click);
            // 
            // button_stop
            // 
            this.button_stop.Location = new System.Drawing.Point(92, 454);
            this.button_stop.Name = "button_stop";
            this.button_stop.Size = new System.Drawing.Size(75, 23);
            this.button_stop.TabIndex = 15;
            this.button_stop.Text = "Stop Job";
            this.button_stop.UseVisualStyleBackColor = true;
            this.button_stop.Click += new System.EventHandler(this.Stop_Click);
            // 
            // button_transformRemove
            // 
            this.button_transformRemove.Location = new System.Drawing.Point(430, 407);
            this.button_transformRemove.Name = "button_transformRemove";
            this.button_transformRemove.Size = new System.Drawing.Size(75, 23);
            this.button_transformRemove.TabIndex = 20;
            this.button_transformRemove.Text = "Remove";
            this.button_transformRemove.UseVisualStyleBackColor = true;
            this.button_transformRemove.Click += new System.EventHandler(this.button_RemoveTransformationPair_Click);
            // 
            // button_transformAdd
            // 
            this.button_transformAdd.Location = new System.Drawing.Point(511, 407);
            this.button_transformAdd.Name = "button_transformAdd";
            this.button_transformAdd.Size = new System.Drawing.Size(75, 23);
            this.button_transformAdd.TabIndex = 19;
            this.button_transformAdd.Text = "Add";
            this.button_transformAdd.UseVisualStyleBackColor = true;
            this.button_transformAdd.Click += new System.EventHandler(this.button_addTransformationPair_Click);
            // 
            // dataGridView_transforms
            // 
            this.dataGridView_transforms.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_transforms.Location = new System.Drawing.Point(11, 313);
            this.dataGridView_transforms.Name = "dataGridView_transforms";
            this.dataGridView_transforms.RowHeadersWidth = 51;
            this.dataGridView_transforms.Size = new System.Drawing.Size(574, 88);
            this.dataGridView_transforms.TabIndex = 18;
            this.dataGridView_transforms.SelectionChanged += new System.EventHandler(this.DataGridView_transforms_SelectionChanged);
            // 
            // label_transformation
            // 
            this.label_transformation.AutoSize = true;
            this.label_transformation.Location = new System.Drawing.Point(11, 297);
            this.label_transformation.Name = "label_transformation";
            this.label_transformation.Size = new System.Drawing.Size(235, 13);
            this.label_transformation.TabIndex = 17;
            this.label_transformation.Text = "Stream analytics job transformation pairs (Query):";
            // 
            // label_jobState
            // 
            this.label_jobState.AutoSize = true;
            this.label_jobState.Location = new System.Drawing.Point(173, 459);
            this.label_jobState.Name = "label_jobState";
            this.label_jobState.Size = new System.Drawing.Size(35, 13);
            this.label_jobState.TabIndex = 23;
            this.label_jobState.Text = "label1";
            // 
            // Form_StreanAnalyticsJob_Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(598, 489);
            this.Controls.Add(this.label_jobState);
            this.Controls.Add(this.button_transformRemove);
            this.Controls.Add(this.button_transformAdd);
            this.Controls.Add(this.dataGridView_transforms);
            this.Controls.Add(this.label_transformation);
            this.Controls.Add(this.button_start);
            this.Controls.Add(this.button_stop);
            this.Controls.Add(this.button_outputRemove);
            this.Controls.Add(this.button_outputAdd);
            this.Controls.Add(this.button_inputRemove);
            this.Controls.Add(this.button_inputAdd);
            this.Controls.Add(this.dataGridView_outputs);
            this.Controls.Add(this.label_output);
            this.Controls.Add(this.dataGridView_inputs);
            this.Controls.Add(this.button_ok);
            this.Controls.Add(this.label_input);
            this.Name = "Form_StreanAnalyticsJob_Settings";
            this.Text = "Stream Analytisc Job Settings";
            this.Load += new System.EventHandler(this.Form_StreanAnalyticsJob_Settings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_inputs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_outputs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_transforms)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label_input;
        private System.Windows.Forms.Button button_ok;
        private System.Windows.Forms.DataGridView dataGridView_inputs;
        private System.Windows.Forms.DataGridView dataGridView_outputs;
        private System.Windows.Forms.Label label_output;
        private System.Windows.Forms.Button button_inputAdd;
        private System.Windows.Forms.Button button_inputRemove;
        private System.Windows.Forms.Button button_outputRemove;
        private System.Windows.Forms.Button button_outputAdd;
        private System.Windows.Forms.Button button_start;
        private System.Windows.Forms.Button button_stop;
        private System.Windows.Forms.Button button_transformRemove;
        private System.Windows.Forms.Button button_transformAdd;
        private System.Windows.Forms.DataGridView dataGridView_transforms;
        private System.Windows.Forms.Label label_transformation;
        private System.Windows.Forms.Label label_jobState;
    }
}
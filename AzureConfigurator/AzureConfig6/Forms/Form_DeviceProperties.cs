﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.Net;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using FireSharp.Response;
using IronBarCode;
using Microsoft.Azure.Devices;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AzureConfig6
{
    public partial class Form_DeviceProperties : Form
    {
        List<Sensor> sensors;
        SpecificationTableRow deviceSpec;
        ConfigurationTableRow deviceConfig;
        Image qr;
        AzureStorageAccount storage;

        public Form_DeviceProperties()
        {
            InitializeComponent();

            linkLabel_firebaseSync.Visible = false;

            label_iotHubName.Text = Globals.iotHub.name;

            storage = Globals.device.getStorageFromStreamAnalytics();

            if(storage != null)
            {
                String qrString = Globals.device.deviceId + "%" + storage.name + "%" + storage.keys[0].value;
                qr = QRCodeWriter.CreateQrCode(qrString, 310, QRCodeWriter.QrErrorCorrectionLevel.Medium).ToImage();
                pictureBox_qr.Image = qr;

                deviceSpec = TableHelper.fetchSpecificationRowWithSdk(storage.name, storage.keys[0].value, Globals.device.deviceId);

                if (deviceSpec != null)
                {
                    deviceConfig = TableHelper.fetchConfigurationRowWithSdk(storage.name, storage.keys[0].value, deviceSpec.Model, deviceSpec.Version);
                }

                if (deviceConfig != null)
                {
                    sensors = deviceConfig.Sensors;
                    refreshSensorsGridView(sensors);
                }

                label_qrInfo.Visible = false;
                button_settings.Enabled = true;
                button_save_qr.Visible = true;
                button_editSpecification.Enabled = true;
                RefreshFirebaseLabels();
            }
            else
            {
                label_qrInfo.Visible = true;
                label_qrInfo.Text = "Unable to generte QR code."
                     + Environment.NewLine + "Device is not associated with storage account.";
                button_settings.Enabled = false;
                button_save_qr.Visible = false;
                button_editSpecification.Enabled = false;
            }
        }

        private void button_switchOff_Click(object sender, EventArgs e)
        {
            ServiceClient serviceClient;
            string connectionString = "HostName="+Globals.iotHub.name+".azure-devices.net;SharedAccessKeyName=iothubowner;SharedAccessKey="+Globals.iotHub.keys["iothubowner"].primaryKey;
            serviceClient = ServiceClient.CreateFromConnectionString(connectionString);
            Microsoft.Azure.Devices.Message commandMessage = new Microsoft.Azure.Devices.Message(Encoding.ASCII.GetBytes("0"));
            serviceClient.SendAsync(Globals.device.deviceId, commandMessage);
        }
        private void button_switchOn_Click(object sender, EventArgs e)
        {
            ServiceClient serviceClient;
            string connectionString = "HostName=" + Globals.iotHub.name + ".azure-devices.net;SharedAccessKeyName=iothubowner;SharedAccessKey=" + Globals.iotHub.keys["iothubowner"].primaryKey;
            serviceClient = ServiceClient.CreateFromConnectionString(connectionString);
            Microsoft.Azure.Devices.Message commandMessage = new Microsoft.Azure.Devices.Message(Encoding.ASCII.GetBytes("1"));
            serviceClient.SendAsync(Globals.device.deviceId, commandMessage);
        }
        private void button_close_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        //Specifications START
        //fetch with REST
        public String fetchSpecificationTable(string storageAccountName, string storageAccountKey)
        {
            WebClient webClient = new WebClient();
            var requestUri = new Uri($"https://{storageAccountName}.table.core.windows.net/specifications()");

            //Date header start
            var RequestDateString = DateTime.UtcNow.ToString("R", CultureInfo.InvariantCulture);

            if (webClient.Headers.Get("x-ms-date") != null)
                webClient.Headers.Remove("x-ms-date");
            webClient.Headers.Add("x-ms-date", RequestDateString);

            if (webClient.Headers.Get("Authorization") != null)
                webClient.Headers.Remove("Authorization");

            if (webClient.Headers.Get("x-ms-version") != null)
                webClient.Headers.Remove("x-ms-version");
            webClient.Headers.Add("x-ms-version", "2015-12-11");

            if (webClient.Headers.Get("Accept") != null)
                webClient.Headers.Remove("Accept");
            webClient.Headers.Add("Accept", "application/json");

            var canonicalizedStringToBuild = string.Format("{0}\n{1}", RequestDateString, $"/{storageAccountName}/{requestUri.AbsolutePath.TrimStart('/')}");
            string signature;
            using (var hmac = new HMACSHA256(Convert.FromBase64String(storageAccountKey)))
            {
                byte[] dataToHmac = Encoding.UTF8.GetBytes(canonicalizedStringToBuild);
                signature = Convert.ToBase64String(hmac.ComputeHash(dataToHmac));
            }

            string authorizationHeader = string.Format($"{storageAccountName}:" + signature);
            webClient.Headers.Add("Authorization", new AuthenticationHeaderValue("SharedKeyLite", authorizationHeader).ToString());

            String responseMessage = webClient.DownloadString(requestUri);


            //Sensors
            String sensorsJson = JObject.Parse(responseMessage)["value"].ToString().Substring(JObject.Parse(responseMessage)["value"].ToString().IndexOf('=') + 1);
            sensorsJson = sensorsJson.Substring(0, sensorsJson.IndexOf("]") + 1);
            sensorsJson = sensorsJson.Replace(@"\", "");
            List<Sensor> storageList = JsonConvert.DeserializeObject<List<Sensor>>(sensorsJson);

            return responseMessage;
        }

        //Fetch with SDK
        private void refreshSensorsGridView(List<Sensor> sensors)
        {
            if(sensors != null)
            {
                dataGridView_sensors.Columns.Clear();

                dataGridView_sensors.AutoGenerateColumns = false;
                dataGridView_sensors.AllowUserToAddRows = false;

                DataGridViewTextBoxColumn colId = new DataGridViewTextBoxColumn();
                colId.HeaderText = "Sensor ID";
                colId.DataPropertyName = "Id";

                DataGridViewTextBoxColumn colName = new DataGridViewTextBoxColumn();
                colName.HeaderText = "Sensor name";
                colName.DataPropertyName = "Name";

                DataGridViewTextBoxColumn colUnits = new DataGridViewTextBoxColumn();
                colUnits.HeaderText = "Sensor units";
                colUnits.DataPropertyName = "Units";

                dataGridView_sensors.Columns.Add(colId);
                dataGridView_sensors.Columns.Add(colName);
                dataGridView_sensors.Columns.Add(colUnits);

                dataGridView_sensors.ReadOnly = true;
                dataGridView_sensors.AllowUserToAddRows = false;
                dataGridView_sensors.RowHeadersVisible = false;
                dataGridView_sensors.AllowUserToResizeRows = false;
                dataGridView_sensors.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

                BindingList<Sensor> bindingList = new BindingList<Sensor>(sensors);
                dataGridView_sensors.DataSource = bindingList;
            }
        }
        private void button_settings_Click(object sender, EventArgs e)
        {
            using (var configurationSettingsForm = new Form_DeviceConfigurationSetings(deviceSpec, deviceConfig, storage))
            {
                DialogResult result = configurationSettingsForm.ShowDialog();
                if (result == DialogResult.OK)
                {
                    deviceConfig = TableHelper.fetchConfigurationRowWithSdk(storage.name, storage.keys[0].value, deviceSpec.Model, deviceSpec.Version);
                    try
                    {
                        sensors = deviceConfig.Sensors;
                        refreshSensorsGridView(sensors);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }
            }
        }
        private void button_editSpecification_Click(object sender, EventArgs e)
        {
            if (deviceSpec != null)
            {
                using (var specificationSettingsForm = new Form_DeviceSpecificationSetings(deviceSpec))
                {
                    DialogResult result = specificationSettingsForm.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        deviceSpec = TableHelper.fetchSpecificationRowWithSdk(Globals.storageAccount.name, Globals.storageAccount.keys[0].value, Globals.device.deviceId);

                        if (deviceSpec != null)
                        {
                            deviceConfig = TableHelper.fetchConfigurationRowWithSdk(storage.name, storage.keys[0].value, deviceSpec.Model, deviceSpec.Version);
                        }

                        sensors.Clear();

                        try
                        {
                            sensors = deviceConfig.Sensors;
                        }catch(Exception ex)
                        {
                            Console.WriteLine(ex);
                        }
                        
                        refreshSensorsGridView(sensors);

                        RefreshFirebaseLabels();
                    }
                }
            }
            else
            {
                using (var specificationSettingsForm = new Form_DeviceSpecificationSetings("ah", Globals.device.deviceId))
                {
                    DialogResult result = specificationSettingsForm.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        deviceSpec = TableHelper.fetchSpecificationRowWithSdk(Globals.storageAccount.name, Globals.storageAccount.keys[0].value, Globals.device.deviceId);

                        if (deviceSpec != null)
                        {
                            deviceConfig = TableHelper.fetchConfigurationRowWithSdk(storage.name, storage.keys[0].value, deviceSpec.Model, deviceSpec.Version);
                        }

                        if (deviceConfig != null)
                        {
                            sensors = deviceConfig.Sensors;
                            refreshSensorsGridView(sensors);
                        }

                        RefreshFirebaseLabels();
                    }
                }
            }
        }
        private void linkLabel_create_spec_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            using (var specificationSettingsForm = new Form_DeviceSpecificationSetings("ah", Globals.device.deviceId))
            {
                DialogResult result = specificationSettingsForm.ShowDialog();
                if (result == DialogResult.OK)
                {
                    deviceSpec = TableHelper.fetchSpecificationRowWithSdk(Globals.storageAccount.name, Globals.storageAccount.keys[0].value, Globals.device.deviceId);

                    if (deviceSpec != null)
                    {
                        deviceConfig = TableHelper.fetchConfigurationRowWithSdk(storage.name, storage.keys[0].value, deviceSpec.Model, deviceSpec.Version);
                    }

                    if (deviceConfig != null)
                    {
                        sensors = deviceConfig.Sensors;
                        refreshSensorsGridView(sensors);
                    }

                    RefreshFirebaseLabels();
                }
            }
        }
        private void linkLabel_create_config_Click(object sender, EventArgs e)
        {
            using (var configurationSettingsForm = new Form_DeviceConfigurationSetings(deviceSpec, deviceConfig, storage))
            {
                DialogResult result = configurationSettingsForm.ShowDialog();
                if (result == DialogResult.OK)
                {
                    deviceConfig = TableHelper.fetchConfigurationRowWithSdk(Globals.storageAccount.name, Globals.storageAccount.keys[0].value, deviceSpec.Model, deviceSpec.Version);
                    try
                    {
                        sensors = deviceConfig.Sensors;
                        refreshSensorsGridView(sensors);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }
            }
        }
        //Specifications END

        public void RefreshFirebaseLabels()
        {
            try
            {
                FirebaseResponse firebaseResponse = Globals.firebaseClient.Get("users/" + Globals.userMail.Replace('.', '-') + "/devices/" + Globals.device.deviceId);

                if (firebaseResponse.Body.Equals("null"))
                {
                    linkLabel_firebaseSync.Visible = true;
                    linkLabel_firebaseSync.Text = "Add device to mobile app";
                }
                else
                {
                    FirebaseDevice fetchedDevice = JsonConvert.DeserializeObject<FirebaseDevice>(firebaseResponse.Body);
                    if (!
                        (fetchedDevice.serial.Equals(Globals.device.deviceId) &&
                        fetchedDevice.storageName.Equals(storage.name) &&
                        fetchedDevice.storageKey.Equals(storage.keys[0].value))
                        )
                    {
                        linkLabel_firebaseSync.Visible = true;
                        linkLabel_firebaseSync.Text = "Synchronize mobile app device's parameters.";
                    }
                    else
                    {
                        linkLabel_firebaseSync.Visible = false;
                    }
                }
            }catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void button_save_qr_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.DefaultExt = ".png";
            dialog.Filter = "PNG image (*.png)|*.png";
            dialog.AddExtension = true;
            dialog.FileName = Globals.device.deviceId + "_QR";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                int width = Convert.ToInt32(qr.Width);
                int height = Convert.ToInt32(qr.Height);
                Bitmap bmp = new Bitmap(width, height);
                qr.Save(dialog.FileName, ImageFormat.Png);
            }
        }
    }
}
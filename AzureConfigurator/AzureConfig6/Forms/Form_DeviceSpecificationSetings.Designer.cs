﻿namespace AzureConfig6
{
    partial class Form_DeviceSpecificationSetings
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.label_specification_title = new System.Windows.Forms.Label();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.button_ok = new System.Windows.Forms.Button();
            this.label_owner_title = new System.Windows.Forms.Label();
            this.label_owner_value = new System.Windows.Forms.Label();
            this.label_model_value = new System.Windows.Forms.Label();
            this.label_model_title = new System.Windows.Forms.Label();
            this.label_version_value = new System.Windows.Forms.Label();
            this.label_version_title = new System.Windows.Forms.Label();
            this.button_edit_owner = new System.Windows.Forms.Button();
            this.button_edit_model = new System.Windows.Forms.Button();
            this.button_edit_version = new System.Windows.Forms.Button();
            this.button_owner_ok = new System.Windows.Forms.Button();
            this.button_owner_cancel = new System.Windows.Forms.Button();
            this.button_model_cancel = new System.Windows.Forms.Button();
            this.button_model_ok = new System.Windows.Forms.Button();
            this.button_version_cancel = new System.Windows.Forms.Button();
            this.button_version_ok = new System.Windows.Forms.Button();
            this.textBox_owner = new System.Windows.Forms.TextBox();
            this.textBox_model = new System.Windows.Forms.TextBox();
            this.textBox_version = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label_specification_title
            // 
            this.label_specification_title.AutoSize = true;
            this.label_specification_title.Location = new System.Drawing.Point(9, 7);
            this.label_specification_title.Name = "label_specification_title";
            this.label_specification_title.Size = new System.Drawing.Size(83, 13);
            this.label_specification_title.TabIndex = 1;
            this.label_specification_title.Text = "Device sensors:";
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(319, 105);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.button_Cancel.TabIndex = 7;
            this.button_Cancel.Text = "Cancel";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // button_ok
            // 
            this.button_ok.Location = new System.Drawing.Point(400, 105);
            this.button_ok.Name = "button_ok";
            this.button_ok.Size = new System.Drawing.Size(75, 23);
            this.button_ok.TabIndex = 6;
            this.button_ok.Text = "OK";
            this.button_ok.UseVisualStyleBackColor = true;
            this.button_ok.Click += new System.EventHandler(this.button_ok_Click);
            // 
            // label_owner_title
            // 
            this.label_owner_title.AutoSize = true;
            this.label_owner_title.Location = new System.Drawing.Point(93, 81);
            this.label_owner_title.Name = "label_owner_title";
            this.label_owner_title.Size = new System.Drawing.Size(76, 13);
            this.label_owner_title.TabIndex = 8;
            this.label_owner_title.Text = "Device owner:";
            // 
            // label_owner_value
            // 
            this.label_owner_value.AutoSize = true;
            this.label_owner_value.Location = new System.Drawing.Point(175, 81);
            this.label_owner_value.Name = "label_owner_value";
            this.label_owner_value.Size = new System.Drawing.Size(10, 13);
            this.label_owner_value.TabIndex = 9;
            this.label_owner_value.Text = "-";
            // 
            // label_model_value
            // 
            this.label_model_value.AutoSize = true;
            this.label_model_value.Location = new System.Drawing.Point(175, 28);
            this.label_model_value.Name = "label_model_value";
            this.label_model_value.Size = new System.Drawing.Size(10, 13);
            this.label_model_value.TabIndex = 13;
            this.label_model_value.Text = "-";
            // 
            // label_model_title
            // 
            this.label_model_title.AutoSize = true;
            this.label_model_title.Location = new System.Drawing.Point(93, 28);
            this.label_model_title.Name = "label_model_title";
            this.label_model_title.Size = new System.Drawing.Size(75, 13);
            this.label_model_title.TabIndex = 12;
            this.label_model_title.Text = "Device model:";
            // 
            // label_version_value
            // 
            this.label_version_value.AutoSize = true;
            this.label_version_value.Location = new System.Drawing.Point(175, 54);
            this.label_version_value.Name = "label_version_value";
            this.label_version_value.Size = new System.Drawing.Size(10, 13);
            this.label_version_value.TabIndex = 15;
            this.label_version_value.Text = "-";
            // 
            // label_version_title
            // 
            this.label_version_title.AutoSize = true;
            this.label_version_title.Location = new System.Drawing.Point(93, 54);
            this.label_version_title.Name = "label_version_title";
            this.label_version_title.Size = new System.Drawing.Size(81, 13);
            this.label_version_title.TabIndex = 14;
            this.label_version_title.Text = "Device version:";
            // 
            // button_edit_owner
            // 
            this.button_edit_owner.Location = new System.Drawing.Point(12, 76);
            this.button_edit_owner.Name = "button_edit_owner";
            this.button_edit_owner.Size = new System.Drawing.Size(75, 23);
            this.button_edit_owner.TabIndex = 16;
            this.button_edit_owner.Text = "Edit";
            this.button_edit_owner.UseVisualStyleBackColor = true;
            this.button_edit_owner.Click += new System.EventHandler(this.button_edit_owner_Click);
            // 
            // button_edit_model
            // 
            this.button_edit_model.Location = new System.Drawing.Point(12, 23);
            this.button_edit_model.Name = "button_edit_model";
            this.button_edit_model.Size = new System.Drawing.Size(75, 23);
            this.button_edit_model.TabIndex = 18;
            this.button_edit_model.Text = "Edit";
            this.button_edit_model.UseVisualStyleBackColor = true;
            this.button_edit_model.Click += new System.EventHandler(this.button_edit_model_Click);
            // 
            // button_edit_version
            // 
            this.button_edit_version.Location = new System.Drawing.Point(12, 49);
            this.button_edit_version.Name = "button_edit_version";
            this.button_edit_version.Size = new System.Drawing.Size(75, 23);
            this.button_edit_version.TabIndex = 19;
            this.button_edit_version.Text = "Edit";
            this.button_edit_version.UseVisualStyleBackColor = true;
            this.button_edit_version.Click += new System.EventHandler(this.button_edit_version_Click);
            // 
            // button_owner_ok
            // 
            this.button_owner_ok.Location = new System.Drawing.Point(12, 76);
            this.button_owner_ok.Name = "button_owner_ok";
            this.button_owner_ok.Size = new System.Drawing.Size(34, 23);
            this.button_owner_ok.TabIndex = 20;
            this.button_owner_ok.Text = "OK";
            this.button_owner_ok.UseVisualStyleBackColor = true;
            this.button_owner_ok.Visible = false;
            this.button_owner_ok.Click += new System.EventHandler(this.button_owner_ok_Click);
            // 
            // button_owner_cancel
            // 
            this.button_owner_cancel.Location = new System.Drawing.Point(53, 76);
            this.button_owner_cancel.Name = "button_owner_cancel";
            this.button_owner_cancel.Size = new System.Drawing.Size(34, 23);
            this.button_owner_cancel.TabIndex = 21;
            this.button_owner_cancel.Text = "X";
            this.button_owner_cancel.UseVisualStyleBackColor = true;
            this.button_owner_cancel.Visible = false;
            this.button_owner_cancel.Click += new System.EventHandler(this.button_owner_cancel_Click);
            // 
            // button_model_cancel
            // 
            this.button_model_cancel.Location = new System.Drawing.Point(53, 23);
            this.button_model_cancel.Name = "button_model_cancel";
            this.button_model_cancel.Size = new System.Drawing.Size(34, 23);
            this.button_model_cancel.TabIndex = 25;
            this.button_model_cancel.Text = "X";
            this.button_model_cancel.UseVisualStyleBackColor = true;
            this.button_model_cancel.Visible = false;
            this.button_model_cancel.Click += new System.EventHandler(this.button_model_cancel_Click);
            // 
            // button_model_ok
            // 
            this.button_model_ok.Location = new System.Drawing.Point(12, 23);
            this.button_model_ok.Name = "button_model_ok";
            this.button_model_ok.Size = new System.Drawing.Size(34, 23);
            this.button_model_ok.TabIndex = 24;
            this.button_model_ok.Text = "OK";
            this.button_model_ok.UseVisualStyleBackColor = true;
            this.button_model_ok.Visible = false;
            this.button_model_ok.Click += new System.EventHandler(this.button_model_ok_Click);
            // 
            // button_version_cancel
            // 
            this.button_version_cancel.Location = new System.Drawing.Point(53, 49);
            this.button_version_cancel.Name = "button_version_cancel";
            this.button_version_cancel.Size = new System.Drawing.Size(34, 23);
            this.button_version_cancel.TabIndex = 27;
            this.button_version_cancel.Text = "X";
            this.button_version_cancel.UseVisualStyleBackColor = true;
            this.button_version_cancel.Visible = false;
            this.button_version_cancel.Click += new System.EventHandler(this.button_version_cancel_Click);
            // 
            // button_version_ok
            // 
            this.button_version_ok.Location = new System.Drawing.Point(12, 49);
            this.button_version_ok.Name = "button_version_ok";
            this.button_version_ok.Size = new System.Drawing.Size(34, 23);
            this.button_version_ok.TabIndex = 26;
            this.button_version_ok.Text = "OK";
            this.button_version_ok.UseVisualStyleBackColor = true;
            this.button_version_ok.Visible = false;
            this.button_version_ok.Click += new System.EventHandler(this.button_version_ok_Click);
            // 
            // textBox_owner
            // 
            this.textBox_owner.Location = new System.Drawing.Point(175, 79);
            this.textBox_owner.Name = "textBox_owner";
            this.textBox_owner.Size = new System.Drawing.Size(300, 20);
            this.textBox_owner.TabIndex = 28;
            this.textBox_owner.Visible = false;
            // 
            // textBox_model
            // 
            this.textBox_model.Location = new System.Drawing.Point(175, 26);
            this.textBox_model.Name = "textBox_model";
            this.textBox_model.Size = new System.Drawing.Size(300, 20);
            this.textBox_model.TabIndex = 30;
            this.textBox_model.Visible = false;
            // 
            // textBox_version
            // 
            this.textBox_version.Location = new System.Drawing.Point(175, 52);
            this.textBox_version.Name = "textBox_version";
            this.textBox_version.Size = new System.Drawing.Size(300, 20);
            this.textBox_version.TabIndex = 31;
            this.textBox_version.Visible = false;
            // 
            // Form_DeviceSpecificationSetings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 136);
            this.Controls.Add(this.textBox_version);
            this.Controls.Add(this.textBox_model);
            this.Controls.Add(this.textBox_owner);
            this.Controls.Add(this.button_version_cancel);
            this.Controls.Add(this.button_version_ok);
            this.Controls.Add(this.button_model_cancel);
            this.Controls.Add(this.button_model_ok);
            this.Controls.Add(this.button_owner_cancel);
            this.Controls.Add(this.button_owner_ok);
            this.Controls.Add(this.button_edit_version);
            this.Controls.Add(this.button_edit_model);
            this.Controls.Add(this.button_edit_owner);
            this.Controls.Add(this.label_version_value);
            this.Controls.Add(this.label_version_title);
            this.Controls.Add(this.label_model_value);
            this.Controls.Add(this.label_model_title);
            this.Controls.Add(this.label_owner_value);
            this.Controls.Add(this.label_owner_title);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.button_ok);
            this.Controls.Add(this.label_specification_title);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimumSize = new System.Drawing.Size(500, 175);
            this.Name = "Form_DeviceSpecificationSetings";
            this.Text = "Device specification settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label_specification_title;
        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.Button button_ok;
        private System.Windows.Forms.Label label_owner_title;
        private System.Windows.Forms.Label label_owner_value;
        private System.Windows.Forms.Label label_model_value;
        private System.Windows.Forms.Label label_model_title;
        private System.Windows.Forms.Label label_version_value;
        private System.Windows.Forms.Label label_version_title;
        private System.Windows.Forms.Button button_edit_owner;
        private System.Windows.Forms.Button button_edit_model;
        private System.Windows.Forms.Button button_edit_version;
        private System.Windows.Forms.Button button_owner_ok;
        private System.Windows.Forms.Button button_owner_cancel;
        private System.Windows.Forms.Button button_model_cancel;
        private System.Windows.Forms.Button button_model_ok;
        private System.Windows.Forms.Button button_version_cancel;
        private System.Windows.Forms.Button button_version_ok;
        private System.Windows.Forms.TextBox textBox_owner;
        private System.Windows.Forms.TextBox textBox_model;
        private System.Windows.Forms.TextBox textBox_version;
    }
}
﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AzureConfig6
{
    public partial class Form_NewStorageAccount : Form
    {
        Timer typingTimer = new Timer();
        public String newSaName;
        String textBoxString;
        Boolean isChecked = false;

        public Form_NewStorageAccount()
        {
            InitializeComponent();
            button_ok.Enabled = false;
            typingTimer.Interval = 300;
            typingTimer.Tick += new EventHandler(this.typingTimerHandler);
        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            this.newSaName = textBox_newStorageAccountName.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private class Response
        {
            public bool nameAvailable { get; set; }
            public String reason { get; set; }
            public String message { get; set; }
        }

        private void textBox_newStorageAccountName_TextChanged(object sender, EventArgs e)
        {
            isChecked = false;
            TextBox textBox = sender as TextBox;
            textBoxString = textBox.Text;
            typingTimer.Stop();
            typingTimer.Start();
        }

        private void typingTimerHandler(object sender, EventArgs e)
        {
            if(!isChecked)
            {
                if(!String.IsNullOrEmpty(textBoxString) && Regex.IsMatch(textBoxString, @"^[a-zA-Z0-9_-]+$") && !Regex.IsMatch(textBoxString, @"^\d"))
                {
                    String responseString = REST.doPOST("https://management.azure.com/subscriptions/" + Globals.subscription.subscriptionId + "/providers/Microsoft.Storage/checkNameAvailability?api-version=2019-04-01",
                    "{\"name\": \"" + textBoxString + "\", \"type\": \"Microsoft.Storage/storageAccounts\"}", Globals.token);

                    Response responseObj = JObject.Parse(responseString).ToObject<Response>();

                    if (responseObj.nameAvailable)
                    {
                        pictureBox.Image = Globals.iconSuccess;
                        button_ok.Enabled = true;
                    }
                    else
                    {
                        pictureBox.Image = Globals.iconError;
                        button_ok.Enabled = false;
                    }
                }
                else
                {
                    pictureBox.Image = Globals.iconError;
                    button_ok.Enabled = false;
                }
            }
            isChecked = true;
        }
    }
}

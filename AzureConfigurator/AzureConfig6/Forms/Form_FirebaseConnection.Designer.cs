﻿namespace AzureConfig6
{
    partial class Form_FirebaseConnection
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.label_login = new System.Windows.Forms.Label();
            this.label_password = new System.Windows.Forms.Label();
            this.textBox_firebaseUrl = new System.Windows.Forms.TextBox();
            this.textBox_firebaseSecret = new System.Windows.Forms.TextBox();
            this.button_saveFirebase = new System.Windows.Forms.Button();
            this.label_info = new System.Windows.Forms.Label();
            this.button_cancel = new System.Windows.Forms.Button();
            this.pictureBox_url = new System.Windows.Forms.PictureBox();
            this.pictureBox1_secret = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_url)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1_secret)).BeginInit();
            this.SuspendLayout();
            // 
            // label_login
            // 
            this.label_login.AutoSize = true;
            this.label_login.Location = new System.Drawing.Point(16, 30);
            this.label_login.Name = "label_login";
            this.label_login.Size = new System.Drawing.Size(123, 13);
            this.label_login.TabIndex = 0;
            this.label_login.Text = "Realtime database URL:";
            // 
            // label_password
            // 
            this.label_password.AutoSize = true;
            this.label_password.Location = new System.Drawing.Point(51, 56);
            this.label_password.Name = "label_password";
            this.label_password.Size = new System.Drawing.Size(88, 13);
            this.label_password.TabIndex = 1;
            this.label_password.Text = "Database secret:";
            // 
            // textBox_firebaseUrl
            // 
            this.textBox_firebaseUrl.Location = new System.Drawing.Point(145, 27);
            this.textBox_firebaseUrl.Name = "textBox_firebaseUrl";
            this.textBox_firebaseUrl.Size = new System.Drawing.Size(481, 20);
            this.textBox_firebaseUrl.TabIndex = 3;
            this.textBox_firebaseUrl.TextChanged += new System.EventHandler(this.TextBox_firebaseUrl_TextChanged);
            // 
            // textBox_firebaseSecret
            // 
            this.textBox_firebaseSecret.Location = new System.Drawing.Point(145, 53);
            this.textBox_firebaseSecret.Name = "textBox_firebaseSecret";
            this.textBox_firebaseSecret.Size = new System.Drawing.Size(481, 20);
            this.textBox_firebaseSecret.TabIndex = 4;
            this.textBox_firebaseSecret.TextChanged += new System.EventHandler(this.TextBox_firebaseSecret_TextChanged);
            // 
            // button_saveFirebase
            // 
            this.button_saveFirebase.Location = new System.Drawing.Point(519, 79);
            this.button_saveFirebase.Name = "button_saveFirebase";
            this.button_saveFirebase.Size = new System.Drawing.Size(107, 30);
            this.button_saveFirebase.TabIndex = 6;
            this.button_saveFirebase.Text = "Save";
            this.button_saveFirebase.UseVisualStyleBackColor = true;
            this.button_saveFirebase.Click += new System.EventHandler(this.button_login_Click);
            // 
            // label_info
            // 
            this.label_info.AutoSize = true;
            this.label_info.Location = new System.Drawing.Point(16, 99);
            this.label_info.Name = "label_info";
            this.label_info.Size = new System.Drawing.Size(35, 13);
            this.label_info.TabIndex = 8;
            this.label_info.Text = "label1";
            // 
            // button_cancel
            // 
            this.button_cancel.Location = new System.Drawing.Point(406, 79);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new System.Drawing.Size(107, 30);
            this.button_cancel.TabIndex = 9;
            this.button_cancel.Text = "Cancel";
            this.button_cancel.UseVisualStyleBackColor = true;
            this.button_cancel.Click += new System.EventHandler(this.Button_cancel_Click);
            // 
            // pictureBox_url
            // 
            this.pictureBox_url.Location = new System.Drawing.Point(632, 24);
            this.pictureBox_url.Name = "pictureBox_url";
            this.pictureBox_url.Size = new System.Drawing.Size(25, 25);
            this.pictureBox_url.TabIndex = 15;
            this.pictureBox_url.TabStop = false;
            // 
            // pictureBox1_secret
            // 
            this.pictureBox1_secret.Location = new System.Drawing.Point(632, 50);
            this.pictureBox1_secret.Name = "pictureBox1_secret";
            this.pictureBox1_secret.Size = new System.Drawing.Size(25, 25);
            this.pictureBox1_secret.TabIndex = 16;
            this.pictureBox1_secret.TabStop = false;
            // 
            // Form_FirebaseConnection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 122);
            this.Controls.Add(this.pictureBox1_secret);
            this.Controls.Add(this.pictureBox_url);
            this.Controls.Add(this.button_cancel);
            this.Controls.Add(this.label_info);
            this.Controls.Add(this.button_saveFirebase);
            this.Controls.Add(this.textBox_firebaseSecret);
            this.Controls.Add(this.textBox_firebaseUrl);
            this.Controls.Add(this.label_password);
            this.Controls.Add(this.label_login);
            this.Name = "Form_FirebaseConnection";
            this.Text = "Connect to mobile app account";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_url)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1_secret)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_login;
        private System.Windows.Forms.Label label_password;
        private System.Windows.Forms.TextBox textBox_firebaseUrl;
        private System.Windows.Forms.TextBox textBox_firebaseSecret;
        private System.Windows.Forms.Button button_saveFirebase;
        private System.Windows.Forms.Label label_info;
        private System.Windows.Forms.Button button_cancel;
        private System.Windows.Forms.PictureBox pictureBox_url;
        private System.Windows.Forms.PictureBox pictureBox1_secret;
    }
}
﻿namespace AzureConfig6
{
    partial class Form_Subscription
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.comboBox_subscriptions = new System.Windows.Forms.ComboBox();
            this.label_Info = new System.Windows.Forms.Label();
            this.label_Description = new System.Windows.Forms.Label();
            this.button_logout = new System.Windows.Forms.Button();
            this.button_next = new System.Windows.Forms.Button();
            this.label_Name = new System.Windows.Forms.Label();
            this.label_Title = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.comboBox_subscriptions);
            this.splitContainer1.Panel2.Controls.Add(this.label_Info);
            this.splitContainer1.Panel2.Controls.Add(this.label_Description);
            this.splitContainer1.Panel2.Controls.Add(this.button_logout);
            this.splitContainer1.Panel2.Controls.Add(this.button_next);
            this.splitContainer1.Panel2.Controls.Add(this.label_Name);
            this.splitContainer1.Panel2.Controls.Add(this.label_Title);
            this.splitContainer1.Size = new System.Drawing.Size(734, 461);
            this.splitContainer1.SplitterDistance = 186;
            this.splitContainer1.TabIndex = 0;
            // 
            // comboBox_subscriptions
            // 
            this.comboBox_subscriptions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_subscriptions.FormattingEnabled = true;
            this.comboBox_subscriptions.Location = new System.Drawing.Point(21, 131);
            this.comboBox_subscriptions.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox_subscriptions.Name = "comboBox_subscriptions";
            this.comboBox_subscriptions.Size = new System.Drawing.Size(485, 21);
            this.comboBox_subscriptions.TabIndex = 11;
            this.comboBox_subscriptions.SelectedIndexChanged += new System.EventHandler(this.comboBox_subscriptions_SelectedIndexChanged);
            // 
            // label_Info
            // 
            this.label_Info.AutoSize = true;
            this.label_Info.Location = new System.Drawing.Point(20, 162);
            this.label_Info.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_Info.Name = "label_Info";
            this.label_Info.Size = new System.Drawing.Size(25, 13);
            this.label_Info.TabIndex = 10;
            this.label_Info.Text = "Info";
            // 
            // label_Description
            // 
            this.label_Description.AutoSize = true;
            this.label_Description.Location = new System.Drawing.Point(20, 53);
            this.label_Description.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_Description.Name = "label_Description";
            this.label_Description.Size = new System.Drawing.Size(60, 13);
            this.label_Description.TabIndex = 9;
            this.label_Description.Text = "Description";
            // 
            // button_logout
            // 
            this.button_logout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_logout.Location = new System.Drawing.Point(11, 410);
            this.button_logout.Name = "button_logout";
            this.button_logout.Size = new System.Drawing.Size(114, 39);
            this.button_logout.TabIndex = 4;
            this.button_logout.Text = "Back";
            this.button_logout.UseVisualStyleBackColor = true;
            this.button_logout.Click += new System.EventHandler(this.button_logout_Click);
            // 
            // button_next
            // 
            this.button_next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_next.Location = new System.Drawing.Point(392, 410);
            this.button_next.Name = "button_next";
            this.button_next.Size = new System.Drawing.Size(114, 39);
            this.button_next.TabIndex = 3;
            this.button_next.Text = "Next";
            this.button_next.UseVisualStyleBackColor = true;
            this.button_next.Click += new System.EventHandler(this.button_next_Click);
            // 
            // label_Name
            // 
            this.label_Name.AutoSize = true;
            this.label_Name.Location = new System.Drawing.Point(19, 115);
            this.label_Name.Name = "label_Name";
            this.label_Name.Size = new System.Drawing.Size(209, 13);
            this.label_Name.TabIndex = 2;
            this.label_Name.Text = "Select your subscription from the list below:";
            // 
            // label_Title
            // 
            this.label_Title.AutoSize = true;
            this.label_Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_Title.Location = new System.Drawing.Point(19, 21);
            this.label_Title.Name = "label_Title";
            this.label_Title.Size = new System.Drawing.Size(106, 17);
            this.label_Title.TabIndex = 0;
            this.label_Title.Text = "Subscriptions";
            // 
            // Form_Subscription
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 461);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form_Subscription";
            this.Load += new System.EventHandler(this.Form_Subscription_Load);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label_Name;
        private System.Windows.Forms.Label label_Title;
        private System.Windows.Forms.Button button_logout;
        private System.Windows.Forms.Button button_next;
        private System.Windows.Forms.Label label_Info;
        private System.Windows.Forms.Label label_Description;
        private System.Windows.Forms.ComboBox comboBox_subscriptions;
    }
}
﻿namespace AzureConfig6
{
    partial class Form_NewIoThub
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.textBox_newIoThubName = new System.Windows.Forms.TextBox();
            this.label_newIoThub = new System.Windows.Forms.Label();
            this.button_ok = new System.Windows.Forms.Button();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.label_nameCheck = new System.Windows.Forms.Label();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox_newIoThubName
            // 
            this.textBox_newIoThubName.Location = new System.Drawing.Point(12, 25);
            this.textBox_newIoThubName.Name = "textBox_newIoThubName";
            this.textBox_newIoThubName.Size = new System.Drawing.Size(195, 20);
            this.textBox_newIoThubName.TabIndex = 0;
            this.textBox_newIoThubName.TextChanged += new System.EventHandler(this.textBox_newStorageAccountName_TextChanged);
            // 
            // label_newIoThub
            // 
            this.label_newIoThub.AutoSize = true;
            this.label_newIoThub.Location = new System.Drawing.Point(12, 9);
            this.label_newIoThub.Name = "label_newIoThub";
            this.label_newIoThub.Size = new System.Drawing.Size(101, 13);
            this.label_newIoThub.TabIndex = 1;
            this.label_newIoThub.Text = "New IoT hub name:";
            // 
            // button_ok
            // 
            this.button_ok.Location = new System.Drawing.Point(162, 54);
            this.button_ok.Name = "button_ok";
            this.button_ok.Size = new System.Drawing.Size(75, 23);
            this.button_ok.TabIndex = 2;
            this.button_ok.Text = "OK";
            this.button_ok.UseVisualStyleBackColor = true;
            this.button_ok.Click += new System.EventHandler(this.button_ok_Click);
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(81, 54);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.button_Cancel.TabIndex = 3;
            this.button_Cancel.Text = "Cancel";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // label_nameCheck
            // 
            this.label_nameCheck.AutoSize = true;
            this.label_nameCheck.Location = new System.Drawing.Point(218, 70);
            this.label_nameCheck.Name = "label_nameCheck";
            this.label_nameCheck.Size = new System.Drawing.Size(0, 13);
            this.label_nameCheck.TabIndex = 4;
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(212, 23);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(25, 25);
            this.pictureBox.TabIndex = 5;
            this.pictureBox.TabStop = false;
            // 
            // Form_NewIoThub
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(244, 81);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.label_nameCheck);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.button_ok);
            this.Controls.Add(this.label_newIoThub);
            this.Controls.Add(this.textBox_newIoThubName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form_NewIoThub";
            this.Text = "New IoT hub";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_newIoThubName;
        private System.Windows.Forms.Label label_newIoThub;
        private System.Windows.Forms.Button button_ok;
        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.Label label_nameCheck;
        private System.Windows.Forms.PictureBox pictureBox;
    }
}
﻿namespace AzureConfig6
{
    partial class Form_NewStreamAnalyticsInput
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.button_Cancel = new System.Windows.Forms.Button();
            this.button_ok = new System.Windows.Forms.Button();
            this.label_datasourse = new System.Windows.Forms.Label();
            this.dataGridView_hubsForInput = new System.Windows.Forms.DataGridView();
            this.label_name = new System.Windows.Forms.Label();
            this.textBox_new_input_name = new System.Windows.Forms.TextBox();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.label_input_info = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_hubsForInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(190, 209);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.button_Cancel.TabIndex = 7;
            this.button_Cancel.Text = "Cancel";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // button_ok
            // 
            this.button_ok.Location = new System.Drawing.Point(271, 209);
            this.button_ok.Name = "button_ok";
            this.button_ok.Size = new System.Drawing.Size(75, 23);
            this.button_ok.TabIndex = 6;
            this.button_ok.Text = "OK";
            this.button_ok.UseVisualStyleBackColor = true;
            this.button_ok.Click += new System.EventHandler(this.button_ok_Click);
            // 
            // label_datasourse
            // 
            this.label_datasourse.AutoSize = true;
            this.label_datasourse.Location = new System.Drawing.Point(12, 72);
            this.label_datasourse.Name = "label_datasourse";
            this.label_datasourse.Size = new System.Drawing.Size(90, 13);
            this.label_datasourse.TabIndex = 8;
            this.label_datasourse.Text = "Input datasource:";
            // 
            // dataGridView_hubsForInput
            // 
            this.dataGridView_hubsForInput.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_hubsForInput.Location = new System.Drawing.Point(13, 88);
            this.dataGridView_hubsForInput.Name = "dataGridView_hubsForInput";
            this.dataGridView_hubsForInput.Size = new System.Drawing.Size(333, 103);
            this.dataGridView_hubsForInput.TabIndex = 10;
            this.dataGridView_hubsForInput.SelectionChanged += new System.EventHandler(this.dataGridView_hubsForInput_SelectionChanged);
            // 
            // label_name
            // 
            this.label_name.AutoSize = true;
            this.label_name.Location = new System.Drawing.Point(12, 18);
            this.label_name.Name = "label_name";
            this.label_name.Size = new System.Drawing.Size(87, 13);
            this.label_name.TabIndex = 11;
            this.label_name.Text = "New input name:";
            // 
            // textBox_new_input_name
            // 
            this.textBox_new_input_name.Location = new System.Drawing.Point(13, 34);
            this.textBox_new_input_name.Name = "textBox_new_input_name";
            this.textBox_new_input_name.Size = new System.Drawing.Size(290, 20);
            this.textBox_new_input_name.TabIndex = 12;
            this.textBox_new_input_name.TextChanged += new System.EventHandler(this.textBox_new_input_name_TextChanged);
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(310, 32);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(25, 25);
            this.pictureBox.TabIndex = 20;
            this.pictureBox.TabStop = false;
            // 
            // label_input_info
            // 
            this.label_input_info.AutoSize = true;
            this.label_input_info.Location = new System.Drawing.Point(12, 194);
            this.label_input_info.Name = "label_input_info";
            this.label_input_info.Size = new System.Drawing.Size(35, 13);
            this.label_input_info.TabIndex = 21;
            this.label_input_info.Text = "label1";
            // 
            // Form_NewStreamAnalyticsInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 237);
            this.Controls.Add(this.label_input_info);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.textBox_new_input_name);
            this.Controls.Add(this.label_name);
            this.Controls.Add(this.dataGridView_hubsForInput);
            this.Controls.Add(this.label_datasourse);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.button_ok);
            this.Name = "Form_NewStreamAnalyticsInput";
            this.Text = "New Stream Analytics Job Input";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_hubsForInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.Button button_ok;
        private System.Windows.Forms.Label label_datasourse;
        private System.Windows.Forms.DataGridView dataGridView_hubsForInput;
        private System.Windows.Forms.Label label_name;
        private System.Windows.Forms.TextBox textBox_new_input_name;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Label label_input_info;
    }
}
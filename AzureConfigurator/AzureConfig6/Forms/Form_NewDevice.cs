﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AzureConfig6
{
    public partial class Form_NewDevice : Form
    {
        Timer typingTimer = new Timer();
        public String name;
        public String primaryKey;
        public String secondaryKey;
        public Boolean autoKeys;
        public ConfigurationTableRow configurationTableRow;
        public SpecificationTableRow specificationTableRow;

        public String selectedOwner;
        private String selectedModel;
        private String selectedVersion;

        String comboPlaceholder = "-- Enter value or choose it from list. --";

        Boolean isChecked  = false;

        List<AzureStorageAccount> storages = new List<AzureStorageAccount>();
        AzureStorageAccount storageAccount = new AzureStorageAccount();

        List<ConfigurationTableRow> configurations = new List<ConfigurationTableRow>();
        List<SpecificationTableRow> specifications = new List<SpecificationTableRow>();
        List<String> models = new List<String>();
        List<String> owners = new List<String>();
        List<String> filteredVersions = new List<String>();

        private class Response
        {
            public bool nameAvailable { get; set; }
            public String reason { get; set; }
            public String message { get; set; }
        }

        private class DeviceConfiguration //To deserialize configuration from json
        {
            public String owner { get; set; }
            public String model { get; set; }
            public String version { get; set; }
            public List<Sensor> sensors { get; set; }
        }

        public Form_NewDevice()
        {
            InitializeComponent();
            typingTimer.Interval = 1000;
            label_newDevice_info.Text = "";
            label_configPath.Text = "";
            //typingTimer.Tick += new EventHandler(this.typingTimerHandler);
            isChecked = checkBox_generateKeys.Checked;
            
            checkBox_generateKeys.Visible = false; // Uncomment this row to allow user to set Device keys automatically

            button_removeJson.Visible = false;
            //AzureStreamAnalyticsInput input;

            try
            {
                AzureStreamAnalyticsJob job = Globals.streamAnalyticsList.Find(x => x.properties.inputs.Find(y => y.properties.datasource.properties.iotHubNamespace.Equals(Globals.iotHub.name)) != null);
                List<AzureTransformationPair> pairs = job.ParseQueryToTransformPairs();
                foreach(AzureTransformationPair pair in pairs)
                {
                    storages.Add(Globals.storageAccountList.Find(x => x.name.Equals(pair.output.properties.datasource.properties.accountName)));
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }

            foreach(AzureStorageAccount sa in storages)
            {
                CloudStorageAccount tempSA = new CloudStorageAccount(new Microsoft.WindowsAzure.Storage.Auth.StorageCredentials(sa.name, sa.keys[0].value), true);
                CloudTableClient tableClient = tempSA.CreateCloudTableClient();
                CloudTable tableReference = tableClient.GetTableReference("specifications");
                if(tableReference != null)
                {
                    storageAccount = sa;
                    break;
                }
            }
            configurations = TableHelper.fetchConfigurationTableWithSdk(storageAccount.name, storageAccount.keys[0].value);
            specifications = TableHelper.fetchSpecificationTableWithSdk(storageAccount.name, storageAccount.keys[0].value);
            if(configurations != null)
            {
                foreach (ConfigurationTableRow config in configurations)
                {
                    models.Add(config.PartitionKey);
                }
            }

            if(specifications != null)
            {
                foreach (SpecificationTableRow spec in specifications)
                {
                    if (!owners.Contains(spec.PartitionKey))
                    {
                        owners.Add(spec.PartitionKey);
                    }
                }
            }

            comboBox_owner.Items.AddRange(owners.ToArray());
            comboBox_model.Items.AddRange(models.ToArray());

            comboBox_owner.Text = comboPlaceholder;
            comboBox_model.Text = comboPlaceholder;
            comboBox_version.Text = comboPlaceholder;
            checkComboBoxes();
        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            this.name = textBox_newDeviceName.Text;
            this.primaryKey = textBox_primaryKey.Text;
            this.secondaryKey = textBox_secondaryKey.Text;
            this.autoKeys = checkBox_generateKeys.Checked;
            if (comboBox_owner.Text.Equals(comboPlaceholder))
            {
                selectedOwner = null;
            }
            if (comboBox_model.Text.Equals(comboPlaceholder))
            {
                selectedModel = null;
            }
            if (comboBox_version.Text.Equals(comboPlaceholder))
            {
                selectedVersion = null;
            }
            //this.specificationTableRow = new SpecificationTableRow(selectedOwner, textBox_newDeviceName.Text, selectedModel, selectedVersion);
            //this.configurationTableRow = new ConfigurationTableRow(selectedModel, selectedVersion, null);
            //Kostyl Start
            this.specificationTableRow = new SpecificationTableRow(comboBox_owner.Text, textBox_newDeviceName.Text, comboBox_model.Text, comboBox_version.Text);
            this.configurationTableRow = new ConfigurationTableRow(comboBox_model.Text, comboBox_version.Text, null);
            //Kostyl End
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void textBox_newResourceGroupName_TextChanged(object sender, EventArgs e)
        {
            isChecked = false;
            TextBox textBox = sender as TextBox;
            //textBoxString = textBox.Text;
            typingTimer.Stop();
            typingTimer.Start();
        }

        private void typingTimerHandler(object sender, EventArgs e)
        {
            if(!isChecked)
            {
                //Boolean rgExists = restOps.doHEAD("https://management.azure.com/subscriptions/" + Globals.subscription.subscriptionId + "/resourcegroups/" + textBoxString + "?api-version=2019-05-10",
                    //Globals.token);

                // Response responseObj = JObject.Parse(responseString).ToObject<Response>();

                //if (!rgExists)
                //{
                //    label_nameCheck.Text = "OK";
                //}
                //else
                //{
                //    label_nameCheck.Text = "X";
                //}
            }
            isChecked = true;
        }

        private void checkComboBoxes()
        {
            if (comboBox_owner.Text.Equals(comboPlaceholder) ||
                comboBox_model.Text.Equals(comboPlaceholder) ||
                comboBox_version.Text.Equals(comboPlaceholder))
            {
                button_ok.Enabled = false;
            }
            else
            {
                button_ok.Enabled = true;
            }
        }

        private void checkKeys()
        {
            if (!String.IsNullOrEmpty(textBox_primaryKey.Text) && 
                Regex.IsMatch(textBox_primaryKey.Text, @"^[a-zA-Z0-9+/=]+$") && 
                textBox_primaryKey.Text.Length >= 36 &&
                textBox_primaryKey.Text.Length%4 == 0)
            {
                pictureBox1.Image = Globals.iconSuccess;
                button_ok.Enabled = true;
                button_ok.Enabled = true;
                label_newDevice_info.Text = "";
            }
            else
            {
                pictureBox1.Image = Globals.iconError;
                button_ok.Enabled = false;
                button_ok.Enabled = false;
                label_newDevice_info.Text = "Keys must have Base64 format.";
            }

            if (!String.IsNullOrEmpty(textBox_primaryKey.Text) &&
                Regex.IsMatch(textBox_primaryKey.Text, @"^[a-zA-Z0-9+/=]+$") &&
                textBox_primaryKey.Text.Length >= 36 &&
                textBox_primaryKey.Text.Length % 4 == 0)
            {
                pictureBox2.Image = Globals.iconSuccess;
                button_ok.Enabled = true;
                button_ok.Enabled = true;
                label_newDevice_info.Text = "";
            }
            else
            {
                pictureBox2.Image = Globals.iconError;
                button_ok.Enabled = false;
                button_ok.Enabled = false;
                label_newDevice_info.Text = "Keys must have Base64 format.";
            }
        }

        private void CheckBox_generateKeys_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox_generateKeys.Checked)
            {
                textBox_primaryKey.Enabled = false;
                textBox_secondaryKey.Enabled = false;
                autoKeys = true;
            }
            else
            {
                textBox_primaryKey.Enabled = true;
                textBox_secondaryKey.Enabled = true;
                autoKeys = false;
            }
        }

        public List<String> getVersionsByModel(String model)
        {
            List<String> versions = new List<String>();
            foreach(ConfigurationTableRow config in configurations)
            {
                if(config.PartitionKey.Equals(model))
                {
                    versions.Add(config.RowKey);
                }
            }
            return versions;
        }

        public ConfigurationTableRow getConfiguration(String model, String version)
        {
            List<ConfigurationTableRow> configurationsByModel = new List<ConfigurationTableRow>();
            foreach(ConfigurationTableRow el in configurations)
            {
                if(el.PartitionKey.Equals(model))
                {
                    configurationsByModel.Add(el);
                }
            }

            foreach(ConfigurationTableRow el in configurationsByModel)
            {
                if(el.RowKey.Equals(version))
                {
                    return el;
                }
            }
            return null;
        }

        private void comboBox_model_SelectedValueChanged(object sender, EventArgs e)
        {
            
            filteredVersions.Clear();
            ComboBox comboboxModel = sender as ComboBox;
            selectedModel  = comboboxModel.Text;
            filteredVersions = getVersionsByModel(selectedModel);
            comboBox_version.Items.Clear();
            comboBox_version.Items.AddRange(filteredVersions.ToArray());
            if(filteredVersions.Count>0)
            {
                comboBox_version.SelectedItem = comboBox_version.Items[0];
            }
            checkComboBoxes();
        }

        private void button_browse_Click(object sender, EventArgs e)
        {
            String fileContent = string.Empty;
            String filePath = string.Empty;
            button_removeJson.Visible = true;

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "json files (*.json)|*.json|All files (*.*)|*.*";
            openFileDialog.DefaultExt = "json";
            openFileDialog.CheckFileExists = true;
            openFileDialog.CheckPathExists = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                //Get the path of specified file
                filePath = openFileDialog.FileName;
                label_configPath.Text = "..."+filePath.Substring(filePath.Length-40);
                //Read the contents of the file into a stream
                var fileStream = openFileDialog.OpenFile();
                comboBox_model.Enabled = false;
                comboBox_version.Enabled = false;

                using (StreamReader reader = new StreamReader(fileStream))
                {
                    fileContent = reader.ReadToEnd();

                    //Parse
                    fileContent = fileContent.Replace(@"\", "");
                    DeviceConfiguration deviceConfiguration = JsonConvert.DeserializeObject<DeviceConfiguration>(fileContent);

                    configurationTableRow = new ConfigurationTableRow(deviceConfiguration.model, deviceConfiguration.version, deviceConfiguration.sensors);
                }
            }
        }

        private void button_removeJson_Click(object sender, EventArgs e)
        {
            label_configPath.Text = "";
            configurationTableRow = null;
            comboBox_model.Enabled = true;
            comboBox_version.Enabled = true;
            button_removeJson.Visible = false;
        }

        private void comboBox_version_SelectedValueChanged(object sender, EventArgs e)
        {
            
            checkComboBoxes();
        }

        private void comboBox_owner_SelectedValueChanged(object sender, EventArgs e)
        {
            checkComboBoxes();
        }

        private void ComboBox_owner_TextChanged(object sender, EventArgs e)
        {
            checkComboBoxes();
        }

        private void ComboBox_model_TextChanged(object sender, EventArgs e)
        {
            checkComboBoxes();
        }

        private void ComboBox_version_TextChanged(object sender, EventArgs e)
        {
            checkComboBoxes();
        }

        private void TextBox_primaryKey_TextChanged(object sender, EventArgs e)
        {
            checkKeys();
        }

        private void TextBox_secondaryKey_TextChanged(object sender, EventArgs e)
        {
            checkKeys();
        }
    }
}

﻿namespace AzureConfig6
{
    partial class Form_ResourceGroup
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.button_remove = new System.Windows.Forms.Button();
            this.button_add = new System.Windows.Forms.Button();
            this.dataGridView_rGroups = new System.Windows.Forms.DataGridView();
            this.label_resourceGroup_combobox = new System.Windows.Forms.Label();
            this.button_back = new System.Windows.Forms.Button();
            this.button_next = new System.Windows.Forms.Button();
            this.label_Title = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_rGroups)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.button_remove);
            this.splitContainer1.Panel2.Controls.Add(this.button_add);
            this.splitContainer1.Panel2.Controls.Add(this.dataGridView_rGroups);
            this.splitContainer1.Panel2.Controls.Add(this.label_resourceGroup_combobox);
            this.splitContainer1.Panel2.Controls.Add(this.button_back);
            this.splitContainer1.Panel2.Controls.Add(this.button_next);
            this.splitContainer1.Panel2.Controls.Add(this.label_Title);
            this.splitContainer1.Size = new System.Drawing.Size(734, 461);
            this.splitContainer1.SplitterDistance = 186;
            this.splitContainer1.TabIndex = 0;
            // 
            // button_remove
            // 
            this.button_remove.Location = new System.Drawing.Point(103, 284);
            this.button_remove.Name = "button_remove";
            this.button_remove.Size = new System.Drawing.Size(75, 23);
            this.button_remove.TabIndex = 14;
            this.button_remove.Text = "Remove";
            this.button_remove.UseVisualStyleBackColor = true;
            this.button_remove.Click += new System.EventHandler(this.button_remove_Click);
            // 
            // button_add
            // 
            this.button_add.Location = new System.Drawing.Point(22, 284);
            this.button_add.Name = "button_add";
            this.button_add.Size = new System.Drawing.Size(75, 23);
            this.button_add.TabIndex = 13;
            this.button_add.Text = "Add";
            this.button_add.UseVisualStyleBackColor = true;
            this.button_add.Click += new System.EventHandler(this.button_add_Click);
            // 
            // dataGridView_rGroups
            // 
            this.dataGridView_rGroups.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_rGroups.Location = new System.Drawing.Point(21, 66);
            this.dataGridView_rGroups.Name = "dataGridView_rGroups";
            this.dataGridView_rGroups.Size = new System.Drawing.Size(505, 211);
            this.dataGridView_rGroups.TabIndex = 12;
            this.dataGridView_rGroups.SelectionChanged += new System.EventHandler(this.dataGridView_rGroups_SelectionChanged);
            // 
            // label_resourceGroup_combobox
            // 
            this.label_resourceGroup_combobox.AutoSize = true;
            this.label_resourceGroup_combobox.Location = new System.Drawing.Point(19, 50);
            this.label_resourceGroup_combobox.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_resourceGroup_combobox.Name = "label_resourceGroup_combobox";
            this.label_resourceGroup_combobox.Size = new System.Drawing.Size(123, 13);
            this.label_resourceGroup_combobox.TabIndex = 9;
            this.label_resourceGroup_combobox.Text = "Chose a resource group:";
            // 
            // button_back
            // 
            this.button_back.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_back.Location = new System.Drawing.Point(22, 410);
            this.button_back.Name = "button_back";
            this.button_back.Size = new System.Drawing.Size(114, 39);
            this.button_back.TabIndex = 4;
            this.button_back.Text = "Back";
            this.button_back.UseVisualStyleBackColor = true;
            this.button_back.Click += new System.EventHandler(this.button_back_Click);
            // 
            // button_next
            // 
            this.button_next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_next.Location = new System.Drawing.Point(412, 410);
            this.button_next.Name = "button_next";
            this.button_next.Size = new System.Drawing.Size(114, 39);
            this.button_next.TabIndex = 3;
            this.button_next.Text = "Next";
            this.button_next.UseVisualStyleBackColor = true;
            this.button_next.Click += new System.EventHandler(this.button_next_Click);
            // 
            // label_Title
            // 
            this.label_Title.AutoSize = true;
            this.label_Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_Title.Location = new System.Drawing.Point(19, 21);
            this.label_Title.Name = "label_Title";
            this.label_Title.Size = new System.Drawing.Size(124, 17);
            this.label_Title.TabIndex = 0;
            this.label_Title.Text = "Resource group";
            // 
            // Form_ResourceGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 461);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form_ResourceGroup";
            this.Load += new System.EventHandler(this.Form_ResourceGroup_Load);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_rGroups)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label_Title;
        private System.Windows.Forms.Button button_back;
        private System.Windows.Forms.Button button_next;
        private System.Windows.Forms.Label label_resourceGroup_combobox;
        private System.Windows.Forms.DataGridView dataGridView_rGroups;
        private System.Windows.Forms.Button button_remove;
        private System.Windows.Forms.Button button_add;
    }
}
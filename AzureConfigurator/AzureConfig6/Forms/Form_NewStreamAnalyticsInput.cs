﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AzureConfig6
{
    public partial class Form_NewStreamAnalyticsInput : Form
    {
        Timer typingTimer = new Timer();
        public String newInputName;
        public AzureIoThub iotHub;

        public Form_NewStreamAnalyticsInput()
        {
            InitializeComponent();
            typingTimer.Interval = 1000;
            //typingTimer.Tick += new EventHandler(this.typingTimerHandler);
            label_input_info.Text = "";
            refreshHubsGridView(Globals.hubList);
            button_ok.Enabled = false;
        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            this.newInputName = textBox_new_input_name.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void refreshHubsGridView(List<AzureIoThub> hubs)
        {
            dataGridView_hubsForInput.Columns.Clear();

            dataGridView_hubsForInput.AutoGenerateColumns = false;
            dataGridView_hubsForInput.AllowUserToAddRows = false;

            DataGridViewTextBoxColumn colName = new DataGridViewTextBoxColumn();
            colName.HeaderText = "IoT hub name";
            colName.DataPropertyName = "name";

            dataGridView_hubsForInput.Columns.Add(colName);

            dataGridView_hubsForInput.ReadOnly = true;
            dataGridView_hubsForInput.AllowUserToAddRows = false;
            dataGridView_hubsForInput.RowHeadersVisible = false;
            dataGridView_hubsForInput.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            if(hubs != null && hubs.Count>0)
            {
                BindingList<AzureIoThub> bindingList = new BindingList<AzureIoThub>(hubs);
                dataGridView_hubsForInput.DataSource = bindingList;
            }else
            {
                button_ok.Enabled = false;
            }
        }

        private void dataGridView_hubsForInput_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                iotHub = (AzureIoThub)dataGridView_hubsForInput.CurrentRow.DataBoundItem;
                if(iotHub.keys != null && iotHub.keys.Count>0)
                {
                    label_input_info.Text = "";
                }else
                {
                    button_ok.Enabled = false;
                    label_input_info.Text = "IoT hub is not ready. Try again in few minutes.";
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
            }
        }

        private void textBox_new_input_name_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(textBox_new_input_name.Text) && !String.IsNullOrWhiteSpace(textBox_new_input_name.Text))
            {
                List<String> inputNames = new List<string>();
                foreach (AzureStreamAnalyticsInput input in Globals.inputList)
                {
                    inputNames.Add(input.name);
                }
                if (Regex.IsMatch(textBox_new_input_name.Text, @"^[a-zA-Z0-9_-]+$") && !inputNames.Contains(textBox_new_input_name.Text) 
                    && textBox_new_input_name.Text.Length>2 && !Regex.IsMatch(textBox_new_input_name.Text, @"^\d"))
                {
                    button_ok.Enabled = true;
                    pictureBox.Image = Globals.iconSuccess;
                }
                else
                {
                    button_ok.Enabled = false;
                    pictureBox.Image = Globals.iconError;
                }
            }
            else
            {
                button_ok.Enabled = false;
                pictureBox.Image = null;
            }
        }
    }
}

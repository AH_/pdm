﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AzureConfig6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            Bitmap iconSuccess = AzureConfig6.Properties.Resources.success;
            Bitmap iconError = AzureConfig6.Properties.Resources.error;

            Globals.iconSuccess = iconSuccess;
            Globals.iconError = iconError;
            Globals.userMail = "pdmtests221@gmail.com";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Form_Login_With_AD form_Login_With_AD = new Form_Login_With_AD();
            form_Login_With_AD.MdiParent = this;
            form_Login_With_AD.FormBorderStyle = FormBorderStyle.None;
            form_Login_With_AD.Dock = DockStyle.Fill;
            form_Login_With_AD.Show();
        }

    }
}

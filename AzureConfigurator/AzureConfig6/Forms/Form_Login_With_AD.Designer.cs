﻿namespace AzureConfig6
{
    partial class Form_Login_With_AD
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.label_tenant_id_title = new System.Windows.Forms.Label();
            this.label_application_id_title = new System.Windows.Forms.Label();
            this.label_client_secret_title = new System.Windows.Forms.Label();
            this.textBox_tenant_id = new System.Windows.Forms.TextBox();
            this.textBox_application_id = new System.Windows.Forms.TextBox();
            this.textBox_client_secret = new System.Windows.Forms.TextBox();
            this.button_login = new System.Windows.Forms.Button();
            this.label_login_info = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label_tenant_id_title
            // 
            this.label_tenant_id_title.AutoSize = true;
            this.label_tenant_id_title.Location = new System.Drawing.Point(94, 293);
            this.label_tenant_id_title.Name = "label_tenant_id_title";
            this.label_tenant_id_title.Size = new System.Drawing.Size(55, 13);
            this.label_tenant_id_title.TabIndex = 0;
            this.label_tenant_id_title.Text = "Tenant id:";
            // 
            // label_application_id_title
            // 
            this.label_application_id_title.AutoSize = true;
            this.label_application_id_title.Location = new System.Drawing.Point(76, 319);
            this.label_application_id_title.Name = "label_application_id_title";
            this.label_application_id_title.Size = new System.Drawing.Size(73, 13);
            this.label_application_id_title.TabIndex = 1;
            this.label_application_id_title.Text = "Application id:";
            // 
            // label_client_secret_title
            // 
            this.label_client_secret_title.AutoSize = true;
            this.label_client_secret_title.Location = new System.Drawing.Point(55, 342);
            this.label_client_secret_title.Name = "label_client_secret_title";
            this.label_client_secret_title.Size = new System.Drawing.Size(94, 13);
            this.label_client_secret_title.TabIndex = 2;
            this.label_client_secret_title.Text = "Application secret:";
            // 
            // textBox_tenant_id
            // 
            this.textBox_tenant_id.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_tenant_id.Location = new System.Drawing.Point(167, 290);
            this.textBox_tenant_id.Name = "textBox_tenant_id";
            this.textBox_tenant_id.Size = new System.Drawing.Size(481, 20);
            this.textBox_tenant_id.TabIndex = 3;
            // 
            // textBox_application_id
            // 
            this.textBox_application_id.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_application_id.Location = new System.Drawing.Point(167, 316);
            this.textBox_application_id.Name = "textBox_application_id";
            this.textBox_application_id.Size = new System.Drawing.Size(481, 20);
            this.textBox_application_id.TabIndex = 4;
            // 
            // textBox_client_secret
            // 
            this.textBox_client_secret.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_client_secret.Location = new System.Drawing.Point(167, 342);
            this.textBox_client_secret.Name = "textBox_client_secret";
            this.textBox_client_secret.Size = new System.Drawing.Size(481, 20);
            this.textBox_client_secret.TabIndex = 5;
            // 
            // button_login
            // 
            this.button_login.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_login.Location = new System.Drawing.Point(541, 368);
            this.button_login.Name = "button_login";
            this.button_login.Size = new System.Drawing.Size(107, 30);
            this.button_login.TabIndex = 6;
            this.button_login.Text = "Log in";
            this.button_login.UseVisualStyleBackColor = true;
            this.button_login.Click += new System.EventHandler(this.button_login_Click);
            // 
            // label_login_info
            // 
            this.label_login_info.AutoSize = true;
            this.label_login_info.Location = new System.Drawing.Point(164, 377);
            this.label_login_info.Name = "label_login_info";
            this.label_login_info.Size = new System.Drawing.Size(35, 13);
            this.label_login_info.TabIndex = 7;
            this.label_login_info.Text = "label1";
            // 
            // Form_Login_With_AD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 461);
            this.Controls.Add(this.label_login_info);
            this.Controls.Add(this.button_login);
            this.Controls.Add(this.textBox_client_secret);
            this.Controls.Add(this.textBox_application_id);
            this.Controls.Add(this.textBox_tenant_id);
            this.Controls.Add(this.label_client_secret_title);
            this.Controls.Add(this.label_application_id_title);
            this.Controls.Add(this.label_tenant_id_title);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form_Login_With_AD";
            this.Text = "Form_Login_With_AD";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_tenant_id_title;
        private System.Windows.Forms.Label label_application_id_title;
        private System.Windows.Forms.Label label_client_secret_title;
        private System.Windows.Forms.TextBox textBox_tenant_id;
        private System.Windows.Forms.TextBox textBox_application_id;
        private System.Windows.Forms.TextBox textBox_client_secret;
        private System.Windows.Forms.Button button_login;
        private System.Windows.Forms.Label label_login_info;
    }
}
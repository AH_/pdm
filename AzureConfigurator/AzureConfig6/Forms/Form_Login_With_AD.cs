﻿using FireSharp.Interfaces;
using FireSharp.Response;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AzureConfig6
{
    public partial class Form_Login_With_AD : Form
    {
        public Form_Login_With_AD()
        {
            InitializeComponent();
            label_login_info.Text = "";
            textBox_tenant_id.Text = "f73483c9-3433-41b7-872f-c33a427f0eab";
            textBox_application_id.Text = "0d221a9d-230e-478d-a231-f6e39b6ed63a";
            textBox_client_secret.Text = "123456789";

            Globals.token = null;
        }

        private async void button_login_Click(object sender, EventArgs e)
        {
            String tenantId = textBox_tenant_id.Text;
            String applicationId = textBox_application_id.Text;
            String clientSecret = textBox_client_secret.Text;

            Globals.token = REST.GetAccessToken(tenantId, applicationId, clientSecret);
            //TODO put auth here!!!

            if(Globals.token != null)
            {
                Globals.tenantId = tenantId;
                Globals.applicationId = applicationId;

                Form_Subscription fSubscription = new Form_Subscription();
                fSubscription.MdiParent = this.MdiParent;
                fSubscription.FormBorderStyle = FormBorderStyle.None;
                fSubscription.Dock = DockStyle.Fill;
                this.Close();
                fSubscription.Show();
            }
            else
            {
                label_login_info.Text = "Can't login, check credentials.";
            }
        }
    }
}
﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AzureConfig6
{
    public partial class Form_NewStreamAnalyticsOutput : Form
    {
        Timer typingTimer = new Timer();
        public String newOutputName;
        public AzureStorageAccount storageAccount;

        public Form_NewStreamAnalyticsOutput()
        {
            InitializeComponent();
            typingTimer.Interval = 1000;
            //typingTimer.Tick += new EventHandler(this.typingTimerHandler);
            refreshStorageGridView(Globals.storageAccountList);
            button_ok.Enabled = false;
        }

        private void refreshStorageGridView(List<AzureStorageAccount> accounts)
        {
            dataGridView_storageAccount.Columns.Clear();

            dataGridView_storageAccount.AutoGenerateColumns = false;
            dataGridView_storageAccount.AllowUserToAddRows = false;

            DataGridViewTextBoxColumn colName = new DataGridViewTextBoxColumn();
            colName.HeaderText = "Storage account name";
            colName.DataPropertyName = "name";

            DataGridViewTextBoxColumn colLocation = new DataGridViewTextBoxColumn();
            colLocation.HeaderText = "Storage account location";
            colLocation.DataPropertyName = "location";

            DataGridViewTextBoxColumn colId = new DataGridViewTextBoxColumn();
            colId.HeaderText = "Storage account id";
            colId.DataPropertyName = "id";

            DataGridViewTextBoxColumn colKind = new DataGridViewTextBoxColumn();
            colId.HeaderText = "Storage account kind";
            colId.DataPropertyName = "kind";

            dataGridView_storageAccount.Columns.Add(colName);
            dataGridView_storageAccount.Columns.Add(colLocation);
            dataGridView_storageAccount.Columns.Add(colId);

            dataGridView_storageAccount.ReadOnly = true;
            dataGridView_storageAccount.AllowUserToAddRows = false;
            dataGridView_storageAccount.RowHeadersVisible = false;
            dataGridView_storageAccount.AllowUserToResizeRows = false;
            dataGridView_storageAccount.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            if(accounts != null && accounts.Count>0)
            {
                BindingList<AzureStorageAccount> bindingList = new BindingList<AzureStorageAccount>(accounts);
                dataGridView_storageAccount.DataSource = bindingList;
                button_ok.Enabled = true;
            }else
            {
                button_ok.Enabled = false;
            }

        }

        private async void dataGridView_sAccs_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                this.storageAccount = (AzureStorageAccount)dataGridView_storageAccount.CurrentRow.DataBoundItem;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            this.newOutputName = textBox_new_output_name.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void textBox_new_output_name_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(textBox_new_output_name.Text) && !String.IsNullOrWhiteSpace(textBox_new_output_name.Text))
            {
                List<String> outputNames = new List<string>();
                foreach(AzureStreamAnalyticsOutput output in Globals.outputList)
                {
                    outputNames.Add(output.name);
                }
                if(Regex.IsMatch(textBox_new_output_name.Text, @"^[a-zA-Z0-9_-]+$") && !outputNames.Contains(textBox_new_output_name.Text) 
                    && textBox_new_output_name.Text.Length>2 && !Regex.IsMatch(textBox_new_output_name.Text, @"^\d"))
                {
                    button_ok.Enabled = true;
                    pictureBox.Image = Globals.iconSuccess;
                }
                else
                {
                    button_ok.Enabled = false;
                    pictureBox.Image = Globals.iconError;
                }
            }
            else
            {
                button_ok.Enabled = false;
                pictureBox.Image = null;
            }
        }
    }
}

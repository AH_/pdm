﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AzureConfig6
{
    public partial class Form_NewStreamAnalyticsTransform : Form
    {
        public AzureTransformationPair transformationPair;
        public AzureStreamAnalyticsInput input;
        public AzureStreamAnalyticsOutput output;

        public Form_NewStreamAnalyticsTransform()
        {
            InitializeComponent();

            refreshInputsGridView(Globals.inputList);
            refreshOutputsGridView(Globals.outputList);

            label_transformation_info.Text = "";
        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            transformationPair = new AzureTransformationPair(input, output);

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void refreshInputsGridView(List<AzureStreamAnalyticsInput> inputs)
        {
            dataGridView_inputs.Columns.Clear();

            dataGridView_inputs.AutoGenerateColumns = false;
            dataGridView_inputs.AllowUserToAddRows = false;

            DataGridViewTextBoxColumn colName = new DataGridViewTextBoxColumn();
            colName.HeaderText = "Input name";
            colName.DataPropertyName = "name";

            dataGridView_inputs.Columns.Add(colName);

            dataGridView_inputs.ReadOnly = true;
            dataGridView_inputs.AllowUserToAddRows = false;
            dataGridView_inputs.RowHeadersVisible = false;
            dataGridView_inputs.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView_inputs.MultiSelect = true;
            dataGridView_inputs.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            if(inputs != null && inputs.Count>0)
            {
                BindingList<AzureStreamAnalyticsInput> bindingList = new BindingList<AzureStreamAnalyticsInput>(inputs);
                dataGridView_inputs.DataSource = bindingList;
                button_ok.Enabled = true;
            }else
            {
                button_ok.Enabled = false;
            }
        }
        private void refreshOutputsGridView(List<AzureStreamAnalyticsOutput> outputs)
        {
            dataGridView_outputs.Columns.Clear();

            dataGridView_outputs.AutoGenerateColumns = false;
            dataGridView_outputs.AllowUserToAddRows = false;

            DataGridViewTextBoxColumn colName = new DataGridViewTextBoxColumn();
            colName.HeaderText = "Output name";
            colName.DataPropertyName = "name";

            dataGridView_outputs.Columns.Add(colName);

            dataGridView_outputs.ReadOnly = true;
            dataGridView_outputs.AllowUserToAddRows = false;
            dataGridView_outputs.RowHeadersVisible = false;
            dataGridView_outputs.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView_outputs.MultiSelect = true;
            dataGridView_outputs.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            if(outputs != null && outputs.Count>0)
            {
                BindingList<AzureStreamAnalyticsOutput> bindingList = new BindingList<AzureStreamAnalyticsOutput>(outputs);
                dataGridView_outputs.DataSource = bindingList;
                button_ok.Enabled = true;
            }else
            {
                button_ok.Enabled = false;
            }
        }
        private void dataGridView_inputs_SelectionChanged(object sender, EventArgs e)
        {
            input = (AzureStreamAnalyticsInput)dataGridView_inputs.CurrentRow.DataBoundItem;
            foreach (AzureTransformationPair pair in Globals.transformationPairs)
            {
                if (pair.input.name.Equals(input.name))
                {
                    button_ok.Enabled = false;
                    label_transformation_info.Text = "This IoT hub is already connected to storage.";
                    break;
                }
                else
                {
                    button_ok.Enabled = true;
                    label_transformation_info.Text = "";
                }
            }


            //Checks whole transformation pair.
            //if(output != null)
            //{
            //    transformationPair = new AzureTransformationPair(input, output);
            //    foreach(AzureTransformationPair pair in Globals.transformationPairs)
            //    {
            //        if(transformationPair.Equals(pair))
            //        {
            //            button_ok.Enabled = false;
            //            label_transformation_info.Text = "This input and output combination already exists.";
            //            break;
            //        }
            //        else
            //        {
            //            button_ok.Enabled = true;
            //            label_transformation_info.Text = "";
            //        }
            //    }
            //}
        }
        private void dataGridView_outputs_SelectionChanged(object sender, EventArgs e)
        {
            output = (AzureStreamAnalyticsOutput)dataGridView_outputs.CurrentRow.DataBoundItem;

            //if(input != null)
            //{
            //    redundant
            //    transformationPair = new AzureTransformationPair(input, output);
            //    foreach (AzureTransformationPair pair in Globals.transformationPairs)
            //    {
            //        if (transformationPair.Equals(pair))
            //        {
            //            button_ok.Enabled = false;
            //            label_transformation_info.Text = "This input and output combination already exists.";
            //            break;
            //        }
            //        else
            //        {
            //            button_ok.Enabled = true;
            //            label_transformation_info.Text = "";
            //        }
            //    }

            //    //Kostyl
            //    input = (AzureStreamAnalyticsInput)dataGridView_inputs.CurrentRow.DataBoundItem;
            //    foreach (AzureTransformationPair pair in Globals.transformationPairs)
            //    {
            //        if (pair.input.name.Equals(input.name))
            //        {
            //            button_ok.Enabled = false;
            //            label_transformation_info.Text = "This IoT hub is already connected to storage.";
            //            break;
            //        }
            //        else
            //        {
            //            button_ok.Enabled = true;
            //            label_transformation_info.Text = "";
            //        }
            //    }
            //}
        }

    }
}

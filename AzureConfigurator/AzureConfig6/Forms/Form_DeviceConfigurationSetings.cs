﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using ObjectFlattenerRecomposer;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AzureConfig6
{
    public partial class Form_DeviceConfigurationSetings : Form
    {
        AzureStorageAccount storage;
        ConfigurationTableRow deviceCofig;
        SpecificationTableRow deviceSpec;
        List<Sensor> sensors;
        List<Sensor> sensorsInTable;
        Boolean specificationChanged = false;

        private class Response
        {
            public bool nameAvailable { get; set; }
            public String reason { get; set; }
            public String message { get; set; }
        }

        public Form_DeviceConfigurationSetings(SpecificationTableRow deviceSpecification, ConfigurationTableRow deviceCofiguration, AzureStorageAccount storage)
        {
            InitializeComponent();
            this.storage = storage;
            sensors = new List<Sensor>();
            sensorsInTable = new List<Sensor>();

            this.deviceSpec = deviceSpecification;

            if (deviceCofiguration != null)
            {
                this.deviceCofig = deviceCofiguration;
            }
            else
            {
                this.deviceCofig = new ConfigurationTableRow();
                this.deviceCofig.PartitionKey = deviceSpec.Model;
                this.deviceCofig.RowKey = deviceSpec.Version;
            }
            
            try
            {
                sensorsInTable = copyList(deviceCofig.Sensors);
                sensors = copyList(deviceCofig.Sensors);
                refreshSensorsGridView(sensors);
            }catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            ClearEmptySensors(sensors);
            if(!listsEquals(sensors, sensorsInTable))
            {
                deviceCofig.Sensors = sensors;
                TableHelper.putConfiguration(storage.name, storage.keys[0].value, deviceCofig);
                Console.WriteLine("Updated...");
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void refreshSensorsGridView(List<Sensor> sensors)
        {
            dataGridView_sensors.Columns.Clear();

            DataGridViewTextBoxColumn colId = new DataGridViewTextBoxColumn();
            colId.HeaderText = "Sensor ID";
            colId.DataPropertyName = "Id";

            DataGridViewTextBoxColumn colName = new DataGridViewTextBoxColumn();
            colName.HeaderText = "Sensor name";
            colName.DataPropertyName = "Name";

            DataGridViewTextBoxColumn colUnits = new DataGridViewTextBoxColumn();
            colUnits.HeaderText = "Sensor units";
            colUnits.DataPropertyName = "Units";

            dataGridView_sensors.Columns.Add(colId);
            dataGridView_sensors.Columns.Add(colName);
            dataGridView_sensors.Columns.Add(colUnits);

            dataGridView_sensors.ReadOnly = false;
            dataGridView_sensors.AutoGenerateColumns = false;
            dataGridView_sensors.AllowUserToAddRows = false;
            dataGridView_sensors.RowHeadersVisible = false;
            dataGridView_sensors.AllowUserToResizeRows = false;
            dataGridView_sensors.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            BindingList<Sensor> bindingList = new BindingList<Sensor>(sensors);
            dataGridView_sensors.DataSource = bindingList;
        }

        private void dataGridView_sensors_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            Console.WriteLine("CHECK...");
            /*
            if(sensorsInTable.Count == sensors.Count)
            {
                Console.WriteLine(sensorsInTable[e.RowIndex].Equals(sensors[e.RowIndex]));
            }
            else
            {
                Console.WriteLine(false);
            }
            */
            //specificationChanged = !listsEquals(sensors, sensorsInTable);
        }

        private List<Sensor> copyList(List<Sensor> list)
        {
            List<Sensor> resultList = new List<Sensor>();
            foreach(Sensor el in list)
            {
                Sensor s = new Sensor();
                s.id = el.id;
                s.name = el.name;
                s.units = el.units;
                resultList.Add(s);
            }
            return resultList;
        }

        private bool listsEquals(List<Sensor> list1, List<Sensor> list2)
        {
            if(list1.Count == list2.Count)
            {
                for(int i=0; i<list1.Count; i++)
                {
                    if(!list1[i].Equals(list2[i]))
                    {
                        return false;
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public void ClearEmptySensors(List<Sensor> sensors)
        {
            for(int i = sensors.Count-1; i>=0; i--)
            {
                if(sensors[i].IsNull())
                {
                    sensors.RemoveAt(i);
                }
            }
        }

        private void button_addRow_Click(object sender, EventArgs e)
        {
            sensors.Add(new Sensor());
            refreshSensorsGridView(sensors);
        }

        private void button_removeRow_Click(object sender, EventArgs e)
        {
            sensors.Remove((Sensor)dataGridView_sensors.CurrentRow.DataBoundItem);
            refreshSensorsGridView(sensors);
        }
    }

    class DeviceSpecificationToFlatten
    {
        public DeviceSpecificationToFlatten(String pk, String rk, String model, String version)
        {
            this.PartitionKey = pk;
            this.RowKey = rk;
            this.Model = model;
            this.Version = version;
        }

        public String PartitionKey { get; set; }        //Owner
        public String RowKey { get; set; }      //Serial
        public String Timestamp { get; set; }
        public String Model { get; set; }
        public String Version { get; set; }
    }

    class DeviceConfigurationToFlatten
    {
        public DeviceConfigurationToFlatten(String pk, String rk, List<Sensor> sensors)
        {
            this.PartitionKey = pk;
            this.RowKey = rk;
            this.Sensors = sensors;
            
        }

        public String PartitionKey { get; set; }        //Model
        public String RowKey { get; set; }      //Version
        public String Timestamp { get; set; }
        public List<Sensor> Sensors { get; set; }
    }
}

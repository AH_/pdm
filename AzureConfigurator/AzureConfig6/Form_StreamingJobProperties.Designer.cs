﻿namespace AzureConfig6
{
    partial class Form_StreamingJobProperties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_iotHub_title = new System.Windows.Forms.Label();
            this.button_ok = new System.Windows.Forms.Button();
            this.label_iotHub = new System.Windows.Forms.Label();
            this.label_device = new System.Windows.Forms.Label();
            this.label_device_title = new System.Windows.Forms.Label();
            this.label_storageAccount = new System.Windows.Forms.Label();
            this.label_storageAccount_title = new System.Windows.Forms.Label();
            this.label_table = new System.Windows.Forms.Label();
            this.label_table_title = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label_iotHub_title
            // 
            this.label_iotHub_title.AutoSize = true;
            this.label_iotHub_title.Location = new System.Drawing.Point(30, 9);
            this.label_iotHub_title.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_iotHub_title.Name = "label_iotHub_title";
            this.label_iotHub_title.Size = new System.Drawing.Size(60, 17);
            this.label_iotHub_title.TabIndex = 0;
            this.label_iotHub_title.Text = "IoT hub:";
            // 
            // button_ok
            // 
            this.button_ok.Location = new System.Drawing.Point(539, 429);
            this.button_ok.Margin = new System.Windows.Forms.Padding(4);
            this.button_ok.Name = "button_ok";
            this.button_ok.Size = new System.Drawing.Size(100, 28);
            this.button_ok.TabIndex = 4;
            this.button_ok.Text = "Close";
            this.button_ok.UseVisualStyleBackColor = true;
            this.button_ok.Click += new System.EventHandler(this.button_ok_Click);
            // 
            // label_iotHub
            // 
            this.label_iotHub.AutoSize = true;
            this.label_iotHub.Location = new System.Drawing.Point(93, 9);
            this.label_iotHub.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_iotHub.Name = "label_iotHub";
            this.label_iotHub.Size = new System.Drawing.Size(49, 17);
            this.label_iotHub.TabIndex = 5;
            this.label_iotHub.Text = "iotHub";
            // 
            // label_device
            // 
            this.label_device.AutoSize = true;
            this.label_device.Location = new System.Drawing.Point(93, 39);
            this.label_device.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_device.Name = "label_device";
            this.label_device.Size = new System.Drawing.Size(49, 17);
            this.label_device.TabIndex = 7;
            this.label_device.Text = "device";
            // 
            // label_device_title
            // 
            this.label_device_title.AutoSize = true;
            this.label_device_title.Location = new System.Drawing.Point(13, 39);
            this.label_device_title.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_device_title.Name = "label_device_title";
            this.label_device_title.Size = new System.Drawing.Size(77, 17);
            this.label_device_title.TabIndex = 6;
            this.label_device_title.Text = "IoT device:";
            // 
            // label_storageAccount
            // 
            this.label_storageAccount.AutoSize = true;
            this.label_storageAccount.Location = new System.Drawing.Point(477, 9);
            this.label_storageAccount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_storageAccount.Name = "label_storageAccount";
            this.label_storageAccount.Size = new System.Drawing.Size(56, 17);
            this.label_storageAccount.TabIndex = 9;
            this.label_storageAccount.Text = "storage";
            // 
            // label_storageAccount_title
            // 
            this.label_storageAccount_title.AutoSize = true;
            this.label_storageAccount_title.Location = new System.Drawing.Point(353, 9);
            this.label_storageAccount_title.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_storageAccount_title.Name = "label_storageAccount_title";
            this.label_storageAccount_title.Size = new System.Drawing.Size(116, 17);
            this.label_storageAccount_title.TabIndex = 8;
            this.label_storageAccount_title.Text = "Storage account:";
            // 
            // label_table
            // 
            this.label_table.AutoSize = true;
            this.label_table.Location = new System.Drawing.Point(477, 39);
            this.label_table.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_table.Name = "label_table";
            this.label_table.Size = new System.Drawing.Size(56, 17);
            this.label_table.TabIndex = 11;
            this.label_table.Text = "storage";
            // 
            // label_table_title
            // 
            this.label_table_title.AutoSize = true;
            this.label_table_title.Location = new System.Drawing.Point(372, 39);
            this.label_table_title.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_table_title.Name = "label_table_title";
            this.label_table_title.Size = new System.Drawing.Size(97, 17);
            this.label_table_title.TabIndex = 10;
            this.label_table_title.Text = "Storage table:";
            // 
            // Form_StreamingJobProperties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(652, 470);
            this.Controls.Add(this.label_table);
            this.Controls.Add(this.label_table_title);
            this.Controls.Add(this.label_storageAccount);
            this.Controls.Add(this.label_storageAccount_title);
            this.Controls.Add(this.label_device);
            this.Controls.Add(this.label_device_title);
            this.Controls.Add(this.label_iotHub);
            this.Controls.Add(this.button_ok);
            this.Controls.Add(this.label_iotHub_title);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form_StreamingJobProperties";
            this.Text = "Form_DeviceProperties";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_iotHub_title;
        private System.Windows.Forms.Button button_ok;
        private System.Windows.Forms.Label label_iotHub;
        private System.Windows.Forms.Label label_device;
        private System.Windows.Forms.Label label_device_title;
        private System.Windows.Forms.Label label_storageAccount;
        private System.Windows.Forms.Label label_storageAccount_title;
        private System.Windows.Forms.Label label_table;
        private System.Windows.Forms.Label label_table_title;
    }
}
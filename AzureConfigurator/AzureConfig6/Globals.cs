﻿using FireSharp.Config;
using FireSharp.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureConfig6
{
    public static class Globals
    {
        public static System.Drawing.Bitmap iconSuccess { get; set; }
        public static System.Drawing.Bitmap iconError { get; set; }
        public static String userMail { get; set; }
        public static String token { get; set; }
        public static String tenantId { get; set; }
        public static String applicationId { get; set; }
        public static AzureSubscription subscription { get; set; }
        public static AzureResourceGroup resourceGroup { get; set; }
        public static AzureStorageAccount storageAccount { get; set; }
        public static List<AzureStorageAccount> storageAccountList { get; set; }
        public static AzureTable table { get; set; }
        public static AzureIoThub iotHub { get; set; }
        public static AzureIotDevice device { get; set; }

        public static AzureStreamAnalyticsJob streamAnalyticsJob { get; set; }
        public static List<AzureStreamAnalyticsJob> streamAnalyticsList { get; set; }

        public static List<AzureIoThub> hubList { get; set; }
        public static List<AzureTable> tableList { get; set; }
        public static List<AzureIotDevice> deviceList { get; set; }

        public static List<AzureStreamAnalyticsInput> inputList { get; set; }
        public static AzureStreamAnalyticsInput input { get; set; }
        public static List<AzureStreamAnalyticsOutput> outputList { get; set; }
        public static AzureStreamAnalyticsOutput output { get; set; }
        public static AzureStreamAnalyticsTransformation transformation { get; set; }
        public static Dictionary<String, AzureStorageAccount> storageAccountDict = new Dictionary<String, AzureStorageAccount>();

        public static List<SpecificationTableRow> specificationTableRows { get; set; }
        public static List<AzureTransformationPair> transformationPairs { get; set; }
        public static AzureTransformationPair transformationPair { get; set; }

        public static FirebaseConnectionData firebaseConnectionData { get; set; }
        public static IFirebaseConfig firebaseConfig;
        public static IFirebaseClient firebaseClient;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureConfig6
{
    public class FirebaseDevice
    {
        public String serial;
        public String storageName;
        public String storageKey;
        public String iotHubName;
        public String devicePrimaryKey;

        //"HostName=applicationhub.azure-devices.net;DeviceId=arduino;SharedAccessKey=9ipQja1TUIWJlbabVY7B93efEKUy6ZtYYLYK25IwiCE="

        public FirebaseDevice()
        {

        }

        public FirebaseDevice(String serial, String storageName, String storageKey, String iotHubName, String devicePrimaryKey)
        {
            this.serial = serial;
            this.storageName = storageName;
            this.storageKey = storageKey;
            this.iotHubName = iotHubName;
            this.devicePrimaryKey = devicePrimaryKey;
        }
    }
}
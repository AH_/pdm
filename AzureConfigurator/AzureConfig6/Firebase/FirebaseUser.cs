﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureConfig6
{
    public class FirebaseUser
    {
        public String tenantId { get; set; }
        public String clientId { get; set; }
        public String email { get; set; }
        public String password { get; set; }
        public Dictionary<String, FirebaseDevice> devices { get; set; }

        public FirebaseUser()
        {

        }

        public FirebaseUser(String email, String tenantId, String clientId)
        {
            this.email = email;
            this.tenantId = tenantId;
            this.clientId = clientId;
        }

        public FirebaseUser(String email, String tenantId, String clientId, String password, Dictionary<String, FirebaseDevice> devices)
        {
            this.email = email;
            this.tenantId = tenantId;
            this.clientId = clientId;
            this.password = password;
            this.devices = devices;
        }

        public Boolean Exists()
        {
            if(!String.IsNullOrEmpty(this.email))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

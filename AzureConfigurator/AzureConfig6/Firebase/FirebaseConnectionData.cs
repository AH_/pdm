﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureConfig6
{
    public class FirebaseConnectionData
    {
        //public FirebaseConnectionData(String url, String secret)
        //{
        //    firebaseUrl = url;
        //    firebaseSecret = secret;
        //    email = null;
        //}

        public FirebaseConnectionData(String url, String secret, String mail)
        {
            firebaseUrl = url;
            firebaseSecret = secret;
            email = mail;
        }

        public String firebaseUrl { get; set; }
        public String firebaseSecret { get; set; }
        public String email { get; set; }
    }
}

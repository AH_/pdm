﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureConfig6
{
    public class Sensor
    {
        public String id { get; set; }
        public String name { get; set; }
        public String units { get; set; }

        public Sensor()
        {
            id = String.Empty;
            name = String.Empty;
            units = String.Empty;
        }

        public override bool Equals(object obj)
        {
            Sensor sensor = obj as Sensor;

            if (sensor != null)
            {
                return (this.id.Equals(sensor.id) && this.name.Equals(sensor.name) && this.units.Equals(sensor.units));
            }
            else
            {
                return false;
            }
        }

        public bool IsNull()
        {
            if(
                String.IsNullOrEmpty(this.id) || String.IsNullOrWhiteSpace(this.id)
                &&
                String.IsNullOrEmpty(this.name) || String.IsNullOrWhiteSpace(this.name)
                &&
                String.IsNullOrEmpty(this.units) || String.IsNullOrWhiteSpace(this.units)
                )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

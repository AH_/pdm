﻿using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp.Response;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureConfig6
{
    /// <summary>
    /// Class provides static methos for work with Firebase Realtime database. Firebase Realtime database is used to store all neccessary data for mobile application.
    /// </summary>
    public class Firebase
    {
        //public static string BasePath = "https://androidiot-746ce.firebaseio.com/";
        //public static string AuthSecret = "zM0aw6GrUrh4mHNW3bpwHB7rgRmlgPk9z4kskiIC";
        public static String firebaseConfigPath = "firebaseConfig.json"; 

        /// <summary>
        /// Creates firebase realtime database client
        /// </summary>
        /// <param name="path"> <see cref="System.String"/> representing path to the firebase realtime database. Can be bound in Firebase portal.</param>
        /// <param name="secret"> <see cref="System.String"/> representing Firebase realtime database secret.  Can be bound in Firebase portal -> Project settings -> Service accounts -> Database secrets.</param>
        public static void InitFirebase()
        {
            try
            {
                Globals.firebaseConfig = new FirebaseConfig
                {
                    AuthSecret = Globals.firebaseConnectionData.firebaseSecret,
                    BasePath = Globals.firebaseConnectionData.firebaseUrl
                };
            }catch(Exception e)
            {
                Console.WriteLine(e);
            }


            Globals.firebaseClient = new FireSharp.FirebaseClient(Globals.firebaseConfig);
        }

        /// <summary>
        /// Just checking internet connection (trying to connect to google.com).
        /// </summary>
        /// <returns> Returns <see cref="System.Boolean"/> value representing state of internet connection.</returns>
        public static bool CheckInternetConnection()
        {
            try
            {
                System.Net.WebClient client = new System.Net.WebClient();
                using (var stream = client.OpenRead("http://www.google.com"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public static int TryDatabaseConnection()
        {
            if(Globals.firebaseClient != null)
            {
                FirebaseResponse firebaseResponse = Globals.firebaseClient.Get("");

                switch (firebaseResponse.Body)
                {
                    case "{\n  \"error\" : \"404 Not Found\"\n}\n":
                        return 1; //Wrong URL
                    case "{\n  \"error\" : \"Could not parse auth token.\"\n}\n":
                        return 2; //Wrong secret
                    default:
                        return 0;
                }
            }else
            {
                return -1;
            }
        }

        /// <summary>
        /// Retreives all devices of selected user in Firebase realtime database.
        /// </summary>
        /// <param name="username">The <see cref="System.String"/> instance that represents username.</param>
        /// <returns>A <see cref="List{T}"/> than contains all user's devices <see cref="AzureConfig6.FirebaseDevice"/> if user found; otherwise, <c>null</c>.</returns>
        public static Dictionary<String, FirebaseDevice> GetDevices(String username)
        {
            //Why GetTaskAsync hangs?
            //FirebaseResponse firebaseResponse = await client.GetTaskAsync("users/" + username.Replace('.', '-'));
            FirebaseResponse firebaseResponse = Globals.firebaseClient.Get("users/" + username);
            if(!firebaseResponse.Body.Equals("null"))
            {
                FirebaseUser user = firebaseResponse.ResultAs<FirebaseUser>();
                return user.devices;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Retreives current user from Firebase realtime database. User selected by applicationId, which is equal to username in Firebase realtime database.
        /// </summary>
        /// <returns>A <see cref="AzureConfig6.FirebaseUser"/> if user found; otherwise, <c>null</c>.</returns>
        public static FirebaseUser GetUser(String userMail)
        {
            //Why GetTaskAsync hangs?
            //FirebaseResponse firebaseResponse = client.GetTaskAsync("users/");
            FirebaseResponse firebaseResponse = Globals.firebaseClient.Get("users/" + userMail.Replace(".", "-") + "/");
            try
            {
                //  How to protect form wrong input format?
                FirebaseUser user = JObject.Parse(firebaseResponse.Body).ToObject<FirebaseUser>();
                //FirebaseUser user = JsonConvert.Deserialize<FirebaseUser>(firebaseResponse.Body);
                return user;
            }
            catch(NullReferenceException nre)
            {
                Console.WriteLine(nre);
                return null;
            }
        }

        /// <summary>
        /// Retreives list of Firebase users from realtime database.
        /// </summary>
        /// <returns>A <see cref="AzureConfig6.FirebaseUser"/> if user found; otherwise, <c>null</c>.</returns>
        public static Dictionary<String,FirebaseUser> ListUsers()
        {
            //Why GetTaskAsync hangs?
            //FirebaseResponse firebaseResponse = client.GetTaskAsync("users/");
            FirebaseResponse firebaseResponse = Globals.firebaseClient.Get("users/");
            try
            {
                //  How to protect form wrong input format?
                Dictionary<String, FirebaseUser> users = JObject.Parse(firebaseResponse.Body).ToObject<Dictionary<String, FirebaseUser>>();
                return users;
            }
            catch (NullReferenceException nre)
            {
                Console.WriteLine(nre);
                return null;
            }catch (JsonReaderException jre)
            {
                Console.WriteLine(jre);
                return null;
            }
        }

        /// <summary>
        /// Creates user in Firebase Realtime database.
        /// </summary>
        /// <param name="user">The <see cref="AzureConfig6.FirebaseUser"/> instance that represents username for new user.</param>
        public static void CreateUser(FirebaseUser user)
        {
            SetResponse setResponse = Globals.firebaseClient.Set("users/" + user.email.Replace(".", "-"), user);
        }

        /// <summary>
        /// Adds device to Firebase Realtime database for current user.
        /// </summary>
        /// <param name="device">The <see cref="AzureConfig6.AzureIotDevice"/> instance that represents a device that will be added for current user.</param>
        public static void AddDeviceToFirebase(AzureIotDevice device, String devicePrimaryKey)
        {
            //FirebaseResponse firebaseResponse = Firebase.client.Get("users/" + Globals.applicationId + "/devices");
            AzureStorageAccount storage = Globals.iotHub.getHubsStorageFromStreamAnalytics();
            SetResponse setResponse = Globals.firebaseClient.Set("users/" + Globals.firebaseConnectionData.email.Replace(".", "-") + "/devices/" + device.deviceId, new FirebaseDevice(device.deviceId, storage.name, storage.keys[0].value,
                Globals.iotHub.name, devicePrimaryKey));
        }

        /// <summary>
        /// Removes device from Firebase Realtime database for current user.
        /// </summary>
        /// <param name="device">The <see cref="AzureConfig6.AzureIotDevice"/> instance that represents a device that will be removed for current user.</param>
        //public static void RemoveDeviceFromFirebase(AzureIotDevice device)
        //{
        //    //FirebaseResponse firebaseResponse = Firebase.client.Get("users/" + Globals.applicationId + "/devices");
        //    //Dictionary<String, FirebaseDevice> devicesDict = JsonConvert.DeserializeObject<Dictionary<String, FirebaseDevice>>(firebaseResponse.Body); // Can be used to check if device already registrated
        //    DeleteResponse deleteResponse = client.Delete("users/" + Globals.userMail.Replace(".", "-") + "/devices/" + device.deviceId);
        //}

        //Used in AzureIoThub.RemoveDevice
        public static void RemoveDeviceFromFirebase(String deviceId)
        {
            //FirebaseResponse firebaseResponse = Firebase.client.Get("users/" + Globals.applicationId + "/devices");
            //Dictionary<String, FirebaseDevice> devicesDict = JsonConvert.DeserializeObject<Dictionary<String, FirebaseDevice>>(firebaseResponse.Body); // Can be used to check if device already registrated
            DeleteResponse deleteResponse = Globals.firebaseClient.Delete("users/" + Globals.userMail.Replace(".", "-") + "/devices/" + deviceId);
        }

        public static void SaveConnectionData(FirebaseConnectionData connectionData)
        {
            String connectionDataJson = JsonConvert.SerializeObject(connectionData);
            File.WriteAllText(firebaseConfigPath, connectionDataJson);
        }

        public static FirebaseConnectionData LoadConnectionData()
        {
            try
            {
                String connectionDataJson = File.ReadAllText(firebaseConfigPath);
                FirebaseConnectionData connectionData = JsonConvert.DeserializeObject<FirebaseConnectionData>(connectionDataJson);
                Globals.firebaseConnectionData = connectionData;
                if(connectionData.firebaseUrl != null && connectionData.firebaseSecret != null)
                {
                    Firebase.InitFirebase();

                    if (connectionData.email != null)
                    {
                        List<String> firebaseUsers = Firebase.ListUsers().Keys.ToList<String>();
                        if (!firebaseUsers.Contains(Globals.firebaseConnectionData.email))
                        {
                            Firebase.CreateUser(new FirebaseUser(Globals.firebaseConnectionData.email, Globals.tenantId, Globals.applicationId));
                        }
                    }
                }
                return connectionData;
            }
            catch(NullReferenceException nre)
            {
                Console.WriteLine(nre);
                return null;
            }
            catch(IOException e)
            {
                Console.WriteLine(e);
                return null;
            }
            catch(JsonSerializationException e)
            {
                Console.WriteLine(e);
                return null;
            }
        }
    }
}

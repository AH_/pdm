package com.example.ah.push;

import androidx.annotation.Nullable;
import java.util.ArrayList;

public class Configuration extends TableEntity{

    ArrayList<Sensor> sensorList;
    String sensors;

    public Configuration() {

    }

    public Configuration(String model, String version, String sensors, String etag) {
        this.partitionKey = model;
        this.rowKey = version;
        this.sensors = sensors;
        this.etag = etag;
    }

    public String getSensors() {
        return sensors;
    }

    public void setSensors(String sensors) {
        this.sensors = sensors;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        Configuration input = (Configuration) obj;
        if(input.partitionKey.equals(this.partitionKey) && input.rowKey.equals(this.rowKey))
        {
            return true;
        }else
        {
            return false;
        }
    }
}
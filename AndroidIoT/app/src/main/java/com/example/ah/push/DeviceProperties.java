package com.example.ah.push;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;
import com.microsoft.azure.storage.table.TableQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;


public class DeviceProperties extends AppCompatActivity {

    ListView listView;
    private static AdapterEditSensors adapterSens;
    FirebaseDevice parcelDevice;

    Button deleteButton;
    Button buttonApply;
    Spinner spinnerModel, spinnerVersion;

    Intent intent;
    String connString;
    String tableName;

    ArrayList<Sensor> deviceSensors;
    private static ArrayList<Configuration> configurationList;
    private Configuration configuration;

    ArrayAdapter<String> adapterModel, adapterVersion;
    Map<String, ArrayList<String>> sortedVersions = new HashMap<String, ArrayList<String>>();

    String deviceModel = null, deviceVersion = null;
    int sensorPosition;
    Boolean configurationChanged = false, specificationChanged = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_properties);

        intent = getIntent();
        parcelDevice = intent.getParcelableExtra("Device");
        getSupportActionBar().setTitle(parcelDevice.serial);

        deleteButton = (Button) findViewById(R.id.measureButton);
        buttonApply = (Button) findViewById(R.id.buttonApply);
        spinnerModel = (Spinner) findViewById(R.id.spinner_model);
        spinnerVersion = (Spinner) findViewById(R.id.spinner_version);

        listView = (ListView) findViewById(R.id.list);
        setListViewHeightBasedOnChildren(listView);

        connString = "DefaultEndpointsProtocol=https;AccountName="+parcelDevice.storageName+";AccountKey="+parcelDevice.storageKey+";EndpointSuffix=core.windows.net";
        tableName = "values";


        GetDeviceSpecificationTask getDeviceSpecificationTask = new GetDeviceSpecificationTask();
        getDeviceSpecificationTask.execute(parcelDevice.serial);
        try {
            Specification s = getDeviceSpecificationTask.get();
            deviceModel = s.Model;
            deviceVersion = s.Version;
        }catch (Exception e)
        {
            e.printStackTrace();
        }


        GetAllConfigurationsTask getAllConfigurationsTask = new GetAllConfigurationsTask();
        getAllConfigurationsTask.execute();



        GetDeviceConfigurationTask getConfigurationTask = new GetDeviceConfigurationTask();
        HashMap<String, String> modelVersionMap = new HashMap<String, String>();
        modelVersionMap.put("Model", deviceModel);
        modelVersionMap.put("Version", deviceVersion);

        try {
            configuration = getConfigurationTask.execute(modelVersionMap).get();
            deviceSensors = configuration.sensorList;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    private void SetModelAdapter(ArrayList<String> modelsList)
    {
        adapterModel = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, modelsList);
        adapterModel.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerModel.setAdapter(adapterModel);
        spinnerModel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SetVersionAdapter(sortedVersions.get(adapterModel.getItem(i)));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                SetVersionAdapter(null);
            }
        });
        int position = adapterModel.getPosition(deviceModel);
        spinnerModel.setSelection(position);
    }

    private void SetVersionAdapter(ArrayList<String> versionsList)
    {
        adapterVersion = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, versionsList);
        adapterVersion.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerVersion.setAdapter(adapterVersion);
        spinnerVersion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //GetDeviceConfigurationTask getDeviceConfigurationTask = new GetDeviceConfigurationTask();
                //getDeviceConfigurationTask.execute();
                for(Configuration c : configurationList)
                {
                    if(c.getPartitionKey().equals(spinnerModel.getSelectedItem().toString())
                            && c.getRowKey().equals(spinnerVersion.getSelectedItem().toString()))
                    {
                        configuration = c;
                        break;
                    }
                }
                configuration.sensorList = new Gson().fromJson(configuration.sensors.substring(40), new TypeToken<ArrayList<Sensor>>(){}.getType());
                deviceSensors = configuration.sensorList;

                adapterSens = new AdapterEditSensors(deviceSensors, getApplicationContext());
                listView.setAdapter(adapterSens);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Intent editSensorIntent = new Intent(getApplicationContext(), EditSensorActivity.class);
                        sensorPosition = i;
                        editSensorIntent.putExtra("SensorName", ((TextView)view.findViewById(R.id.name)).getText().toString());
                        editSensorIntent.putExtra("SensorId", ((TextView)view.findViewById(R.id.id)).getText().toString());
                        editSensorIntent.putExtra("SensorUnits", ((TextView)view.findViewById(R.id.units)).getText().toString());

                        startActivityForResult(editSensorIntent, 0);
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        int position = adapterVersion.getPosition(deviceVersion);
        spinnerVersion.setSelection(position);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            if (resultCode == Activity.RESULT_OK) //    Sensor has been changed => replace Configuration Row
            {
                configurationChanged = true;
                Sensor editedSensor = new Sensor();
                editedSensor.id = data.getStringExtra("NewSensorId");
                editedSensor.name = data.getStringExtra("NewSensorName");
                editedSensor.units = data.getStringExtra("NewSensorUnits");

                deviceSensors.set(sensorPosition, editedSensor);
                //adapterSens.notifyDataSetChanged();

                //SetDeviceConfiguration(deviceSensors);
                SetConfigurationsTask setConfigurationsTask = new SetConfigurationsTask();
                setConfigurationsTask.execute(CreateConfigurationTest(deviceSensors));

                //      CONTINUE FROM HERE
                //      Exception on second update (eTag?)
            }
        }
    }

    private class GetDeviceSpecificationTask extends AsyncTask<String, Void, Specification>
    {

        @Override
        protected Specification doInBackground(String... serial) {
            try
            {
                // Retrieve storage account from connection-string.
                CloudStorageAccount storageAccount =
                        CloudStorageAccount.parse(connString);

                // Create the table client.
                CloudTableClient tableClient = storageAccount.createCloudTableClient();

                // Create a cloud table object for the table.
                CloudTable cloudTable = tableClient.getTableReference("specifications");

                // Retrieve the entity with partition key of "ah" and row key equals to device's serial.
                TableOperation retrieveSerial =
                        TableOperation.retrieve("ah", serial[0], Specification.class);

                // Submit the operation to the table service and get the specific entity.
                Specification specificEntity =
                        cloudTable.execute(retrieveSerial).getResultAsType();

                return  specificEntity;
            }
            catch (Exception e) {
                // Output the stack trace.
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Specification specification) {
            super.onPostExecute(specification);
            //getSupportActionBar().setTitle(pickedFirebaseDevice.serial+" "+stringStringHashMap.get("Model")+" ver: "+stringStringHashMap.get("Version"));

            deviceModel = specification.Model;
            deviceVersion = specification.Version;
        }
    }

    private class GetAllSpecificationsTask extends AsyncTask<Void, Void, ArrayList<Specification>>
    {
        @Override
        protected ArrayList<Specification> doInBackground(Void... voids)
        {
            try
            {
                // Retrieve storage account from connection-string.
                CloudStorageAccount storageAccount =
                        CloudStorageAccount.parse(connString);

                // Create the table client.
                CloudTableClient tableClient = storageAccount.createCloudTableClient();

                // Create a cloud table object for the table.
                CloudTable cloudTable = tableClient.getTableReference("specifications");

                String partitionFilter = TableQuery.generateFilterCondition(
                        "PartitionKey", TableQuery.QueryComparisons.NOT_EQUAL, "");

                TableQuery<Specification> query = TableQuery.from(
                        Specification.class
                ).where(partitionFilter);

                ArrayList<Specification> result = new ArrayList<>();
                // Output the entity.
                for(Specification el : cloudTable.execute(query))
                {
                    result.add(el);
                }
                return  result;
            }
            catch (Exception e) {
                // Output the stack trace.
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(ArrayList<Specification> specificationList)
        {
            super.onPostExecute(specificationList);

        }
    }

    private class GetDeviceConfigurationTask extends AsyncTask<HashMap<String, String>, Void, Configuration>
    {
        @Override
        protected Configuration doInBackground(HashMap<String, String>... hashMaps) {
            String model = hashMaps[0].get("Model");
            String version = hashMaps[0].get("Version");

            try
            {
                // Retrieve storage account from connection-string.
                CloudStorageAccount storageAccount =
                        CloudStorageAccount.parse(connString);

                // Create the table client.
                CloudTableClient tableClient = storageAccount.createCloudTableClient();

                // Create a cloud table object for the table.
                CloudTable cloudTable = tableClient.getTableReference("configurations");

                // Retrieve the entity with partition key of "Smith" and row key of "Jeff"
                TableOperation retrieveSerial =
                        TableOperation.retrieve(model, version, Configuration.class);

                // Submit the operation to the table service and get the specific entity.
                Configuration specificEntity =
                        cloudTable.execute(retrieveSerial).getResultAsType();

                specificEntity.sensorList = new Gson().fromJson(specificEntity.sensors.substring(40), new TypeToken<ArrayList<Sensor>>(){}.getType());

                return  specificEntity;
            }
            catch (Exception e) {
                // Output the stack trace.
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Configuration configuration) {
            super.onPostExecute(configuration);

            //deviceSensors = configuration.sensorList;
        }


    }

    private class GetAllConfigurationsTask extends AsyncTask<Void, Void, ArrayList<Configuration>>
    {
        @Override
        protected ArrayList<Configuration> doInBackground(Void... voids)
        {
            try
            {
                // Retrieve storage account from connection-string.
                CloudStorageAccount storageAccount =
                        CloudStorageAccount.parse(connString);

                // Create the table client.
                CloudTableClient tableClient = storageAccount.createCloudTableClient();

                // Create a cloud table object for the table.
                CloudTable cloudTable = tableClient.getTableReference("configurations");

                String partitionFilter = TableQuery.generateFilterCondition(
                        "PartitionKey", TableQuery.QueryComparisons.NOT_EQUAL, "");

                TableQuery<Configuration> query = TableQuery.from(
                        Configuration.class
                ).where(partitionFilter);

                ArrayList<Configuration> result = new ArrayList<>();
                // Output the entity.
                for(Configuration el : cloudTable.execute(query))
                {
                    result.add(el);
                }
                return  result;
            }
            catch (Exception e) {
                // Output the stack trace.
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(ArrayList<Configuration> configurations)
        {
            super.onPostExecute(configurations);
            configurationList = configurations;
            sortedVersions = SortVersionsByModels(configurations);
            SetModelAdapter(new ArrayList<String>(sortedVersions.keySet()));
        }
    }

    private class SetConfigurationsTask extends AsyncTask<Configuration, Void, Void>
    {
        @Override
        protected Void doInBackground(Configuration... c)
        {
            try
            {
                CloudStorageAccount storageAccount =
                        CloudStorageAccount.parse(connString);

                // Create the table client.
                CloudTableClient tableClient = storageAccount.createCloudTableClient();

                // Create a cloud table object for the table.
                CloudTable cloudTable = tableClient.getTableReference("configurations");

                cloudTable.execute(TableOperation.replace(c[0]));

                return null;
            }
            catch (Exception e) {
                // Output the stack trace.
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Void v)
        {
            super.onPostExecute(v);

            //  TEST
            GetDeviceConfigurationTask getConfigurationTask = new GetDeviceConfigurationTask();
            HashMap<String, String> modelVersionMap = new HashMap<>();
            modelVersionMap.put("Model", deviceModel);
            modelVersionMap.put("Version", deviceVersion);
            try {
                configuration = getConfigurationTask.execute(modelVersionMap).get();

            }catch (Exception e)
            {
                e.printStackTrace();
            }
            deviceSensors = configuration.sensorList;
            adapterSens.notifyDataSetChanged();
        }
    }

    private Configuration CreateConfigurationTest(ArrayList<Sensor> s)
    {
            //  Test

        Configuration editedConfiguration = new Configuration();
        editedConfiguration.setPartitionKey(deviceModel);
        editedConfiguration.setRowKey(deviceVersion);
        editedConfiguration.sensors = new Gson().toJson(s);
        editedConfiguration.sensors = "<|>jsonSerializedIEnumerableProperty<|>="+editedConfiguration.sensors;
        editedConfiguration.setEtag(configuration.getEtag());
        return editedConfiguration;
    }

    private Map<String, ArrayList<String>> SortVersionsByModels(ArrayList<Configuration> configurationList)
    {
        Map<String, ArrayList<String>> result = new HashMap<>();

        for(Configuration c : configurationList)
        {
            if(!result.containsKey(c.getPartitionKey()))
            {
                ArrayList<String> versions = new ArrayList<>();
                for(Configuration d : configurationList)
                {
                    if(d.getPartitionKey().equals(c.getPartitionKey()))
                    {
                        versions.add(d.getRowKey());
                    }
                }
                result.put(c.getPartitionKey(), versions);
            }
        }
        return  result;
    }

    public void onButtonDeleteClick(View v) {
        Intent answerIntent = new Intent();
        answerIntent.putExtra("indexToDelete", intent.getIntExtra("index", -1));
        setResult(RESULT_OK, answerIntent);
        DeviceProperties.this.finish();
    }

    public void onButtonSaveClick(View v)
    {

    }

    public void ToastNotify(final String notificationMessage) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(DeviceProperties.this, notificationMessage, Toast.LENGTH_LONG).show();
                //TextView helloText = (TextView) findViewById(R.id.text_hello);
                //helloText.setText(notificationMessage);
            }
        });
    }
}

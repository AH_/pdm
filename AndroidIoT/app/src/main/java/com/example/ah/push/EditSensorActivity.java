package com.example.ah.push;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class EditSensorActivity extends AppCompatActivity {

    EditText editTextName;
    EditText editTextId;
    EditText editTextUnits;

    String sensorName;
    String sensorId;
    String sensorUnits;

    Boolean sensorParameterChanged = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_sensor);

        editTextName = (EditText)findViewById(R.id.editText_name);
        editTextId = (EditText)findViewById(R.id.editText_Id);
        editTextUnits = (EditText)findViewById(R.id.editText_units);

        Button buttonSave = (Button)findViewById(R.id.button_save);

        Intent incomingIntent = getIntent();

        sensorName = incomingIntent.getStringExtra("SensorName");
        sensorId = incomingIntent.getStringExtra("SensorId");
        sensorUnits = incomingIntent.getStringExtra("SensorUnits");

        editTextName.setText(sensorName);
        editTextId.setText(sensorId);
        editTextUnits.setText(sensorUnits);
    }

    public void ButtonSaveClick(View v)
    {
        Intent resultIntent = new Intent();

        if(!sensorName.equals(editTextName.getText().toString()))
        {
            resultIntent.putExtra("NewSensorName", editTextName.getText().toString());
            sensorParameterChanged = true;
        }else
        {
            resultIntent.putExtra("NewSensorName", sensorName);
        }

        if(!sensorId.equals(editTextId.getText().toString()))
        {
            resultIntent.putExtra("NewSensorId", editTextId.getText().toString());
            sensorParameterChanged = true;
        }else
        {
            resultIntent.putExtra("NewSensorId", sensorId);
        }

        if(!sensorUnits.equals(editTextUnits.getText().toString()))
        {
            resultIntent.putExtra("NewSensorUnits", editTextUnits.getText().toString());
            sensorParameterChanged = true;
        }else
        {
            resultIntent.putExtra("NewSensorUnits", sensorUnits);
        }

        if(sensorParameterChanged)
        {
            setResult(Activity.RESULT_OK, resultIntent);
        }else
        {
            setResult(Activity.RESULT_CANCELED);
        }

        this.finish();
    }


}

package com.example.ah.push;

public class Sensor {

    String id;
    String name;
    String units;

    public Sensor()
    {
        id = null;
        name = null;
        units = null;
    }

    public Sensor(String id, String name, String units)
    {
        this.id = id;
        this.name = name;
        this.units = units;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }
}

package com.example.ah.push;

import com.google.gson.Gson;
import com.microsoft.azure.sdk.iot.device.DeviceClient;
import com.microsoft.azure.sdk.iot.device.IotHubEventCallback;
import com.microsoft.azure.sdk.iot.device.IotHubStatusCode;
import com.microsoft.azure.sdk.iot.device.Message;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class PhoneAzureDevice {

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss,SSS");

    private static class Board{
        public String BoardSerial;
        public Sensor sensors[];
    }

    private static class SystemProp{
        public String contentType;
        public String contentEncoding;
        public String Time;

        public SystemProp(String time){
            this.Time = time;
            this.contentType = "application/json";
            this.contentEncoding = "utf-8";
        }
    }

    private  static class Mes{
        public SystemProp systemProperties;
        public Board body;
    }

    private static class Sensor{
        public String SensorId;
        public String Value;
    }
    // Specify the telemetry to send to your IoT hub.
    private static class TelemetryDataPoint {
        public Mes message;

        // Serialize object to JSON format.
        public String serialize() {
            Gson gson = new Gson();
            return gson.toJson(this);
        }
    }
    // Serialize object to JSON format.
    public static String serialize(Board b) {
        Gson gson = new Gson();
        return gson.toJson(b);
    }



    // Print the acknowledgement received from IoT Hub for the telemetry message sent.
    private static class EventCallback implements IotHubEventCallback {
        public void execute(IotHubStatusCode status, Object context) {
            System.out.println("IoT Hub responded to message with status: " + status.name());

            if (context != null) {
                synchronized (context) {
                    context.notify();
                }
            }
        }
    }

    public static class MessageSender implements Runnable {
        String BoardSerial;
        String sensorId;
        float [] value;
        DeviceClient client;

        public MessageSender(DeviceClient client, String boardId, String sensorId){
            this.client = client;
            this.BoardSerial = boardId;
            this.sensorId = sensorId;
        }

        public void setValue(float [] values) {
            this.value = values;
        }

        public float [] getValue() {
            return value;
        }

        public String getBoardId() {
            return BoardSerial;
        }

        public String getSensorId() {
            return sensorId;
        }

        public void run() {
            try {
                    // Simulate telemetry.

                String time = dateFormat.format(Calendar.getInstance().getTime());

                Board board = new Board();
                board.BoardSerial = BoardSerial;
                board.sensors = new Sensor[value.length];

                for(int i = 0; i<value.length; i++){
                    board.sensors[i] = new Sensor();
                    board.sensors[i].SensorId = sensorId+" "+i;
                    board.sensors[i].Value = String.valueOf(value[i]);
                }

                Mes mes = new Mes();
                mes.systemProperties = new SystemProp(time);

                TelemetryDataPoint telemetryDataPoint = new TelemetryDataPoint();
                telemetryDataPoint.message = mes;
                telemetryDataPoint.message.body = board;

                String msgStr = telemetryDataPoint.serialize();

                byte messageBytes[] = msgStr.getBytes(Charset.forName("UTF-8"));
                Message msg = new Message(messageBytes);

                Object lockobj = new Object();

                EventCallback callback = new EventCallback();
                client.sendEventAsync(msg, callback, lockobj);

                 synchronized (lockobj) {
                     lockobj.wait();
                 }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws IOException, URISyntaxException {



        // Create new thread and start sending messages

    }
}

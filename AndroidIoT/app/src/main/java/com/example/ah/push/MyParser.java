package com.example.ah.push;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ah on 25/07/2018.
 */

public class MyParser {

    public static Map parseQr(String connstr) {
        Map<String, String> deviceInfo = new HashMap<String, String>();
        String[] parts = connstr.split("%");
        deviceInfo.put("NotificationHubName", parts[0]);
        deviceInfo.put("SenderId", parts[1]);
        deviceInfo.put("NotHubConnectionString", parts[2]);
        deviceInfo.put("TableName", parts[3]);
        deviceInfo.put("StorageConnectionString", parts[4]);

        return deviceInfo;
    }

    public static Map parseQrWithIotHub(String connstr) {
        //String withoutQuotes = connstr.substring(1, connstr.length()-1);
        Map<String, String> deviceInfo = new HashMap<String, String>();
        String[] parts = connstr.split("%");
        deviceInfo.put("NotificationHubName", parts[0]);
        deviceInfo.put("SenderId", parts[1]);
        deviceInfo.put("NotHubConnectionString", parts[2]);
        deviceInfo.put("TableName", parts[3]);
        deviceInfo.put("StorageConnectionString", parts[4]);
        deviceInfo.put("IotHubConnectionString", parts[5]);
        deviceInfo.put("DeviceName", parts[6]);

        return deviceInfo;
    }

    public static Map parseReducedQrWithIotHub(String connstr) {
        Map<String, String> deviceInfo = new HashMap<String, String>();
        String[] parts = connstr.split("%");
        deviceInfo.put("NotificationHubName", parts[0]);
        deviceInfo.put("SenderId", parts[1]);
        deviceInfo.put("NotHubConnectionString", "Endpoint=sb://"+parts[2]+".servicebus.windows.net/;SharedAccessKeyName="+parts[3]+";SharedAccessKey="+parts[4]);
        deviceInfo.put("TableName", parts[5]);
        deviceInfo.put("StorageConnectionString", "DefaultEndpointsProtocol=http;AccountName="+parts[6]+";AccountKey="+parts[7]);
        deviceInfo.put("IotHubConnectionString", "HostName="+parts[8]+".azure-devices.net;SharedAccessKeyName="+parts[9]+";SharedAccessKey="+parts[10]);
        deviceInfo.put("DeviceName", parts[11]);

        return deviceInfo;
    }

    public static FirebaseDevice parseQrTodevice(String qrString) {
        FirebaseDevice device = new FirebaseDevice();
        String[] parts = qrString.split("%");
        device.serial = parts[0];
        device.storageName = parts[1];
        device.storageKey = parts[2];

        return device;
    }
}


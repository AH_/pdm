package com.example.ah.push;

import android.app.TimePickerDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.gson.reflect.TypeToken;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.PointsGraphSeries;

import android.content.Intent;
import android.text.format.DateUtils;
import android.util.Pair;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.app.DatePickerDialog;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.google.gson.*;
import com.microsoft.azure.sdk.iot.device.DeviceClient;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.EntityProperty;
import com.microsoft.azure.storage.table.EntityResolver;
import com.microsoft.azure.storage.table.TableOperation;
import com.microsoft.azure.storage.table.TableQuery;

import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;


public class DisplayPhoneActivity extends AppCompatActivity {

    public static DisplayPhoneActivity displayPhoneActivity;
    public static Boolean isVisible = false;
    private static final String TAG = "DisplayPhoneActivity";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    ArrayList<DataModelSens> checkListItems;
    ListView listView;
    private static CustomAdapterSens adapterSens;
    FirebaseDevice pickedFirebaseDevice;
    int position;

    Calendar dateAndTime = Calendar.getInstance();
    TextView fromDate;
    TextView toDate;

    ProgressBar progressBar;

    Button deleteButton;
    Button buttonApply;

    Intent intent;
    String storageConnString;
    String tableName = "values";

    GraphView graph;
    Double yMin = 0.0;
    Double yMax = 0.0;
    Double xMin;
    Double xMax;

    Boolean chklistTest;

    ArrayList<Sensor> deviceSensors;
    HashMap<String, ArrayList<Pair<String, String>>> fetchedData;
    HashMap<String, LineGraphSeries<DataPoint>> lineSeries = new HashMap<>();
    HashMap<String, PointsGraphSeries<DataPoint>> pointSeries = new HashMap<>();
    DataPoint[][] series;

    MyParser parser = new MyParser();

    SimpleDateFormat updateTimeFormatter = new SimpleDateFormat("HH:mm:ss");

    public DataPoint[] mapToDp(ArrayList<Pair<String, String>> map) {
        DataPoint[] result = new DataPoint[map.size()];
        int i = 0;
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss,SSS");
        //format.setTimeZone(TimeZone.getTimeZone("GMT+02:00"));
        for (Pair<String, String> el : map) {
            DataPoint dp = null;
            try {
                Date date = format.parse((String) el.first);
                Double value = Double.parseDouble((String) el.second);
                dp = new DataPoint(date, value);
            } catch (Exception e) {
                e.printStackTrace();
            }
            result[i] = dp;
            i++;
        }
        for (int k = result.length - 1; i > 0; i--) {
            for (int j = 0; j < k; j++) {
                if (result[j].getX() > result[j + 1].getX()) {
                    DataPoint tmp = result[j];
                    result[j] = result[j + 1];
                    result[j + 1] = tmp;
                }
            }
        }
        return result;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
/*
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported by Google Play Services.");
                ToastNotify("This device is not supported by Google Play Services.");
                finish();
            }
            return false;
        }
        return true;
    }

    public void registerWithNotificationHubs()
    {
        if (checkPlayServices()) {
            // Start IntentService to register this application with FCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }
*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_phone);

        //registerWithNotificationHubs();

        progressBar = (ProgressBar) findViewById(R.id.progressBarShow);

        chklistTest = false;

        intent = getIntent();
        pickedFirebaseDevice = intent.getParcelableExtra("Device");
        position = intent.getIntExtra("index", -1);

        storageConnString = "DefaultEndpointsProtocol=https;AccountName="+ pickedFirebaseDevice.storageName+";AccountKey="+ pickedFirebaseDevice.storageKey+";EndpointSuffix=core.windows.net";
        tableName = "values";

        Globals.deviceName = pickedFirebaseDevice.serial;
        Globals.connString = "HostName="+pickedFirebaseDevice.iotHubName+".azure-devices.net;DeviceId="+Globals.deviceName+";" +
                "SharedAccessKey="+pickedFirebaseDevice.devicePrimaryKey+"";

        try{
            Globals.client = new DeviceClient(Globals.connString, Globals.protocol);
        }catch (URISyntaxException e){
            e.printStackTrace();
        }

        getSupportActionBar().setTitle(pickedFirebaseDevice.serial);

        fromDate = (TextView)findViewById(R.id.fromDate);
        toDate = (TextView)findViewById(R.id.toDate);

        deleteButton = (Button)findViewById(R.id.measureButton);
        buttonApply = (Button)findViewById(R.id.buttonApply);

        listView=(ListView)findViewById(R.id.list);

        setInitialFromDateTime();
        setInitialToDateTime();

        graph = (GraphView) findViewById(R.id.graph);

        setListViewHeightBasedOnChildren(listView);

        displayPhoneActivity = this;

        GetSpecificatonTask getSpecificatonTask = new GetSpecificatonTask();
        try {
            getSpecificatonTask.execute();
            HashMap<String, String> spec = getSpecificatonTask.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.display_phone_activity_menu, menu);
        return  true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == R.id.disassociate)
        {
            SharedPreferences sharedPrefs = getApplicationContext().getSharedPreferences("associated_device", 0);
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.putString("device_serial", "");
            editor.putString("device_storage_key", "");
            editor.putString("device_storage_name", "");
            editor.commit();

            Intent reopenAsDisplayActivityIntent = new Intent(this, DisplayActivity.class);
            reopenAsDisplayActivityIntent.putExtra("Device", pickedFirebaseDevice);
            reopenAsDisplayActivityIntent.putExtra("index", position);
            startActivity(reopenAsDisplayActivityIntent);
            this.finish();

        }
        return true;
    }

    private class GetSpecificatonTask extends AsyncTask<Void, Void, HashMap<String, String>>
    {

        @Override
        protected HashMap<String, String> doInBackground(Void... voids) {
            try
            {
                // Retrieve storage account from connection-string.
                CloudStorageAccount storageAccount =
                        CloudStorageAccount.parse(storageConnString);

                // Create the table client.
                CloudTableClient tableClient = storageAccount.createCloudTableClient();

                // Create a cloud table object for the table.
                CloudTable cloudTable = tableClient.getTableReference("specifications");

                // Retrieve the entity with partition key of "Smith" and row key of "Jeff"
                TableOperation retrieveSerial =
                        TableOperation.retrieve("ah", pickedFirebaseDevice.serial, Specification.class);

                // Submit the operation to the table service and get the specific entity.
                Specification specificEntity =
                        cloudTable.execute(retrieveSerial).getResultAsType();

                HashMap<String, String> result = new HashMap<>();
                // Output the entity.
                if (specificEntity != null)
                {
                    result.put("Model", specificEntity.Model);
                    result.put("Version", specificEntity.Version);
                }

                return  result;
            }
            catch (Exception e) {
                // Output the stack trace.
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(HashMap<String, String> stringStringHashMap) {
            super.onPostExecute(stringStringHashMap);
            getSupportActionBar().setTitle(pickedFirebaseDevice.serial);

            DisplayPhoneActivity.GetConfigurationTask getConfigurationTask = new DisplayPhoneActivity.GetConfigurationTask();
            try {
                getConfigurationTask.execute(stringStringHashMap);
                deviceSensors = getConfigurationTask.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

    private class GetConfigurationTask extends AsyncTask<HashMap<String, String>, Void, ArrayList<Sensor>>
    {

        @Override
        protected ArrayList<Sensor> doInBackground(HashMap<String, String>... hashMaps) {
            String model = hashMaps[0].get("Model");
            String version = hashMaps[0].get("Version");

            try
            {
                // Retrieve storage account from connection-string.
                CloudStorageAccount storageAccount =
                        CloudStorageAccount.parse(storageConnString);

                // Create the table client.
                CloudTableClient tableClient = storageAccount.createCloudTableClient();

                // Create a cloud table object for the table.
                CloudTable cloudTable = tableClient.getTableReference("configurations");

                // Retrieve the entity with partition key of "Smith" and row key of "Jeff"
                TableOperation retrieveSerial =
                        TableOperation.retrieve(model, version, Configuration.class);

                // Submit the operation to the table service and get the specific entity.
                Configuration specificEntity =
                        cloudTable.execute(retrieveSerial).getResultAsType();

                ArrayList<Sensor> result = new ArrayList<>();
                // Output the entity.
                if (specificEntity != null)
                {
                    String sensorsJson = specificEntity.sensors.substring(40);
                    result = new Gson().fromJson(sensorsJson, new TypeToken<ArrayList<Sensor>>(){}.getType());
                }

                return  result;
            }
            catch (Exception e) {
                // Output the stack trace.
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(ArrayList<Sensor> sensorArrayList) {
            super.onPostExecute(sensorArrayList);

        }


    }

    public void onButtonShowClick(View v) {
        fetchedData = new HashMap<String, ArrayList<Pair<String, String>>>();
        graph.removeAllSeries();
        checkListItems = null;

        String fromString = fromDate.getText().toString();
        String toString = toDate.getText().toString();

        Date dateFrom = null;
        Date dateTo = null;

        SimpleDateFormat formatter = new SimpleDateFormat();

        try {
            formatter.applyPattern("dd MMMM yyyy");
            dateFrom = formatter.parse(fromString);
            dateTo = formatter.parse(toString);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            formatter.applyPattern("MMMM dd, yyyy");
            dateFrom = formatter.parse(fromString);
            dateTo = formatter.parse(toString);
        } catch (Exception e) {
            e.printStackTrace();
        }

        formatter.applyPattern("dd-MM-yyyy HH:mm:ss,SSS");
        fromString = formatter.format(dateFrom);
        toString = formatter.format(dateTo);
        toString = toString.substring(0, 11) + "23:59:59,999";

        FetchTableTask fetchTableTask = new FetchTableTask();
        fetchTableTask.execute(storageConnString, tableName, fromString, toString, pickedFirebaseDevice.serial);
    }

    public void onButtonMeasureClick(View v) {
        Intent intent = new Intent(DisplayPhoneActivity.this, SensorsActivity.class);
        startActivity(intent);
    }

    public void setFromDate(View v){
        new DatePickerDialog(DisplayPhoneActivity.this, fd, dateAndTime.get(Calendar.YEAR),
                dateAndTime.get(Calendar.MONTH), dateAndTime.get(Calendar.DAY_OF_MONTH)).show();
    }

    public void setToDate(View v){
        new DatePickerDialog(DisplayPhoneActivity.this, td, dateAndTime.get(Calendar.YEAR),
                dateAndTime.get(Calendar.MONTH), dateAndTime.get(Calendar.DAY_OF_MONTH)).show();
    }

    public void setTime(View v) {
        new TimePickerDialog(DisplayPhoneActivity.this, t, dateAndTime.get(Calendar.HOUR_OF_DAY),
                dateAndTime.get(Calendar.MINUTE), true).show();
    }

    private void setInitialFromDateTime(){
        fromDate.setText(DateUtils.formatDateTime(this, dateAndTime.getTimeInMillis(),
                DateUtils.FORMAT_SHOW_DATE| DateUtils.FORMAT_SHOW_YEAR));
    }

    private void setInitialToDateTime(){

        toDate.setText(DateUtils.formatDateTime(this, dateAndTime.getTimeInMillis(),
                DateUtils.FORMAT_SHOW_DATE| DateUtils.FORMAT_SHOW_YEAR));
    }

    DatePickerDialog.OnDateSetListener fd = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            dateAndTime.set(Calendar.YEAR, year);
            dateAndTime.set(Calendar.MONTH, monthOfYear);
            dateAndTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setInitialFromDateTime();
        }
    };

    DatePickerDialog.OnDateSetListener td = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            dateAndTime.set(Calendar.YEAR, year);
            dateAndTime.set(Calendar.MONTH, monthOfYear);
            dateAndTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setInitialToDateTime();
        }
    };

    TimePickerDialog.OnTimeSetListener t = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            dateAndTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
            dateAndTime.set(Calendar.MINUTE, minute);
            //setInitialFromDateTime();
            //setInitialToDateTime();
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        isVisible = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isVisible = false;
        chklistTest = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isVisible = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        isVisible = false;
        chklistTest = false;
    }

    public void ToastNotify(final String notificationMessage) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(DisplayPhoneActivity.this, notificationMessage, Toast.LENGTH_LONG).show();
                //TextView helloText = (TextView) findViewById(R.id.text_hello);
                //helloText.setText(notificationMessage);
            }
        });
    }

    public void checkList(HashMap<String, ArrayList<Pair<String, String>>> sensors) {

        chklistTest = true;

        try {
            checkListItems = new ArrayList<>();
            Random rand = new Random();

            if (sensors.size() == 0) {
                adapterSens.clear();
            } else {
                for (Map.Entry<String, ArrayList<Pair<String, String>>> entry : sensors.entrySet()) {
                    if (!checkListItems.contains(entry.getKey())) {
                        int col = Color.rgb(rand.nextInt(100) + 120, rand.nextInt(100) + 120, rand.nextInt(100) + 120);

                        DataModelSens sensorToAdd = new DataModelSens();
                        sensorToAdd.sensorId = entry.getKey();
                        sensorToAdd.color = col;
                        try {
                            for (Sensor s : deviceSensors) {
                                if (s.id.equals(sensorToAdd.sensorId)) {
                                    sensorToAdd.name = s.name;
                                    sensorToAdd.units = s.units;
                                }
                            }
                        }catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                        checkListItems.add(sensorToAdd);
                    }
                }
            }

            adapterSens = new CustomAdapterSens(checkListItems, this);
            listView.setAdapter(adapterSens);

        } catch (Exception e) {
            e.printStackTrace();
        }

        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                SparseBooleanArray selectedPositions = listView.getCheckedItemPositions();

                if (selectedPositions.get(position)) {

                    //Color
                    view.setBackgroundColor(checkListItems.get(position).color);
                    int color = checkListItems.get(position).getColor();

                    //Datapoints
                    DataPoint[] datapoints = mapToDp(fetchedData.get(checkListItems.get(position).sensorId));

                    if(xMin==null || xMax==null)
                    {
                        xMin = datapoints[0].getX();
                        xMax = datapoints[0].getX();
                    }

                    //Min - Max
                    for (DataPoint dp : datapoints) {
                        if (dp.getY() > yMax) {
                            yMax = dp.getY();
                        }
                        if (dp.getY() < yMin) {
                            yMin = dp.getY();
                        }

                        if (dp.getX() > xMax) {
                            xMax = dp.getX();
                        }
                        if (dp.getX() < xMin) {
                            xMin = dp.getX();
                        }
                    }
                    graph.getViewport().setYAxisBoundsManual(true);
                    graph.getViewport().setScalable(true);
                    graph.getViewport().setMaxY(yMax);
                    graph.getViewport().setMinY(yMin);
                    graph.getViewport().setMaxX(xMax);
                    graph.getViewport().setMinX(xMin);

                    //Spline
                    LineGraphSeries<DataPoint> spline = new LineGraphSeries<>(datapoints);
                    spline.setColor(color);

                    //Dots
                    PointsGraphSeries<DataPoint> dots = new PointsGraphSeries<>(datapoints);
                    dots.setColor(color);
                    dots.setSize(8f);

                    graph.addSeries(spline);
                    graph.addSeries(dots);

                    lineSeries.put(checkListItems.get(position).sensorId, spline);
                    pointSeries.put(checkListItems.get(position).sensorId, dots);

                    graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(DisplayPhoneActivity.this));
                    graph.getGridLabelRenderer().setNumHorizontalLabels(2);

                    /*
                    if (datapoints.length > 1) {
                        graph.getViewport().setMaxX(datapoints[datapoints.length - 1].getX());
                        if (datapoints.length > 2) {
                            graph.getViewport().setMinX(datapoints[(datapoints.length - 1) - datapoints.length / 3].getX());
                        }
                        if (datapoints.length == 2) {
                            graph.getViewport().setMinX(datapoints[0].getX());
                        }
                        graph.getViewport().setScalable(true);
                    } else {
                        graph.getViewport().setXAxisBoundsManual(false);
                    }
                    */
                } else {
                    view.setBackgroundColor(0);
                    graph.removeSeries(lineSeries.get(checkListItems.get(position).sensorId));
                    graph.removeSeries(pointSeries.get(checkListItems.get(position).sensorId));
                }

                graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(DisplayPhoneActivity.this));
                graph.getGridLabelRenderer().setNumHorizontalLabels(2);
            }
        });
    }

    class FetchTableTask extends AsyncTask<String, Void, HashMap<String, ArrayList<Pair<String, String>>>> {

        HashMap<String, ArrayList<Pair<String, String>>> res = new HashMap<String, ArrayList<Pair<String, String>>>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            buttonApply.setText("Loading");
            buttonApply.setEnabled(false);
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected HashMap<String, ArrayList<Pair<String, String>>> doInBackground(String... params) {
            if (android.os.Debug.isDebuggerConnected())
            {
                android.os.Debug.waitForDebugger();
            }

            try {
                // Retrieve storage account from connection-string.
                CloudStorageAccount storageAccount = CloudStorageAccount.parse(storageConnString);



            } catch (Exception e) {
                // Output the stack trace.
                e.printStackTrace();
            }


            try {
                final String PARTITION_KEY = "PartitionKey";
                final String ROW_KEY = "RowKey";
                final String TIMESTAMP = "Timestamp";
                final String BOARDSERIAL = "boardserial";

                // Retrieve storage account from connection-string.
                CloudStorageAccount storageAccount =
                        CloudStorageAccount.parse(storageConnString);

                // Create the table client.
                CloudTableClient tableClient = storageAccount.createCloudTableClient();

                // Create a cloud table object for the table.
                CloudTable cloudTable = tableClient.getTableReference(tableName);

                //String macFilter = TableQuery.generateFilterCondition(MAC, TableQuery.QueryComparisons.EQUAL, params[4]);

                String boardFilter = TableQuery.generateFilterCondition(BOARDSERIAL, TableQuery.QueryComparisons.EQUAL, params[4]);

                String timeLower = TableQuery.generateFilterCondition(ROW_KEY, TableQuery.QueryComparisons.GREATER_THAN_OR_EQUAL, params[2]);

                String timeUpper = TableQuery.generateFilterCondition(ROW_KEY, TableQuery.QueryComparisons.LESS_THAN_OR_EQUAL, params[3]);

                // Combine the two conditions into a filter expression.
                String timeFilter = TableQuery.combineFilters(timeLower,
                        TableQuery.Operators.AND, timeUpper);

                String boardAndTimeFilter = TableQuery.combineFilters(timeFilter,
                        TableQuery.Operators.AND, boardFilter);


                // Define a projection query that retrieves only the Email property
                TableQuery<TableEntity> projectionQuery =
                        TableQuery.from(TableEntity.class)
                                .select(new String[]{"sensorid", "value", "time"}).where(boardAndTimeFilter);

                // Define a Entity resolver to project the entity to the Email value.
                EntityResolver<HashMap<String, ArrayList<Pair<String, String>>>> emailResolver = new EntityResolver<HashMap<String, ArrayList<Pair<String, String>>>>() {
                    @Override
                    public HashMap<String, ArrayList<Pair<String, String>>> resolve(String PartitionKey, String RowKey, Date timeStamp, HashMap<String, EntityProperty> properties, String etag) {

                        HashMap<String, ArrayList<Pair<String, String>>> resolvedEntity = new HashMap<String, ArrayList<Pair<String, String>>>();
                        ArrayList<Pair<String, String>> valAndTime = new ArrayList<Pair<String, String>>();
                        valAndTime.add(new Pair<>(properties.get("time").getValueAsString(), (properties.get("value").getValueAsString())));
                        resolvedEntity.put(properties.get("sensorid").getValueAsString(), valAndTime);
                        return resolvedEntity;
                    }
                };

                // Loop through the results, displaying the Email values.
                for (HashMap<String, ArrayList<Pair<String, String>>> el : cloudTable.execute(projectionQuery, emailResolver)) {
                    String[] keys = el.keySet().toArray(new String[el.size()]);
                    if (!res.containsKey(keys[0])) {
                        res.put(keys[0].toString(), new ArrayList<Pair<String, String>>());
                    }
                    res.get(keys[0]).addAll(el.get(keys[0]));
                }
            } catch (Exception e) {
                // Output the stack trace.
                e.printStackTrace();
            }
            return res;
        }

        @Override
        protected void onPostExecute(HashMap<String, ArrayList<Pair<String, String>>> result) {
            super.onPostExecute(result);
            fetchedData = result;
            series = new DataPoint[fetchedData.size()][];
            buttonApply.setText("Load");
            buttonApply.setEnabled(true);
            progressBar.setVisibility(View.INVISIBLE);
            checkList(fetchedData); //TODO checkList
            setListViewHeightBasedOnChildren(listView);
        }
    }
}

package com.example.ah.push;

public class Specification extends TableEntity{

    public Specification(String owner, String serial) {
        this.partitionKey = owner;
        this.rowKey = serial;
    }

    public Specification() {

    }

    String Model;
    String Version;

    @Override
    public String toString()
    {
        return Model;
    }

    public String getModel() {
        return this.Model;
    }

    public void setModel(String model) {
        this.Model = model;
    }

    public String getVersion() {
        return this.Version;
    }

    public void setVersion(String version) {
        this.Version = version;
    }
}

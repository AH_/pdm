package com.example.ah.push;

import android.os.Parcel;
import android.os.Parcelable;

public class FirebaseDevice implements Parcelable {

    String serial;
    String storageName;
    String storageKey;
    String iotHubName;
    String devicePrimaryKey;
    //Boolean isOnline = false;

    public FirebaseDevice() {
        this.serial = null;
        this.storageName = null;
        this.storageKey = null;
        this.iotHubName = null;
        this.devicePrimaryKey = null;
        //this.isOnline=false;
    }

    public FirebaseDevice(String deviceSerial, String storageName, String storageKey, String iotHubName, String iotHubKey) {
        this.serial = deviceSerial;
        this.storageName = storageName;
        this.storageKey = storageKey;
        this.iotHubName = iotHubName;
        this.devicePrimaryKey = iotHubKey;
        //this.isOnline=false;
    }

    public FirebaseDevice(Parcel in) {
        this.serial=in.readString();
        this.storageName=in.readString();
        this.storageKey=in.readString();
        this.iotHubName = in.readString();
        this.devicePrimaryKey = in.readString();
    }

    public String getSerial() {
        return serial;
    }
    public void setSerial(String deviceSerial) {
        this.serial=deviceSerial;
    }

    public String getStorageName() {
        return storageName;
    }
    public void setStorageName(String storageName) {
        this.storageName=storageName;
    }

    public String getStorageKey() {
        return storageKey;
    }
    public void setStorageKey(String storageKey) {
        this.storageKey=storageKey;
    }

    public String getIotHubName() {
        return iotHubName;
    }
    public void setIotHubName(String iotHubName) {
        this.iotHubName=iotHubName;
    }

    public String getIotHubKey() {
        return devicePrimaryKey;
    }
    public void setIotHubKey(String iotHubKey) {
        this.devicePrimaryKey=iotHubKey;
    }

    //public Boolean getIsOnline() {
    //    return isOnline;
    //}
    //public void setIsOnline(Boolean isOnline) {
    //    this.isOnline=isOnline;
    //}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(serial);
        dest.writeString(storageName);
        dest.writeString(storageKey);
        dest.writeString(iotHubName);
        dest.writeString(devicePrimaryKey);
    }

    public static  final Creator<FirebaseDevice> CREATOR = new Creator<FirebaseDevice>(){
        public FirebaseDevice createFromParcel(Parcel in){
            return new FirebaseDevice(in);
        }

        public FirebaseDevice[] newArray(int size){
            return new FirebaseDevice[size];
        }
    };
}

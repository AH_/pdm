package com.example.ah.push;


import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;



public class FirebaseOperations{

    private DatabaseReference mDatabase;
    boolean isInDbResult = false;
    int index = 0;

    class FirebaseDevice
    {
        public String connectionString;
        public String serial;
    }

    public boolean isUserInDb(String userId) throws InterruptedException{
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mRef = database.getReference().child("users");
        mRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                isInDbResult = false;
                for(DataSnapshot ds: dataSnapshot.getChildren()){
                    if(ds.getKey() == userId){
                        isInDbResult = true;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        return isInDbResult;
    }

    public void getDevices(DatabaseReference dbRef){

        ArrayList<String> my_list = new ArrayList<>();

        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.v("Async101", "Done loading bookmarks");
                for(DataSnapshot ds : dataSnapshot.getChildren()) {
                    String connStr = ds.getKey();
                    my_list.add(connStr);
                    Log.d("TAG", connStr);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
/*
    public void addDeviceToDb(FirebaseDatabase db, FirebaseUser user, String connstr){

        FirebaseDevice fbDevice = new FirebaseDevice();
        fbDevice.connectionString = connstr;
        fbDevice.serial = "testik";

        //TODO add device using new connectionString

        //String userId = user.getUid();
        String userId = user.getEmail().replace('.', '-');
        DatabaseReference dbRef = db.getReference().child("users").child(userId).child("devices");

        ArrayList<Integer> int_list = new ArrayList<>();



        dbRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int_list.clear();
                for(DataSnapshot ds : dataSnapshot.getChildren()) {
                    if(ds.getValue().equals(connstr)){
                        break;
                    }
                    String key = ds.getKey();
                    int_list.add(Integer.parseInt(key));

                }

                for(int i = 0; i<=int_list.size(); ++i){
                    try {
                        if(!int_list.contains(i)){
                            index = i;
                            //dbRef.child(Integer.toString(index)).setValue(connstr);
                            dbRef.child(Integer.toString(index)).setValue(fbDevice);
                            break;
                        }
                    }catch (IndexOutOfBoundsException e){
                        e.printStackTrace();
                        //dbRef.child(Integer.toString(index)).setValue(connstr);
                        dbRef.child(Integer.toString(index)).setValue(fbDevice);
                        break;
                    }
                }
                dbRef.removeEventListener(this);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
*/
    public static void addDeviceToDb(FirebaseDatabase db, FirebaseUser user, com.example.ah.push.FirebaseDevice device){

        //TODO add device using new connectionString

        //String userId = user.getUid();
        String userId = user.getEmail().replace('.', '-');
        DatabaseReference dbRef = db.getReference().child("users").child(userId).child("devices");


        dbRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                dbRef.child(device.serial).setValue(device);
                dbRef.removeEventListener(this);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });

    }

    public static void addUserToDb(FirebaseDatabase db, FirebaseUser user, String password){
        String userId = user.getUid();
        String userEmail = user.getEmail().replace('.', '-');
        DatabaseReference mRef = db.getReference().child("users");
        mRef.child(userEmail).child("email").setValue(user.getEmail());
        mRef.child(userEmail).child("password").setValue(password);
    }

    public static void removeDeviceFromDb(FirebaseDatabase db, FirebaseUser user, com.example.ah.push.FirebaseDevice deviceToRemove){

        String userId = user.getUid();
        DatabaseReference dbRef = db.getReference().child("users").child(userId).child("devices");

        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(android.os.Debug.isDebuggerConnected())
                    android.os.Debug.waitForDebugger();

                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    if(ds.getValue().toString().equals(deviceToRemove.serial)){
                        ds.getRef().removeValue();
                    }
                }
                dbRef.removeEventListener(this);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}

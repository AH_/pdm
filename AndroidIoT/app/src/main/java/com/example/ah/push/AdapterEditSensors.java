package com.example.ah.push;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by ah on 12/07/2018.
 */

public class AdapterEditSensors extends ArrayAdapter<Sensor> implements View.OnClickListener{

    private ArrayList<Sensor> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView txtId;
        TextView txtName;
        TextView txtUnits;
        TextView txtValue;
    }

    public AdapterEditSensors(ArrayList<Sensor> data, Context context) {
        super(context, R.layout.row_item, data);
        this.dataSet = data;
        this.mContext=context;

    }

    @Override
    public void onClick(View v) {

        int position=(Integer) v.getTag();
        Object object= getItem(position);
        DataModelSens dataModel=(DataModelSens) object;

    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Sensor dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_item, parent, false);
            viewHolder.txtId = (TextView) convertView.findViewById(R.id.id);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.name);
            viewHolder.txtUnits = (TextView) convertView.findViewById(R.id.units);
            //viewHolder.txtValue = (TextView) convertView.findViewById(R.id.vlaue);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        //Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        //result.startAnimation(animation);
        //lastPosition = position;

        viewHolder.txtId.setText(dataModel.id);
        if(dataModel.name != null)
        {
            viewHolder.txtName.setText(dataModel.name);
        }else
        {
            viewHolder.txtName.setText("not found");
        }

        if(dataModel.units != null)
        {
            viewHolder.txtUnits.setText(dataModel.units);
        }else
        {
            viewHolder.txtUnits.setText("not found");
        }

        return convertView;
    }
}
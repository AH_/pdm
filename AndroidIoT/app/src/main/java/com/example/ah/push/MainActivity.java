package com.example.ah.push;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.microsoft.azure.sdk.iot.device.DeviceClient;

import java.net.URISyntaxException;
import java.util.ArrayList;

/**
 * Created by AH on 4/25/2018.
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int STRING_CAPTURE = 12;
    //AuthenticationActivity authAct = new AuthenticationActivity();

    private FirebaseAuth mAuth;

    public static MainActivity mainActivity;
    public static Boolean isVisible = false;
    private static final String TAG = "MainActivity";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    WifiManager manager;
    WifiInfo info;

    ListView mainList;
    Button buttonAdd;
    TextView labelMainList;

    ArrayList<FirebaseDevice> devices = new ArrayList<>();

    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
    DatabaseReference mDatabaseRef = mDatabase.getReference().child("users").child(user.getEmail().replace('.', '-')).child("devices");

    String associatedDeviceSerial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainActivity = this;
        mAuth = FirebaseAuth.getInstance();
        registerWithNotificationHubs();
        MyFirebaseMessagingService.createChannelAndHandleNotifications(getApplicationContext());

        manager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        info = manager.getConnectionInfo();

        mainList = (ListView)findViewById(R.id.list_main);
        buttonAdd = (Button)findViewById(R.id.buttonAdd);
        labelMainList = (TextView)findViewById(R.id.textView_mainList);
        labelMainList.setText("Connecting...");


        SharedPreferences sharedPrefs = getApplicationContext().getSharedPreferences("associated_device", 0);
        associatedDeviceSerial = sharedPrefs.getString("device_serial", null);



        mainList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent intent = new Intent();
                if(devices.get(position).serial.equals(associatedDeviceSerial)){
                    intent = new Intent(mainActivity, DisplayPhoneActivity.class);
                    intent.putExtra("Device", devices.get(position));
                    intent.putExtra("index", position);
                }else{
                    intent = new Intent(mainActivity, DisplayActivity.class);
                    intent.putExtra("Device", devices.get(position));
                    intent.putExtra("index", position);
                }
                startActivityForResult(intent,0);
            }
        });

        mainList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent intent = new Intent();
                intent = new Intent(mainActivity, DeviceProperties.class);
                intent.putExtra("Device", devices.get(position));
                intent.putExtra("index", position);

                startActivityForResult(intent,0);

                return true;
            }
        });

        updateListFromFirebase(mDatabaseRef);
    }

    public void onButtonAddClick(View v)
    {
        Intent intent = new Intent(mainActivity, AddActivity.class);
        startActivityForResult(intent, STRING_CAPTURE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        try {
            if (requestCode == STRING_CAPTURE) {
                if (resultCode == CommonStatusCodes.SUCCESS) {
                    String stringFromQr = data.getStringExtra("ConnectionString");

                    FirebaseDevice device = MyParser.parseQrTodevice(stringFromQr);
                    FirebaseOperations.addDeviceToDb(mDatabase, user, device);
                }
            }

            if (requestCode == 0) {
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    int indexToDelete;
                    try{
                        indexToDelete = bundle.getInt("indexToDelete", -1);
                    }catch (Exception e){
                        e.printStackTrace();
                        indexToDelete = -1;
                    }
                    //TODO delete device from FB
                    FirebaseOperations.removeDeviceFromDb(mDatabase, user, devices.get(indexToDelete));
                }
            }
        }catch (Exception e){
            System.out.println("No incoming connection data.");
        }
        updateListFromFirebase(mDatabaseRef);
        //checkDevicesConnection(devices);
    }

    public void updateListFromFirebase(DatabaseReference dbRef){

        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                devices.clear();
                for(DataSnapshot ds : dataSnapshot.getChildren()) {
                    FirebaseDevice device = new FirebaseDevice();
                    device.serial = ds.child("serial").getValue().toString();

                    try{
                        device.storageName = ds.child("storageName").getValue().toString();
                        device.storageKey = ds.child("storageKey").getValue().toString();
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                    try{
                        device.iotHubName = ds.child("iotHubName").getValue().toString();
                        device.devicePrimaryKey = ds.child("devicePrimaryKey").getValue().toString();
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                    devices.add(device);
                    //Log.d("TAG", connStr);
                }

                Log.d("AH-TAG", "WOOHOO");
                showList(devices);
                if(devices.size()>0)
                {
                    labelMainList.setText("");
                }else
                {
                    labelMainList.setText("No devices found for current user.");
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void showList(ArrayList<FirebaseDevice> deviceList){

        ArrayList<MyMainListItem> arrayToShow = new ArrayList<>();

        for(FirebaseDevice device: deviceList) {
            if(device.serial.equals(Build.MODEL))
            {
                arrayToShow.add(new MyMainListItem(R.drawable.green, "THIS DEVICE"));
            }else
            {
                arrayToShow.add(new MyMainListItem(R.drawable.green, device.serial));
            }
        }

        Context c = mainActivity;
        MyIndicatorAdapter adapter = new MyIndicatorAdapter(c, arrayToShow);
        mainList.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuPurchasesListNewRecord:
                MainActivity.this.finish();
                mAuth.signOut();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        isVisible = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isVisible = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isVisible = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        isVisible = false;
    }

    @Override
    public void onBackPressed(){
        moveTaskToBack(true);
    }

    public void ToastNotify(final String notificationMessage) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, notificationMessage, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
         if (i == R.id.sign_out_button)
         {
             MainActivity.this.finish();
             mAuth.signOut();
         }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported by Google Play Services.");
                ToastNotify("This device is not supported by Google Play Services.");
                finish();
            }
            return false;
        }
        return true;
    }

    public void registerWithNotificationHubs()
    {
        if (checkPlayServices()) {
            // Start IntentService to register this application with FCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }
}
